-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2017 at 05:47 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjusters`
--

DROP TABLE IF EXISTS `adjusters`;
CREATE TABLE `adjusters` (
  `adjusterId` int(11) NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adjusters`
--

INSERT INTO `adjusters` (`adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Graham', 'Whitelaw', 'graham', 'graham@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(2, 'Mark', 'Smith', 'smith', 'mark@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 15:38:03'),
(3, 'Chris', 'Wylie', 'chris', 'chris@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 15:37:58'),
(4, 'Jerin', 'Koshy', 'koshy', 'jerin@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(5, 'Moein', 'Hani', 'hani', 'moein@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(6, 'Sham', 'Jacob', 'sham', 'sham@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(7, 'Craig', 'Jones', 'jones', 'craig@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(8, 'Steven', 'Smith', 'steven', 'steven@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19'),
(9, 'Shadi', 'Abu Razouq', 'shadi', 'shadi@whitelaw.ae', NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, '2017-03-25 05:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `brokers`
--

DROP TABLE IF EXISTS `brokers`;
CREATE TABLE `brokers` (
  `brokerId` int(11) NOT NULL,
  `employeeId` varchar(50) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `poBox` varchar(50) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brokers`
--

INSERT INTO `brokers` (`brokerId`, `employeeId`, `firstName`, `lastName`, `phoneNumber`, `emailId`, `poBox`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, '1', 'ACE Commercial Enterprises Company', NULL, NULL, NULL, '1100', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(2, '2', 'ACE Commercial Enterprises Company ', NULL, NULL, NULL, '38417', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(3, '3', 'Addison Bradley Arabia Holding', NULL, NULL, NULL, '113684', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(4, '4', 'AES Middel East Insurance Brokers', NULL, NULL, NULL, '191905', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(5, '5', 'AFIA Insurance Brokerage Services', NULL, NULL, NULL, '26423', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(6, '6', 'Al Ahlia General Insurance Brokers', NULL, NULL, NULL, '33355', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(7, '7', 'Al Berwaz Insurance Brokers', NULL, NULL, NULL, '31116', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(8, '8', 'Al Bustan Insurance Brokers', NULL, NULL, NULL, '48684', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(9, '9', 'Al Dawliyah Insurance Services ', NULL, NULL, NULL, '25962', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(10, '10', 'Al Dawliyah Insurance Services ', NULL, NULL, NULL, '4289', NULL, 'Fujiarah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(11, '11', 'Al Dawliyah Insurance Services ', NULL, NULL, NULL, '65573', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(12, '12', 'Al Diplomacy Insurance Services', NULL, NULL, NULL, '65815', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(13, '13', 'Al Faris Insurance Services', NULL, NULL, NULL, '14142', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(14, '14', 'Al Futtaim Willis', NULL, NULL, NULL, '152', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(15, '15', 'Al Futtaim Willis', NULL, NULL, NULL, '45210', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(16, '16', 'Al Himayah Insurance Brokerage', NULL, NULL, NULL, '185258', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(17, '17', 'Al Manara Insurance Services', NULL, NULL, NULL, '19644', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(18, '18', 'Al Nabooda Insurance Brokers', NULL, NULL, NULL, '50523', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(19, '19', 'Al Nahda National Insurance Broker (L L C)', NULL, NULL, NULL, '45430', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(20, '20', 'Al Quds Insurance Brokers', NULL, NULL, NULL, '93532', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(21, '21', 'Al Rami Commercial Brokerage (LLC)', NULL, NULL, NULL, '22482', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(22, '22', 'Al Rehda Insurance Brokers LLC ', NULL, NULL, NULL, '112152', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(23, '23', 'Al Sahara Insurance Brokers', NULL, NULL, NULL, '2307', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(24, '24', 'Al Salam Insurance Services Co. LLC', NULL, NULL, NULL, '22251', NULL, 'Sharjah ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(25, '25', 'Al Salam Insurance Services Co. LLC', NULL, NULL, NULL, '26549', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(26, '26', 'Al Sayegh Insurance Brokers', NULL, NULL, NULL, '87542', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(27, '27', 'Al Shorafa Insurance Services', NULL, NULL, NULL, '50761', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(28, '28', 'Alpha Lloyds Insurance and Reinsurance Brokers', NULL, NULL, NULL, '215461', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(29, '29', 'Alpha Lloyds Insurance and Reinsurance Brokers', NULL, NULL, NULL, '42491', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(30, '30', 'Aon (DIFC) Gulf', NULL, NULL, NULL, '506746', NULL, 'DIFC', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(31, '31', 'Aon Middle East', NULL, NULL, NULL, '10764', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(32, '32', 'Aon Middle East', NULL, NULL, NULL, '94429', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(33, '33', 'Aon Middle East', NULL, NULL, NULL, '3023', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(34, '34', 'Apex International Insurance Mediations', NULL, NULL, NULL, '35764', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(35, '35', 'Arab European Reinsurance Brokers Ltd', NULL, NULL, NULL, '50251', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(36, '36', 'Arab Insurance Brokerage ', NULL, NULL, NULL, '21375', NULL, 'Safat ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(37, '37', 'ARB Insurance Brokers', NULL, NULL, NULL, '110021', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(38, '38', 'Arlo Insurnace Brokers', NULL, NULL, NULL, '214909', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(39, '39', 'Arya Insurance Brokerage', NULL, NULL, NULL, '8811', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(40, '40', 'Associated Insurance Consultants', NULL, NULL, NULL, '35049', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(41, '41', 'Avon Insurance Brokers ', NULL, NULL, NULL, '29069', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(42, '42', 'Axis Insurance Brokers', NULL, NULL, NULL, '45032', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(43, '43', 'Berns Brett Masaood Insurance', NULL, NULL, NULL, '11566', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(44, '44', 'Capital Shield Insurance Brokers', NULL, NULL, NULL, '130085', NULL, 'Abu Dhabi ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(45, '45', 'Capital Shield Insurance Brokers', NULL, NULL, NULL, '23864', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(46, '46', 'Care Insurance Brokers', NULL, NULL, NULL, '3227', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(47, '47', 'City Marine Insurance Brokers', NULL, NULL, NULL, '26629', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(48, '48', 'Clover Brokers Insurance Management & Conslutants ', NULL, NULL, NULL, '-', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(49, '49', 'Cogent Insurance Brokers', NULL, NULL, NULL, '37131', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(50, '50', 'Colemont Insurance Brokers LLC ', NULL, NULL, NULL, '507', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(51, '51', 'Commercial Insuarnce Brokerage Services', NULL, NULL, NULL, '124277', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(52, '52', 'Compass Financial Solutions ', NULL, NULL, NULL, '241075', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(53, '53', 'Continental Insurance Brokers', NULL, NULL, NULL, '505', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(54, '54', 'Continental Insurance Brokers', NULL, NULL, NULL, '37589', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(55, '55', 'Continental Insurance Brokers', NULL, NULL, NULL, '26588', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(56, '56', 'Cosmos Insurance Brokers', NULL, NULL, NULL, '33913', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(57, '57', 'Cosmos Insurance Brokers', NULL, NULL, NULL, '21455', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(58, '58', 'Crescent Emirates Insurance Brokers ', NULL, NULL, NULL, '110021', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(59, '59', 'Dana Insurance Brokers', NULL, NULL, NULL, '215676', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(60, '60', 'Delta Insurance Services ', NULL, NULL, NULL, '805', NULL, 'Sharjah ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(61, '61', 'Devere Acuma Insurance Brokers', NULL, NULL, NULL, '23940', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(62, '62', 'Driassure Middle East ', NULL, NULL, NULL, '118354', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(63, '63', 'Dynamics Insurance Brokers', NULL, NULL, NULL, '14855', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(64, '64', 'Earnest Insurance Brokers', NULL, NULL, NULL, '114088', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(65, '65', 'Edge Insurance Brokers', NULL, NULL, NULL, '', NULL, 'Ras Al Khaimah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(66, '66', 'Egyptian Insurance Brokers', NULL, NULL, NULL, '53664', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(67, '67', 'Elite Insurance Brokers', NULL, NULL, NULL, '36118', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(68, '68', 'Emirates International Insurance Brokers', NULL, NULL, NULL, '88888', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(69, '69', 'Expeditors Insurance Brokers', NULL, NULL, NULL, '112447', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(70, '70', 'Federal Insurance Services and Consultancy', NULL, NULL, NULL, '46105', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(71, '71', 'Federal Insurance Services and Consultancy', NULL, NULL, NULL, '5406', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(72, '72', 'Fenchurch Faris', NULL, NULL, NULL, '45052', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(73, '73', 'Fenchurch Faris', NULL, NULL, NULL, '393166', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(74, '74', 'Fidlity Insurance Services ', NULL, NULL, NULL, '25221', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(75, '75', 'First Insurance Brokers Co. LLC ', NULL, NULL, NULL, '32700', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(76, '76', 'Future Insurnace Broker Services', NULL, NULL, NULL, '28365', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(77, '77', 'Future Insurnace Broker Services', NULL, NULL, NULL, '262830', NULL, 'Jebel Ali', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(78, '78', 'Galaxy Insurance Brokers', NULL, NULL, NULL, '25279', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(79, '79', 'Galaxy Insurance Brokers', NULL, NULL, NULL, '233275', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(80, '80', 'Gargash Insurance', NULL, NULL, NULL, '50251', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(81, '81', 'Gargash Insurance', NULL, NULL, NULL, '33140', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(82, '82', 'Gargash Insurance', NULL, NULL, NULL, '107001', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(83, '83', 'Gateway Insurance Broker', NULL, NULL, NULL, '92321', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(84, '84', 'Genavco Insurance & Reinsurance Brokers Col ', NULL, NULL, NULL, '-', NULL, 'London ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(85, '85', 'Global Risk Management ', NULL, NULL, NULL, '282197', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(86, '86', 'Goodwill Insurance Brokers', NULL, NULL, NULL, '2948', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(87, '87', 'Gras Savoye Gulf Insurance Brokers', NULL, NULL, NULL, '130667', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(88, '88', 'Greenshield Insurance Brokers', NULL, NULL, NULL, '43656', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(89, '89', 'Gulf Oasis Insurance Brokers', NULL, NULL, NULL, '45218', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(90, '90', 'Gulf Ocean International Insurance Brokers', NULL, NULL, NULL, '44161', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(91, '91', 'Gulf Resources Insurance Management Services', NULL, NULL, NULL, '14639', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(92, '92', 'Guradian Insurance Brokers', NULL, NULL, NULL, '51012', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(93, '93', 'Guradian Insurance Brokers', NULL, NULL, NULL, '124000', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(94, '94', 'GWM Middle East Brokerage', NULL, NULL, NULL, '146280', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(95, '95', 'Holborn Assets Insurance Brokers', NULL, NULL, NULL, '333851', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(96, '96', 'Howden Insurance Brokers', NULL, NULL, NULL, '49195', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(97, '97', 'Indo Arab- Insurance Brokers LLC ', NULL, NULL, NULL, '380612', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(98, '98', 'Insigthers Insurance Brokers Company ', NULL, NULL, NULL, '318', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(99, '99', 'Interactive Insurance Brokers', NULL, NULL, NULL, '4914', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(100, '100', 'JB Boda Reinsurance Brokers', NULL, NULL, NULL, '120687', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(101, '101', 'JLT Insure Direct (Brokers)', NULL, NULL, NULL, '57066', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(102, '102', 'Joie de Vivre International Insurance Brokerage', NULL, NULL, NULL, '48795', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(103, '103', 'Joie de Vivre International Insurance Brokerage', NULL, NULL, NULL, '34660', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(104, '104', 'Juelmin Insurance Services', NULL, NULL, NULL, '15958', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(105, '105', 'K.M.Datsur & Company (Insurance Brokers)', NULL, NULL, NULL, '9762', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(106, '106', 'Leaders Insurance Brokers', NULL, NULL, NULL, '790', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(107, '107', 'Life Plus Insurance Brokers LLC ', NULL, NULL, NULL, '28626', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(108, '108', 'Lifecare International Insurance Brokers', NULL, NULL, NULL, '71208', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(109, '109', 'Links Insurance Brokers', NULL, NULL, NULL, '20026', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(110, '110', 'Lloyd & Co (Emirates) Insurance Brokers', NULL, NULL, NULL, '59872', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(111, '111', 'LMG UAE LLC ', NULL, NULL, NULL, '26940', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(112, '112', 'Lockton Insurance Brokers', NULL, NULL, NULL, '33004', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(113, '113', 'Lockton Insurance Brokers', NULL, NULL, NULL, '506794', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(114, '114', 'Lonsdale and Associates Insurance Brokers', NULL, NULL, NULL, '42098', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(115, '115', 'Malakut Insurance Brokers', NULL, NULL, NULL, '487242', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(116, '116', 'Malakut Insurance Brokers', NULL, NULL, NULL, '450644', NULL, 'Jumeirah Lake Towers', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(117, '117', 'Marina Insurance Brokers', NULL, NULL, NULL, '42032', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(118, '118', 'Marsh Emirates Insurance Brokerage', NULL, NULL, NULL, '64057', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(119, '119', 'Marsh Emirates Insurance Brokerage', NULL, NULL, NULL, '54733', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(120, '120', 'Masters Insurance Brokers', NULL, NULL, NULL, '75530', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(121, '121', 'Medgulf Insurance Brokers ', NULL, NULL, NULL, '113684', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(122, '122', 'MEPA Gulf ', NULL, NULL, NULL, '46486', NULL, 'Abu Dhabi ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(123, '123', 'Metropolitan Insurance Brokers', NULL, NULL, NULL, '119483', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(124, '124', 'Middle East Insurance Brokers', NULL, NULL, NULL, '54396', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(125, '125', 'Middle East Insurance Brokers', NULL, NULL, NULL, '92292', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(126, '126', 'Millenium Insurance Brokers', NULL, NULL, NULL, '34602', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(127, '127', 'NASCO Emirates', NULL, NULL, NULL, '30840', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(128, '128', 'NASCO Middle East', NULL, NULL, NULL, '62528', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(129, '129', 'National Resources Insurance Services', NULL, NULL, NULL, '71950', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(130, '130', 'National Resources Insurance Services', NULL, NULL, NULL, '34737', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(131, '131', 'New Age Insurance Brokers', NULL, NULL, NULL, '33916', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(132, '132', 'New Shield Insurance Brokers', NULL, NULL, NULL, '233640', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(133, '133', 'Nexus Insurance Brokers', NULL, NULL, NULL, '124422', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(134, '134', 'Nexus Insurance Brokers', NULL, NULL, NULL, '94455', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(135, '135', 'Noble Insurance Brokers', NULL, NULL, NULL, '84694', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(136, '136', 'Noble Insurance Brokers', NULL, NULL, NULL, '64755', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(137, '137', 'North Star Insurance Brokers', NULL, NULL, NULL, '35432', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(138, '138', 'Northern Insurance Brokers', NULL, NULL, NULL, '96092', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(139, '139', 'Omega Insurance Brokers', NULL, NULL, NULL, '49474', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(140, '140', 'Opals International Insurance Brokers', NULL, NULL, NULL, '118006', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(141, '141', 'Oryx Insurance Brokers', NULL, NULL, NULL, '41344', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(142, '142', 'Pearl Insurance Brokers', NULL, NULL, NULL, '31127', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(143, '143', 'Pearl Insurance Brokers', NULL, NULL, NULL, '', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(144, '144', 'Petra Insurance Brokers', NULL, NULL, NULL, '3384', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(145, '145', 'Pinnacle Insurance Brokers ', NULL, NULL, NULL, '121341', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(146, '146', 'Pioneer Insurance Brokers', NULL, NULL, NULL, '922', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(147, '147', 'Prime Insurance Brokers', NULL, NULL, NULL, '32380', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(148, '148', 'Prime Insurance Brokers', NULL, NULL, NULL, '31130', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(149, '149', 'Professional Practice Insurance Brokers ', NULL, NULL, NULL, '63973', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(150, '150', 'Prominent Insurance Brokers', NULL, NULL, NULL, '124518', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(151, '151', 'Promise Insurance Services', NULL, NULL, NULL, '214319', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(152, '152', 'Promise Insurance Services', NULL, NULL, NULL, '25538', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(153, '153', 'Protection Insurance Brokers LLC ', NULL, NULL, NULL, '107042', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(154, '154', 'Prudence Insurance Brokers', NULL, NULL, NULL, '49599', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(155, '155', 'PWS Gulf Insurance Brokers', NULL, NULL, NULL, '22135', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(156, '156', 'Reliance Insurance Brokers ', NULL, NULL, NULL, '6292', NULL, 'Sharjah ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(157, '157', 'Savington International Insurance Brokers', NULL, NULL, NULL, '96695', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(158, '158', 'Secure Insurance Brokers', NULL, NULL, NULL, '54771', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(159, '159', 'Silver Globe Insurance Brokers ', NULL, NULL, NULL, '2684', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(160, '160', 'Southwest Insurance Agent ', NULL, NULL, NULL, '-', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(161, '161', 'Star Insurance Services', NULL, NULL, NULL, '21844', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(162, '162', 'Sun Insurance Brokers ', NULL, NULL, NULL, '27286', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(163, '163', 'Suraksha Insurance Brokers', NULL, NULL, NULL, '181159', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(164, '164', 'Tawasul Insurance Services', NULL, NULL, NULL, '106119', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(165, '165', 'Technical Insurance Services Est.  ', NULL, NULL, NULL, '45610', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(166, '166', 'Ultimate Insurance Brokers LLC', NULL, NULL, NULL, '210', NULL, 'Dubai ', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(167, '167', 'United Gulf Insurance Brokers', NULL, NULL, NULL, '15797', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(168, '168', 'United Insurance Brokers', NULL, NULL, NULL, '506533', NULL, 'DIFC', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(169, '169', 'United Insurance Brokers', NULL, NULL, NULL, '8832', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(170, '170', 'Unitrust Insurance Brokers', NULL, NULL, NULL, '114648', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(171, '171', 'Unity Insurance Brokers ', NULL, NULL, NULL, '45218', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(172, '172', 'Wehbe Insurance Services', NULL, NULL, NULL, '2550', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(173, '173', 'Wehbe Insurance Services', NULL, NULL, NULL, '42284', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-24 16:50:02'),
(174, '0', 'NA', NULL, NULL, NULL, '000', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 07:09:10'),
(175, '0', 'To Be Advised', NULL, NULL, NULL, '000', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 07:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `categorymaster`
--

DROP TABLE IF EXISTS `categorymaster`;
CREATE TABLE `categorymaster` (
  `categoryId` int(11) NOT NULL,
  `category` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active,I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorymaster`
--

INSERT INTO `categorymaster` (`categoryId`, `category`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Surveys', 'G', 'A', '1', '2017-01-20 21:40:20', '', '2017-01-26 14:53:21'),
(2, 'Engineering', 'G', 'A', '1', '2017-01-20 21:47:06', '', '2017-01-20 08:59:49'),
(3, 'Domestic', 'G', 'A', '1', '2017-01-20 21:47:32', '', '2017-01-20 08:59:53'),
(4, 'Marine', 'M', 'A', '1', '2017-01-22 10:15:33', '', '2017-01-21 06:45:33'),
(5, 'Machinery', 'G', 'A', '1', '2017-01-22 10:17:09', '', '2017-01-21 06:47:09'),
(6, 'Commercial', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 06:47:30'),
(7, 'Liability', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 06:47:59'),
(8, 'Miscellaneous', 'G', 'A', '1', '2017-01-22 10:18:17', '', '2017-01-21 06:48:17'),
(9, 'Pecuniary', 'G', 'A', '1', '2017-01-22 10:50:58', '', '2017-01-21 07:20:58');

-- --------------------------------------------------------

--
-- Table structure for table `claimmaster`
--

DROP TABLE IF EXISTS `claimmaster`;
CREATE TABLE `claimmaster` (
  `claimId` int(11) NOT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `officeId` int(11) DEFAULT NULL,
  `policyNumber` varchar(50) DEFAULT NULL,
  `insuredName` varchar(50) DEFAULT NULL,
  `insurerName` varchar(200) DEFAULT NULL,
  `claimReference` varchar(50) DEFAULT NULL,
  `brokerReference` varchar(50) DEFAULT NULL,
  `contactPerson` varchar(50) DEFAULT NULL,
  `contactNumber` varchar(50) DEFAULT NULL,
  `insurerContact` varchar(50) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `brokerId` int(11) DEFAULT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `instructionTime` varchar(20) DEFAULT NULL,
  `instructionDate` varchar(20) DEFAULT NULL,
  `contactTime` varchar(20) DEFAULT NULL,
  `contactDate` varchar(20) DEFAULT NULL,
  `surveyTime` varchar(20) DEFAULT NULL,
  `surveyDate` varchar(20) DEFAULT NULL,
  `preliminaryDate` varchar(50) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `jobStatus` varchar(2) NOT NULL DEFAULT 'O' COMMENT 'O-Open, V-Visit,P-Preliminary, W-Working, C-Closed',
  `frozen` varchar(2) NOT NULL DEFAULT 'N',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `claimmaster`
--

INSERT INTO `claimmaster` (`claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`) VALUES
(1, 'DXB/G/0001/17', NULL, 1, '2/1/020/00010119', 'Philadelphia Private School', '', '9585', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 128, 8, 6, 38, '13:00', '03/01/2017', '14:00', '03/01/2017', '13:00', '03/02/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:33:54', '2017-03-25 07:13:39', NULL),
(2, 'DXB/G/0002/17', NULL, 1, 'P/01/1002/2014/503', 'Al Futtaim Auto Centre', '', '9586', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 5, 6, 35, '13:00', '03/01/2017', '14:00', '03/01/2017', '15:00', '03/01/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:46:08', '2017-03-25 07:16:08', NULL),
(3, 'DXB/G/0003/17', NULL, 1, '4001/2014/01/00049', 'Al Jaber Transport & Gen. Cont.', '', '9587', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 4, 2, 6, '13:00', '03/01/2017', '14:00', '03/01/2017', '13:00', '03/02/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:51:39', '2017-03-25 07:21:39', NULL),
(4, 'DXB/G/0004/17', NULL, 1, '2211099', 'Signature Terrace Restaurant', '', '9588', 'CL/DXB/SCL63/2017/000008', 'NA', 'NA', 'NA', 'NA', 5, 80, 9, 6, 38, '13:00', '03/01/2017', '13:00', '03/01/2017', '14:00', '03/02/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:58:02', '2017-03-25 07:28:02', NULL),
(5, 'DXB/G/0005/17', NULL, 1, 'P/02/ENG/MBB/2016/00001', 'Kimoha Entrepreneurs Limited', '', '9589', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 8, 5, 26, '13:00', '03/02/2017', '13:00', '03/02/2017', '13:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 13:03:08', '2017-03-25 07:33:08', NULL),
(6, 'DXB/G/0006/17', NULL, 1, 'KH/DE/4769/16', 'Al Nasr Contracting & Demolition LLC', '', '9590', 'ARAYA/FGA/CLM/1960/28-02-17', 'NA', 'NA', 'NA', 'NA', 12, 39, 2, 2, 7, '13:00', '03/02/2017', '14:00', '03/02/2017', '13:00', '03/05/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:06:52', '2017-03-25 08:36:52', NULL),
(7, 'DXB/G/0007/17', NULL, 1, '01-311-133-11-75', 'Caboodle Pampers & Play LLC', '', '9591', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 7, 42, '13:00', '03/02/2017', '14:00', '03/02/2017', '15:00', '03/03/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:10:27', '2017-03-25 08:40:27', NULL),
(8, 'DXB/G/0008/17', NULL, 1, 'PI/2006/100001', 'Sky Oryx JV', '', '9592', 'NA', 'NA', 'NA', 'NA', 'NA', 8, 174, 2, 7, 43, '13:00', '03/02/2017', '14:00', '03/02/2017', '15:00', '03/02/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:13:22', '2017-03-25 08:43:22', NULL),
(9, 'DXB/G/0009/17', NULL, 1, '2/2/020/04002484/3', 'Benjamin David Hughes', '', '9593', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 174, 3, 3, 22, '13:00', '03/02/2017', '14:00', '03/02/2017', '15:00', '03/02/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:16:11', '2017-03-25 08:46:11', NULL),
(10, 'DXB/G/0010/17', NULL, 1, '2/1/020/00010162', 'Chelsea Plaza Hotel', '', '9594', 'PAR17011', 'NA', 'NA', 'NA', 'NA', 44, 80, 8, 6, 37, '13:00', '03/02/2017', '14:00', '03/02/2017', '13:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:19:13', '2017-03-25 08:49:13', NULL),
(11, 'DXB/G/0011/17', NULL, 1, 'HFAP200500001548/075', 'Giordano Fashion LLC', '', '9595', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 142, 2, 6, 35, '13:00', '03/04/2017', '14:00', '03/04/2017', '13:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:21:55', '2017-03-25 08:51:55', NULL),
(12, 'DXB/G/0012/17', NULL, 1, '2/1/021/0000145', 'Yateem Optician', '', '9596', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 2, 6, 35, '22:00', '03/04/2017', '23:00', '03/04/2017', '22:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:24:29', '2017-03-25 08:54:29', NULL),
(13, 'DXB/G/0013/17', NULL, 1, 'HFAP200500001554/701', 'Al Maya International Ltd FZC', '', '9597', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 2, 6, 35, '10:00', '03/04/2017', '11:00', '03/04/2017', '10:00', '03/09/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:26:19', '2017-03-25 08:56:19', NULL),
(14, 'DXB/G/0014/17', NULL, 1, 'P/11/1002/2014/71', 'Life Health Care Group', '', '9598', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:28:05', '2017-03-25 08:58:05', NULL),
(15, 'DXB/G/0015/17', NULL, 1, 'P/001/2033/2016/16', 'Allied Transport LLC', '', '9599', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:30:33', '2017-03-25 09:00:33', NULL),
(16, 'DXB/G/0016/17', NULL, 1, 'HFAR201500002271', 'Jereh Energy Corpoporation', '', '9600', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 9, 2, 18, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/05/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:38:51', '2017-03-25 09:08:51', NULL),
(17, 'DXB/G/0017/17', NULL, 1, '2/1/020/22005341', 'Wayne Charles Dooley', '', '9601', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 8, 3, 21, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:41:26', '2017-03-25 09:11:26', NULL),
(18, 'DXB/G/0018/17', NULL, 1, '2003/2017/03/00005', 'Ajmal Group of Companies', '', '9602', 'CL/AZB/SCL15/2017/000006', 'NA', 'NA', 'NA', 'NA', 8, 81, 2, 6, 35, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:43:21', '2017-03-25 09:13:21', NULL),
(19, 'DXB/G/0019/17', NULL, 1, 'D/02/F350/1048/16', 'Matalan Department Stores', '', '9603', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 49, 82, 2, 6, 35, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:46:14', '2017-03-25 09:16:14', NULL),
(20, 'DXB/G/0020/17', NULL, 1, 'P/04/PRO/PRL/2016/10403', 'Union Cement Company', '', '9604', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 2, 5, 27, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:48:09', '2017-03-25 09:18:09', NULL),
(21, 'DXB/G/0021/17', NULL, 1, 'Z1-F13-000228-2', 'Lake Shore Tower', '', '9605', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 2, 53, 3, 6, 37, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '02/23/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:51:01', '2017-03-25 09:21:01', NULL),
(22, 'DXB/G/0022/17', NULL, 1, 'HFHM201700000097', 'Rixos Hotel', '', '9606', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 115, 5, 6, 37, '10:00', '03/05/2017', '11:00', '03/05/2017', '10:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:28:18', '2017-03-25 09:58:18', NULL),
(23, 'DXB/G/0023/17', NULL, 1, '2/1/020/02304266', 'Boston Foods', '', '9607', 'CL/DXB/SCL63/2017/000009', 'NA', 'NA', 'NA', 'NA', 44, 82, 2, 6, 35, '10:00', '03/06/2017', '11:00', '03/06/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:29:59', '2017-03-25 09:59:59', NULL),
(24, 'DXB/M/0024/17', NULL, 1, 'To Be Advised', 'Pioneer Gulf Services', '', '9608', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 23, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:32:04', '2017-03-25 10:02:04', NULL),
(25, 'DXB/G/0025/17', NULL, 1, 'HFAP201300004194', 'Sultan Group Investment', '', '9609', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 137, 5, 6, 35, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:34:01', '2017-03-25 10:04:01', NULL),
(26, 'DXB/G/0026/17', NULL, 1, 'P/01/1002/2014/364', 'Al Futtaim Watches & Jewellery', '', '9610', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 2, 6, 35, '10:00', '03/06/2017', '11:00', '03/06/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:36:42', '2017-03-25 10:06:42', NULL),
(27, 'DXB/G/0027/17', NULL, 1, '2/2/020/02306066/2', 'The Ice Cream Lab', '', '9611', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 57, 2, 6, 35, '10:00', '03/06/2017', '11:00', '03/06/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:38:46', '2017-03-25 10:08:46', NULL),
(28, 'DXB/M/0028/17', NULL, 1, 'To Be Advised', 'Al Khayyat Investment (ALPHAMED)', '', '9612', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 27, 175, 9, 4, 23, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:41:08', '2017-03-25 10:11:08', NULL),
(29, 'DXB/G/0029/17', NULL, 1, '03/1101/13/2015/10', 'Mammut Group', '', '9613', 'NA', 'NA', 'NA', 'NA', 'NA', 23, 128, 4, 2, 18, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:45:06', '2017-03-25 10:15:06', NULL),
(30, 'DXB/G/0030/17', NULL, 1, 'PR2/138586/2016/NGI', 'Rivoli Group', '', '9614', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 33, 101, 2, 6, 35, '10:00', '03/06/2017', '11:00', '03/06/2017', '10:00', '03/14/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:53:34', '2017-03-25 10:23:34', NULL),
(31, 'DXB/G/0031/17', NULL, 1, 'EFER20150000021', 'Centaur Electromechanical', '', '9615', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 26, 5, 2, 9, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:08:58', '2017-03-25 10:38:58', NULL),
(32, 'DXB/G/0032/17', NULL, 1, 'DFTP201600001379', 'Frigoglass', '', '9616', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 2, 7, 44, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/06/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:10:55', '2017-03-25 10:40:55', NULL),
(33, 'DXB/G/0033/17', NULL, 1, 'AFCT201000000397', 'Emke Group', '', '9617', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 3, 9, 30, '10:00', '03/07/2017', '11:00', '03/07/2017', '12:00', '03/07/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:13:08', '2017-03-25 10:43:08', NULL),
(34, 'DXB/G/0034/17', NULL, 1, '01/1101/11/2016/290', 'Patchi LLC', '', '9618', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 87, 5, 6, 35, '10:00', '03/07/2017', '11:00', '03/07/2017', '11:00', '03/08/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:15:06', '2017-03-25 10:45:06', NULL),
(35, 'DXB/G/0035/17', NULL, 1, 'To Be Advised', 'National Marine Dredging Co.', '', '9619', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 9, 2, 18, '10:00', '03/07/2017', '11:00', '03/07/2017', '10:00', '03/08/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:17:42', '2017-03-25 10:47:42', NULL),
(36, 'DXB/G/0036/17', NULL, 1, 'P/2004/02/20011/2016/00034', 'DKC Veterinary Clinic', '', '9620', 'NA', 'NA', 'NA', 'NA', 'NA', 3, 174, 8, 6, 37, '10:00', '03/08/2017', '11:00', '03/08/2017', '10:00', '03/10/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:19:39', '2017-03-25 10:49:39', NULL),
(37, 'DXB/G/0037/17', NULL, 1, 'P2/20/16-0807-001404', 'Damas', '', '9621', 'C/00012870', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 1, 1, '10:00', '03/09/2017', '11:00', '03/09/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:23:18', '2017-03-25 10:53:18', NULL),
(38, 'DXB/G/0038/17', NULL, 1, 'UAE/01/220/11952', 'Cocoon Nursery', '', '9622', 'NA', 'NA', 'NA', 'NA', 'NA', 5, 174, 7, 6, 36, '10:00', '03/09/2017', '11:00', '03/09/2017', '10:00', '03/15/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:38:32', '2017-03-25 14:08:32', NULL),
(39, 'DXB/G/0039/17', NULL, 1, 'To Be Advised', 'Green Land Textile', '', '9623', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 5, 175, 8, 6, 37, '10:00', '03/09/2017', '11:00', '03/09/2017', '10:00', '03/11/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:40:20', '2017-03-25 14:10:20', NULL),
(40, 'DXB/G/0040/17', NULL, 1, 'P2/20/16-0404-002739', 'Damas', '', '9624', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 6, 35, '10:00', '03/09/2017', '11:00', '03/09/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:42:17', '2017-03-25 14:12:17', NULL),
(41, 'DXB/G/0041/17', NULL, 1, 'P2/20/16-0404-002540', 'Lifestyle LLC', '', '9625', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '03/12/2017', '11:00', '03/12/2017', '12:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:43:35', '2017-03-25 14:13:35', NULL),
(42, 'DXB/G/0042/17', NULL, 1, 'To Be Advised', 'Bed, Bags & Beyond LLC', '', '9626', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 26, 175, 2, 6, 35, '10:00', '03/12/2017', '11:00', '03/12/2017', '12:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:45:25', '2017-03-25 14:15:26', NULL),
(43, 'DXB/G/0043/17', NULL, 1, 'Z1-F13-000211-2', 'Al Ghurair Exchange Limited', '', '9627', 'C/00012837', 'NA', 'NA', 'NA', 'NA', 2, 118, 2, 6, 35, '10:00', '03/12/2017', '11:00', '03/12/2017', '12:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:47:12', '2017-03-25 14:17:12', NULL),
(44, 'DXB/G/0044/17', NULL, 1, 'P/01/1002/2014/723', 'Cozmo Travel LLC', '', '9628', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '03/13/2017', '11:00', '03/13/2017', '12:00', '03/13/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:49:10', '2017-03-25 14:19:10', NULL),
(45, 'DXB/G/0045/17', NULL, 1, 'HFHM201400000057', 'Seven Tides Limited &/ or Anantara Palm Jumeirah', '', '9629', 'ARYA/FGA/CLM/2378/13-03-2017', 'NA', 'NA', 'NA', 'NA', 38, 39, 8, 6, 37, '10:00', '03/13/2017', '11:00', '03/13/2017', '12:00', '03/13/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:51:32', '2017-03-25 14:21:32', NULL),
(46, 'DXB/M/0046/17', NULL, 1, '01/3214/36/2016/72', 'Fadel Saif Mubarak Al Mazrouei', '', '9630', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 24, '10:00', '03/13/2017', '11:00', '03/13/2017', '10:00', '03/14/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:53:48', '2017-03-25 14:23:48', NULL),
(47, 'DXB/G/0047/17', NULL, 1, 'EFER201400001273', 'Palm Jumeirah Co. LLC', '', '9631', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 9, 7, 42, '10:00', '03/13/2017', '11:00', '03/13/2017', '10:00', '03/15/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:55:43', '2017-03-25 14:25:43', NULL),
(48, 'DXB/G/0048/17', NULL, 1, 'P/01/6004/2014/20', 'Al Futtaim Engineering', '', '9632', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 43, '10:00', '03/14/2017', '11:00', '03/14/2017', '10:00', '03/19/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:58:04', '2017-03-25 14:28:04', NULL),
(49, 'DXB/G/0049/17', NULL, 1, 'P/04/2033/2016/36', 'Dashmesh General Transport', '', '9633', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '03/14/2017', '11:00', '03/14/2017', '10:00', '02/15/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:08:29', '2017-03-25 14:38:29', NULL),
(50, 'DXB/G/0050/17', NULL, 1, 'P/01/1002/2014/206', 'Al Futtaim Real Estate Private Limited', '', '9634', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 6, 37, '10:00', '03/15/2017', '11:00', '03/15/2017', '10:00', '03/16/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:10:29', '2017-03-25 14:40:29', NULL),
(51, 'DXB/G/0051/17', NULL, 1, 'DP/01/3040/17/00004', 'Mazrui Holdings LLC', '', '9635', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 24, 87, 5, 9, 30, '10:00', '03/15/2017', '11:00', '03/15/2017', '10:00', '03/15/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:12:15', '2017-03-25 14:42:15', NULL),
(52, 'DXB/G/0052/17', NULL, 1, 'P/04/6001/2014/300', 'QGB Group of Companies', '', '9636', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 42, '10:00', '03/16/2017', '11:00', '03/16/2017', '12:00', '03/16/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:14:13', '2017-03-25 14:44:13', NULL),
(53, 'DXB/G/0053/17', NULL, 1, 'P/01/1002/2015/346', 'Saif Belhasa Holding', '', '9637', '14991 PK', 'NA', 'NA', 'NA', 'NA', 40, 15, 8, 6, 35, '10:00', '03/16/2017', '11:00', '03/16/2017', '12:00', '03/16/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:16:17', '2017-03-25 14:46:17', NULL),
(54, 'DXB/G/0054/17', NULL, 1, 'P2/20/17-0807-001551', 'Lifestyle LLC', '', '9638', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '03/19/2017', '11:00', '03/19/2017', '10:00', '03/12/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:17:54', '2017-03-25 14:47:54', NULL),
(55, 'LON/G/0001/17', NULL, 2, 'P/40/1002/2014/123', 'Jawhara Jewellery LLC', '', '354', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 17, 8, 6, 35, '10:00', '03/16/2017', '11:00', '03/16/2017', '12:00', '03/16/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:21:36', '2017-03-25 14:51:36', NULL),
(56, 'LON/G/0002/17', NULL, 2, 'To Be Advised', 'One of One Jewellery', '', '354', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '03/01/2017', '11:00', '03/01/2017', '12:00', '03/01/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:06:01', '2017-03-25 16:36:01', NULL),
(57, 'LON/G/0003/17', NULL, 2, 'To Be Advised', 'Al Mumayyaz Jewellers', '', '355', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 51, 175, 8, 1, 1, '10:00', '03/06/2017', '11:00', '03/06/2017', '12:00', '03/13/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:07:27', '2017-03-25 16:46:38', NULL),
(58, 'LON/G/0004/17', NULL, 2, 'To Be Advised', 'Damas Jewellery', '', '356', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 52, 118, 3, 1, 1, '10:00', '03/08/2017', '11:00', '03/08/2017', '12:00', '03/08/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:11:19', '2017-03-25 16:41:19', NULL),
(59, 'LON/G/0005/17', NULL, 2, 'P/40/5021/2015/4', 'Jawhara Jewellery LLC', '', '357', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 17, 8, 6, 35, '10:00', '03/13/2017', '11:00', '03/13/2017', '12:00', '03/13/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:13:52', '2017-03-25 16:43:52', NULL),
(60, 'LON/G/0006/17', NULL, 2, 'To Be Advised', 'Whitestar DMCC', '', '359', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 53, 101, 3, 1, 3, '10:00', '03/19/2017', '11:00', '03/19/2017', '12:00', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:15:36', '2017-03-25 16:45:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clientmaster`
--

DROP TABLE IF EXISTS `clientmaster`;
CREATE TABLE `clientmaster` (
  `clientId` int(11) NOT NULL,
  `referenceId` varchar(100) DEFAULT NULL,
  `clientName` varchar(300) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `poBox` varchar(50) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A - Active, I - Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientmaster`
--

INSERT INTO `clientmaster` (`clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `poBox`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, '1', 'Abu Dhabi National Insurance Company', '02 408 0380', 'h.padmanabhan@adnic.ae', '839', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(2, '2', 'Abu Dhabi National Insurance Company', '05 515 4861', 'j.nair@adnic.ae', '11236', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(3, '3', 'Abu Dhabi National Takaful Company', '02 410 7735', 'F.AbuYazbek@takaful.ae', '35335', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(4, '4', 'Adamjee Insurance Company ', '04 360 9762', 'awais.khalid@adamjee.com', '4256', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(5, '5', 'AIG', '04 601 4673', 'Michael.Wijemanne@aig.com', '54400', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(6, '6', 'Al Ain Ahlia Insurance Company', '02 611 9999', 's.gharaibeh@alaininsurance.com', '3077', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(7, '7', 'Al Buhaira National Insurance Company', '06 517 4509', 'maslam@albuhaira.com', '6000', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(8, '8', 'Al Dhafra Insurance Company ', '02 694 9460', 'claims@aldhafrainsurance.ae', '319', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(9, '9', 'Al Fujairah  National Insurance Company', '04 222 3208', 'wilfred.noronha@fujinsco.ae', '277', NULL, 'Fujairah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(10, '10', 'Al Hilal Takaful Company', '02 499 4292', 'svalappil@alhilaltakaful.ae', '111644', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(11, '11', 'Al Ittihad Al Watani Insurance Company', '04 282 3266 x 200', 'a.azzi@unic.ae', '3000', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(12, '12', 'Al Khazna Insurance Company', '02 696 9823', 'tarik@alkhazna.ae', '73343', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(13, '13', 'Al Sagr National Insurance Company', '04 702 8650', 'Ramkumar@alsagrins.ae', '14614', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(14, '14', 'Al Wathba National Insurance Company', '02 418 5430', 'd_sahal@awnic.com', '45154', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(15, '15', 'Alliance Insurance', '4605111', 'nmclaims@alliance-me.com', '5501', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(16, '16', 'Arabia Insurance', '04228 0022', 'mkanchiraju@arabiainsurance.com', '1050', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(17, '17', 'Arabian Scandinavian Insurance Company ', '42824403', 'v.rajendran@ascana.net', '1993', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(18, '18', 'AXA Insurance', '04 440 3726', 'rajeev.nambiar@axa-gulf.com', '5862', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(19, '19', 'Dar Al Takaful', '04 304 1605', 'akumar@dat.ae', '235353', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(20, '20', 'Dubai Insurance', '04 269 3030', 'menezes.l@dubins.ae', '3027', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(21, '21', 'Dubai Islamic Insurance & Reinsurance Company (AMAN)', '04 319 3214', 'akther.hussain@aman.ae', '157', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(22, '22', 'Dubai National Insurance & Reinsurance Company ', '04 295 6700 Ext.430', 'shakir@dnirc.com', '1806', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(23, '23', 'Emirates Insurance Company', '02 698 1569', 'kthomas@eminsco.com', '3856', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(24, '24', 'Insurance House', '02 493 4524', 'imad.shishnieh@insurancehouse.ae', '129921', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(25, '25', 'Iran Insurance Company', '04 221 47 47', 'tqureshi@bimehir.ae', '2004', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(26, '26', 'Iran Insurance Company', '04 221 4747', 'fakher@bimehir.ae', '2004', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(27, '27', 'Islamic Arab Insurance Comapny (SALAMA)', '04 404 0114', 'shahida.khan@salama.ae', '10214', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(28, '28', 'Jordan Insurance', '02 634 4800', 'sunil@jicad.ae', '2197', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(29, '29', 'Jordan Insurance', '02 634 4800', 'elias@jicad.ae', '2197', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(30, '30', 'Methaq Takaful Insurance Company', '02 656 5333', 'dalal.habib@methaq.ae', '32774', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(31, '31', 'Mitsui Sumitomo Insurance Company', '02 627 4834 / 04 336 5335', 'wajih@msi-dubai.ae', '25190', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(32, '32', 'Nasco Middle East ', '04 305 1611', 'Abdul.Khader@nascodubai.com', '62528', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(33, '33', 'National General Insurance Company', '04 211 5912', 'rajan@ngiuae.com', '154', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(34, '34', 'National Takaful Company  (Watania) ', '800928264', 'mohammad.shadab@watania.ae', '6457', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(35, '35', 'New India Assurance Company', '04 352 5563 Ext.4368', 'n.nikhil@nia-dubai.com', '5701', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(36, '36', 'New India Assurance Company', '02 644 0428 Ext.225', 'binu@newindia-uae.com', '46743', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(37, '37', 'Noor Takaful', '04 426 8964', 'andrew.greenwood@noortakaful.com', '49998', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(38, '38', 'Oman Insurance Company', '04 233 7467', 'Balaji.Ganapathy@Tameen.ae', '', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(39, '39', 'Oriental Insurance Company', '04 353 8688 Ext.135', 'yogesh@oicgulf.ae', '478', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:46:32'),
(40, '40', 'Orient Insurance', '04 253 1592', 'Natarajan.Ramesh@alfuttaim.com', '27966', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(41, '41', 'Qatar General Insurance & Reinsurance Company', '04 268 8688', 'vkr@qgirco.com', '8080', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(42, '42', 'Qatar Insurance Company', '04 2224045 Ext.37 ', 'sriraman@qici.com.qa', '4066', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(43, '43', 'RAK Insurance', '07 227 3000', 'balu@rakinsurance.com', '506', NULL, 'Ras Al Khaimah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(44, '44', 'Royal & Sun Alliance Insurance Company', '04 3029901', 'zia-ul.jaweed@ae.rsagroup.com', '28648', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:46:57'),
(45, '45', 'Saudi Arabian Insurance Company', '02 645 8060', 'saicoauh@emirates.net.ae', '585', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(46, '46', 'Sharjah Insurance', '06 519 5666', 'alaa.alqasa@shjins.ae', '792', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(47, '47', 'Tokio Marine & Nichido Fire Insurance Company', '04 361 9777', 'majid@tmnf.ae', '152', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(48, '48', 'Union Insurance Company', '04 378 7678 ', 'narayan.i@unioninsurance.ae', '119227', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:44:34'),
(49, '49', 'United Insurance Company', '04 212 8045', 'joshua.abiero@uic.ae', '110', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 05:47:17'),
(50, 'U1', 'Underwriters per PWS (Dubai)', '9988776655', 'u1@pws.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:02:43', NULL, '2017-03-25 16:32:43'),
(51, 'U2', 'Underwriters per Chedid RE - Dxb', '9988776655', 'u2@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:03:26', NULL, '2017-03-25 16:33:26'),
(52, 'U3', 'Underwriters per RK Harisson', '9988776655', 'u3@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:03:54', NULL, '2017-03-25 16:33:54'),
(53, 'U4', 'Underwriters per Jacky Grunfeld, Belgium', '9988776655', 'u4@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:04:14', NULL, '2017-03-25 16:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `currencydetails`
--

DROP TABLE IF EXISTS `currencydetails`;
CREATE TABLE `currencydetails` (
  `currencyId` int(11) NOT NULL,
  `currencyCode` varchar(10) DEFAULT NULL,
  `currencyName` varchar(100) DEFAULT NULL,
  `currencyShort` varchar(20) DEFAULT NULL,
  `payableTo` varchar(400) DEFAULT NULL,
  `ibanNumber` varchar(50) DEFAULT NULL,
  `swiftCode` varchar(50) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currencydetails`
--

INSERT INTO `currencydetails` (`currencyId`, `currencyCode`, `currencyName`, `currencyShort`, `payableTo`, `ibanNumber`, `swiftCode`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'AED', 'UAE Dirhams', 'Dhs', 'WHITELAW LOSS ADJUSTERS LLC', 'AE660260001012327362303', 'EBILAEAD', 1, 0, '2017-03-24 00:00:00', 0, '2017-03-24 15:45:28');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

DROP TABLE IF EXISTS `invoicedetails`;
CREATE TABLE `invoicedetails` (
  `detailsId` int(11) NOT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `narrationText` varchar(400) DEFAULT NULL,
  `timelyRate` varchar(200) DEFAULT NULL,
  `timelyText` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoicemaster`
--

DROP TABLE IF EXISTS `invoicemaster`;
CREATE TABLE `invoicemaster` (
  `invoiceId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `invoiceTitle` varchar(200) DEFAULT NULL,
  `invoiceNumber` varchar(100) DEFAULT NULL,
  `currencyId` int(11) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `faoName` varchar(200) DEFAULT NULL,
  `toName` varchar(200) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `invoiceDate` varchar(20) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `narrationText` varchar(500) DEFAULT NULL,
  `totalAmount` varchar(100) DEFAULT NULL,
  `totalInWords` varchar(500) DEFAULT NULL,
  `invoiceTerms` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `officemaster`
--

DROP TABLE IF EXISTS `officemaster`;
CREATE TABLE `officemaster` (
  `officeId` int(11) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officemaster`
--

INSERT INTO `officemaster` (`officeId`, `location`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Dubai', 'Whitelaw Chartered Loss Adjusters & Surveyors', 'DXB', 'A', '', '2017-01-14 00:00:00', '', '2017-01-13 04:35:38'),
(2, 'London', 'Whitelaw Chartered Loss Adjusters London', 'LON', 'A', '', '2017-01-14 00:00:00', '', '2017-01-26 14:41:42');

-- --------------------------------------------------------

--
-- Table structure for table `prelimupdates`
--

DROP TABLE IF EXISTS `prelimupdates`;
CREATE TABLE `prelimupdates` (
  `prelimId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'R-Report, F-Fees',
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receiptdetails`
--

DROP TABLE IF EXISTS `receiptdetails`;
CREATE TABLE `receiptdetails` (
  `receiptId` int(11) NOT NULL,
  `receiptNumber` varchar(50) DEFAULT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `receiptDate` varchar(50) DEFAULT NULL,
  `receiptAmount` varchar(50) DEFAULT NULL,
  `paymentMode` varchar(20) DEFAULT NULL,
  `chequeNumber` varchar(50) DEFAULT NULL,
  `chequeDate` varchar(50) DEFAULT NULL,
  `receivedFrom` varchar(200) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `servicemaster`
--

DROP TABLE IF EXISTS `servicemaster`;
CREATE TABLE `servicemaster` (
  `serviceId` int(11) NOT NULL,
  `service` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A- Active, I- Inactive',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicemaster`
--

INSERT INTO `servicemaster` (`serviceId`, `service`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Professional Loss Adjusting Fees', 'A', 1, '2017-01-31 07:09:16', 1, '2017-02-25 06:20:25'),
(2, 'Photographs / Photocopying', 'A', 1, '2017-01-31 23:12:35', 1, '2017-02-25 06:19:00'),
(3, 'Telephone / Post / Fax', 'A', 1, '2017-01-31 23:12:44', 1, '2017-02-25 06:20:29'),
(4, 'Translation', 'A', 1, '2017-02-25 11:49:21', NULL, '2017-02-25 06:19:21'),
(5, 'Travel Expenses', 'A', 1, '2017-02-25 11:49:34', NULL, '2017-02-25 06:19:34'),
(7, 'Miscellaneous', 'A', 1, '2017-02-25 11:50:45', NULL, '2017-02-25 06:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE `subcategories` (
  `subId` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subId`, `categoryId`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'Jewellers Block', 'G', 'A', '1', '2017-01-21 12:51:02', '', '2017-01-26 14:57:18'),
(2, 2, 'Contractors All Risks (Fire)', 'G', 'A', '1', '2017-01-21 12:51:20', '', '2017-01-20 09:21:20'),
(3, 1, 'Risk Survey', 'G', 'A', '1', '2017-01-22 10:20:09', '', '2017-01-26 14:53:06'),
(4, 1, 'Marine Warranty Survey', 'G', 'A', '1', '2017-01-22 10:22:29', '', '2017-01-26 14:53:10'),
(5, 2, 'Contractors All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:24:52', '', '2017-01-21 07:06:24'),
(6, 2, 'Contractors All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:25:12', '', '2017-01-21 07:06:41'),
(7, 2, 'Contractors All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:28:19', '', '2017-01-21 07:06:46'),
(8, 2, 'Contractors All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:28:54', '', '2017-01-21 07:06:50'),
(9, 2, 'Contractors All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:29:42', '', '2017-01-21 07:06:54'),
(10, 2, 'Erection All Risks (Fire)', 'G', 'A', '1', '2017-01-22 10:29:59', '', '2017-01-21 07:06:58'),
(11, 2, 'Erection All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:32:03', '', '2017-01-21 07:07:04'),
(12, 2, 'Erection All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:34:57', '', '2017-01-21 07:07:08'),
(13, 2, 'Erection All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:39:12', '', '2017-01-21 07:09:12'),
(14, 2, 'Erection All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:40:40', '', '2017-01-21 07:10:40'),
(15, 2, 'Erection All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:40:55', '', '2017-01-21 07:10:55'),
(16, 2, 'Contractors Plant and Machinery (Theft)', 'G', 'A', '1', '2017-01-22 10:41:30', '', '2017-01-21 07:11:30'),
(17, 2, 'Contractors Plant and Machinery (Fire)', 'G', 'A', '1', '2017-01-22 10:42:02', '', '2017-01-21 07:12:02'),
(18, 2, 'Contractors Plant and Machinery (Other)', 'G', 'A', '1', '2017-01-22 10:42:35', '', '2017-01-21 07:12:35'),
(19, 3, 'Domestic / Household (Fire)', 'G', 'A', '1', '2017-01-22 10:43:07', '', '2017-01-21 07:13:07'),
(20, 3, 'Domestic / Household (Theft)', 'G', 'A', '1', '2017-01-22 10:43:52', '', '2017-01-21 07:13:52'),
(21, 3, 'Domestic / Household (Water)', 'G', 'A', '1', '2017-01-22 10:44:23', '', '2017-01-21 07:14:23'),
(22, 3, 'Domestic / Household (Other)', 'G', 'A', '1', '2017-01-22 10:44:44', '', '2017-01-21 07:14:44'),
(23, 4, 'Marine Cargo', 'M', 'A', '1', '2017-01-22 10:45:06', '', '2017-01-21 07:15:06'),
(24, 4, 'Marine Full', 'M', 'A', '1', '2017-01-22 10:45:24', '', '2017-01-21 07:15:24'),
(25, 4, 'Goods in Transit', 'M', 'A', '1', '2017-01-22 10:46:26', '', '2017-01-21 07:16:26'),
(26, 5, 'Machinery Breakdown', 'G', 'A', '1', '2017-01-22 10:47:43', '', '2017-01-21 07:17:43'),
(27, 5, 'Machinery Breakdown + LOP', 'G', 'A', '1', '2017-01-22 10:48:03', '', '2017-01-21 07:18:03'),
(28, 5, 'Stock Deterioration', 'G', 'A', '1', '2017-01-22 10:48:43', '', '2017-01-21 07:18:43'),
(29, 5, 'Stock Deterioration + LOP', 'G', 'A', '1', '2017-01-22 10:50:11', '', '2017-01-21 07:20:11'),
(30, 9, 'Fidelity Guarantee', 'G', 'A', '1', '2017-01-22 10:51:48', '', '2017-01-21 07:21:48'),
(31, 9, 'Loss of Money', 'G', 'A', '1', '2017-01-22 10:52:58', '', '2017-01-21 07:22:58'),
(32, 9, 'Business Interuption', 'G', 'A', '1', '2017-01-22 10:53:14', '', '2017-01-21 07:23:14'),
(33, 9, 'Bankers Blanket Bond', 'G', 'A', '1', '2017-01-22 10:53:31', '', '2017-01-21 07:23:31'),
(34, 9, 'Travel', 'G', 'A', '1', '2017-01-22 10:53:45', '', '2017-01-21 07:23:45'),
(35, 6, 'Commercial property (Fire)', 'G', 'A', '1', '2017-01-22 10:54:04', '', '2017-01-21 07:24:04'),
(36, 6, 'Commercial property (Theft)', 'G', 'A', '1', '2017-01-22 10:54:21', '', '2017-01-21 07:24:21'),
(37, 6, 'Commercial property (Water)', 'G', 'A', '1', '2017-01-22 10:54:51', '', '2017-01-21 07:24:51'),
(38, 6, 'Commercial property (Other)', 'G', 'A', '1', '2017-01-22 10:55:51', '', '2017-01-21 07:25:51'),
(39, 6, 'Commercial property + BI', 'G', 'A', '1', '2017-01-22 10:56:20', '', '2017-01-21 07:26:20'),
(40, 7, 'Employers Liability', 'G', 'A', '1', '2017-01-22 10:58:44', '', '2017-01-21 07:28:44'),
(41, 7, 'Third party / Public Liability (Injury)', 'G', 'A', '1', '2017-01-22 10:59:54', '', '2017-01-21 07:29:54'),
(42, 7, 'Third party / Public Liability (Non-Injury)', 'G', 'A', '1', '2017-01-22 11:00:11', '', '2017-01-21 07:30:11'),
(43, 7, 'Professional Identity', 'G', 'A', '1', '2017-01-22 11:00:53', '', '2017-01-21 07:30:53'),
(44, 7, 'Product Liability', 'G', 'A', '1', '2017-01-22 11:01:12', '', '2017-01-21 07:31:12'),
(45, 7, 'Hauliers Liability', 'G', 'A', '1', '2017-01-22 11:02:40', '', '2017-01-21 07:32:40'),
(46, 7, 'Motor Liability', 'G', 'A', '1', '2017-01-22 11:02:59', '', '2017-01-21 07:32:59'),
(47, 7, 'Workman''s Compensation', 'G', 'A', '1', '2017-01-22 11:03:13', '', '2017-01-21 07:33:13'),
(48, 7, 'Motor', 'G', 'A', '1', '2017-01-22 11:05:15', '', '2017-01-21 07:35:15'),
(49, 8, 'Consultancy / Valuation', 'G', 'A', '1', '2017-01-22 11:05:39', '', '2017-01-21 07:35:39'),
(50, 8, 'Recovery', 'G', 'A', '1', '2017-01-22 11:06:02', '', '2017-01-21 07:36:02'),
(51, 8, 'Experts opinion/ Arbitration', 'G', 'A', '1', '2017-01-22 11:06:33', '', '2017-01-21 07:36:33');

-- --------------------------------------------------------

--
-- Table structure for table `timeandexpense`
--

DROP TABLE IF EXISTS `timeandexpense`;
CREATE TABLE `timeandexpense` (
  `reportId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `reportDate` varchar(50) DEFAULT NULL,
  `reportTime` varchar(50) DEFAULT NULL,
  `serviceId` int(11) DEFAULT NULL,
  `ratePerHour` varchar(100) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usermaster`
--

DROP TABLE IF EXISTS `usermaster`;
CREATE TABLE `usermaster` (
  `userId` int(11) NOT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `lastLoggedIn` datetime DEFAULT NULL,
  `isAdmin` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'Y-Yes, N-No',
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `aboutMe` varchar(500) DEFAULT NULL,
  `profilePicture` varchar(100) DEFAULT NULL,
  `userAccess` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usermaster`
--

INSERT INTO `usermaster` (`userId`, `adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `password`, `lastLoggedIn`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `aboutMe`, `profilePicture`, `userAccess`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'graham', 'Graham', 'Whitelaw', 'graham@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Y', 'test', 'Dubai', 'UAE', '', 'test', '1484555604678.jpg', '1,2,3,4,5,6', 'A', '', '2017-01-16 00:00:00', '1', '2017-03-25 15:39:15'),
(2, 2, 'smith', 'Mark', 'Smith', 'mark.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:45:23', '', '2017-03-25 15:39:27'),
(3, 3, 'chris', 'Chris', 'Wylie', 'chris.wylie@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:46:51', '', '2017-03-25 15:38:09'),
(4, 7, 'craig', 'Craig', 'Jones', 'craig.jones@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'CDE street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:47:59', '', '2017-03-25 15:38:20'),
(5, 4, 'koshy', 'Jerin', 'Koshy', 'jerin.koshy@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'QWE Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:49:13', '', '2017-03-25 15:39:23'),
(6, 6, 'sham', 'Sham', 'Jacob', 'sham.jacob@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:50:22', '', '2017-03-25 15:38:35'),
(7, 8, 'steven', 'Steve', 'Smith', 'steve.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ASD Road', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:12', '1', '2017-03-25 15:38:42'),
(8, 5, 'hani', 'Moein', 'Hani', 'moein.hani@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'MNO Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:57', '', '2017-03-25 15:38:56'),
(9, 9, 'shadi', 'Shadi', 'Abu Razouq', 'shadi.razouq@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'QWERTY Street', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:52:39', '', '2017-03-25 15:38:49'),
(10, NULL, 'simbulan', 'Maricel', 'Simbulan', 'maricel.simbulan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:02', '', '2017-01-31 21:38:25'),
(11, NULL, 'jen', 'Jen', 'Mamalayan', 'jen.mamalayan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:40', '', '2017-01-31 21:38:27'),
(12, NULL, 'jackie', 'Jackie', 'Whitelaw', 'jackie.whitelaw@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:55:44', '', '2017-01-31 21:38:29'),
(13, NULL, 'flory', 'Flory', 'Dâ€™Mello', 'flory.dmello@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:56:23', '', '2017-01-31 21:38:32');

-- --------------------------------------------------------

--
-- Table structure for table `visitupdates`
--

DROP TABLE IF EXISTS `visitupdates`;
CREATE TABLE `visitupdates` (
  `visitId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'N-Notes, P-Photo, E- Email',
  `updateContent` varchar(500) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `workingupdates`
--

DROP TABLE IF EXISTS `workingupdates`;
CREATE TABLE `workingupdates` (
  `workingId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL,
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjusters`
--
ALTER TABLE `adjusters`
  ADD PRIMARY KEY (`adjusterId`);

--
-- Indexes for table `brokers`
--
ALTER TABLE `brokers`
  ADD PRIMARY KEY (`brokerId`);

--
-- Indexes for table `categorymaster`
--
ALTER TABLE `categorymaster`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `claimmaster`
--
ALTER TABLE `claimmaster`
  ADD PRIMARY KEY (`claimId`);

--
-- Indexes for table `clientmaster`
--
ALTER TABLE `clientmaster`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `currencydetails`
--
ALTER TABLE `currencydetails`
  ADD PRIMARY KEY (`currencyId`);

--
-- Indexes for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD PRIMARY KEY (`detailsId`);

--
-- Indexes for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  ADD PRIMARY KEY (`invoiceId`);

--
-- Indexes for table `officemaster`
--
ALTER TABLE `officemaster`
  ADD PRIMARY KEY (`officeId`);

--
-- Indexes for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  ADD PRIMARY KEY (`prelimId`);

--
-- Indexes for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  ADD PRIMARY KEY (`receiptId`);

--
-- Indexes for table `servicemaster`
--
ALTER TABLE `servicemaster`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`subId`);

--
-- Indexes for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  ADD PRIMARY KEY (`reportId`);

--
-- Indexes for table `usermaster`
--
ALTER TABLE `usermaster`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `visitupdates`
--
ALTER TABLE `visitupdates`
  ADD PRIMARY KEY (`visitId`);

--
-- Indexes for table `workingupdates`
--
ALTER TABLE `workingupdates`
  ADD PRIMARY KEY (`workingId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjusters`
--
ALTER TABLE `adjusters`
  MODIFY `adjusterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `brokers`
--
ALTER TABLE `brokers`
  MODIFY `brokerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `categorymaster`
--
ALTER TABLE `categorymaster`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `claimmaster`
--
ALTER TABLE `claimmaster`
  MODIFY `claimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `clientmaster`
--
ALTER TABLE `clientmaster`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `currencydetails`
--
ALTER TABLE `currencydetails`
  MODIFY `currencyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  MODIFY `detailsId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  MODIFY `invoiceId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `officemaster`
--
ALTER TABLE `officemaster`
  MODIFY `officeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  MODIFY `prelimId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  MODIFY `receiptId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `servicemaster`
--
ALTER TABLE `servicemaster`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  MODIFY `reportId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usermaster`
--
ALTER TABLE `usermaster`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `visitupdates`
--
ALTER TABLE `visitupdates`
  MODIFY `visitId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `workingupdates`
--
ALTER TABLE `workingupdates`
  MODIFY `workingId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
