<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    }
    // print_r($_POST);exit;
    if((isset($_POST['monthandyear'])) && (!empty($_POST['monthandyear']))){
    $monthandyear       = (empty($_REQUEST['monthandyear']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['monthandyear']));
    $location           = (empty($_REQUEST['location']))        ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['location']));
    $selectclient       = (empty($_REQUEST['selectclient']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['selectclient']));
    $losscategory       = (empty($_REQUEST['losscategory']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['losscategory']));
    $dateselected        = date('m/Y',strtotime($monthandyear));
    $dateselectedyear    = date('Y',strtotime($monthandyear));

        if($location != ""){
            //Get specific location
            $officewhere = " where officeId = '$location'and active='A'";
        } else {
            $officewhere = " where  active='A'";
        }
        
        //Get offices
        $officeidarray = array();
        $summarytotal1 = array();
        $summarytotal2 = array();
        $summarytotal3 = array();
        $summarytotal4 = array();
        $summarytotal5 = array();
        $summarytotal6 = array();
        $summarytotal7 = array();
        $summarytotal8 = array();
        $summarytotal9 = array();
        $officeprefixarray = array();
        $get_locations = "select `officeId`, `location`, `name`, `prefix` from `officemaster` $officewhere";
        // echo $get_locations;exit;
        $stmt       = mysqli_query($connection, $get_locations); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $officeId   = (empty($row['officeId']))    ? '' : $row['officeId'];
            array_push($officeidarray, $officeId);
            $location   = (empty($row['location']))    ? '' : $row['location'];
            $name       = (empty($row['name']))        ? '' : $row['name'];
            $prefix     = (empty($row['prefix']))      ? '' : $row['prefix'];
            array_push($officeprefixarray, $prefix);

          }
      }
      // print_r($officeprefixarray);
        if($selectclient != ""){
            //Get specific location
            $clientwhere = " where clientId = '$selectclient'";
        } else {
            $clientwhere = "";
        }
        $adjusteridarray = array();
        $adjusternamearray = array();
        $get_adjusters = "select `adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `address`, `city`, `postalCode`, `country` from `adjusters`";
        $stmt       = mysqli_query($connection, $get_adjusters); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $adjusterId     = $row['adjusterId']; 
            $userName   = (empty($row['userName']))     ? '' : $row['userName'];
            $firstName    = (empty($row['firstName']))      ? '' : $row['firstName'];
            $lastName     = (empty($row['lastName']))       ? '' : $row['lastName'];
            // $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            $adjustershortname = $firstName[0].$lastName[0];
            array_push($adjusteridarray, $adjusterId);
            array_push($adjusternamearray, $adjustershortname);
          }
      }
            $newofficematcharray        = array();
            $newadjustermatcharray    = array();
            $allofficematcharray      = array();
            $alladjustermatcharray    = array();
            $newclaimarray = array();
            $newclaimarrayyear = array();
            $allclaimarray = array();
            $projectedfeeopenarray = array();
            $projectedfeemontharray = array();
            $debtors = array();
            $clientarray = array();
            $claimwhere = "";
      if($selectclient != ""){
        $claimwhere = " where clientId = '$selectclient'";
      }
      if($losscategory != ""){
            $claimwhere = " where categoryId = '$losscategory'";
        }
        if(($losscategory != "")&&($selectclient != "")){
            $claimwhere = "where categoryId = '$losscategory' and clientId = '$selectclient'";
        }
      //Get all claims
      $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `createdDate` from `claimmaster` $claimwhere";
      // echo $get_details;exit;
            $detailstmt       = mysqli_query($connection, $get_details); 
            $getclaimcount   = mysqli_num_rows($detailstmt);
            if($getclaimcount > 0){
                
              while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                $claimId            = $row['claimId']; 
                $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
                $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
                $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
                $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
                $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
                $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
                $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
                $subId              = (empty($row['subId']))            ? '' : $row['subId'];
                $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
                $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
                $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
                $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
                $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
                $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
                $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
                $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
                $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
                $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
                $createdDate        = (empty($row['createdDate']))      ? '' : $row['createdDate'];
                $formattedDate      = date('m/Y',strtotime($createdDate));
                $formattedDateYear  = date('Y', strtotime($createdDate));
                if($formattedDate == $dateselected){
                    // if(($officeId == $location)&&($adjusterId == $adjusterid)){
                    // echo "1";
                    if(!in_array($clientId, $clientarray)){
                        array_push($clientarray, $clientId);
                        if(isset($debtors[$officeId][$adjusterId])){    
                            $debtors[$officeId][$adjusterId] = $debtors[$officeId][$adjusterId] + 1;
                        } else {
                            $debtors[$officeId][$adjusterId] = 1;
                        }
                    }
                    // }
                    if(isset($newclaimarray[$officeId][$adjusterId])){
                        $newclaimarray[$officeId][$adjusterId] = $newclaimarray[$officeId][$adjusterId] + 1;
                    } else {
                        $newclaimarray[$officeId][$adjusterId] = 1;
                    }
                    $newofficematcharray[$officeId] = $claimId;
                    $newadjustermatcharray[$adjusterId] = $claimId;
                }
                if($formattedDateYear == $dateselectedyear){
                    // if(($officeId == $location)&&($adjusterId == $adjusterid)){

                    // }
                    if(isset($newclaimarrayyear[$officeId][$adjusterId])){
                        $newclaimarrayyear[$officeId][$adjusterId] = $newclaimarrayyear[$officeId][$adjusterId] + 1;
                    } else {
                        $newclaimarrayyear[$officeId][$adjusterId] = 1;
                    }
                    $newofficematcharray[$officeId] = $claimId;
                    $newadjustermatcharray[$adjusterId] = $claimId;
                }
                    if($jobStatus == "O"){
                        if(isset($allclaimarray[$officeId][$adjusterId])){
                            $allclaimarray[$officeId][$adjusterId] = $allclaimarray[$officeId][$adjusterId] + 1;
                        } else {
                            $allclaimarray[$officeId][$adjusterId] = 1;
                        }
                        $allofficematcharray[$officeId] = $claimId;
                        $alladjustermatcharray[$adjusterId] = $claimId;
                    }
                    $get_details3 = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `prelimupdates`  where claimId = '$claimId' and updateType = 'F'";
                    $detailstmt3       = mysqli_query($connection, $get_details3); 
                    $getcount3   = mysqli_num_rows($detailstmt3);
                    if($getcount3 > 0){
                        
                      while($row3 = mysqli_fetch_array($detailstmt3, MYSQLI_ASSOC)){
                        $prelimId            = $row3['prelimId']; 
                        $updateType         = (empty($row3['updateType']))        ? '' : $row3['updateType'];
                        $updateContent      = (empty($row3['updateContent']))     ? '' : $row3['updateContent'];
                        $originalFilename   = (empty($row3['originalFilename']))  ? 0 : $row3['originalFilename'];
                        $createdDate        = (empty($row3['createdDate']))       ? '' : date('d M, Y', strtotime($row3['createdDate']));         
                        if($formattedDate == $dateselected){
                                if(isset($projectedfeemontharray[$officeId][$adjusterId])){
                                    $projectedfeemontharray[$officeId][$adjusterId] = $projectedfeemontharray[$officeId][$adjusterId] + $updateContent;
                                } else {
                                    $projectedfeemontharray[$officeId][$adjusterId] = $updateContent;
                                }
                            }
                            if($jobStatus == 'O'){
                                if(isset($projectedfeeopenarray[$officeId][$adjusterId])){
                                    $projectedfeeopenarray[$officeId][$adjusterId] = $projectedfeeopenarray[$officeId][$adjusterId] + $updateContent;
                                } else {
                                    $projectedfeeopenarray[$officeId][$adjusterId] = $updateContent;
                                }
                            }
                    }
                }
                $amountreceivedarray = array();
                $totalamountarray = array();
                $totalamountyeararray = array();
                //Get received money details
                $get_received = "select t1.receiptAmount, t2.totalAmount from receiptdetails as t1, invoicemaster as t2 where t1.invoiceId = t2.invoiceId and t2.claimId = '$claimId'";
                $recievedstmt       = mysqli_query($connection, $get_received); 
                $getrcount   = mysqli_num_rows($recievedstmt);
                    if($getrcount > 0){
                        
                      while($row4 = mysqli_fetch_array($recievedstmt, MYSQLI_ASSOC)){
                        $receiptAmount         = (empty($row4['receiptAmount']))        ? '' : $row4['receiptAmount'];
                        $totalAmount           = (empty($row4['totalAmount']))          ? '' : $row4['totalAmount'];
                            if(isset($amountreceivedarray[$officeId][$adjusterId])){
                                    $amountreceivedarray[$officeId][$adjusterId] = $amountreceivedarray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $amountreceivedarray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                if($formattedDate == $dateselected){
                                    if(isset($totalamountarray[$officeId][$adjusterId])){
                                    $totalamountarray[$officeId][$adjusterId] = $totalamountarray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $totalamountarray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                }
                                if($formattedDateYear == $dateselectedyear){
                                    if(isset($totalamountyeararray[$officeId][$adjusterId])){
                                    $totalamountyeararray[$officeId][$adjusterId] = $totalamountyeararray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $totalamountyeararray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                }
                      }
                  }
                  // print_r($newclaimarray);exit;
        }
    }
    } else {
        header("Location:closed.php");
     }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="closed.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" id="printablecontent">
                            <div class="header hideperint">
                                <h4 class="title"></h4>
                                <p class="category">Report for <?php echo $monthandyear; ?></p>
                            </div>
                            <div class="col-xs-12 printheader paddingrl0" align="center">
                                    <!-- <img src="assets/img/header.png"> -->
                                    <div style="height:60px;"></div>
                                   <h3>Report for <?php echo $monthandyear; ?></h3>
                                </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="exportcontents">
                                    <thead align="center">
                                        <td align="center">Adjuster</td>
                                    	<td align="center">New claims received for month</td>
                                    	<td align="center">New claims for year to date</td>
                                    	<td align="center">Open claims</td>
                                    	<td align="center">Fees for the month</td>
                                    	<td align="center">Fees for the year to date</td>
                                    	<td align="center">Projected fees -Open files</td>
                                    	<td align="center">Month projected fees</td>
                                    	<td align="center">Debtors</td>
                                    	<td align="center">Amount received</td>
                                    </thead>
                                    <?php 
                                         foreach ($officeprefixarray as $thisofficeid => $officeprefix) {
                                    ?>
                                    <tr>
                                        <td align="center"></td>
                                    	<td align="center"><?php echo $officeprefix;?></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    </tr>
									<tbody>
                                    <?php 
                                    
                                    foreach ($adjusternamearray as $thisadjusterid => $adjustername) {
                                        $thisadjusterid1 = $adjusteridarray[$thisadjusterid];
                                        $thisofficeid1  = $officeidarray[$thisofficeid];
                                     ?>
                                        <tr>
                                        	<td><?php echo $adjustername;?></td>
                                        	<td align="center"><?php if(isset($newclaimarray[$thisofficeid1][$thisadjusterid1])){
                                                echo $newclaimarray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal1[$thisadjusterid1])){
                                                    $summarytotal1[$thisadjusterid1] = $summarytotal1[$thisadjusterid1] + $newclaimarray[$thisofficeid1][$thisadjusterid1];
                                                } else{
                                                    $summarytotal1[$thisadjusterid1] = 1;
                                                }
                                                // print_r($summarytotal1);
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($newclaimarrayyear[$thisofficeid1][$thisadjusterid1])){
                                                echo $newclaimarrayyear[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal2[$thisadjusterid1])){
                                                    $summarytotal2[$thisadjusterid1] = $summarytotal2[$thisadjusterid1] + $newclaimarrayyear[$thisofficeid1][$thisadjusterid1];
                                                } else{
                                                    $summarytotal2[$thisadjusterid1] = $newclaimarrayyear[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($allclaimarray[$thisofficeid1][$thisadjusterid1])){
                                                echo $allclaimarray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal3[$thisadjusterid1])){
                                                    $summarytotal3[$thisadjusterid1] = $summarytotal3[$thisadjusterid1] + $allclaimarray[$thisofficeid1][$thisadjusterid1];
                                                } else{
                                                    $summarytotal3[$thisadjusterid1] = $allclaimarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($totalamountarray[$thisofficeid1][$thisadjusterid1])){
                                                echo $totalamountarray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal5[$thisadjusterid1])){
                                                    $summarytotal8[$thisadjusterid1] = $summarytotal8[$thisadjusterid1] + $totalamountarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                 else {
                                                    $summarytotal8[$thisadjusterid1] = $totalamountarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($totalamountyeararray[$thisofficeid1][$thisadjusterid1])){
                                                echo $totalamountyeararray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal9[$thisadjusterid1])){
                                                    $summarytotal9[$thisadjusterid1] = $summarytotal9[$thisadjusterid1] + $totalamountyeararray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                 else {
                                                    $summarytotal9[$thisadjusterid1] = $totalamountyeararray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                            <td align="center"><?php if(isset($projectedfeeopenarray[$thisofficeid1][$thisadjusterid1])){
                                                echo $projectedfeeopenarray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal5[$thisadjusterid1])){
                                                    $summarytotal5[$thisadjusterid1] = $summarytotal5[$thisadjusterid1] + $projectedfeeopenarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                 else {
                                                    $summarytotal5[$thisadjusterid1] = $projectedfeeopenarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($projectedfeemontharray[$thisofficeid1][$thisadjusterid1])){
                                                echo $projectedfeemontharray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal4[$thisadjusterid1])){
                                                    $summarytotal4[$thisadjusterid1] = $summarytotal4[$thisadjusterid1] + $projectedfeemontharray[$thisofficeid1][$thisadjusterid1];
                                                } else {
                                                    $summarytotal4[$thisadjusterid1] = $projectedfeemontharray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	
                                        	<td align="center"><?php if(isset($debtors[$thisofficeid1][$thisadjusterid1])){
                                                echo $debtors[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal7[$thisadjusterid1])){
                                                    $summarytotal7[$thisadjusterid1] = $summarytotal7[$thisadjusterid1] + $debtors[$thisofficeid1][$thisadjusterid1];
                                                } else {
                                                    $summarytotal7[$thisadjusterid1] = $debtors[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	<td align="center"><?php if(isset($amountreceivedarray[$thisofficeid1][$thisadjusterid1])){
                                                echo $amountreceivedarray[$thisofficeid1][$thisadjusterid1];
                                                if(isset($summarytotal6[$thisadjusterid1])){
                                                    $summarytotal6[$thisadjusterid1] = $summarytotal6[$thisadjusterid1] + $amountreceivedarray[$thisofficeid1][$thisadjusterid1];
                                                } else {
                                                    $summarytotal6[$thisadjusterid1] = $amountreceivedarray[$thisofficeid1][$thisadjusterid1];
                                                }
                                                } else {echo "0";}?></td>
                                        	
                                        </tr>
                                        <?php } ?>
										<tr>
											<td align="center">TOTAL</td>
											<td align="center"><?php if(array_sum($newclaimarray[$thisofficeid1]) != "") { echo array_sum($newclaimarray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($newclaimarrayyear[$thisofficeid1]) != "") { echo array_sum($newclaimarrayyear[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($allclaimarray[$thisofficeid1]) != "") { echo array_sum($allclaimarray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($totalamountarray[$thisofficeid1]) != "") { echo array_sum($totalamountarray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($totalamountyeararray[$thisofficeid1]) != "") { echo array_sum($totalamountyeararray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($projectedfeeopenarray[$thisofficeid1]) != "") { echo array_sum($projectedfeeopenarray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($projectedfeemontharray[$thisofficeid1]) != "") { echo array_sum($projectedfeemontharray[$thisofficeid1]);} else { echo "0";} ?></td>
											<td align="center"><?php if(array_sum($debtors[$thisofficeid1]) != "") { echo array_sum($debtors[$thisofficeid1]);} else {echo "0";} ?></td>
											<td align="center"><?php if(array_sum($amountreceivedarray[$thisofficeid1]) != "") { echo array_sum($amountreceivedarray[$thisofficeid1]);} else { echo "0";} ?></td>
										</tr>
									
										<tr><td colspan="10"></td></tr>
                                        <?php  } ?>
                                        
                                        
										<thead align="center">
											<td align="center"></td>
											<td align="center">SUMMARY TOTAL</td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
											<td align="center"></td>
										</thead>
                                    <?php 
                                        $totalsummary1 = 0;
                                        $totalsummary2 = 0;
                                        $totalsummary3 = 0;
                                        $totalsummary4 = 0;
                                        $totalsummary5 = 0;
                                        $totalsummary6 = 0;
                                        $totalsummary7 = 0;
                                        $totalsummary8 = 0;
                                        $totalsummary9 = 0;
                                    foreach ($adjusternamearray as $thisadjusterid => $adjustername) {
                                     ?>
                                        <tr>
                                            <td><?php echo $adjustername;?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal1[$thisadjusterid1])){ echo $summarytotal1[$thisadjusterid1]; $totalsummary1 = $totalsummary1 + $summarytotal1[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal2[$thisadjusterid1])){ echo $summarytotal2[$thisadjusterid1];  $totalsummary2 = $totalsummary2 + $summarytotal2[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal3[$thisadjusterid1])){ echo $summarytotal3[$thisadjusterid1];  $totalsummary3 = $totalsummary3 + $summarytotal3[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal8[$thisadjusterid1])){ echo $summarytotal8[$thisadjusterid1];  $totalsummary8 = $totalsummary8 + $summarytotal8[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal9[$thisadjusterid1])){ echo $summarytotal9[$thisadjusterid1];  $totalsummary9 = $totalsummary9 + $summarytotal9[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                           <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal4[$thisadjusterid1])){ echo $summarytotal4[$thisadjusterid1];  $totalsummary4 = $totalsummary4 + $summarytotal4[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal5[$thisadjusterid1])){ echo $summarytotal5[$thisadjusterid1];  $totalsummary5 = $totalsummary5 + $summarytotal5[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal7[$thisadjusterid1])){ echo $summarytotal7[$thisadjusterid1];  $totalsummary7 = $totalsummary7 + $summarytotal7[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            <td align="center"><?php $thisadjusterid1 = $adjusteridarray[$thisadjusterid]; if(isset($summarytotal6[$thisadjusterid1])){ echo $summarytotal6[$thisadjusterid1];  $totalsummary6 = $totalsummary6 + $summarytotal6[$thisadjusterid1];} else {
                                                echo 0;}?></td>
                                            
                                        </tr>
                                        <?php } ?>
										<tr>
											<td align="center">TOTAL</td>
											<td align="center"><?php echo $totalsummary1; ?></td>
											<td align="center"><?php echo $totalsummary2; ?></td>
											<td align="center"><?php echo $totalsummary3; ?></td>
											<td align="center"><?php echo $totalsummary8; ?></td>
											<td align="center"><?php echo $totalsummary9; ?></td>
											<td align="center"><?php echo $totalsummary4; ?></td>
											<td align="center"><?php echo $totalsummary5; ?></td>
											<td align="center"><?php echo $totalsummary7; ?></td>
											<td align="center"><?php echo $totalsummary6; ?></td>
										</tr>
										
										<tr><td colspan="10"></td></tr>
									<tr>
                                        <td align="center"></td>
                                    	<td align="center">TRANSLATION</td>
                                    	<td align="center"></td>
                                    	<td align="center" colspan="2">Fees for the month</td>
                                    	<td align="center" colspan="2">Fees for the year to date</td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
                                    	<td align="center"></td>
										
                                    </tr>
                                    
                                        <tr>
											<td></td>
											<td align="center">TOTAL</td>											
											<td></td>
                                        	<td align="center" colspan="2"><?php echo $value = array_sum($summarytotal8);?></td>
                                        	<td align="center" colspan="2"><?php echo $value = array_sum($summarytotal9);?></td>
											<td></td>
											<td></td>
											<td></td>
                                        </tr>
                                </table>
                                <table style="margin-left:49%;width: 50%;">
                                    <tr>
                                            
                                            <td>
                                                <button class="btn btn-info btn-fill pull-right" id="printbutton">PRINT</button>
                                            </td>
                                            <td>
                                                <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'csv', fileName:'monthlyReportCsv'});">Export as CSV</button>
                                            </td>
                                            <td>
                                                <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'excel', fileName:'monthlyReportExcel'});"> Export as Excel</button>
                                            </td>
                                        </tr>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
 <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
 <script type="text/javascript" src="assets/js/FileSaver.min.js"></script>
 <script type="text/javascript" src="assets/js/xlsx.core.min.js"></script>
 <script type="text/javascript" src="assets/js/tableExport.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#reports').addClass("active");
            $('#printbutton').click(function(){
                $('#printablecontent').print();
            });
        });
    </script>
   

</html>
