<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
}
//Add Client
  if((isset($_POST['currencyname'])) && (!empty($_POST['currencyname']))){
     // print_r($_FILES);exit;
    $currencyname           = (empty($_REQUEST['currencyname']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencyname'])); 
    $currencyshortname      = (empty($_REQUEST['currencyshortname']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencyshortname']));
    $currencycode           = (empty($_REQUEST['currencycode']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencycode']));
    $payableto              = (empty($_REQUEST['payableto']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['payableto']));
    $ibannumber             = (empty($_REQUEST['ibannumber']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['ibannumber']));
    $swiftcode              = (empty($_REQUEST['swiftcode']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['swiftcode']));
    $insert_details = "insert into `currencydetails`(`currencyCode`, `currencyName`, `currencyShort`, `payableTo`, `ibanNumber`, `swiftCode`, `active`, `createdBy`, `createdDate`) values ('$currencycode', '$currencyname', '$currencyshortname', '$payableto', '$ibannumber', '$swiftcode', '1', '$loggedin_userid', now())";
    mysqli_query($connection, $insert_details);
    header("Location:currencylist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="client_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add Currency</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg: US Dollars" name="currencyname" id="currencyname" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Short Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg $ or Dhs" name="currencyshortname" id="currencyshortname" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Code <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg: AED" name="currencycode" id="currencycode" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Payable <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Payable To" name="payableto" id="payableto" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>IBAN <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="IBAN Number" name="ibannumber" id="ibannumber" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Swift Code <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Swift Code"  name="swiftcode" id="swiftcode" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>

									<button type="submit" class="btn btn-info btn-fill pull-right">ADD CURRENCY</button>  <a href="currency.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#adminpanel').addClass("active");
        });
    </script>
   

</html>
