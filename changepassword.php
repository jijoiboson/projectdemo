<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    $get_details = "select `userId`, `userName`, `password`, `firstName`, `lastName`, `emailId`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `aboutMe`, `profilePicture` FROM `usermaster` WHERE userId = '$loggedin_userid' and active='A'";
    $stmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($stmt);
    if($getcount > 0){
      while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
        $userId                 = (empty($row['userId']))           ? '' : $row['userId'];
        $userName               = (empty($row['userName']))         ? '' : $row['userName'];
        $currentpassword        = (empty($row['password']))         ? '' : $row['password'];
        $firstName              = (empty($row['firstName']))        ? '' : $row['firstName'];
        $lastName               = (empty($row['lastName']))         ? '' : $row['lastName'];
        $emailId                = (empty($row['emailId']))          ? '' : $row['emailId'];
        $address                = (empty($row['address']))          ? '' : $row['address'];
        $city                   = (empty($row['city']))             ? '' : $row['city'];
        $country                = (empty($row['country']))          ? '' : $row['country'];
        $postalCode             = (empty($row['postalCode']))       ? '' : $row['postalCode'];
        $aboutMe                = (empty($row['aboutMe']))          ? '' : $row['aboutMe'];
        $profilePicture         = (empty($row['profilePicture']))   ? 'assets/img/faces/face-3.jpg' : "uploads/profile/".$row['profilePicture'];
      }
    } else {
        header("Location:logout.php");
    }
  }

  if((isset($_POST['confirmpassword'])) && (!empty($_POST['confirmpassword']))){
     // print_r($_REQUEST);exit;

    $oldpassword            = (empty($_REQUEST['oldpassword']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['oldpassword']));
    $oldpassword            = md5($oldpassword);
    $newpassword            = (empty($_REQUEST['newpassword']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['newpassword']));
    $newpassword            = md5($newpassword);
    $confirmpassword        = (empty($_REQUEST['confirmpassword']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['confirmpassword']));
    // echo $currentpassword;exit;

    if($oldpassword != $currentpassword){
        header("Location:changepassword.php?error=true");
    } else {

    $update_details = "update `usermaster` set `password`='$newpassword', `updatedBy`='$loggedin_userid' WHERE userId = '$loggedin_userid'";
    mysqli_query($connection, $update_details);
    ?>
    <script type="text/javascript">
        alert("Password Changed successfully");
        window.location.href = "user.php";
    </script>
    <?php
  }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>
        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="user.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Change Password</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Old Password <span class="mandatorystar">*</span></label>
                                                <input type="password" name="oldpassword" id="oldpassword" class="form-control" placeholder="Old Password" pattern=".{8,20}" title="Password should be between 8 to 20 characters" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="color:red;">
                                            <?php 
                                                if(isset($_REQUEST['error'])){
                                                    echo "You have entered a wrong Password";
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>New Password <span class="mandatorystar">*</span></label>
                                                <input type="password" name="newpassword" id="newpassword" class="form-control" placeholder="New Password" pattern=".{8,20}" pattern=".{8,20}" title="Password should be between 8 to 20 characters" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Confirm Password <span class="mandatorystar">*</span></label>
                                                <input type="password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="Confirm Password" pattern=".{8,20}" pattern=".{8,20}" title="Password should be between 8 to 20 characters" onkeyup="validatePassword()" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="errormessage" style="display: none;">Passwords don't match.</div>
                                        </div>
                                    </div>
                                    <button type="submit" name="changepassword" id="changepassword" class="btn btn-info btn-fill pull-right">Change Password</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                    <img class="avatar border-gray" src="<?php echo $profilePicture;?>" alt="..."/>

                                      <h4 class="title"><?php echo $firstName." ".$lastName;?><br />
                                         <small><?php echo $userName;?></small>
                                      </h4>
                                    </a>
                                </div>
                                <p class="description text-center"></p>
                            </div>
                            <hr>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#userprofile').addClass("active");
        });    
            function validatePassword(){
              var newpassword = document.getElementById("newpassword");
              var confirmpassword = document.getElementById("confirmpassword");
              if(newpassword.value != confirmpassword.value) {
                document.getElementById("errormessage").style.display = 'block';
                return false;
              } else {
                document.getElementById("errormessage").style.display = 'none';
              }
            }
        
    </script>
   

</html>
