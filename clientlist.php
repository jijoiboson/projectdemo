<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

		
	<link href="res/ddmenu-ext.css" rel="stylesheet" type="text/css" />
    <script src="res/ddmenu-ext.js" type="text/javascript"></script>
</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="client_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">All Insurers <a href="client_master.php"><button class="btn btn-info btn-fill pull-right">ADD INSURER</button></a>  <a href="client_login.php"><button class="btn pull-right marginrl10">CLOSE</button></a></p>
                            </div>
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped">
                                    <thead>
                                        <th>#</th>
                                    	<th>Reference ID</th>
                                    	<th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>City</th>
                                    </thead>
                                    <tbody>
                                        <!-- Select all users -->
                                        <?php 
                                            $get_categories = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where active = 'A'";
                                                $stmt       = mysqli_query($connection, $get_categories); 
                                                $getcount   = mysqli_num_rows($stmt);
                                                $count = 0;
                                                if($getcount > 0){
                                                    
                                                  while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                    $count = $count+1;
                                                    $clientId     = $row['clientId']; 
                                                    $referenceId  = (empty($row['referenceId']))   ? '' : $row['referenceId'];
                                                    $clientName   = (empty($row['clientName']))       ? '' : $row['clientName'];
                                                    $phoneNumber  = (empty($row['phoneNumber']))       ? '' : $row['phoneNumber'];
                                                    $emailId      = (empty($row['emailId']))       ? '' : $row['emailId'];
                                                    $city         = (empty($row['city']))       ? '' : $row['city'];
                                        ?>
                                            <tr>
                                                <td><?php echo $count;?></td>
                                                <td><?php echo $referenceId;?></td>
                                                <td><?php echo $clientName;?></td>
                                                <td><?php echo $phoneNumber;?></td>
                                                <td><?php echo $emailId;?></td>
                                                <td><?php echo $city;?></td>
                                                <td><button class="btn btn-info btn-fill pull-right editclients" id="<?php echo $clientId;?>">EDIT</button></td>
                                                <td><button class="btn btn-info btn-fill pull-right deactivateitems" id="<?php echo $clientId;?>">DEACTIVATE</button></td>
                                            </tr>

                                        <?php
                                                  }
                                              }
                                        ?>
                                        			
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".editclients").click(function(){
                var clientId = $(this).attr("id");
                // alert(clientId);
                window.location.href="editclient.php?clientId="+clientId;
            });
            $(".deactivateitems").click(function(){
                var deactivate = confirm("This Insurer will be deactivated. Click OK to continue.");
                if (deactivate == true) {
                    var clientid = $(this).attr("id");
                    window.location.href = "deactivate.php?clientid="+clientid;
                } else {
                    
                }
            });
        });
    </script>
   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>

</html>
