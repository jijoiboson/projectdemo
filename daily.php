<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_adjusterid    = $_SESSION["loggedin_adjusterid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    }
     if((isset($_POST['dateselected'])) && (!empty($_POST['dateselected']))){
     	// print_r($_POST);exit;
     	$location		= (empty($_REQUEST['location']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['location']));
     	$selectclient	= (empty($_REQUEST['selectclient']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['selectclient']));
     	$adjusterid		= (empty($_REQUEST['adjusterid']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['adjusterid']));
     	$dateselected	= (empty($_REQUEST['dateselected']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['dateselected']));
      $dateselected2 = (empty($_REQUEST['dateselected']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['dateselected']));
     	$dateselected        = date('d/m/Y',strtotime($dateselected));
     	$dateselectedmonth   = date('m/Y',strtotime($dateselected2));
     	$dateselectedyear    = date('Y',strtotime($dateselected2));

		if($location != ""){
			//Get specific location
			$officewhere = " where officeId = '$location'and active='A'";
		} else {
			$officewhere = " where  active='A'";
		}
		//Get offices
		$officeidarray = array();
		$officeprefixarray = array();
		$get_locations = "select `officeId`, `location`, `name`, `prefix` from `officemaster` $officewhere ";
		// echo $get_locations;exit;
        $stmt       = mysqli_query($connection, $get_locations); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $officeId   = (empty($row['officeId']))    ? '' : $row['officeId'];
           	array_push($officeidarray, $officeId);
            $location   = (empty($row['location']))    ? '' : $row['location'];
            $name       = (empty($row['name']))        ? '' : $row['name'];
            $prefix     = (empty($row['prefix']))      ? '' : $row['prefix'];
            array_push($officeprefixarray, $prefix);

          }
      }
      // print_r($officeprefixarray);
		if($selectclient != ""){
			//Get specific location
			$clientwhere = " where clientId = '$selectclient'";
		} else {
			$clientwhere = "";
		}
		if($adjusterid != ""){
			//Get specific location
			$adjusterwhere = " where adjusterId = '$adjusterid'";
		} else {
			$adjusterwhere = "";
		}
		$adjusteridarray = array();
		$adjusternamearray = array();
		$get_adjusters = "select `adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `address`, `city`, `postalCode`, `country` from `adjusters` $adjusterwhere";
        $stmt       = mysqli_query($connection, $get_adjusters); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $adjusterId     = $row['adjusterId']; 
            $userName   = (empty($row['userName']))     ? '' : $row['userName'];
            $firstName    = (empty($row['firstName']))      ? '' : $row['firstName'];
            $lastName     = (empty($row['lastName']))       ? '' : $row['lastName'];
            // $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            $adjustershortname = $firstName[0].$lastName[0];
            array_push($adjusteridarray, $adjusterId);
            array_push($adjusternamearray, $adjustershortname);
          }
      }
//Location-Adjuster-Date claim
      //Todays's claims
      // if(($location != '')&&($adjusterid != '')){
      // 	$ladwhere = " where DATE_FORMAT(createdDate, '%d/%m/%Y') = '$dateselected' and officeId = '$location' and adjusterId = '$adjusterid'";
      // } else if (($location == '')&&($adjusterid == '')){
      // 	$ladwhere = " where DATE_FORMAT(createdDate, '%d/%m/%Y') = '$dateselected'";
      // }  else if (($location != '')&&($adjusterid == '')){
      // 	$ladwhere = " where DATE_FORMAT(createdDate, '%d/%m/%Y') = '$dateselected' and officeId = '$location'";
      // }  else if (($location == '')&&($adjusterid != '')){
      // 	$ladwhere = " where DATE_FORMAT(createdDate, '%d/%m/%Y') = '$dateselected'  and adjusterId = '$adjusterid'";
      // }
      $newofficematcharray 		= array();
      $newadjustermatcharray 	= array();
      $allofficematcharray 		= array();
      $alladjustermatcharray 	= array();
      $prelimamountarray      = array();
      $invoicedadmountarray     = array();
      $receiptamountarray     = array();
      $amountreceivedarray    = array();
      $totalamountarray       = array();
      $totalamountyeararray     = array();
      $totalfeeamountarray    = array();
      $bankedamountarray      = array();
      $depositamountarray     = array();
      $newclaimarray = array();
      $allclaimarray = array();
      if($selectclient != ""){
      	$clientwhere = " where clientId = '$selectclient'";
      } else {
      	$clientwhere = "";
      }
      //Get all claims
      $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `fileStatus`, `frozen`, `createdDate` from `claimmaster` $clientwhere";
      if($loggedin_isadmin != "Y"){
              $get_details .= " and adjusterId = '$loggedin_adjusterid'";
          }
	        $detailstmt       = mysqli_query($connection, $get_details); 
	        $getclaimcount   = mysqli_num_rows($detailstmt);
	        if($getclaimcount > 0){
	            
	          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
	            $claimId            = $row['claimId']; 
	            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
	            $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
	            $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
	            $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
	            $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
	            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
	            $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
	            $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
	            $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
	            $subId              = (empty($row['subId']))            ? '' : $row['subId'];
	            $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
	            $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
	            $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
	            $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
	            $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
	            $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
	            $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
	            $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
	            $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
	            $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
              $fileStatus          = (empty($row['fileStatus']))        ? '' : $row['fileStatus'];
	            $createdDate 		= (empty($row['createdDate'])) 		? '' : $row['createdDate'];
	            $formattedDate 		= date('d/m/Y',strtotime($createdDate));
	            $formattedDateMonth = date('m/Y',strtotime($createdDate));
                $formattedDateYear  = date('Y', strtotime($createdDate));
	            if($formattedDate == $dateselected){
	            	// if(($officeId == $location)&&($adjusterId == $adjusterid)){

	            	// }
	            	if(isset($newclaimarray[$officeId][$adjusterId])){
	            		$newclaimarray[$officeId][$adjusterId] = $newclaimarray[$officeId][$adjusterId] + 1;
	            	} else {
	            		$newclaimarray[$officeId][$adjusterId] = 1;
	            	}
	            	$newofficematcharray[$officeId] = $claimId;
	            	$newadjustermatcharray[$adjusterId] = $claimId;
	            }
	            	if($jobStatus == "O"){
		            	if(isset($allclaimarray[$officeId][$adjusterId])){
		            		$allclaimarray[$officeId][$adjusterId] = $allclaimarray[$officeId][$adjusterId] + 1;
		            	} else {
		            		$allclaimarray[$officeId][$adjusterId] = 1;
		            	}
		            	$allofficematcharray[$officeId] = $claimId;
		        		$alladjustermatcharray[$adjusterId] = $claimId;
	            	}
	            	$get_details3 = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `prelimupdates`  where claimId = '$claimId' and updateType = 'F'";
                // echo $get_details;exit;
                    $detailstmt3       = mysqli_query($connection, $get_details3); 
                    $getcount3   = mysqli_num_rows($detailstmt3);
                    if($getcount3 > 0){
                        
                      while($row3 = mysqli_fetch_array($detailstmt3, MYSQLI_ASSOC)){
                        $prelimId            = $row3['prelimId']; 
                        $updateType         = (empty($row3['updateType']))        ? '' : $row3['updateType'];
                        $updateContent      = (empty($row3['updateContent']))     ? '' : $row3['updateContent'];
                        $originalFilename   = (empty($row3['originalFilename']))  ? 0 : $row3['originalFilename'];
                        $createdDate        = (empty($row3['createdDate']))       ? '' : date('d M, Y', strtotime($row3['createdDate']));         
                        // echo $formattedDate;
                        if($formattedDate == $dateselected){
                        	if(isset($prelimamountarray[$adjusterId])){
                                    $prelimamountarray[$adjusterId] = $prelimamountarray[$adjusterId] + $updateContent;
                                } else {
                                    $prelimamountarray[$adjusterId] = $updateContent;
                                }
                        }
                        if($formattedDateMonth == $dateselectedmonth){
                                if(isset($projectedfeemontharray[$officeId][$adjusterId])){
                                    $projectedfeemontharray[$officeId][$adjusterId] = $projectedfeemontharray[$officeId][$adjusterId] + $updateContent;
                                } else {
                                    $projectedfeemontharray[$officeId][$adjusterId] = $updateContent;
                                }
                            }
                            if($jobStatus == 'O'){
                                if(isset($projectedfeeopenarray[$officeId][$adjusterId])){
                                    $projectedfeeopenarray[$officeId][$adjusterId] = $projectedfeeopenarray[$officeId][$adjusterId] + $updateContent;
                                } else {
                                    $projectedfeeopenarray[$officeId][$adjusterId] = $updateContent;
                                }
                            }
                    }
                }

                //Get received money details
                $get_received = "select t1.receiptAmount, t1.paymentMode, t2.totalAmount from receiptdetails as t1, invoicemaster as t2 where t1.invoiceId = t2.invoiceId and t2.claimId = '$claimId'";
                // echo $get_received;exit;
                // echo "1";
                $recievedstmt       = mysqli_query($connection, $get_received); 
                $getrcount   = mysqli_num_rows($recievedstmt);
                    if($getrcount > 0){
                        
                      while($row4 = mysqli_fetch_array($recievedstmt, MYSQLI_ASSOC)){
                        $receiptAmount         = (empty($row4['receiptAmount']))        ? '' : $row4['receiptAmount'];
                        $totalAmount           = (empty($row4['totalAmount']))          ? '' : $row4['totalAmount'];
                        $paymentMode           = (empty($row4['paymentMode']))          ? '' : $row4['paymentMode'];
                            if(isset($amountreceivedarray[$officeId][$adjusterId])){
                                    $amountreceivedarray[$officeId][$adjusterId] = $amountreceivedarray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $amountreceivedarray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                if($formattedDate == $dateselected){
                                  // echo $totalAmount;
		                        	if(isset($invoicedadmountarray[$adjusterId])){
		                                    $invoicedadmountarray[$adjusterId] = $invoicedadmountarray[$adjusterId] + $totalAmount;
		                                } else {
		                                    $invoicedadmountarray[$adjusterId] = $totalAmount;
		                                }

		                             if(isset($receiptamountarray[$adjusterId])){
		                                    $receiptamountarray[$adjusterId] = $receiptamountarray[$adjusterId] + $receiptAmount;
		                                } else {
		                                    $receiptamountarray[$adjusterId] = $receiptAmount;
		                                }

                                    if(isset($totalfeeamountarray[$officeId][$adjusterId])){
                                    $totalfeeamountarray[$officeId][$adjusterId] = $totalfeeamountarray[$officeId][$adjusterId] + $totalAmount;
                                } else {
                                    $totalfeeamountarray[$officeId][$adjusterId] = $totalAmount;
                                }
                                if($paymentMode == "Cash"){
                                	if(isset($bankedamountarray[$officeId][$adjusterId])){
                                    $bankedamountarray[$officeId][$adjusterId] = $bankedamountarray[$officeId][$adjusterId] + $receiptAmount;
	                                } else {
	                                    $bankedamountarray[$officeId][$adjusterId] = $receiptAmount;
	                                }
                                }
                                if($paymentMode == "Cheque"){
                                	if(isset($depositamountarray[$officeId][$adjusterId])){
                                    $depositamountarray[$officeId][$adjusterId] = $depositamountarray[$officeId][$adjusterId] + $receiptAmount;
	                                } else {
	                                    $depositamountarray[$officeId][$adjusterId] = $receiptAmount;
	                                }
                                }
                                }
                                // echo $formattedDateMonth;
                                if($formattedDateMonth == $dateselectedmonth){
                                    if(isset($totalamountarray[$officeId][$adjusterId])){
                                    $totalamountarray[$officeId][$adjusterId] = $totalamountarray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $totalamountarray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                }
                                if($formattedDateYear == $dateselectedyear){
                                    if(isset($totalamountyeararray[$officeId][$adjusterId])){
                                    $totalamountyeararray[$officeId][$adjusterId] = $totalamountyeararray[$officeId][$adjusterId] + $receiptAmount;
                                } else {
                                    $totalamountyeararray[$officeId][$adjusterId] = $receiptAmount;
                                }
                                }
                      }
                  }
	    }
	}
	    // print_r($invoicedadmountarray); echo "</pre>";exit;
     } else {
     	header("Location:closed.php");
     }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="closed.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" id="printablecontent">
                            <div class="header hideprint">
                                <h4 class="title"></h4>
                                <p class="category">Report of the day, <?php echo $dateselected;?></p>
                            </div>
                            <div class="col-xs-12 printheader paddingrl0" align="center">
                                  <!-- <img src="assets/img/header.png"> -->
                                    <div style="height:60px;"></div>
                                   <h3>Report of the day, <?php echo $dateselected;?></h3>
                                </div>
							<br>
                            <div class="content table-responsive table-full-width">
                            <div id="printablecontent">
                            <table class="table table-hover table-striped" id="exportcontents">
								<tr>
									<td></td>
									<td align="center" colspan="4">New Claims</td>
									<td></td>
									<td align="center" colspan="4">Open Claims</td>
								</tr>
								<tr>
									<td align="center">Adjusters</td>
									<?php foreach ($officeprefixarray as $officeprefix) {
										echo "<td align='center'>$officeprefix</td>";
									} ?>
									<td align="center">TOTAL</td>
									<td align="center">Adjusters</td>
									<?php foreach ($officeprefixarray as $officeprefix) {
										echo "<td align='center'>$officeprefix</td>";
									} ?>
									<td align="center">TOTAL</td>
								</tr>
								
								<?php foreach ($adjusternamearray as $thisadjusterid => $adjustername) {
										
									?>
									<tr>
									<td align="center"><?php echo $adjustername;?></td>
									<?php 
										$totalcount = 0;
												// echo "1";exit;
										foreach ($officeidarray as $thisofficeid1) {
											# code...
										
											// $thisofficeid1 = $officeidarray[$thisadjusterid];
											$thisadjusterid1 = $adjusteridarray[$thisadjusterid];
											if(isset($newclaimarray[$thisofficeid1][$thisadjusterid1])){
												$thiscount = $newclaimarray[$thisofficeid1][$thisadjusterid1];
												$totalcount = $totalcount + $thiscount;
												echo "<td align='center'>$thiscount</td>";
											} else {
												echo "<td align='center'>0</td>";
											}
										}
											echo "<td align='center'>$totalcount</td>";
									?>
									<td align="center"><?php echo $adjustername;?></td>
									<?php 
										$totalcount = 0;
												// echo "1";exit;
										foreach ($officeidarray as $thisofficeid1) {
											// $thisofficeid1 = $officeidarray[$thisadjusterid];
											$thisadjusterid1 = $adjusteridarray[$thisadjusterid];
											if(isset($allclaimarray[$thisofficeid1][$thisadjusterid1])){
												$thiscount = $allclaimarray[$thisofficeid1][$thisadjusterid1];
												$totalcount = $totalcount + $thiscount;
												echo "<td align='center'>$thiscount</td>";
											} else {
												echo "<td align='center'>0</td>";
											}
										}
											echo "<td align='center'>$totalcount</td>";
									?>
									</tr>
									<?php
									} ?>
								
							</table><br>
							<table class="table table-hover table-striped">
								<tr>
									<td align="center" colspan="10">Fee for the month</td>
									<!-- <td align="center">TOTAL</td> -->
								</tr>
								<tr>
									<td align="center">Adjusters</td>
									<?php foreach ($officeprefixarray as $officeprefix) {
										echo "<td align='center'>$officeprefix</td>";
										echo "<td align='center'>Interim</td>";
										echo "<td align='center'>Total</td>";
									} ?>
									<td align="center">TOTAL</td>
								</tr>
								<!-- <tr>
									<td align="center">Adjusters</td>
									<td align="center">DXB</td>
									<td align="center">Interim</td>
									<td align="center">Total</td>
									<td align="center">AD</td>
									<td align="center">Interim</td>
									<td align="center">Total</td>
									<td align="center">LON</td>
									<td align="center">Interim</td>
									<td align="center">Total</td>
									<td align="center"></td>
								</tr> -->
								<?php 
								foreach ($adjusternamearray as $thisadjusterid => $adjustername) {
										$rowtotalamount = 0;
									?>
								<tr>
									<td align="center"><?php echo $adjustername;?></td>
									<?php foreach ($officeprefixarray as $thisofficeid => $officeprefix) {
										$thisofficeid1  = $officeidarray[$thisofficeid];
										$thistotalamount = 0;
										echo "<td align='center'>"; 
										if(isset($totalamountarray[$thisofficeid1][$thisadjusterid1])){
											echo $totalamountarray[$thisofficeid1][$thisadjusterid1];
											$thistotalamount = $thistotalamount + $totalamountarray[$thisofficeid1][$thisadjusterid1];
                                        } 
                                        else {
                                        	echo "0";
                                        	} 
                                        echo "</td>";
										echo "<td align='center'>"; 
										if(isset($projectedfeemontharray[$thisofficeid1][$thisadjusterid1])){
											echo $projectedfeemontharray[$thisofficeid1][$thisadjusterid1];
											$thistotalamount = $thistotalamount + $projectedfeemontharray[$thisofficeid1][$thisadjusterid1];
                                        } 
                                        else {
                                        	echo "0";
                                        	} 
                                        echo "</td>";
										echo "<td align='center'>$thistotalamount</td>";
										$rowtotalamount = $rowtotalamount + $thistotalamount;
									} ?>
									<td align="center"><?php echo $rowtotalamount;?></td>
								</tr>
								<?php 
									}
								?>

							</table>
							<table class="table table-hover table-striped">
								<div class="header">
									<h4 class="title"></h4>
									<p class="category">Summary of the report</p>
								</div>
								<tr>
									<td align="center">Summary</td>
									<td align="center">New Claims</td>
									<td align="center">Open Claims</td>
									<td align="center">Fee</td>
								</tr>
									<?php foreach ($officeprefixarray as $thisofficeid => $officeprefix) {
										echo "<tr><td align='center'>$officeprefix</td><td align='center'>";
										if(array_sum($newclaimarray[$thisofficeid1]) != "") { echo array_sum($newclaimarray[$thisofficeid1]);} else { echo "0";}
										echo "</td><td align='center'>";
										if(array_sum($allclaimarray[$thisofficeid1]) != "") { echo array_sum($allclaimarray[$thisofficeid1]);} else { echo "0";}
										echo "</td><td align='center'>";
										if(array_sum($totalfeeamountarray[$thisofficeid1]) != "") { echo array_sum($totalfeeamountarray[$thisofficeid1]);} else { echo "0";}
										echo "</td></tr>";
									} ?>
								<tr>
									<td align="center">TOTAL</td>
									<td align="center"><?php if(array_sum($newclaimarray) != "") { echo array_sum($newclaimarray);} else { echo "0";}?></td>
									<td align="center"><?php if(array_sum($allclaimarray) != "") { echo array_sum($allclaimarray);} else { echo "0";}?></td>
									<td align="center"><?php if(array_sum($totalfeeamountarray) != "") { echo array_sum($totalfeeamountarray);} else { echo "0";}?></td>
								</tr>
							</table>
							<table class="table table-hover table-striped">
								<div class="header">
									<h4 class="title"></h4>
									<p class="category">Collection of the report</p>
								</div>
								<tr>
									<td align="center">Collection</td>
									<td align="center">Banked</td>
									<td align="center">To deposit</td>
									<td align="center">Interim 1</td>
									<td align="center">Interim 2</td>
									<td align="center">Interim 3</td>
									<td align="center">TOTAL</td>
								</tr>
								<?php
									$completecollection = 0;
								 foreach ($officeprefixarray as $thisofficeid => $officeprefix) {
									$totalamountcollection = 0;
									$thisofficeid1  = $officeidarray[$thisofficeid];
										echo "<tr><td align='center'>$officeprefix</td><td align='center'>"; 
										if(array_sum($depositamountarray[$thisofficeid1]) != "") { echo array_sum($depositamountarray[$thisofficeid1]); $totalamountcollection += array_sum($depositamountarray[$thisofficeid1]);} else { echo "0";}
										echo"</td><td align='center'>";
										if(array_sum($bankedamountarray[$thisofficeid1]) != "") { echo array_sum($bankedamountarray[$thisofficeid1]);  $totalamountcollection += array_sum($bankedamountarray[$thisofficeid1]);} else { echo "0";}
										echo "</td><td align='center'>0</td><td align='center'>0</td><td align='center'>0</td><td align='center'>$totalamountcollection</td></tr>";
										$completecollection += $totalamountcollection;
									} 

									?>

								<tr>
									<td align="center" colspan="6">TOTAL</td>
									<td align="center"><?php echo $completecollection;?></td>
								</tr>
							</table>
							<table class="table table-hover table-striped">
								<div class="header">
									<h4 class="title"></h4>
									<p class="category">Outstanding prelims</p>
								</div>
								<tr>
									<td></td>
									<td align="center">Preliminary Amount</td>
									<td align="center">Invoiced Amount</td>
									<td align="center">Receipt Amount</td>
								</tr>
								<?php foreach ($adjusternamearray as $thisadjusterid => $adjustername) {
									$thisadjusterid1 = $adjusteridarray[$thisadjusterid];
										echo "<tr><td>$adjustername</td><td align='center'>";
										if(isset($prelimamountarray[$thisadjusterid1])){
											echo $prelimamountarray[$thisadjusterid1];
										} else {
											echo "0";
										}
										
										echo "</td><td align='center'>";
										if(isset($invoicedadmountarray[$thisadjusterid1])){
											echo $invoicedadmountarray[$thisadjusterid1];
										} else {
											echo "0";
										}
										echo "</td><td align='center'>";
										if(isset($receiptamountarray[$thisadjusterid1])){
											echo $receiptamountarray[$thisadjusterid1];
										} else {
											echo "0";
										}
										echo "</td></tr>";
										}
									?>
								<tr>
									<td>TOTAL</td>
									<td align="center"><?php echo array_sum($prelimamountarray);?></td>
									<td align="center"><?php echo array_sum($invoicedadmountarray);?></td>
									<td align="center"><?php echo array_sum($receiptamountarray);?></td>
								</tr>
							</table>
							</div>
							<table style="margin-left:49%;width: 50%;">
                  <tr>
                          
                          <td>
                              <button class="btn btn-info btn-fill pull-right" id="printbutton">PRINT</button>
                          </td>
                          <td>
                              <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'csv', fileName:'dailyReportCsv'});">Export as CSV</button>
                          </td>
                          <td>
                              <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'excel', fileName:'dailyReportExcel'});"> Export as Excel</button>
                          </td>
                      </tr>
              </table>
                                                    	
                            </div>
                    </div>

				</div>
        

    </div>
</div>

</div>
</div>
</div>
</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
     <script type="text/javascript" src="assets/js/FileSaver.min.js"></script>
 <script type="text/javascript" src="assets/js/xlsx.core.min.js"></script>
 <script type="text/javascript" src="assets/js/tableExport.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
        	$('.sidebaritems').removeClass("active");
            $('#reports').addClass("active");
            $('#printbutton').click(function(){
                $('#printablecontent').print();
            });
        });
    </script>

</html>
