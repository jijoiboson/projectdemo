<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
}
//Add Client
  if((isset($_POST['referenceid'])) && (!empty($_POST['referenceid']))){
     // print_r($_FILES);exit;
    $referenceid         = (empty($_REQUEST['referenceid']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['referenceid']));
    $emailid             = (empty($_REQUEST['emailid']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailid']));
    $phonenum            = (empty($_REQUEST['phonenum']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['phonenum']));
    $companyname         = (empty($_REQUEST['companyname']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['companyname']));
    $homeaddress         = (empty($_REQUEST['homeaddress']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['homeaddress']));
    $clientcity          = (empty($_REQUEST['clientcity']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['clientcity']));
    $clientcountry       = (empty($_REQUEST['clientcountry']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['clientcountry']));
    $poboxnumber       = (empty($_REQUEST['poboxnumber']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['poboxnumber']));
    $zipcode             = (empty($_REQUEST['zipcode'])) ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['zipcode']));
    $insert_details = "insert into `clientmaster`(`referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `poBox`) values ('$referenceid', '$companyname', '$phonenum', '$emailid', '$homeaddress', '$clientcity', '$zipcode', '$clientcountry', 'A', '$loggedin_userid', now(), '$poboxnumber')";
    mysqli_query($connection, $insert_details);
    header("Location:clientlist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="client_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Insurer master</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Reference ID <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Reference ID" name="referenceid" id="referenceid" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" class="form-control" placeholder="Email" name="emailid" id="emailid" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" placeholder="Phone" name="phonenum" id="phonenum" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Company" name="companyname" id="companyname" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Address" name="homeaddress" id="homeaddress" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="City" name="clientcity" id="clientcity" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Country"  name="clientcountry" id="clientcountry" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" class="form-control onlynumbers" placeholder="Postal Code"  name="zipcode" id="zipcode"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>PO Box Number <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" placeholder="PO Box Number" name="poboxnumber" id="poboxnumber" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                    </div>

									<button type="submit" class="btn btn-info btn-fill pull-right">ADD INSURER</button>  <a href="client_login.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>
   

</html>
