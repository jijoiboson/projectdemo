<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}
if((isset($_POST['username'])) && (!empty($_POST['username']))){
     // print_r($_FILES);exit;
    $username      = (empty($_REQUEST['username']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['username']));
    $emailid         = (empty($_REQUEST['emailid']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailid']));
    // $phonenum        = (empty($_REQUEST['phonenum']))     ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['phonenum']));
    $firstname       = (empty($_REQUEST['firstname']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['firstname']));
    $lastname        = (empty($_REQUEST['lastname']))     ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['lastname']));
    $homeaddress     = (empty($_REQUEST['homeaddress']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['homeaddress']));
    $city            = (empty($_REQUEST['city']))         ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['city']));
    $country         = (empty($_REQUEST['country']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['country']));
    $zipcode         = (empty($_REQUEST['zipcode']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['zipcode']));
    $insert_details  = "insert into `adjusters` (`userName`, `firstName`, `lastName`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`) VALUES ('$username', '$firstname', '$lastname', '$emailid', '$homeaddress', '$city', '$zipcode', '$country', 'A', '$loggedin_userid', now())";
    // echo $insert_details;exit;
    mysqli_query($connection, $insert_details);
    $adjusterId = mysqli_insert_id($connection);

    $adjustercount = 0;
    $get_adjuster_count = "select count(*) as adjustercount from `usermaster` where userName = '$username'";
    $stmt       = mysqli_query($connection, $get_adjuster_count); 
    $getcount   = mysqli_num_rows($stmt);
    if($getcount > 0){
     while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
        $adjustercount     = $row['adjustercount']; 
        }
    }
    if($adjustercount <= 0){
         $password           = md5('password');
    $insert_user_details = "insert into `usermaster`(`adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `password`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `active`, `createdBy`, `createdDate`, `userAccess`) values ('$adjusterId', '$username', '$firstname', '$lastname', '$emailid', '$password', 'N', '$homeaddress', '$city', '$country', '$zipcode', 'A', '$loggedin_userid', now(), '1,2,3,4,5,6')";
    mysqli_query($connection, $insert_user_details);
    }



    header("Location:adjusterlist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="adjuster_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Adjuster master</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Company (disabled)</label>
                                                <input type="text" class="form-control" disabled placeholder="Company" value="Whitelaw Chartered Loss Adjusters & Surveyors" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Username <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Username" name="username" id="username" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" class="form-control" placeholder="Email" name="emailid" id="emailid" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="First Name" name="firstname" id="firstname" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Last Name" name="lastname" id="lastname" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Home Address" name="homeaddress" id="homeaddress" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="City" name="city" id="city" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Country" name="country" id="country" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" class="form-control" placeholder="ZIP Code" name="zipcode" id="zipcode"/>
                                            </div>
                                        </div>
                                    </div>

									<button type="submit" class="btn btn-info btn-fill pull-right">ADD ADJUSTER</button> <a href="adjuster_login.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>

</html>
