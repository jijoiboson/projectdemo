<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_REQUEST["id"])) && (!empty($_REQUEST["id"]))){
    $currentclaimid         = $_REQUEST["id"];
    $_SESSION["updatedclaimid"] = $currentclaimid;
    $_SESSION["claimidforreceipt"] = $currentclaimid;
    // $currentclaimid         = "3";

    //Get claim details
    $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `createdDate`, `projectedFee`, `reserveAmount`, `statusReportIssued`, `prelimReportIssued`, `liveProjectedFee` from `claimmaster` where claimId = '$currentclaimid'";
    $detailstmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($detailstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
        $claimId            = $row['claimId']; 
        $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
        $_SESSION["updatedjobnumber"] = $jobNumber;
        $formattedJobnumber = str_replace("/", "", $jobNumber);
        $path               = "uploads/$formattedJobnumber/";
        $path2               = "uploads/$formattedJobnumber/status/";
        if (!file_exists($path2)) {
            mkdir($path2, 0777, true);
        }
        $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
        $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
        $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
        $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
        $claimReference     = (empty($row['claimReference']))   ? '' : $row['claimReference'];
        $brokerReference    = (empty($row['brokerReference']))  ? '' : $row['brokerReference'];
        $contactPerson      = (empty($row['contactPerson']))    ? '' : $row['contactPerson'];
        $contactNumber      = (empty($row['contactNumber']))    ? '' : $row['contactNumber'];
        $insurerContact     = (empty($row['insurerContact']))   ? '' : $row['insurerContact'];
        $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
        $currentclientId    = $clientId;
        $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
        $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
        $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
        $subId              = (empty($row['subId']))            ? '' : $row['subId'];
        $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
        $instructionDate    = (empty($row['instructionDate']))  ? '' : date('d-m-Y',strtotime($row['instructionDate']));
        $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
        $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
        $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
        $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
        $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
        $preliminaryDate    = (empty($row['preliminaryDate']))  ? '' : $row['preliminaryDate'];
        $createdDate        = (empty($row['createdDate']))      ? '' : $row['createdDate'];
        $startDate          = (empty($row['startDate']))        ? date('d-m-Y', strtotime($row['createdDate'])) : date('d-m-Y', strtotime($row['startDate']));
        $startTime          = (empty($row['startTime']))        ? date('h:i:s', strtotime($row['createdDate'])) : date('h:i:s', strtotime($row['startTime']));
        $endDate            = (empty($row['endDate']))          ? '' : date('d/m/Y', strtotime($row['endDate']));
        $endTime            = (empty($row['endTime']))          ? '' : date('h:i:s', strtotime($row['endTime']));
        $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
        $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
        $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
        $projectedFee          = (empty($row['projectedFee']))        ? '' : $row['projectedFee'];
        $reserveAmount          = (empty($row['reserveAmount']))        ? '' : $row['reserveAmount'];
        $prelimReportIssued     = (empty($row['prelimReportIssued']))        ? '' : $row['prelimReportIssued'];
        $statusReportIssued     = (empty($row['statusReportIssued']))        ? '' : $row['statusReportIssued'];
        $liveProjectedFee   = (empty($row['liveProjectedFee'])) ? '0' : $row['liveProjectedFee'];
        $jobStatusText = "";
        if($jobStatus == "O"){
            $jobStatusText = "Open";
        } elseif ($jobStatus == "V") {
            $jobStatusText = "Visit";
        } elseif ($jobStatus == "P") {
            $jobStatusText = "Preliminiary";
        } elseif ($jobStatus == "W") {
            $jobStatusText = "Working";
        } elseif ($jobStatus == "C") {
            $jobStatusText = "Closed";
        } elseif ($jobStatus == "I") {
            $jobStatusText = "Invoiced";
        } elseif ($jobStatus == "R") {
            $jobStatusText = "Receipt";
        } elseif ($jobStatus == "S") {
            $jobStatusText = "Status Report Issued";
        } else {
            $jobStatusText = "";
        }
      }
  }
  //Client name
  $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
        $clientstmt       = mysqli_query($connection, $get_client); 
        $getclientcount   = mysqli_num_rows($clientstmt);
        if($getclientcount > 0){
            
          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
            $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
          }
      }
  //Broker name
    $get_brokers = "select  `brokerId`, `employeeId`, `firstName`, `lastName` from `brokers` where active = 'A'";
    $brokeroptions    = "";
    $brokerstmt       = mysqli_query($connection, $get_brokers); 
    $getbrokercount   = mysqli_num_rows($brokerstmt);
    if($getbrokercount > 0){        
      while($brokerrow = mysqli_fetch_array($brokerstmt, MYSQLI_ASSOC)){
        $thisbrokerid       = (empty($brokerrow['brokerId']))     ? '' : $brokerrow['brokerId'];
        $brokeremployeeId   = (empty($brokerrow['employeeId']))      ? '' : $brokerrow['employeeId'];
        $brokerfirstName    = (empty($brokerrow['firstName']))      ? '' : $brokerrow['firstName'];
        $brokerlastName     = (empty($brokerrow['lastName']))       ? '' : $brokerrow['lastName'];
        if($brokerId == $thisbrokerid){
            $brokeroptions      .= "<option value='$thisbrokerid' selected>$brokerfirstName $brokerlastName</option>";
        } else {
            $brokeroptions      .= "<option value='$thisbrokerid'>$brokerfirstName $brokerlastName</option>";
        }
      }
  }

  //Adjuster name
    $get_adjusters = "select `adjusterId`, `firstName`, `lastName` from `adjusters` where `active` = 'A'";
    $adjusteroptions    = "";
    $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
    $getadjustercount   = mysqli_num_rows($adjusterstmt);
    if($getadjustercount > 0){
        
      while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
        $adjusterfirstName    = (empty($adjusterrow['firstName']))      ? '' : $adjusterrow['firstName'];
        $adjusterlastName     = (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
        $thisadjusterid       = (empty($adjusterrow['adjusterId']))     ? '' : $adjusterrow['adjusterId'];
        if($adjusterId == $thisadjusterid){
            $adjusteroptions      .= "<option value='$thisadjusterid' selected>$adjusterfirstName $adjusterlastName</option>";
        } else {
            $adjusteroptions      .= "<option value='$thisadjusterid'>$adjusterfirstName $adjusterlastName</option>";
        }
      }
     }
     //Nature of loss
    $selectedlossnature = $categoryId."~~".$subId;
    $lossnatureoptions = "";
    $get_subcategories = "select t1.subId, t1.categoryId, t1.name, t1.prefix, t2.category from `subcategories` as t1, categorymaster as t2 where t1.categoryId = t2.categoryId and t1.active = 'A'";
    // echo $get_subcategories;exit;
            $stmt       = mysqli_query($connection, $get_subcategories); 
            $getcount   = mysqli_num_rows($stmt);
            if($getcount > 0){
                
              while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                $thiscategory           = (empty($row['categoryId']))   ? '' : $row['categoryId'];
                $thissubcategory        = (empty($row['subId']))        ? '' : $row['subId'];
                $categoryname           = (empty($row['category']))     ? '' : $row['category'];
                $subcategoryname        = (empty($row['name']))         ? '' : $row['name'];
                $thislossnature         = $thiscategory."~~".$thissubcategory;
                if($selectedlossnature == $thislossnature){
                    $lossnatureoptions      .= "<option value='$thiscategory~~$thissubcategory' selected>$subcategoryname - $categoryname</option>";
                } else {
                    $lossnatureoptions      .= "<option value='$thiscategory~~$thissubcategory'>$subcategoryname - $categoryname</option>";
                }
            }
        }
        $paidAmount = 0;$totalAmount = 0;
        $get_invoices = "select `invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `narrationText`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate` FROM `invoicemaster` where `claimId` = '$currentclaimid'";
        // echo $get_invoices;
        $detailstmt       = mysqli_query($connection, $get_invoices); 
        $getcount   = mysqli_num_rows($detailstmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
            $invoiceId          = $row['invoiceId']; 
            // $claimId            = (empty($row['claimId']))          ? '' : $row['claimId'];
            // $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
            // $invoiceNumber      = (empty($row['invoiceNumber']))    ? '' : $row['invoiceNumber'];
            $currency           = (empty($row['currency']))         ? '' : $row['currency'];
            // $faoName            = (empty($row['faoName']))          ? '' : $row['faoName'];
            // $toName             = (empty($row['toName']))           ? '' : $row['toName'];
            // $yourReference      = (empty($row['yourReference']))    ? '' : $row['yourReference'];
            // $invoiceDate        = (empty($row['invoiceDate']))      ? '' : $row['invoiceDate'];
            // $invoiceDate        = date('d/m/Y',strtotime($invoiceDate));
            // $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
            // $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
            $totalAmount        += (empty($row['totalAmount']))      ? 0 : $row['totalAmount'];
            // $totalInWords       = (empty($row['totalInWords']))     ? '' : $row['totalInWords'];
            // $invoiceTerms       = (empty($row['invoiceTerms']))     ? '' : $row['invoiceTerms'];
            // $narrationText      = (empty($row['narrationText']))   ? '' : $row['narrationText'];
        
          
          $get_receipts = "select `receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom` from `receiptdetails` where invoiceId = '$invoiceId'";
          $receiptstmt       = mysqli_query($connection, $get_receipts); 
          $getreceiptcount   = mysqli_num_rows($receiptstmt);
          if($getreceiptcount > 0){
            while($row = mysqli_fetch_array($receiptstmt, MYSQLI_ASSOC)){
                    $receiptId          = $row['receiptId']; 
                    $receiptAmount      = (empty($row['receiptAmount']))          ? '' : $row['receiptAmount'];
                    $paidAmount  = $paidAmount + $receiptAmount;
                }
            }
            
        }
    }
    $balanceAmount = $totalAmount - $paidAmount;

        $prelimreportarray = array();
        $prelimfeesarray = array();
        $prelimamountarray = array();

        $get_prelimdetails = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`, `prelimDate` FROM `prelimupdates`  where claimId = '$currentclaimid'";
        $prelimdetailstmt       = mysqli_query($connection, $get_prelimdetails); 
        $getprelimcount   = mysqli_num_rows($prelimdetailstmt);
        if($getprelimcount > 0){
            
          while($prelimrow = mysqli_fetch_array($prelimdetailstmt, MYSQLI_ASSOC)){
            $prelimId            = $prelimrow['prelimId']; 
            $updateType         = (empty($prelimrow['updateType']))        ? '' : $prelimrow['updateType'];
            $updateContent      = (empty($prelimrow['updateContent']))     ? '' : $prelimrow['updateContent'];
            $originalFilename   = (empty($prelimrow['originalFilename']))  ? '' : $prelimrow['originalFilename'];
            $prelimDate   = (empty($prelimrow['prelimDate']))  ? '' : $prelimrow['prelimDate'];
            $createdDate        = (empty($prelimrow['createdDate']))       ? '' : date('d M, Y', strtotime($prelimrow['createdDate']));         
            if($updateType == "R"){
                $thisreportarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $prelimDate, "name" => $originalFilename);
                array_push($prelimreportarray, $thisreportarray);
            }
            if($updateType == "F"){
                $thisfeesarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $createdDate);
                array_push($prelimfeesarray, $thisfeesarray);
            }
            if($updateType == "A"){
                $thisfeesarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $createdDate);
                array_push($prelimamountarray, $thisfeesarray);
            }
        }
    }
    //         $statusreportarray = array();
    //         $statusfeearray = array();

    //     $get_statusdetails = "select `statusId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `statusupdates`  where claimId = '$currentclaimid'";
    //     $statusdetailstmt       = mysqli_query($connection, $get_statusdetails); 
    //     $getstatuscount   = mysqli_num_rows($statusdetailstmt);
    //     if($getstatuscount > 0){
            
    //       while($statusrow = mysqli_fetch_array($statusdetailstmt, MYSQLI_ASSOC)){
    //         $statusId            = $statusrow['statusId']; 
    //         $updateType         = (empty($statusrow['updateType']))        ? '' : $statusrow['updateType'];
    //         $updateContent      = (empty($statusrow['updateContent']))     ? '' : $statusrow['updateContent'];
    //         $originalFilename   = (empty($statusrow['originalFilename']))  ? '' : $statusrow['originalFilename'];
    //         $createdDate        = (empty($statusrow['createdDate']))       ? '' : date('d M, Y', strtotime($statusrow['createdDate']));    
    //         $thisreportarray = array();     
    //         if($updateType == "R"){
    //             $thisreportarray = array("Id" => $statusId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
    //             array_push($statusreportarray, $thisreportarray);
    //         }
    //         if($updateType == "F"){
    //             $thisreportarray = array("Id" => $statusId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
    //             array_push($statusfeearray, $thisreportarray);
    //         }
            
    //     }
    // }
        $statusreportarray = array();

        $get_statusdetails = "select `statusId`, `claimId`, `report`, `reserveAmount`, `statusDate`, `originalFilename`, `createdBy`, `createdDate` FROM `statusreports`  where claimId = '$currentclaimid'";
        $statusdetailstmt       = mysqli_query($connection, $get_statusdetails); 
        $getstatuscount   = mysqli_num_rows($statusdetailstmt);
        if($getstatuscount > 0){
            
          while($statusrow = mysqli_fetch_array($statusdetailstmt, MYSQLI_ASSOC)){
            $statusId            = $statusrow['statusId']; 
            $report         = (empty($statusrow['report']))        ? '' : $statusrow['report'];
            $reserveAmount      = (empty($statusrow['reserveAmount']))     ? '' : $statusrow['reserveAmount'];
            $originalFilename      = (empty($statusrow['originalFilename']))     ? '' : $statusrow['originalFilename'];
            $statusDate   = (empty($statusrow['statusDate']))  ? '' : date('d M, Y', strtotime($statusrow['statusDate']));
            // $createdDate        = (empty($statusrow['createdDate']))       ? '' : date('d M, Y', strtotime($statusrow['createdDate']));    
            $thisreportarray = array();     
                $thisreportarray = array("Id" => $statusId, "content" => $report, "amount" => $reserveAmount, "statusDate" => $statusDate, "name" => $originalFilename);
                array_push($statusreportarray, $thisreportarray);
        }
    }

        $visitphotoarray = array();
        $visitemailarray = array();
        $visitnotesarray = array();
        $getvisitcount   = 0;
        $get_visitdetails = "select `visitId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `visitupdates`  where claimId = '$currentclaimid'";
        $visitdetailstmt       = mysqli_query($connection, $get_visitdetails); 
        $getvisitcount   = mysqli_num_rows($visitdetailstmt);
        if($getvisitcount > 0){
            
          while($visitrow = mysqli_fetch_array($visitdetailstmt, MYSQLI_ASSOC)){
            $visitId            = $visitrow['visitId']; 
            $updateType         = (empty($visitrow['updateType']))        ? '' : $visitrow['updateType'];
            $updateContent      = (empty($visitrow['updateContent']))     ? '' : $visitrow['updateContent'];
            $originalFilename   = (empty($visitrow['originalFilename']))  ? '' : $visitrow['originalFilename'];
            $createdDate        = (empty($visitrow['createdDate']))       ? '' : date('d M, Y', strtotime($visitrow['createdDate']));           
            if($updateType == "P"){
                $thisphotoarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
                array_push($visitphotoarray, $thisphotoarray);
            }
            if($updateType == "E"){
                $thisemailarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
                array_push($visitemailarray, $thisemailarray);
            }
            if($updateType == "N"){
                $thisnotesarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate);
                array_push($visitnotesarray, $thisnotesarray);
            }
        }
    }
            $workingphotoarray = array();
        $workingdocumentarray = array();

        $get_workingdetails = "select `workingId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` from `workingupdates` where claimId = '$currentclaimid'";
        $workingdetailstmt       = mysqli_query($connection, $get_workingdetails); 
        $getworkingcount   = mysqli_num_rows($workingdetailstmt);
        if($getworkingcount > 0){
            
          while($workingrow = mysqli_fetch_array($workingdetailstmt, MYSQLI_ASSOC)){
            $workingId          = $workingrow['workingId']; 
            $updateType         = (empty($workingrow['updateType']))        ? '' : $workingrow['updateType'];
            $updateContent      = (empty($workingrow['updateContent']))     ? '' : $workingrow['updateContent'];
            $originalFilename   = (empty($workingrow['originalFilename']))  ? '' : $workingrow['originalFilename'];
            $createdDate        = (empty($workingrow['createdDate']))       ? '' : date('d M, Y', strtotime($workingrow['createdDate']));         
            if($updateType == "P"){
                $thisphotoarray = array("Id" => $workingId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
                array_push($workingphotoarray, $thisphotoarray);
            }
            if($updateType == "D"){
                $thisemailarray = array("Id" => $workingId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
                array_push($workingdocumentarray, $thisemailarray);
            }
        }
    }
  } else {
    header("Location:open.php");
}
} 

if((isset($_POST['policynumber'])) && (!empty($_POST['policynumber']))){
    // print_r($_REQUEST);exit;
    // $id                 = (empty($_REQUEST['id']))                  ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['id']));
    $policynumber       = (empty($_REQUEST['policynumber']))        ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['policynumber']));
    $insurername        = (empty($_REQUEST['insurername']))         ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['insurername']));
    $lossnature         = (empty($_REQUEST['lossnature']))          ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['lossnature']));
    $categories         = explode("~~", $lossnature);
    $newcategoryid      = $categories[0];
    $newsubid           = $categories[1];
    $adjustername       = (empty($_REQUEST['adjustername']))        ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['adjustername']));
    $brokername         = (empty($_REQUEST['brokername']))          ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['brokername']));
    $instructiondate    = (empty($_REQUEST['instructiondate']))     ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['instructiondate']));
    $instructiondate    = date("Y-m-d", strtotime($instructiondate));
    $preliminarydate    = (empty($_REQUEST['preliminarydate']))     ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['preliminarydate']));
    $startdate          = (empty($_REQUEST['startdate']))           ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['startdate']));
    $starttime          = (empty($_REQUEST['starttime']))           ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['starttime']));
    $enddate            = (empty($_REQUEST['enddate']))             ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['enddate']));
    $endtime            = (empty($_REQUEST['endtime']))             ? ''    : mysqli_real_escape_string($connection, trim($_REQUEST['endtime']));
    $claimreference     = (empty($_REQUEST['claimreference'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['claimreference']));
    $brokerreference    = (empty($_REQUEST['brokerreference'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['brokerreference']));
    $contactperson      = (empty($_REQUEST['contactperson'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['contactperson']));
    $contactnumber      = (empty($_REQUEST['contactnumber'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['contactnumber']));
    $insurercontact     = (empty($_REQUEST['insurercontact'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insurercontact']));
    $insuredname        = (empty($_REQUEST['insuredname'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insuredname']));
    $clientname         = (empty($_REQUEST['clientname'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['clientname']));
    $projectedfee       = (empty($_REQUEST['projectedfee'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['projectedfee']));
    $reserveamount      = (empty($_REQUEST['reserveamount'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['reserveamount']));
    $prelimreportissued      = (!isset($_REQUEST['prelimreportissued'])) ? 'N' : mysqli_real_escape_string($connection, trim($_REQUEST['prelimreportissued']));
    $statusreportissued      = (!isset($_REQUEST['statusreportissued'])) ? 'N' : mysqli_real_escape_string($connection, trim($_REQUEST['statusreportissued']));
    $update_claim = "update `claimmaster` set `policyNumber`='$policynumber', `insurerName`='$insurername', `claimReference`='$claimreference',`brokerReference`='$brokerreference',`contactPerson`='$contactperson',`contactNumber`='$contactperson',`insurerContact`='$insurercontact', `brokerId`='$brokername', `adjusterId`='$adjustername', `categoryId`='$newcategoryid',`subId`='$newsubid', `instructionDate`='$instructiondate', `preliminaryDate`='$preliminarydate', `startDate`='$startdate', `startTime`='$starttime', `endDate`='$enddate', `endTime`='$endtime', `updatedDate`='$loggedin_userid', `updatedBy`= now(), `insuredName` = '$insuredname', `clientId` = '$clientname', `projectedFee` = '$projectedfee', `reserveAmount` = '$reserveamount', `prelimreportissued` = '$prelimreportissued ', `statusReportIssued` = '$statusreportissued' where claimId = '$currentclaimid'";
    // echo $update_claim;exit;
    mysqli_query($connection, $update_claim);
    $_SESSION['updatedclaimid'] = $currentclaimid;
    header("Location:update.php");
}
if((isset($_FILES['statusreport']['name']))&&(!empty($_FILES['statusreport']['name']))){
    $statusreport    = (empty($_FILES['statusreport']['name']))  ? '' : $_FILES['statusreport']['name'];
    $fullfilename       = "";
    if($statusreport){
        $date          = round(microtime(true)*1000);
        $imgtype       = explode('.', $statusreport);
        $ext           = end($imgtype);
        $filename      = $imgtype[0];
        $fullfilename  = $filename.$date.".".$ext;
        $originalname  = $filename.".".$ext;
        $fullpath      = $path2 . $fullfilename;
        move_uploaded_file($_FILES['statusreport']['tmp_name'], $fullpath);
    }
    $statusfee     = (empty($_REQUEST['statusfee']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['statusfee']));
    $statusdate     = (empty($_REQUEST['statusdate']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['statusdate']));
    $insert_photo = "insert into `statusreports`(`claimId`, `report`, `reserveAmount`, `statusDate`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$currentclaimid', '$fullfilename', '$statusfee', '$statusdate', '$originalname', '$loggedin_userid', now())";
    mysqli_query($connection, $insert_photo);
    $update_status = "update claimmaster set jobStatus = 'S', statusDate = curdate() where claimId = '$currentclaimid'";
    mysqli_query($connection, $update_status);
    header("Refresh:0");
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="open.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">View reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    	<th>Insurer</th>
                                    	<th>Claim no.</th>
                                    	<th>Insured</th>
                                    	<th>Stage</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        	<td><?php echo $clientName;?></td>
                                        	<td><?php echo $jobNumber;?></td>
                                        	<td><?php echo $insuredName;?></td>
                                        	<td><?php echo $jobStatusText;?></td>                                        	
                                        </tr>
									</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if($getvisitcount > 0){
                    ?>
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category"><strong>Visit Updates</strong></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <p>Photographs:</p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $photocount = 0;
                                        // print_r($visitphotoarray);
                                            foreach ($visitphotoarray as $visitphotos) {

                                                $photocount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $photocount; ?> : <a href="<?php echo $path."visit/".$visitphotos['content'];?>" target="_blank"><?php echo $visitphotos['name'];?></a></td>
                                                    <td>Uploaded on : <?php echo $visitphotos['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if ($photocount == 0) {
                                                echo "<tr><td colspan='2'>No Photographs</td></tr>";
                                            }
                                        ?>
                                        <tr>
                                            <td colspan="2">
                                                <p>Notes: </p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $notecount = 0;
                                        // print_r($visitphotoarray);
                                            foreach ($visitnotesarray as $visitnote) {

                                                $notecount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $notecount; ?> : <?php echo $visitnote['content'];?></td>
                                                    <td>Written on : <?php echo $visitnote['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if ($notecount == 0) {
                                                echo "<tr><td colspan='2'>No Visit Notes</td></tr>";
                                            }
                                        ?>
                                        <tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php 
                        if($getprelimcount > 0){
                    ?>
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category"><strong>Prelim Updates</strong></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
                                    <tbody>
                                         <tr height="50">
                                            <td colspan="3">
                                                <p>Report: </p>
                                            </td>
                                            
                                        </tr>
                                        <?php 
                                        $reportcount = 0;
                                        // print_r($prelimreportarray);
                                            foreach ($prelimreportarray as $prelimreports) {

                                                $reportcount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $reportcount; ?> : <a href="<?php echo $path."prelim/".$prelimreports['content'];?>" target="_blank"><?php echo $prelimreports['name'];?></a></td>
                                                    <td><?php echo $prelimreports['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if($reportcount == 0){
                                                echo "<tr><td colspan='2'>No Reports</td></tr>";
                                            }
                                        ?>
                                        <tr height="50">
                                            <td colspan="3">
                                                <p>Projected fees: </p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $feecount = 0;
                                        // print_r($visitphotoarray);
                                            foreach ($prelimfeesarray as $prelimfees) {

                                                $feecount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $feecount; ?> : <?php echo $prelimfees['content'];?></td>
                                                    <td>Updated on : <?php echo $prelimfees['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if($feecount == 0){
                                                echo "<tr><td colspan='2'>No Projected Fees</td></tr>";
                                            }
                                        ?>
                                        <tr height="50">
                                            <td colspan="3">
                                                <p>Reserved Amount: </p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $amountcount = 0;
                                        // print_r($visitphotoarray);
                                            foreach ($prelimamountarray as $prelimamount) {

                                                $amountcount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $amountcount; ?> : <?php echo $prelimamount['content'];?></td>
                                                    <td>Updated on : <?php echo $prelimamount['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if($amountcount == 0){
                                                echo "<tr><td colspan='2'>No Reserved Amount</td></tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                                        <?php 
                        if($getstatuscount > 0){
                    ?>
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category"><strong>Status Updates</strong></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
                                    <tbody>
                                        <?php 
                                        $reportcount = 0;
                                        // print_r($statusreportarray);
                                            foreach ($statusreportarray as $statusreports) {

                                                $reportcount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $reportcount; ?></td>
                                                    <td><a href="<?php echo $path."status/".$statusreports['content'];?>" target="_blank"><?php echo $statusreports['name'];?></a></td>
                                                    <td><?php echo $statusreports['reserveAmount'];?></td>
                                                    <td><?php echo $statusreports['statusDate'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if($reportcount == 0){
                                                echo "<tr><td colspan='4'>No Reports</td></tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                     <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category"><strong>Add Status Report</strong></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <form method="POST" action="" enctype="multipart/form-data"  onsubmit="return confirm('Click OK to submit the form.');">
                                    <table class="table table-hover table-striped" border=0>
                                        <tbody>
                                            <tr>
                                                <td>Select a file to attach <input type="file" name="statusreport" id="statusreport" required /></td>
                                                <td><input style="width:250px" type="text" value="" id="statusfee" name="statusfee" placeholder="Enter status fees" class="onlynumbers form-control" required /></td>
                                                <td><input style="width:200px;" type="text" name="statusdate" class="dateinputs form-control" id="statusdate" required/></td>
                                                <td><input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/></td>
                                                <td><input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>

                                        <?php 
                        if($getworkingcount > 0){
                    ?>
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category"><strong>Working Updates</strong></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <p>Photographs:</p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $photocount = 0;
                                        // print_r($workingphotoarray);
                                            foreach ($workingphotoarray as $workingphotos) {

                                                $photocount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $photocount; ?> : <a href="<?php echo $path."working/".$workingphotos['content'];?>" target="_blank"><?php echo $workingphotos['name'];?></a></td>
                                                    <td>Uploaded on : <?php echo $workingphotos['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if ($photocount == 0) {
                                                echo "<tr><td colspan='2'>No Photographs</td></tr>";
                                            }
                                        ?>
                                        <tr>
                                            <td colspan="2">
                                                <p>Documents: </p>
                                            </td>
                                        </tr>
                                        <?php 
                                        $doccount = 0;
                                        // print_r($workingphotoarray);
                                            foreach ($workingdocumentarray as $workingdocs) {

                                                $doccount++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $doccount; ?> : <a href="<?php echo $path."working/".$workingdocs['content'];?>" target="_blank"><?php echo $workingdocs['name'];?></a></td>
                                                    <td>Uploaded on : <?php echo $workingdocs['date'];?></td>
                                                </tr>
                                                <?php
                                            }
                                            if ($doccount == 0) {
                                                echo "<tr><td colspan='2'>No Documents</td></tr>";
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php 
                                                                    //Get all invoices
                        $get_invoices = "select `invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `narrationText`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate` from `invoicemaster` where claimId = '$selectedclaimid'";
                        $detailstmt       = mysqli_query($connection, $get_invoices); 
                        $getcount   = mysqli_num_rows($detailstmt);
                        if($getcount > 0){
                    ?>
                    <div class="col-xs-12">
                        <div class="card">
                         <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>INSURER</th>
                                        <th>INVOICE NUMBER</th>
                                        <th>INVOICE DATE</th>
                                        <!-- <th>NARRATION</th> -->
                                        <th>INVOICE AMOUNT</th>
                                        <th>BALANCE AMOUNT</th>
                                        <th>VIEW</th>
                                    </thead>
                                    <tbody>
                                        <?php 


                                                
                                              while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                                $invoiceId          = $row['invoiceId']; 
                                                $claimId            = (empty($row['claimId']))          ? '' : $row['claimId'];
                                                $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                                                $invoiceNumber      = (empty($row['invoiceNumber']))    ? '' : $row['invoiceNumber'];
                                                $currency           = (empty($row['currency']))         ? '' : $row['currency'];
                                                $faoName            = (empty($row['faoName']))          ? '' : $row['faoName'];
                                                $toName             = (empty($row['toName']))           ? '' : $row['toName'];
                                                $yourReference      = (empty($row['yourReference']))    ? '' : $row['yourReference'];
                                                $invoiceDate        = (empty($row['invoiceDate']))      ? '' : $row['invoiceDate'];
                                                $invoiceDate        = date('d/m/Y',strtotime($invoiceDate));
                                                $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                                                $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
                                                $totalAmount        = (empty($row['totalAmount']))      ? '' : $row['totalAmount'];
                                                $totalInWords       = (empty($row['totalInWords']))     ? '' : $row['totalInWords'];
                                                $invoiceTerms       = (empty($row['invoiceTerms']))     ? '' : $row['invoiceTerms'];
                                                $narrationText      = (empty($row['narrationText']))   ? '' : $row['narrationText'];
                                            
                                                $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
                                                $clientstmt       = mysqli_query($connection, $get_client); 
                                                $getclientcount   = mysqli_num_rows($clientstmt);
                                                if($getclientcount > 0){
                                                    
                                                  while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                    $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
                                                    $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                  }
                                              }
                                              $paidAmount = 0;
                                              $get_receipts = "select `receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom` from `receiptdetails` where invoiceId = '$invoiceId'";
                                              $receiptstmt       = mysqli_query($connection, $get_receipts); 
                                              $getreceiptcount   = mysqli_num_rows($receiptstmt);
                                              $receiptcount      = 0;
                                              if($getreceiptcount > 0){
                                                $receiptcount++;
                                                while($row = mysqli_fetch_array($receiptstmt, MYSQLI_ASSOC)){
                                                        $receiptId          = $row['receiptId']; 
                                                        $receiptAmount      = (empty($row['receiptAmount']))          ? '' : $row['receiptAmount'];
                                                        $paidAmount  = $paidAmount + $receiptAmount;
                                                    }
                                                }
                                                $balanceAmount = $totalAmount - $paidAmount;
                                              ?>
                                              <tr>
                                                <td><?php echo $clientName;?></td>
                                                <td><?php echo $invoiceNumber;?></td>
                                                <td><?php echo $invoiceDate;?></td>
                                                <!-- <td><?php echo $narrationText;?></td> -->
                                                <td><?php echo number_format($totalAmount)." ".$currency;?></td>
                                                <td><?php echo number_format($balanceAmount)." ".$currency;?></td>
                                                <td>
                                                    <?php 
                                                    if($receiptcount > 0){
                                                        ?>
                                                    <a href="viewreceipts.php?id=<?php echo $invoiceId;?>"><button class="btn btn-info btn-fill pull-right">VIEW RECEIPTS</button></a>                                                        
                                                        <?php
                                                    }
                                                ?>
                                                </td>
                                            </tr>
                                              <?php
                                              }
                                          

                                        ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
        } 
            ?>
		<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <p class="category">View / Update reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped">
								<form name="claimupdateform" action="#" id="claimupdateform" method="POST">
                                    <tbody>
                                        <tr>
                                        	<!-- <td>Claim number</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" placeholder="Claim number" value="XXXX" required/></td> -->
                                            <td>Policy number <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" placeholder="Policy number" name="policynumber" id="policynumber" value="<?php echo $policyNumber;?>" required/></td>
                                        	<td>Claim Reference <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text nonumbers" placeholder="Claim Reference" name="claimreference" id="claimreference" value="<?php echo $claimReference;?>" required/></td>
                                        </tr>
                                        <tr>
											<td>Nature of loss <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><select name="lossnature" id="lossnature" required>
                                             <?php echo $lossnatureoptions;?>   
                                            </select></td>
                                        	<td>Adjuster <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td>
												<select style="width:200px;" name="adjustername" id="adjustername" required>
													<?php echo $adjusteroptions;?>
												</select>
											</td>
                                        </tr>
                                        <tr>
                                            <td>Broker Reference <span class="mandatorystar">*</span></td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text" placeholder="Broker Reference" name="brokerreference" id="brokerreference" value="<?php echo $brokerReference;?>" required/></td>
                                            <td>Contact Person <span class="mandatorystar">*</span></td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text nonumbers" placeholder="Contact Person" name="contactperson" id="contactperson" value="<?php echo $contactPerson;?>" required/></td>
                                        </tr>
                                        <tr>
                                            <td>Contact number <span class="mandatorystar">*</span></td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text" placeholder="Contact Number" name="policynumber" id="policynumber" value="<?php echo $contactNumber;?>" required/></td>
                                            <td>Insurer Contact <span class="mandatorystar">*</span></td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text nonumbers" placeholder="Insurer Contact" name="insurercontact" id="insurercontact" value="<?php echo $insurerContact;?>" required/></td>
                                        </tr>
                                        <tr>
											<td>Broker <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><select name="brokername" id="brokername" required>
                                             <?php echo $brokeroptions;?>   
                                            </select></td>
											<td>Date of instruction <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" name="instructiondate" class="dateinputs" id="instructiondate" value="<?php echo $instructionDate;?>" required/></td>
                                        </tr>
                                        <tr>
                                        	<td>Date of preliminary report</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" name="preliminarydate" class="dateinputs"  id="preliminarydate" value="<?php echo $preliminaryDate;?>"/></td>
                                        	<td>Start date</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" name="startdate" class="dateinputs" id="startdate" value="<?php echo $startDate;?>"></td>
                                        </tr>
                                        <tr>
                                        	<td>Start time:</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="time" name="starttime" id="starttime" value="<?php echo $startTime;?>" /></td>
											<td>End date</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" name="enddate" class="dateinputs"  id="enddate" value="<?php echo $endDate;?>"></td>
                                        </tr>
                                        <tr>
                                        	<td>End time</td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="time" name="endtime" id="endtime" value="<?php echo $endTime;?>"></td>
                                            <td>Insured Name</td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text" name="insuredname" id="insuredname" value="<?php echo $insuredName;?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Projected Fee</td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text" name="projectedfee" id="projectedfee" value="<?php echo $projectedFee;?>"></td>
                                            <td>Reserve Amount</td>
                                            <td>:</td>
                                            <td><input style="width:200px;" type="text" name="reserveamount" id="reserveamount" value="<?php echo $reserveAmount;?>"></td>
                                        </tr>
                                        <?php if ($jobStatus == "O") {?>
                                        <tr>
                                            <td>Insurer Name</td>
                                            <td>:</td>
                                            <td><select style="width:200px;" name="clientname" id="clientname">
                                                    <?php 
                                                    $get_categories = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where `active` = 'A'";
                                                        $clientstmt       = mysqli_query($connection, $get_categories); 
                                                        $getclientcount   = mysqli_num_rows($clientstmt);
                                                        if($getclientcount > 0){
                                                            
                                                          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                            $clientId     = $clientrow['clientId']; 
                                                            $referenceId  = (empty($clientrow['referenceId']))   ? '' : $clientrow['referenceId'];
                                                            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                            $phoneNumber  = (empty($clientrow['phoneNumber']))       ? '' : $clientrow['phoneNumber'];
                                                            $emailId      = (empty($clientrow['emailId']))       ? '' : $clientrow['emailId'];
                                                            $city         = (empty($clientrow['city']))       ? '' : $clientrow['city'];
                                                            if($currentclientId == $clientId){
                                                                ?>
                                                                    <option value="<?php echo $clientId;?>" selected><?php echo $clientName;?></option>

                                                                    <?php
                                                            } else {
                                                                ?>
                                                                    <option value="<?php echo $clientId;?>"><?php echo $clientName;?></option>

                                                                 <?php
                                                            }
                                                
                                                          }
                                                      }
                                                ?>                      
                                                </select></td>
                                            <td colspan="3"></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td>Live Projected Fee</td>
                                            <td>:</td>
                                            <td><?php echo $liveProjectedFee;?></td>
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <input type="checkbox" value="Y" id="prelimreportissued" name="prelimreportissued" <?php if($prelimReportIssued == "Y") {echo "checked";} ?>/> Preliminary Report Issued
                                            
                                            </td>
                                            <td colspan="3">
                                                <input type="checkbox" value="Y" id="statusreportissued" name="statusreportissued" <?php if($statusReportIssued == "Y") {echo "checked";} ?>/> Status Report Issued
                                            </td>
                                        </tr>
									</tbody>
								</table>
                                
                                <table class="table table-striped">
                                    <tr>
                                        <td style="width: 300px;"></td>
                                        <td style="width: 300px;"></td>
                                        <td><button class="btn btn-info btn-fill pull-right updateclaimbtn">UPDATE / VIEW</button></td></form>
                                        <td><a href="open.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">CANCEL</button></a></td>
                                    </tr>
                                </table>
									<table class="table table-hover table-striped">
										<tr>
											<td><a href="visit.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">VISIT PAGE</button></a></td>
											<td><a href="prelim.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">PRELIMINARY</button></a></td>
                                            <!-- <td><a href="statusreport.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">STATUS</button></a></td> -->
                                            <td><a href="invoice.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">INVOICE</button></a></td><!-- </tr>
                                            <tr> -->
                                            <td><a href="receipt.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">RECEIPT</button></a></td>
											<td><a href="timeandexpense.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">TIME</button></a></td>
											<td><a href="working.php"><button class="btn btn-info btn-fill pull-right" style="width:128px;">WORKING</button></a></td>
                                            <?php 
                                                if(($balanceAmount <! 0)&&($jobStatus != "O")){
                                                    ?>
                                                    <td><button class="btn btn-info btn-fill pull-right closejobbtn" id="<?php echo $currentclaimid;?>">CLOSE</button></td>
                                                    <?php
                                                }
                                            ?>
											<!-- <td><button class="btn btn-info btn-fill pull-right" style="width:128px;">CLOSED</button></a></td> -->
										</tr>
										
                                </table> 
								
							
                            </div>
                        </div>
                    </div>
		

    </div>
</div>


</body>
    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/jQuery-ui.js"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.dateinputs').datepicker({ dateFormat: 'dd-mm-yy' });
            $('.closejobbtn').click(function(){
                var answer = confirm("This file will be freezed. Click OK to continue.")
                if (answer){
                    var claimid = $(this).attr("id");
                    window.location.href = "closejob.php?claimid="+claimid;
                }
                else{
                    
                }
            });
        });
    </script>
   

</html>
