-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2017 at 05:47 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `claimmaster`
--

DROP TABLE IF EXISTS `claimmaster`;
CREATE TABLE `claimmaster` (
  `claimId` int(11) NOT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `officeId` int(11) DEFAULT NULL,
  `policyNumber` varchar(50) DEFAULT NULL,
  `insuredName` varchar(50) DEFAULT NULL,
  `insurerName` varchar(200) DEFAULT NULL,
  `claimReference` varchar(50) DEFAULT NULL,
  `brokerReference` varchar(50) DEFAULT NULL,
  `contactPerson` varchar(50) DEFAULT NULL,
  `contactNumber` varchar(50) DEFAULT NULL,
  `insurerContact` varchar(50) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `brokerId` int(11) DEFAULT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `instructionTime` varchar(20) DEFAULT NULL,
  `instructionDate` date DEFAULT NULL,
  `contactTime` varchar(20) DEFAULT NULL,
  `contactDate` varchar(20) DEFAULT NULL,
  `surveyTime` varchar(20) DEFAULT NULL,
  `surveyDate` varchar(20) DEFAULT NULL,
  `preliminaryDate` varchar(50) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `jobStatus` varchar(2) NOT NULL DEFAULT 'O' COMMENT 'O-Open, V-Visit,P-Preliminary, W-Working, C-Closed',
  `frozen` varchar(2) NOT NULL DEFAULT 'N',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `claimmaster`
--

INSERT INTO `claimmaster` (`claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`) VALUES
(1, 'DXB/G/9585/17', NULL, 1, '2/1/020/00010119', 'Philadelphia Private School', '', '9585', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 128, 8, 6, 38, '13:00', '2017-03-01', '14:00', '01-03-2017', '13:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:33:54', '2017-03-27 15:35:44', NULL),
(2, 'DXB/G/9586/17', NULL, 1, 'P/01/1002/2014/503', 'Al Futtaim Auto Centre', '', '9586', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 5, 6, 35, '13:00', '2017-03-01', '14:00', '01-03-2017', '15:00', '01-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:46:08', '2017-03-27 15:35:50', NULL),
(3, 'DXB/G/9587/17', NULL, 1, '4001/2014/01/00049', 'Al Jaber Transport & Gen. Cont.', '', '9587', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 4, 2, 6, '13:00', '2017-03-01', '14:00', '01-03-2017', '13:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:51:39', '2017-03-27 15:35:54', NULL),
(4, 'DXB/G/9588/17', NULL, 1, '2211099', 'Signature Terrace Restaurant', '', '9588', 'CL/DXB/SCL63/2017/000008', 'NA', 'NA', 'NA', 'NA', 5, 80, 9, 6, 38, '13:00', '2017-03-01', '13:00', '01-03-2017', '14:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:58:02', '2017-03-27 15:35:58', NULL),
(5, 'DXB/G/9589/17', NULL, 1, 'P/02/ENG/MBB/2016/00001', 'Kimoha Entrepreneurs Limited', '', '9589', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 8, 5, 26, '13:00', '2017-03-02', '13:00', '02-03-2017', '13:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 13:03:08', '2017-03-27 15:36:39', NULL),
(6, 'DXB/G/9590/17', NULL, 1, 'KH/DE/4769/16', 'Al Nasr Contracting & Demolition LLC', '', '9590', 'ARAYA/FGA/CLM/1960/28-02-17', 'NA', 'NA', 'NA', 'NA', 12, 39, 2, 2, 7, '13:00', '2017-03-02', '14:00', '02-03-2017', '13:00', '05-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:06:52', '2017-03-27 15:36:42', NULL),
(7, 'DXB/G/9591/17', NULL, 1, '01-311-133-11-75', 'Caboodle Pampers & Play LLC', '', '9591', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 7, 42, '13:00', '2017-03-02', '14:00', '02-03-2017', '15:00', '03-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:10:27', '2017-03-27 15:36:45', NULL),
(8, 'DXB/G/9592/17', NULL, 1, 'PI/2006/100001', 'Sky Oryx JV', '', '9592', 'NA', 'NA', 'NA', 'NA', 'NA', 8, 174, 2, 7, 43, '13:00', '2017-03-02', '14:00', '02-03-2017', '15:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:13:22', '2017-03-27 15:36:52', NULL),
(9, 'DXB/G/9593/17', NULL, 1, '2/2/020/04002484/3', 'Benjamin David Hughes', '', '9593', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 174, 3, 3, 22, '13:00', '2017-03-02', '14:00', '02-03-2017', '15:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:16:11', '2017-03-27 15:37:06', NULL),
(10, 'DXB/G/9594/17', NULL, 1, '2/1/020/00010162', 'Chelsea Plaza Hotel', '', '9594', 'PAR17011', 'NA', 'NA', 'NA', 'NA', 44, 80, 8, 6, 37, '13:00', '2017-03-02', '14:00', '02-03-2017', '13:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:19:13', '2017-03-27 15:37:09', NULL),
(11, 'DXB/G/9595/17', NULL, 1, 'HFAP200500001548/075', 'Giordano Fashion LLC', '', '9595', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 142, 2, 6, 35, '13:00', '2017-03-04', '14:00', '04-03-2017', '13:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:21:55', '2017-03-27 15:37:21', NULL),
(12, 'DXB/G/9596/17', NULL, 1, '2/1/021/0000145', 'Yateem Optician', '', '9596', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 2, 6, 35, '22:00', '2017-03-04', '23:00', '04-03-2017', '22:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:24:29', '2017-03-27 15:37:24', NULL),
(13, 'DXB/G/9597/17', NULL, 1, 'HFAP200500001554/701', 'Al Maya International Ltd FZC', '', '9597', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 2, 6, 35, '10:00', '2017-03-05', '11:00', '04-03-2017', '10:00', '09-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:26:19', '2017-03-27 15:37:49', NULL),
(14, 'DXB/G/9598/17', NULL, 1, 'P/11/1002/2014/71', 'Life Health Care Group', '', '9598', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:28:05', '2017-03-27 15:37:52', NULL),
(15, 'DXB/G/9599/17', NULL, 1, 'P/001/2033/2016/16', 'Allied Transport LLC', '', '9599', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:30:33', '2017-03-27 15:37:55', NULL),
(16, 'DXB/G/9600/17', NULL, 1, 'HFAR201500002271', 'Jereh Energy Corpoporation', '', '9600', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 9, 2, 18, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '05-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:38:51', '2017-03-27 15:37:59', NULL),
(17, 'DXB/G/9601/17', NULL, 1, '2/1/020/22005341', 'Wayne Charles Dooley', '', '9601', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 8, 3, 21, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:41:26', '2017-03-27 15:38:10', NULL),
(18, 'DXB/G/9602/17', NULL, 1, '2003/2017/03/00005', 'Ajmal Group of Companies', '', '9602', 'CL/AZB/SCL15/2017/000006', 'NA', 'NA', 'NA', 'NA', 8, 81, 2, 6, 35, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:43:21', '2017-03-27 15:38:14', NULL),
(19, 'DXB/G/9603/17', NULL, 1, 'D/02/F350/1048/16', 'Matalan Department Stores', '', '9603', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 49, 82, 2, 6, 35, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:46:14', '2017-03-27 15:38:51', NULL),
(20, 'DXB/G/9604/17', NULL, 1, 'P/04/PRO/PRL/2016/10403', 'Union Cement Company', '', '9604', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 2, 5, 27, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:48:09', '2017-03-27 15:38:55', NULL),
(21, 'DXB/G/9605/17', NULL, 1, 'Z1-F13-000228-2', 'Lake Shore Tower', '', '9605', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 2, 53, 3, 6, 37, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '23-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:51:01', '2017-03-27 15:38:58', NULL),
(22, 'DXB/G/9606/17', NULL, 1, 'HFHM201700000097', 'Rixos Hotel', '', '9606', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 115, 5, 6, 37, '10:00', '2017-03-05', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:28:18', '2017-03-27 15:39:02', NULL),
(23, 'DXB/G/9607/17', NULL, 1, '2/1/020/02304266', 'Boston Foods', '', '9607', 'CL/DXB/SCL63/2017/000009', 'NA', 'NA', 'NA', 'NA', 44, 82, 2, 6, 35, '10:00', '2017-03-06', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:29:59', '2017-03-27 15:39:06', NULL),
(24, 'DXB/M/9608/17', NULL, 1, 'To Be Advised', 'Pioneer Gulf Services', '', '9608', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 23, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:32:04', '2017-03-27 15:39:11', NULL),
(25, 'DXB/G/9609/17', NULL, 1, 'HFAP201300004194', 'Sultan Group Investment', '', '9609', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 137, 5, 6, 35, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:34:01', '2017-03-27 15:39:14', NULL),
(26, 'DXB/G/9610/17', NULL, 1, 'P/01/1002/2014/364', 'Al Futtaim Watches & Jewellery', '', '9610', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 2, 6, 35, '10:00', '2017-03-06', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:36:42', '2017-03-27 15:39:23', NULL),
(27, 'DXB/G/9611/17', NULL, 1, '2/2/020/02306066/2', 'The Ice Cream Lab', '', '9611', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 57, 2, 6, 35, '10:00', '2017-03-06', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:38:46', '2017-03-27 15:39:26', NULL),
(28, 'DXB/M/9612/17', NULL, 1, 'To Be Advised', 'Al Khayyat Investment (ALPHAMED)', '', '9612', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 27, 175, 9, 4, 23, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:41:08', '2017-03-27 15:39:35', NULL),
(29, 'DXB/G/9613/17', NULL, 1, '03/1101/13/2015/10', 'Mammut Group', '', '9613', 'NA', 'NA', 'NA', 'NA', 'NA', 23, 128, 4, 2, 18, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:45:06', '2017-03-27 15:39:39', NULL),
(30, 'DXB/G/9614/17', NULL, 1, 'PR2/138586/2016/NGI', 'Rivoli Group', '', '9614', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 33, 101, 2, 6, 35, '10:00', '2017-03-06', '11:00', '06-03-2017', '10:00', '14-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:53:34', '2017-03-27 15:39:42', NULL),
(31, 'DXB/G/9615/17', NULL, 1, 'EFER20150000021', 'Centaur Electromechanical', '', '9615', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 26, 5, 2, 9, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:08:58', '2017-03-27 15:39:46', NULL),
(32, 'DXB/G/9616/17', NULL, 1, 'DFTP201600001379', 'Frigoglass', '', '9616', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 2, 7, 44, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:10:55', '2017-03-27 15:39:49', NULL),
(33, 'DXB/G/9617/17', NULL, 1, 'AFCT201000000397', 'Emke Group', '', '9617', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 3, 9, 30, '10:00', '2017-03-07', '11:00', '07-03-2017', '12:00', '07-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:13:08', '2017-03-27 15:39:59', NULL),
(34, 'DXB/G/9618/17', NULL, 1, '01/1101/11/2016/290', 'Patchi LLC', '', '9618', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 87, 5, 6, 35, '10:00', '2017-03-07', '11:00', '07-03-2017', '11:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:15:06', '2017-03-27 15:40:03', NULL),
(35, 'DXB/G/9619/17', NULL, 1, 'To Be Advised', 'National Marine Dredging Co.', '', '9619', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 9, 2, 18, '10:00', '2017-03-07', '11:00', '07-03-2017', '10:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:17:42', '2017-03-27 15:40:07', NULL),
(36, 'DXB/G/9620/17', NULL, 1, 'P/2004/02/20011/2016/00034', 'DKC Veterinary Clinic', '', '9620', 'NA', 'NA', 'NA', 'NA', 'NA', 3, 174, 8, 6, 37, '10:00', '2017-03-08', '11:00', '08-03-2017', '10:00', '10-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:19:39', '2017-03-27 15:40:10', NULL),
(37, 'DXB/G/9621/17', NULL, 1, 'P2/20/16-0807-001404', 'Damas', '', '9621', 'C/00012870', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 1, 1, '10:00', '2017-03-09', '11:00', '09-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:23:18', '2017-03-27 15:40:14', NULL),
(38, 'DXB/G/9622/17', NULL, 1, 'UAE/01/220/11952', 'Cocoon Nursery', '', '9622', 'NA', 'NA', 'NA', 'NA', 'NA', 5, 174, 7, 6, 36, '10:00', '2017-03-09', '11:00', '09-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:38:32', '2017-03-27 15:40:19', NULL),
(39, 'DXB/G/9623/17', NULL, 1, 'To Be Advised', 'Green Land Textile', '', '9623', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 5, 175, 8, 6, 37, '10:00', '2017-03-09', '11:00', '09-03-2017', '10:00', '11-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:40:20', '2017-03-27 15:40:23', NULL),
(40, 'DXB/G/9624/17', NULL, 1, 'P2/20/16-0404-002739', 'Damas', '', '9624', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 6, 35, '10:00', '2017-03-09', '11:00', '09-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:42:17', '2017-03-27 15:40:26', NULL),
(41, 'DXB/G/9625/17', NULL, 1, 'P2/20/16-0404-002540', 'Lifestyle LLC', '', '9625', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '2017-03-12', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:43:35', '2017-03-27 15:40:32', NULL),
(42, 'DXB/G/9626/17', NULL, 1, 'To Be Advised', 'Bed, Bags & Beyond LLC', '', '9626', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 26, 175, 2, 6, 35, '10:00', '2017-03-12', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:45:25', '2017-03-27 15:40:36', NULL),
(43, 'DXB/G/9627/17', NULL, 1, 'Z1-F13-000211-2', 'Al Ghurair Exchange Limited', '', '9627', 'C/00012837', 'NA', 'NA', 'NA', 'NA', 2, 118, 2, 6, 35, '10:00', '2017-03-12', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:47:12', '2017-03-27 15:40:42', NULL),
(44, 'DXB/G/9628/17', NULL, 1, 'P/01/1002/2014/723', 'Cozmo Travel LLC', '', '9628', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '2017-03-13', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:49:10', '2017-03-27 15:40:46', NULL),
(45, 'DXB/G/9629/17', NULL, 1, 'HFHM201400000057', 'Seven Tides Limited &/ or Anantara Palm Jumeirah', '', '9629', 'ARYA/FGA/CLM/2378/13-03-2017', 'NA', 'NA', 'NA', 'NA', 38, 39, 8, 6, 37, '10:00', '2017-03-13', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:51:32', '2017-03-27 15:40:49', NULL),
(46, 'DXB/M/9630/17', NULL, 1, '01/3214/36/2016/72', 'Fadel Saif Mubarak Al Mazrouei', '', '9630', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 24, '10:00', '2017-03-13', '11:00', '13-03-2017', '10:00', '14-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:53:48', '2017-03-27 15:40:52', NULL),
(47, 'DXB/G/9631/17', NULL, 1, 'EFER201400001273', 'Palm Jumeirah Co. LLC', '', '9631', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 9, 7, 42, '10:00', '2017-03-13', '11:00', '13-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:55:43', '2017-03-27 15:40:56', NULL),
(48, 'DXB/G/9632/17', NULL, 1, 'P/01/6004/2014/20', 'Al Futtaim Engineering', '', '9632', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 43, '10:00', '2017-03-14', '11:00', '14-03-2017', '10:00', '19-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:58:04', '2017-03-27 15:41:00', NULL),
(49, 'DXB/G/9633/17', NULL, 1, 'P/04/2033/2016/36', 'Dashmesh General Transport', '', '9633', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '2017-03-14', '11:00', '14-03-2017', '10:00', '15-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:08:29', '2017-03-27 15:41:06', NULL),
(50, 'DXB/G/9634/17', NULL, 1, 'P/01/1002/2014/206', 'Al Futtaim Real Estate Private Limited', '', '9634', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 6, 37, '10:00', '2017-03-15', '11:00', '15-03-2017', '10:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:10:29', '2017-03-27 15:41:13', NULL),
(51, 'DXB/G/9635/17', NULL, 1, 'DP/01/3040/17/00004', 'Mazrui Holdings LLC', '', '9635', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 24, 87, 5, 9, 30, '10:00', '2017-03-15', '11:00', '15-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:12:15', '2017-03-27 15:41:21', NULL),
(52, 'DXB/G/9636/17', NULL, 1, 'P/04/6001/2014/300', 'QGB Group of Companies', '', '9636', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 42, '10:00', '2017-03-16', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:14:13', '2017-03-27 15:41:31', NULL),
(53, 'DXB/G/9637/17', NULL, 1, 'P/01/1002/2015/346', 'Saif Belhasa Holding', '', '9637', '14991 PK', 'NA', 'NA', 'NA', 'NA', 40, 15, 8, 6, 35, '10:00', '2017-03-16', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:16:17', '2017-03-27 15:41:46', NULL),
(54, 'DXB/G/9638/17', NULL, 1, 'P2/20/17-0807-001551', 'Lifestyle LLC', '', '9638', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '2017-03-19', '11:00', '19-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:17:54', '2017-03-27 15:41:50', NULL),
(55, 'LON/G/0358/17', NULL, 2, 'P/40/1002/2014/123', 'Jawhara Jewellery LLC', '', '358', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 17, 8, 6, 35, '10:00', '2017-03-16', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:21:36', '2017-03-27 15:41:35', NULL),
(56, 'LON/G/0354/17', NULL, 2, 'To Be Advised', 'One of One Jewellery', '', '354', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '2017-03-01', '11:00', '01-03-2017', '12:00', '01-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:06:01', '2017-03-27 15:41:54', NULL),
(57, 'LON/G/0355/17', NULL, 2, 'To Be Advised', 'Al Mumayyaz Jewellers', '', '355', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 51, 175, 8, 1, 1, '10:00', '2017-03-06', '11:00', '06-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:07:27', '2017-03-27 15:41:58', NULL),
(58, 'LON/G/0356/17', NULL, 2, 'To Be Advised', 'Damas Jewellery', '', '356', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 52, 118, 3, 1, 1, '10:00', '2017-03-08', '11:00', '08-03-2017', '12:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:11:19', '2017-03-27 15:42:02', NULL),
(59, 'LON/G/0357/17', NULL, 2, 'P/40/5021/2015/4', 'Jawhara Jewellery LLC', '', '357', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 17, 8, 6, 35, '10:00', '2017-03-13', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:13:52', '2017-03-27 15:42:06', NULL),
(60, 'LON/G/0359/17', NULL, 2, 'To Be Advised', 'Whitestar DMCC', '', '359', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 53, 101, 3, 1, 3, '10:00', '2017-03-19', '11:00', '19-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:15:36', '2017-03-27 15:42:21', NULL),
(61, 'DXB/G/9639/17', NULL, 1, 'DFAR2015781', 'Bilal General Transport', '', '9639', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 88, 9, 2, 18, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:28:29', '2017-03-27 15:42:24', NULL),
(62, 'DXB/G/9640/17', NULL, 1, 'To Be Advised', 'Royal Gardens Agricultural Contracting LLC', '', '9640', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 9, 2, 9, '10:00', '2017-03-20', '11:00', '20-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:37:22', '2017-03-27 15:42:28', NULL),
(63, 'DXB/G/9641/17', NULL, 1, 'P2/20/16/0501/001635', 'Proscape', '', '9641', '15007 PK', 'NA', 'NA', 'NA', 'NA', 42, 15, 5, 2, 6, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:41:56', '2017-03-27 15:42:31', NULL),
(64, 'DXB/G/9642/17', NULL, 1, 'To Be Advised', 'Dubai Investments Real Estate', '', '9462', 'C/00013050', 'NA', 'NA', 'NA', 'NA', 33, 119, 8, 6, 37, '10:00', '2017-03-20', '11:00', '20-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:44:24', '2017-03-27 15:42:35', NULL),
(65, 'DXB/G/9643/17', NULL, 1, '20/3007/30/2017/2', 'Ejar Cranes & Equipment (Qatar)', '', '9643', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 14, 73, 4, 2, 7, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:47:36', '2017-03-27 15:42:49', NULL),
(66, 'DXB/G/9644/17', NULL, 1, 'HFCO201500000071/1', 'Menafactors Ltd', '', '9644', '18594', 'NA', 'NA', 'NA', 'NA', 38, 18, 7, 6, 37, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:49:47', '2017-03-27 15:42:53', NULL),
(67, 'DXB/M/9645/17', NULL, 1, 'To Be Advised', 'ADCO', '', '9645', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 2, 175, 9, 4, 23, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:53:30', '2017-03-27 15:42:57', NULL),
(68, 'DXB/M/9646/17', NULL, 1, 'To Be Advised', 'ADCO', '', '9646', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 1, 175, 9, 4, 23, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:56:59', '2017-03-27 15:43:00', NULL),
(69, 'DXB/G/9647/17', NULL, 1, 'CBPLI2017000106', 'Sama Events Management', '', '9647', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 48, 16, 3, 7, 42, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:59:31', '2017-03-27 15:43:03', NULL),
(70, 'DXB/G/9648/17', NULL, 1, 'P/01/6001/2014/129', 'Al Futtaim Group Real Estate &/or DFC', '', '9648', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 6, 38, '10:00', '2017-03-20', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:32:53', '2017-03-27 15:43:07', NULL),
(71, 'DXB/G/9649/17', NULL, 1, 'P/01/1030/2014/30', 'Gloria Yassat Hotel Apartments FZ LLC', '', '9649', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 6, 37, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:37:13', '2017-03-27 15:43:13', NULL),
(72, 'DXB/G/9650/17', NULL, 1, '06-311-134-10-8', 'West Wood Hotel', '', '9650', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 8, 6, 37, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '26-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:42:16', '2017-03-27 15:43:16', NULL),
(73, 'DXB/G/9651/17', NULL, 1, 'P/01/1002/2014/404', 'Leader Sports', '', '9651', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 5, 6, 38, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:45:38', '2017-03-27 15:43:19', NULL),
(74, 'DXB/G/9652/17', NULL, 1, 'P/01/6001/2014/129', 'Al Futtaim Real Estate Private Limited', '', '9652', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 7, 42, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:47:56', '2017-03-27 15:43:22', NULL),
(75, 'DXB/G/9653/17', NULL, 1, 'SFC201400002233', 'Ghantoot Gulf Contracting', '', '9653', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 4, 2, 7, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:50:14', '2017-03-27 15:43:26', NULL),
(76, 'DXB/G/9654/17', NULL, 1, 'P2/20/16-0404-002540', 'Lifestyle LLC', '', '9654', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '2017-03-21', '11:00', '21-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:52:23', '2017-03-27 15:43:34', NULL),
(77, 'DXB/G/9655/17', NULL, 1, '03/2101/21/2016/75', 'Deco Emirates', '', '9655', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 17, 2, 6, 38, '10:00', '2017-03-22', '11:00', '22-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:55:17', '2017-03-27 15:43:40', NULL),
(78, 'DXB/G/9656/17', NULL, 1, '03/2101/21/2016/75', 'Deco Emirates', '', '9656', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 17, 2, 6, 38, '10:00', '2017-03-22', '11:00', '22-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 18:57:05', '2017-03-27 15:43:45', NULL),
(79, 'DXB/G/9657/17', NULL, 1, 'P/01/6001/2014/35', 'Cleanco Cleaning Services', '', '9657', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 6, 38, '10:00', '2017-03-22', '11:00', '22-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:00:05', '2017-03-27 15:43:49', NULL),
(80, 'DXB/G/9658/17', NULL, 1, 'P/09/2033/2017/11', 'DHH Heavy & Light Trucks Transport LLC', '', '9658', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:03:27', '2017-03-27 15:44:05', NULL),
(81, 'DXB/G/9659/17', NULL, 1, '2003/2017/03/00030', 'Al Yousuf LLC', '', '9659', '15033 KA', 'NA', 'NA', 'NA', 'NA', 8, 15, 5, 6, 38, '10:00', '2017-03-23', '11:00', '23-03-2017', '12:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:06:31', '2017-03-27 15:44:09', NULL),
(82, 'DXB/G/9660/17', NULL, 1, '505011593', 'Pixel Digital Systems LLC', '', '9660', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 39, 96, 7, 9, 30, '10:00', '2017-03-23', '11:00', '23-03-2017', '12:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:08:51', '2017-03-27 15:44:12', NULL),
(83, 'DXB/G/9661/17', NULL, 1, 'P/09/4011/2016/1', 'Bahwan Engineering', '', '9661', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 2, 7, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '26-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:11:19', '2017-03-27 15:44:15', NULL),
(84, 'DXB/G/9662/17', NULL, 1, 'P/01/4012/2015/136', 'J.P. Kalwani / Al Seeb Investment', '', '9662', '15037 KA', 'NA', 'NA', 'NA', 'NA', 40, 15, 8, 2, 9, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:14:04', '2017-03-27 15:44:18', NULL),
(85, 'DXB/G/9663/17', NULL, 1, 'To be Advised', 'Al Futtaim Real Estate', '', '9663', 'To be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 6, 38, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '26-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:16:32', '2017-03-27 15:44:22', NULL),
(86, 'DXB/M/9664/17', NULL, 1, 'To Be Advised', 'Al Rawabi Dairy', '', '9664', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 9, 4, 23, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:19:05', '2017-03-27 15:44:26', NULL),
(87, 'DXB/G/9665/17', NULL, 1, 'SFC201400002233', 'Ghantoot Gulf Conracting', '', '9665', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 4, 2, 7, '10:00', '2017-03-23', '11:00', '23-03-2017', '10:00', '25-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:23:44', '2017-03-27 15:44:29', NULL),
(88, 'DXB/G/9666/17', NULL, 1, 'To be Advised', 'Hellman Worldwide Logistics', '', '9666', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 47, 64, 6, 6, 37, '10:00', '2017-03-25', '11:00', '25-03-2017', '10:00', '26-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:31:18', '2017-03-27 15:44:37', NULL),
(89, 'LON/G/0336/17', NULL, 2, 'To be Advised', 'Kooheji Jewellery', '', '336', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 174, 8, 1, 1, '10:00', '2017-01-03', '11:00', '03-01-2017', '10:00', '12-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:37:16', '2017-03-27 15:44:51', NULL),
(90, 'LON/G/0338/17', NULL, 2, 'To be Advised', 'Emerald Jewellers  / Al Jawhara Al Ula', '', '338', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 174, 8, 1, 1, '10:00', '2017-01-03', '11:00', '03-01-2017', '10:00', '13-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 19:38:33', '2017-03-27 15:44:55', NULL),
(91, 'LON/G/0339/17', NULL, 2, 'NA', 'Itan Jewels', '', '339', 'NA', 'NA', 'NA', 'NA', 'NA', 54, 174, 8, 1, 3, '10:00', '2017-01-24', '11:00', '24-01-2017', '10:00', '05-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:17:56', '2017-03-27 15:45:04', NULL),
(92, 'LON/G/0340/17', NULL, 2, 'NA', 'Onyx Rose Gold', '', '340', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '2017-01-21', '11:00', '21-01-2017', '10:00', '08-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:20:37', '2017-03-27 15:45:14', NULL),
(93, 'LON/G/0341/17', NULL, 2, 'To be Advised', 'L. Sunderdas Zaveri Jewellery LLC', '', '341', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 59, 35, 3, 9, 30, '10:00', '2017-01-29', '11:00', '29-01-2017', '12:00', '29-01-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:23:14', '2017-03-27 15:45:22', NULL),
(94, 'LON/G/0342/17', NULL, 2, 'NA', 'Five Star Diamond', '', '342', 'NA', 'NA', 'NA', 'NA', 'NA', 50, 174, 8, 1, 1, '10:00', '2017-01-30', '10:00', '30-01-2017', '11:00', '16-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:28:07', '2017-03-27 15:45:28', NULL),
(95, 'LON/G/0343/17', NULL, 2, '25/1101/15/2015/39', 'Burberry Group', '', '343', 'NA', 'NA', 'NA', 'NA', 'NA', 55, 174, 3, 8, 50, '10:00', '2017-02-01', '11:00', '01-02-2017', '12:00', '01-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:30:23', '2017-03-27 15:45:32', NULL),
(96, 'LON/G/0344/17', NULL, 2, 'To be Advised', 'Star Diamonds', '', '344', 'NA', 'NA', 'NA', 'NA', 'NA', 56, 175, 3, 1, 1, '10:00', '2017-02-02', '11:00', '02-02-2017', '12:00', '02-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:31:53', '2017-03-27 15:45:38', NULL),
(97, 'LON/G/0345/17', NULL, 2, 'B1230PC01164A17', 'Al Masaood Jewellery', '', '345', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 57, 174, 8, 1, 1, '10:00', '2017-02-09', '11:00', '09-02-2017', '12:00', '27-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:33:48', '2017-03-27 15:45:42', NULL),
(98, 'LON/G/0346/17', NULL, 2, 'P/01/4012/2016/47', 'Expolink Consortium', '', '346', '2017800017', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 2, 7, '10:00', '2017-02-09', '11:00', '09-02-2017', '10:00', '12-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:36:24', '2017-03-27 15:45:47', NULL),
(99, 'LON/G/0347/17', NULL, 2, 'To be Advised', 'Priority Jewellers', '', '347', 'NA', 'NA', 'NA', 'NA', 'NA', 51, 175, 7, 1, 3, '10:00', '2017-02-13', '11:00', '13-02-2017', '12:00', '13-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:39:16', '2017-03-27 15:45:54', NULL),
(100, 'LON/G/0348/17', NULL, 2, 'NA', 'Dar Ramah Majestic Jewellery', '', '348', 'NA', 'NA', 'NA', 'NA', 'NA', 50, 174, 8, 2, 7, '10:00', '2017-02-13', '11:00', '13-02-2017', '12:00', '13-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:41:16', '2017-03-27 15:45:59', NULL),
(101, 'LON/G/0349/17', NULL, 2, 'To be Advised', 'Al Futtaim Jewellery Watches', '', '349', 'NA', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '2017-02-19', '11:00', '19-02-2017', '10:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:43:59', '2017-03-27 15:46:07', NULL),
(102, 'LON/G/0350/17', NULL, 2, 'NA', 'Fine Star Gold Bullion', '', '350', 'NA', 'NA', 'NA', 'NA', 'NA', 50, 174, 8, 1, 1, '10:00', '2017-02-21', '11:00', '21-02-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:46:09', '2017-03-27 15:46:14', NULL),
(103, 'LON/G/0351/17', NULL, 2, 'To be Advised', 'Ibraham Al Sukhon Sons', '', '351', 'NA', 'NA', 'NA', 'NA', 'NA', 58, 175, 3, 1, 1, '10:00', '2017-02-23', '11:00', '23-02-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:48:15', '2017-03-27 15:46:19', NULL),
(104, 'LON/G/0352/17', NULL, 2, 'To be Advised', 'Kiram Imseeh', '', '352', 'NA', 'NA', 'NA', 'NA', 'NA', 58, 175, 3, 1, 1, '10:00', '2017-02-23', '11:00', '23-02-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:51:01', '2017-03-27 15:46:28', NULL),
(105, 'LON/G/0353/17', NULL, 2, 'To be Advised', 'Sila Jewels LLC', '', '353', 'NA', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '2017-02-28', '11:00', '28-02-2017', '12:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:53:25', '2017-03-27 15:46:35', NULL),
(106, 'LON/G/0360/17', NULL, 2, 'To be Advised', 'Azziz G Haddad', '', '360', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '2017-03-21', '11:00', '21-03-2017', '12:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 20:55:46', '2017-03-27 15:46:40', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `claimmaster`
--
ALTER TABLE `claimmaster`
  ADD PRIMARY KEY (`claimId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `claimmaster`
--
ALTER TABLE `claimmaster`
  MODIFY `claimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
