
<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_adjusterid    = $_SESSION["loggedin_adjusterid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_REQUEST['id'])) && (!empty($_REQUEST['id']))){
        $_SESSION["updatedclaimid"] = $_REQUEST['id'];
        header("Location:timeandexpense.php");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        Whitelaw
    </title>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        
    <link href="res/ddmenu-ext.css" rel="stylesheet" type="text/css" />
    <script src="res/ddmenu-ext.js" type="text/javascript"></script>
    <script>

        function selectHandler(opWrapper, selectedBs) {

            //actions for the Ajax call. We just simulate it with a logger

            var logger = document.getElementById("log");
            var str = "We have selected [";
            for (var i = 0; i < selectedBs.length; i++) {
                str += selectedBs[i].id + ", ";
            }
            str += "]";
            str = str.replace(", ]", "]");
            logger.innerHTML = "Filter ID: " + opWrapper.filterId + "<br>View All: " +
                ((!selectedBs.length || selectedBs.length === opWrapper.bs.length) ? "true<br>" : "false<br>") + str + "<br><br><hr><br>";


            var allOpWrappers = myFilter.opWrappers;
            var str2 = "", count = 0;
            for (var i = 0; i < allOpWrappers.length; i++) {
                str2 += "<h3>" + allOpWrappers[i].filterId + " has chosen:</h3>";
                count = 0;
                for (var j = 0; j < allOpWrappers[i].bs.length; j++) {
                    if (allOpWrappers[i].bs[j].className == "selected") {
                        str2 += allOpWrappers[i].bs[j].id + "<br>";
                        count++;
                    }
                }
                if (count == 0) {
                    str2 += "View All";
                }
            }

            logger.innerHTML += str2;
        }

    </script>
</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="dashboard.php"><< Back to Dashboard</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">View Reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table align="left" >
                                <tr>
                                <td>
<!--                                <nav id="ddmenu" align="left">    
                                            <ul>
                                                <li>
                                                    <span class="top-heading">Status</span>
                                                    <i class="caret"></i>
                                                    <div class="dropdown">
                                                        <div class="dd-inner op-wrapper">
                                                            <p id="filter_Size">
                                                                <b class="viewall">View All</b>
                                                                <b id="open">Open</b>
                                                                <b id="visit">Visit</b>
                                                                <b id="preliminary">Preliminiary</b>
                                                                <b id="working">Working</b>
                                                                <b id="closed">Closed</b>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <span class="top-heading">Client</span>
                                                    <i class="caret"></i>
                                                    <div class="dropdown">
                                                        <div class="dd-inner op-wrapper">
                                                            <p id="filter_Size">
                                                                <b class="viewall">View All</b>
                                                                <b id="client_name_1">Client name 1</b>
                                                                <b id="client_name_2">Client name 2</b>
                                                                <b id="client_name_3">Client name 3</b>
                                                                <b id="client_name_4">Client name 4</b>
                                                                <b id="client_name_5">Client name 5</b>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                </nav><br> -->
                                </td>
                                </tr>
                                </table>
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <!-- <th>File</th> -->
                                        <th>Number</th>
                                        <th>Insurer</th>
                                        <th>Claim no.</th>
                                        <th>Insured</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                       <!--  <tr>
                                            <td>XXXXXX</td>
                                            <td>XXXXXX</td>
                                            <td>XXXXXX</td>
                                            <td>XXXXXX</td>
                                            <td>XXXXXX</td>
                                            <td>XXXXXX</td>
                                            <td>
                                            <form action="report.php">
                                                <input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
                                            </form>
                                            </td>
                                        </tr> -->
                                        <?php
                                        //Get claim details
                                        $count = 0;
                                        $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster`";
                                        if($loggedin_isadmin != "Y"){
                                            $get_details .= " where adjusterId = '$loggedin_adjusterid'";
                                        }
                                        $detailstmt       = mysqli_query($connection, $get_details); 
                                        $getcount   = mysqli_num_rows($detailstmt);
                                        if($getcount > 0){
                                            
                                          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                            $count++;
                                            $claimId            = $row['claimId']; 
                                            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                                            $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
                                            $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
                                            $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
                                            $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
                                            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                                            $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
                                            $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
                                            $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
                                            $subId              = (empty($row['subId']))            ? '' : $row['subId'];
                                            $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
                                            $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
                                            $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
                                            $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
                                            $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
                                            $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
                                            $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
                                            $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
                                            $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
                                            $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
                                            $jobStatusText = "";
                                            if($jobStatus == "O"){
                                                $jobStatusText = "Open";
                                            } elseif ($jobStatus == "V") {
                                                $jobStatusText = "Visit";
                                            } elseif ($jobStatus == "P") {
                                                $jobStatusText = "Preliminiary";
                                            } elseif ($jobStatus == "W") {
                                                $jobStatusText = "Working";
                                            } elseif ($jobStatus == "I") {
                                                $jobStatusText = "Invoiced";
                                            } elseif ($jobStatus == "C") {
                                                $jobStatusText = "Closed";
                                            } elseif ($jobStatus == "R") {
                                                $jobStatusText = "Receipt";
                                            } elseif ($jobStatus == "S") {
                                                $jobStatusText = "Status Report Issued";
                                            } else {
                                                $jobStatusText = "";
                                            }
                                      //Client name
                                      $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
                                            $clientstmt       = mysqli_query($connection, $get_client); 
                                            $getclientcount   = mysqli_num_rows($clientstmt);
                                            if($getclientcount > 0){
                                                
                                              while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
                                                $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                              }
                                          }
                                    
                                    ?>
                                        <tr>
                                            <td><?php echo $count;?></td>
                                            <td><?php echo $clientName;?></td>
                                            <td><?php echo $jobNumber;?></td>
                                            <td><?php echo $insuredName;?></td>
                                            <td><?php echo $jobStatusText;?></td>
                                            <td>
                                                <a href="selectjob.php?id=<?php echo $claimId; ?>"><button class="btn btn-info btn-fill pull-right">SELECT</button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                                                                  }
                                      }
                                        ?>
                                        
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#timeandexpense').addClass("active");
        });
    </script>
   

</html>
