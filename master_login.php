<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div align="center" class="content">
            <div class="container-fluid">
                <div class="row">
                    <div align="center" class="col-md-5">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Master Page</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table align="center" width="40%" border=0>
									<form action="masters.php">
										  <h1>Log in</h1>
										  <div class="inset">
										  <p>
											<label for="email">USERNAME: </label>
											<input type="text" name="name" id="name" required/>
										  </p>
										  <p>
											<label for="password">PASSWORD: </label>
											<input type="password" name="password" id="password" required/>
										  </p>
										  <p class="p-container">
											<input type="submit" name="go" id="go" value="Log in">
										  </p>
										</form>
								</table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>
   

</html>
