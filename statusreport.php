 <?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if(isset($_SESSION['updatedclaimid'])){
    	$updatedclaimid         = $_SESSION["updatedclaimid"];
    	$updatedjobnumber 		= $_SESSION["updatedjobnumber"];
		$formattedJobnumber = str_replace("/", "", $updatedjobnumber);
		$path       		= "uploads/$formattedJobnumber/status/";
		if (!file_exists($path)) {
		    mkdir($path, 0777, true);
		}

   //      $statusReportIssued          = "N";
   //      $statusreportIssued          = "N";    
   //      //Get claim details
   // $get_details = "select `statusreportIssued`, `statusReportIssued` from `claimmaster` where claimId = '$updatedclaimid'";
   //  $detailstmt       = mysqli_query($connection, $get_details); 
   //  $getcount   = mysqli_num_rows($detailstmt);
   //  if($getcount > 0){
        
   //    while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
   //      $statusReportIssued          = (empty($row['statusReportIssued']))        ? '' : $row['statusReportIssued'];
   //      $statusreportIssued          = (empty($row['statusreportIssued']))        ? '' : $row['statusreportIssued'];
   //      }
   //  }
    	$statusreportarray = array();
    	$statusfeesarray = array();

    	$get_details = "select `statusId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `statusupdates`  where claimId = '$updatedclaimid'";
	    $detailstmt       = mysqli_query($connection, $get_details); 
	    $getcount   = mysqli_num_rows($detailstmt);
	    if($getcount > 0){
	        
	      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
	        $statusId            = $row['statusId']; 
	        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
	        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
	        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
	        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));	        
	        if($updateType == "R"){
	        	$thisreportarray = array("Id" => $statusId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
	        	array_push($statusreportarray, $thisreportarray);
	        }
	        if($updateType == "F"){
	        	$thisfeesarray = array("Id" => $statusId, "content" => $updateContent, "date" => $createdDate);
	        	array_push($statusfeesarray, $thisfeesarray);
	        }
	    }
	}
    } else {
    	header("Location:open.php");
    }
    
}

if((isset($_FILES['statusreport']['name']))&&(!empty($_FILES['statusreport']['name']))){
	$statusreport    = (empty($_FILES['statusreport']['name']))  ? '' : $_FILES['statusreport']['name'];
	$fullfilename  		= "";
	if($statusreport){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $statusreport);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['statusreport']['tmp_name'], $fullpath);
  	}
  	$insert_photo = "insert into `statusupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'R', '$fullfilename', '$originalname', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_photo);
  	$statusfee    = (empty($_REQUEST['statusfee']))  ? '' : $_REQUEST['statusfee'];
  	$insert_notes = "insert into `statusupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'F', '$statusfee', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_notes);
  	$update_status = "update claimmaster set jobStatus = 'S', statusDate = curdate() where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:statusreport.php?r=success");
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>


<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped" border=0>
									<tbody>
                                        <tr height="50">
											<td colspan="3">
												<h5><strong>Status report: </strong></h5>
											</td>
											
										</tr>
										<?php 
										$reportcount = 0;
										
											foreach ($statusreportarray as $statusreports) {
												// print_r($statusfeesarray);
												$reportcount++;
												?>
												<tr>
													<td><?php echo $reportcount; ?> : <a href="<?php echo $path.$statusreports['content'];?>" target="_blank"><?php echo $statusreports['name'];?></a></td>
													<td><?php echo $statusfeesarray[$reportcount-1]['content'];?></td>
													<td>Uploaded on : <?php echo $statusreports['date'];?><!-- <input type="hidden" id="print<?php $reportcount;?>" value="<?php echo $statusreports['content'];?>"> --></td>
													<td><!-- <button class="btn btn-info btn-fill pull-right printbuttons" id="<?php $reportcount;?>">PRINT</button> --></td>
												</tr>
												<?php
											}
										?>
										<tr height="80">
											<td colspan="3">
												<form method="POST" action="" enctype="multipart/form-data"  onsubmit="return confirm('Click OK to submit the form.');">
												Select a file to attach  <span class="mandatorystar">*</span>:<input type="file" name="statusreport" id="statusreport" required /></br>
												<input style="width:250px" type="text" value="" id="statusfee" name="statusfee" placeholder="Enter status fees" class="onlynumbers form-control" required />
												<table width="40%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/>
													</td>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/>
													</td>
												</tr>
												</table>
												</form>
											</td>
											
											
										</tr>
                                    </tbody>
                                </table>
                                <table align="center">
									<tr>
										<td>
											<a href="report.php?id=<?php echo $updatedclaimid;?>"><button class="btn btn-info btn-fill">BACK</button></a>
										</td>
									</tr>
								</table>
                            </div>
                        </div>
                    </div>

    </div>
</div>


</body>
	<script>
	function isNumber(evt) {
    			evt = (evt) ? evt : window.event;
    			var charCode = (evt.which) ? evt.which : evt.keyCode;
    			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        		return false;
   			 }
    			return true;
		}
	</script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		// $('.printbuttons').click(function(){
    		// 	var id = $(this).attr("id");
    		// 	var report = $("#print"+id).val();
    		// 	$.print(report);
    		// });
    	});
    </script>
   

</html>