
<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    $updatedclaimid         = $_SESSION["updatedclaimid"];
    // $updatedclaimid         = "3";

    //Get claim details
   $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `reserveAmount`, `projectedFee` from `claimmaster` where claimId = '$updatedclaimid'";
    $detailstmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($detailstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
        $claimId            = $row['claimId']; 
        $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
        $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
        $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
        $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
        $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
        $claimReference     = (empty($row['claimReference']))   ? '' : $row['claimReference'];
        $brokerReference    = (empty($row['brokerReference']))  ? '' : $row['brokerReference'];
        $contactPerson      = (empty($row['contactPerson']))    ? '' : $row['contactPerson'];
        $contactNumber      = (empty($row['contactNumber']))    ? '' : $row['contactNumber'];
        $insurerContact     = (empty($row['insurerContact']))   ? '' : $row['insurerContact'];
        $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
        $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
        $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
        $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
        $subId              = (empty($row['subId']))            ? '' : $row['subId'];
        $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
        $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
        $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
        $contactTime        = (empty($row['contactTime']))      ? '' : date('h:i A',strtotime($row['contactTime']));
        $contactDate        = (empty($row['contactDate']))      ? '' : date('d M, Y',strtotime($row['contactDate']));
        $contactmade        = $contactDate. ", ".$contactTime;
        $surveyTime         = (empty($row['surveyTime']))       ? '' : date('h:i A',strtotime($row['surveyTime']));
        $preliminary        = (empty($row['preliminaryDate']))  ? '' : date('d M, Y',strtotime($row['preliminaryDate']));
        // $preliminary        = $preliminaryDate));
        $startDate          = (empty($row['startDate']))        ? '' : date('d M, Y',strtotime($row['startDate']));
        $startTime          = (empty($row['startTime']))        ? '' : date('h:i A', strtotime($row['startTime']));
        $endDate            = (empty($row['endDate']))          ? '' : date('d M, Y',strtotime($row['endDate']));
        $endTime            = (empty($row['endTime']))          ? '' : date('h:i A', strtotime($row['endTime']));
        $surveyDate         = (empty($row['surveyDate']))       ? '' : date('d M, Y',strtotime($row['surveyDate']));
        $surveyset          = $surveyDate. ", ".$surveyTime;
        $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
        $projectedFee          = (empty($row['projectedFee']))        ? '' : $row['projectedFee'];
        $reserveAmount          = (empty($row['reserveAmount']))        ? '' : $row['reserveAmount'];
        $jobStatusText = "";
        if($jobStatus == "O"){
            $jobStatusText = "Open";
        } elseif ($jobStatus == "V") {
            $jobStatusText = "Visit";
        } elseif ($jobStatus == "P") {
            $jobStatusText = "Preliminiary";
        } elseif ($jobStatus == "W") {
            $jobStatusText = "Working";
        } elseif ($jobStatus == "C") {
            $jobStatusText = "Closed";
        } elseif ($jobStatus == "I") {
            $jobStatusText = "Invoiced";
        } elseif ($jobStatus == "R") {
            $jobStatusText = "Recipt";
        } elseif ($jobStatus == "S") {
            $jobStatusText = "Status Report Issued";
        } else {
            $jobStatusText = "";
        }
      }
  }
  //Client name
  $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
        $clientstmt       = mysqli_query($connection, $get_client); 
        $getclientcount   = mysqli_num_rows($clientstmt);
        if($getclientcount > 0){
            
          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
            $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
          }
      }
  //Broker name
    $get_brokers = "select  `employeeId`, `firstName`, `lastName` from `brokers` where brokerId = '$brokerId'";
    $brokerstmt       = mysqli_query($connection, $get_brokers); 
    $getbrokercount   = mysqli_num_rows($brokerstmt);
    if($getbrokercount > 0){        
      while($brokerrow = mysqli_fetch_array($brokerstmt, MYSQLI_ASSOC)){
        $brokeremployeeId   = (empty($brokerrow['employeeId']))     ? '' : $brokerrow['employeeId'];
        $brokerfirstName    = (empty($brokerrow['firstName']))      ? '' : $brokerrow['firstName'];
        $brokerlastName     = (empty($brokerrow['lastName']))       ? '' : $brokerrow['lastName'];
      }
  }

  //Adjuster name
    $get_adjusters = "select `firstName`, `lastName` from `adjusters` where adjusterId = '$adjusterId'";
    $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
    $getadjustercount   = mysqli_num_rows($adjusterstmt);
    if($getadjustercount > 0){
        
      while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
        $adjusterfirstName    = (empty($adjusterrow['firstName']))      ? '' : $adjusterrow['firstName'];
        $adjusterlastName     = (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
      }
     }
     //Nature of loss
    $get_subcategories = "select t1.name, t1.prefix, t2.category from `subcategories` as t1, categorymaster as t2 where t1.subId = '$subId' and t1.categoryId = t2.categoryId";
    // echo $get_subcategories;exit;
            $stmt       = mysqli_query($connection, $get_subcategories); 
            $getcount   = mysqli_num_rows($stmt);
            if($getcount > 0){
                
              while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                $categoryname       = (empty($row['category']))         ? '' : $row['category'];
                $subcategoryname     = (empty($row['name']))       ? '' : $row['name'];
            }
        }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
                <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="report.php?id=<?php echo $updatedclaimid;?>"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
								<h3 align="center"> REPORT UPDATED </h3>

                                <p class="category">Updated Report</p>
                                <p class="category"><?php echo $jobNumber;?></p>
                            </div>
                            <div  id="printcontent">
                                 <div class="col-xs-12 printheader paddingrl0" align="center">
                                    <!-- <img src="assets/img/header.png"> -->
                                    <div style="height:60px;"></div>
                                    <h3>REPORT UPDATED</h3>
                                </div>
                            <div class="content table-responsive table-full-width" id="wordcontent">
                                <table class="table table-hover table-striped" id="printtopdftable">
								
									<tbody>
                                        <tr height="80" style="display:none;">
                                            <td>REPORT UPDATED</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr height="80" style="display:none;">
                                            <td>REPORT UPDATED</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
										<tr height="80">
                                        	<td>Claim number: </td>
                                        	<td><?php echo $jobNumber;?></td>
                                            <td>Insured: </td>
                                        	<td><?php echo $insuredName;?></td>
                                        </tr>
										<tr height="80">
                                        	<td>Policy number: </td>
                                        	<td><?php echo $policyNumber;?></td>
											<td>Insurer: </td>
                                        	<td><?php echo $clientName;?></td>
                                        </tr>
                                        <tr height="80">
                                            <td>Broker: </td>
                                            <td><?php echo $brokerfirstName." ".$brokerlastName;?></td>
                                            <td>Broker reference: </td>
                                            <td><?php echo $brokerReference;?></td>
                                        </tr>
                                        <tr height="80">
                                            <td>Claim Reference: </td>
                                            <td><?php echo $claimReference;?></td>
                                            <td>Contact Person: </td>
                                            <td><?php echo $contactPerson;?></td>
                                            
                                        </tr>
                                        <tr height="80">
                                            <td>Contact Number: </td>
                                            <td><?php echo $contactNumber;?></td>
                                            <td>Insurer Contact: </td>
                                            <td><?php echo $insurerContact;?></td>
                                            
                                        </tr>
										<tr height="80">
											<td>Nature of loss: </td>
                                        	<td><?php echo $subcategoryname." - ".$categoryname;?></td>
    										<td>Adjuster: </td>
                                        	<td><?php echo $adjusterfirstName." ".$adjusterlastName; ?></td>
                                        </tr>
										<tr height="80">
                                        	<td>Date of Preliminary report: </td>
                                        	<td><?php echo $preliminary;?></td>
											<td>Date of instruction: </td>
                                        	<td><?php echo $instruction;?></td>
                                        </tr>
                                        <tr height="80">
                                            <td>Contact Made: </td>
                                            <td><?php echo $contactmade;?></td>
                                            <td>Survey Date: </td>
                                            <td><?php echo $surveyset;?></td>
                                        </tr>
										<tr height="80">
                                        	<td>Start date: </td>
                                        	<td><?php echo $startDate;?></td>
											<td>Start time: </td>
                                        	<td><?php echo $startTime;?></td>
                                        </tr>
										<tr height="80">
                                        	<td>End date: </td>
                                        	<td><?php echo $endDate;?></td>
											<td>End time: </td>
                                        	<td><?php echo $endTime;?></td>
                                        </tr>
                                        <tr height="80">
                                            <td>Projected Fee: </td>
                                            <td><?php echo $projectedFee;?></td>
                                            <td>Reserve Amount: </td>
                                            <td><?php echo $reserveAmount;?></td>
                                        </tr>
									</tbody>
                                </table>
								
                            </div>
                            </div>
                        </div>
                    </div>
				<table width="40%" align="right">
				<tr>
					<td align="center">
						<button class="btn btn-info btn-fill pull-right printbutton">PRINT</button>
					</td>
					<td align="center">
						<button class="btn btn-info btn-fill pull-right" id="word-export">SAVE AS WORD</button>
					</td>
					<td align="center">
                        <button class="btn btn-info btn-fill pull-right" id="saveaspdf">SAVE AS PDF</button>
                    </td>
				</tr>
				</table>

    </div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script src="assets/js/FileSaver.js"></script> 
    <script src="assets/js/jquery.wordexport.js"></script> 
    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript" src="assets/js/jspdf.min.js"></script>
    <script type="text/javascript" src="assets/js/jspdf.plugin.autotable.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.printbutton').click(function(){
                $('#printcontent').print();
            });
            $("#word-export").click(function(event) {
                $("#wordcontent").wordExport("UpdatedClaim");
            });
            $("#saveaspdf").click(function(){
                var doc = new jsPDF('p', 'pt');
                var elem = document.getElementById("printtopdftable");
                var res = doc.autoTableHtmlToJson(elem);
                doc.autoTable(res.columns, res.data);
                var imgData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA/UAAABFCAYAAAALmcYRAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA44SURBVHhe7d1faGRnGcfxveiFghUREaGCXvSihQpSpAgKLtibgjeKsL0ywnq3lizIrmDjRbwwpTQi3Vaam7X+2YslhbTUEIkEgimRECRGWdeSTNkkbWaSNpNkJ5Ns0l0ezzMzz+Sdk/fMTGbmnJnTfF/4kOS87znvzMnV77zved9zQqFQKBQKhUKhUCgUCiVVZWVlTQ4O7gmhnkKhUCgUCoVCoVAolJSVaqh/9/wzAgAAAAAA0oNQDwAAAABAShHqAQAAAABIKUI9AAAAAAApRagHAAAAACClCPUAAAAAAKQUoR4AAAAAgJQi1AMAAAAAkFKEegAAAAAAUopQDwAAAABAShHqAQCxKrzzD4mzPCjsefs9ym1UWsRTiguL3n5ffeJJOXfuXEse+8zD3msCAABEIdQDAGL1wcCvKjE4vpJ74Tcn+s2/8WalNr7y3rM/PtGvevihh7yhvZGhx77mvR4AAEAUQj0AIHYP9vYqMTie4hs118Add9EHB+F+1Y++/BVvaK9HHwTMf/tp7/UAAACiEOoBALHr1qj5veVMpTaeEjX1f+qb3/EG93q+/6VHvNcCAACoh1APAIjdnZ9cqsTg+Ipv1Fyn5cddfFP/1VOf+7w3vEd58xvf8l4HAACgHkI9ACAR3Rg1X/reDyu18RVdCDDcr9L3433h3YcF8gAAQKsI9QCARGy+MlKJwfEV36j57l//VqmNr7S7YN4vHn3cez4AAEAjhHoAQCK6NWqexOr7H73+5xP9qmYXzGOBPAAA0CpCPQAgMXHvWa/FN2oe9571R9nciT5VMwvmNVogT2c47C8slvjqlbZRvjq1/69/l87PvjBcPZZ/Y6zhdZWeE9Uu++Jv5e70jOTH3j4+5rRvRD+DnaefsVFZvXy11Fbv63NffbTlLQD/+PWnmpododfXfrS9r179NKjXz+Or0/Pa+ZwAADSDUA8ASEy3Rs2TWH1fv1u4X9Vowbx6gVG598xCbdjHuQ15UCh46/QcK+75boh224fp/bQSrtt+8+3Scfehhtu+UdHPYOedJtTrPdN7p/fWzj8N+580WpzQZlpE9fPqE0+W6rWdr/67X/hiqZ6dDQAAcSLUAwASFfee9b5R8yT2rG9lwbxHPvVp7zku97UF3wMLN7T7HixEhfK4Qn1ppD64tnEXSNSHD26db6Re10DQPn0yz/aV2rYT6jXI2/1vFLYttCtfvYX2qP+jranASD0AIE6EegBAoro1ah736vtafFP/9X35qAXzml0gzwKv/gzXuQsBFt6ZPVFv54br7LgW93iYhmkr4TpfqA9zHzrotXxtlH0edzZBlHZCvQZ5939Qbz0DrbN2GvCj6nz17sODqOn5AAB0AqEeAJCoJPas942aJ7FnfVRoDQdJ0+wCee7OATpyb8d9iw/aaLaxEn7n/iyG+nAQV/pOvK+t0e0GtV14ir0+kHGvo6P2vvpmZmMAANAOQj0AIHHdGDVPYvX9qHDrjtqa07xn7T4IcWch6FR3Kzq1XYsbnN1AHQ77ZzHUa4DX83TmhL0v/9ngd19bY+0eD8K9e1zDuh63n8p9SGNT86PetwcAoFMI9QCAxLkjz3EVX4BMYs/6qAXzbMTXaDD1tYtiaxG476HbwxGdWq/HtbgB2wK5Bn47Zno11O9OTMpHv/+TV7vv1NtrEBq0NYA388679aUstLsPaXTavQV793UKu3Z4Wj4AAJ1GqAcAJK5bo+buSvJxlWYWzGtlSrY9kLi3tFz6Ozx6r4HXij1YqAbl4Fz3Wm7daUr4GnGE+nrFRvFbCfXu/bd33G3kvtH/w86z8G+vU9h54dF8N/Q3+4oFAACtItQDALoiiT3rfaPmce9Zr6XRgnmN3uP2cafaa4C3kXkdwbc27si9/m3Fdx/OWqi3mRLuaw/u/6TezAntx87Vc3TKvvt/dEO8/m7v02uf4WsBANBphHoAQFd0a9Q8idX3o8KrjfC2shq6O7tBA75vOr77WoN7f93F9YwbosN1rqSn31twr+e0od6dQv+D4H/wXBDGjY6u6/HwQncud0TfN+KvbAq+jtrbQwDepwcAJIFQDwDomrj3rNcSHjVPYs/6qJCro7j1wmMjNhKv17fiLoDnBv8HhULpp57jXsOcpVBvD1MaiXrY4j4UsIcA4b5tdN5G8RXv0wMAkkCoBwB0TbdGzZNYfX/t8s9P9Kva2bPcptxb0SAcbqNT790SFaTPSqjX+61tdZq9nudj78S7U/PDbJq+CS+uZ/243HoAAOJCqAcAdE0Se9b7AmcSe9b7FqdrlxuQteg0/HCb8GsNUSH5rIR6mzpfbyq8BXIdZY9a2M62qFMa8H1t3B0Omp1FAABAuwj1AICu6saoeRKr72vxvcveLntlQX9GXd9t46tXZyHUu4vaNZohYaE9ahFDm16voh4QuO/bt7IYIgAArSDUAwC6Kok9632j5knsWa/fLdxvu2x6fb2ZADZN31bB9+nVUF+vWOC3UK+L07mL3hmt13YWspsZNdf33+2avvrwCve+NvoQwdrYZwAAIG6EegBAV3Vr1DyJ1ffrhd1W2dZ2+uqCr17Zaw31Hip8EkJ9FBsltxXpm12wztqH35c3Ou2+0TZ1NuLvqwMAIA6EegBA1yWxZ70v4CaxZ33Ugnmt0tXu64Vjo23clfHD9OGAtml0LQ3SUe0+GPx1Kdh/+IcbJ+qMfV5lodzH/TxR7PvoVHoN7lE09Ououf0d7iuKhn9tH/UQQMN+oxF4HcWPeigAAEAcCPUAgK7r1qi5BsW4SxwL5gEAABhCPQCgJySxZ3141DyJPeu1xLFgHgAAgCLUAwB6QhJ71vtGzYtNvMvdboljwTwAAABFqAcA9IQk9qzXEh41T2LP+jgWzAMAAFCEegBAz0hiz/rwqLmG/G5M/QcAAOgEQj0AoGdo4I67+EbNk9izvrUF8wYkPzMvu2+NyfbUouzP/EXyS6tSvOZr6+qTzAXfcZ9B2VnJOde8LNmpZfl4aVY2nnfbnVaf5OeCa5w47vR347YcTA2G6sNO813KMtdmpTgzLXtTr3nqpoO6cdmZW5a90do6AADSiFAPAOgZSe1ZH+5XR9HjLjoLIdxvI9m5nBy8ZdvSXZXNkZfl3YlFKY4MyPrzVyWjxy9clfcHh2TtYhBY+wdktf8l2frnshRHB+TO+UuyFtS93x8E41LdQPD7L6vHNDCvPj8k+VtuqA9cW5TDBTcQl9utX6nsjX8x6L/SZ83v1faB/knZy7qfX4X6C85du6ifcVBWL9jP8vHyNftkfep29Hd58SVv3xsLq8E9uikFT6jXuv0R/R6vyca1k32Xr/2KfPj670rfN9M/WPne1v/x/Tuuq70/jT4fAACdRKgHAPSUuPes1+v7+o17z3rdPs/Xbz06ol3oDx2fuB0E5WHZXlqVvcFnZHNhUbbPj8vB0qxk55blYGpc8v8NQn0Qmjdm5mVn6LoUs0HbSl0x87/KseXg2KIUrvRJfql+qN9YuF1qVwrLQbudW7clf/GmbI3U/l49P5CdmJTN/mk5yk7Lqh2r6S8Iwm8ty/5EJYTr5/Vcv/QQI+K73MuunOz7wpDk53JyP+h37cK07C+NlR8UWP2VseAaBbm/Mi25IGzX9n2peg93/rMi+/pAYmRctq+4/S/L3ZnV8sOKG9OyO6Tnuvfn+BrezwcAQIcR6gEAPSXuPeujVqKPe8/6Vt6pzy9tlQKn/Z25EARJC7mVMLo2Oiu7N8Zkf2GyWmc/d4LAX5r+HpyX8Ry7G4TnreB3u1a175pQf1X2cquyo79PrMrR1GXJDE/L4daW7I9erfm9ev7567I7Nyn50bHSaP1eEHz1uIb1mv6CfoqeUF9zzTrfxde3vp5Q6NcHBwU5ujUpm6Gp+zpyriPr66P6isHYib6r9zD4DsWlWdkevVmaEVHTv9atzEthYrL0wGJnpfb+2DX89wYAgM4i1AMAek6cC9fpKvu+PuPcs16/j6/Phq5MymF2WXZGhiQ3Oi5bgz+T9Zll2b8xIFu3csHPS7Kjo94Ts3K48p4UMjqCHATtidtyOHdd8jM5ub+yKHtzf5ftuXLdRvWYvleeC9qNy+7KVlBn77aXR9A/XpmWzeGbQZidr4yMvyxbc4uycyUIsXPzsjk4LcW54Zrfy+dfkuxMcP2h8n2+E1zr/tJ4abR8I9xfJdRnJpblaGFSCpXjNdes813unuhbQ30Q5hemZTf4rAfZreBcfehxHKo3gu9SHH1Z1m8E9TPDob5fk2zl2hrk9fdiZZQ969y39eDv1amg3Wj51YK14BrH90dH6qM/HwAAnUaoBwD0nLj2rD/KbXj7M3HtWR815b9Zd/or789/kuiIdyXUe+vbcHy/+uTOxXLwNqXZDucvBcePj53Wnf4B2ZialE1PHQAASSPUAwB6Tlx71jdagT6uPev1ur7+zrLV0XnZn7kua5663tYnGzOLsjtc+7AAAIBuIdQDAHpSHHvW6/v6vr5MXHvW69R+X38AAADtItQDAHpSHKPmGtp9fbk6vWd9oyn/AAAA7SDUAwB6Uqf3rG92n/hO71mv6wP4+gEAAOgEQj0AoGd1ctT8o1PsE6+j650qjab8AwAAtINQDwDoWZ3cs/40+8TrA4BOlWam/AMAALSKUA8A6GmdGDU/7T7xndqzXrfI810fAACgUwj1AAAAAACkFKEeAAAAAICUItQDAAAAAJBShHoAAAAAAFKKUA8AAAAAQEoR6gEAAAAASClCPQAAAAAAKUWoBwAAAAAgpQj1AAAAAACkFKEeAAAAAICUItQDAAAAAJBShHoAAAAAAFKqGuozmYwAAAAAAID0qIb6w8NDAQAAAAAA6VEO9ffk/7tbuVOmfWc5AAAAAElFTkSuQmCC";
                doc.addImage(imgData, 'PNG', 40, 22, 520, 40);
                doc.save("ReceiptGenerated.pdf");
            });
        });
    </script>
   

</html>
