<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    //Get user details
    $get_details = "select `userId`, `userName`, `firstName`, `lastName`, `emailId`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `aboutMe`, `profilePicture` FROM `usermaster` WHERE userId = '$loggedin_userid' and active='A'";
    $stmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($stmt);
    if($getcount > 0){
      while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
        $userId         = (empty($row['userId']))           ? '' : $row['userId'];
        $userName       = (empty($row['userName']))         ? '' : $row['userName'];
        $firstName      = (empty($row['firstName']))        ? '' : $row['firstName'];
        $lastName       = (empty($row['lastName']))         ? '' : $row['lastName'];
        $emailId        = (empty($row['emailId']))          ? '' : $row['emailId'];
        $address        = (empty($row['address']))          ? '' : $row['address'];
        $city           = (empty($row['city']))             ? '' : $row['city'];
        $country        = (empty($row['country']))          ? '' : $row['country'];
        $postalCode     = (empty($row['postalCode']))       ? '' : $row['postalCode'];
        $aboutMe        = (empty($row['aboutMe']))          ? '' : $row['aboutMe'];
        $profilePicture = (empty($row['profilePicture']))   ? 'assets/img/faces/face-3.jpg' : "uploads/profile/".$row['profilePicture'];
      }
    } else {
        header("Location:logout.php");
    }
  }

  if((isset($_POST['userName'])) && (!empty($_POST['userName']))){
     // print_r($_FILES);exit;
    $userName           = (empty($_REQUEST['userName']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['userName']));
    $firstName          = (empty($_REQUEST['firstName']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['firstName']));
    $lastName           = (empty($_REQUEST['lastName']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['lastName']));
    $emailId            = (empty($_REQUEST['emailId']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailId']));
    $address            = (empty($_REQUEST['address']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['address']));
    $city               = (empty($_REQUEST['city']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['city']));
    $country            = (empty($_REQUEST['country']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['country']));
    $postalCode         = (empty($_REQUEST['postalCode'])) ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['postalCode']));
    $aboutMe            = (empty($_REQUEST['aboutMe']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['aboutMe']));
    $profilepicture     = (empty($_FILES['profilepicture']['name']))  ? '' : $_FILES['profilepicture']['name'];
    $path               = "uploads/profile/";
    $fullfilename       = "";
    if($profilepicture){
        $date          = round(microtime(true)*1000);
        $imgtype       = explode('.', $profilepicture);
        $ext           = end($imgtype);
        $filename      = $imgtype[0];
        $fullfilename  = $date.".".$ext;
        $fullpath      = $path . $fullfilename;
        move_uploaded_file($_FILES['profilepicture']['tmp_name'], $fullpath);
    }
    if($fullfilename != ""){
        $update_details = "update `usermaster` set `userName`='$userName',`firstName`='$firstName',`lastName`='$lastName',`emailId`='$emailId',`address`='$address',`city`='$city',`country`='$country',`postalCode`='$postalCode',`aboutMe`='$aboutMe',`profilePicture`='$fullfilename',`updatedBy`='$loggedin_userid' WHERE userId = '$loggedin_userid'";
    } else {
        $update_details = "update `usermaster` set `userName`='$userName',`firstName`='$firstName',`lastName`='$lastName',`emailId`='$emailId',`address`='$address',`city`='$city',`country`='$country',`postalCode`='$postalCode',`aboutMe`='$aboutMe',`updatedBy`='$loggedin_userid' WHERE userId = '$loggedin_userid'";
    }
   
    mysqli_query($connection, $update_details);
    header("Location:user.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>
        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="dashboard.php"><< Back to Dashboard</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Company (disabled)</label>
                                                <input type="text" name="company" id="company" class="form-control" disabled placeholder="Company" value="Whitelaw Chartered Loss Adjusters & Surveyors">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Username <span class="mandatorystar">*</span></label>
                                                <input type="text" name="userName" id="userName" class="form-control" placeholder="Username" value="<?php echo $userName;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="emailId">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" name="emailId" id="emailId" class="form-control" placeholder="Email" value="<?php echo $emailId;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name <span class="mandatorystar">*</span></label>
                                                <input type="text" name="firstName" id="firstName" class="form-control nonumbers" placeholder="First Name" value="<?php echo $firstName;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name <span class="mandatorystar">*</span></label>
                                                <input type="text" name="lastName" id="lastName" class="form-control nonumbers" placeholder="Last Name" value="<?php echo $lastName;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" name="address" id="address" class="form-control" placeholder="Home Address" value="<?php echo $address;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" name="city" id="city" class="form-control nonumbers" placeholder="City" value="<?php echo $city;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" name="country" id="country" class="form-control nonumbers" placeholder="Country" value="<?php echo $country;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" name="postalCode" id="postalCode" class="form-control onlynumbers" placeholder="Postal Code" value="<?php echo $postalCode;?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>About Me</label>
                                                <textarea rows="5" name="aboutMe" id="aboutMe" class="form-control" placeholder="Here can be your description"><?php echo $aboutMe;?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Profile Picture</label>
                                                <input type="file" name="profilepicture" id="profilepicture" accept="image/*">
                                            </div>
                                        </div>
                                    </div>
                                     <button type="reset" name="resetprofile" id="resetprofile" class="btn btn-info btn-fill pull-right" style="margin-right:10px;margin-left:10px;">Reset</button>
                                    <button type="submit" name="updateprofile" id="updateprofile" class="btn btn-info btn-fill pull-right">Update Profile</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                    <img class="avatar border-gray" src="<?php echo $profilePicture;?>" alt="..."/>

                                      <h4 class="title"><?php echo $firstName." ".$lastName;?><br />
                                         <small><?php echo $userName;?></small>
                                      </h4>
                                    </a>
                                </div>
                                <p class="description text-center"></p>
                                <div align="center" style="padding: 10px;"><a href="changepassword.php" style="color:#000;"><button class="btn btn-fill btn-info">Change Password</button></a></div>
                            </div>
                            <hr>
                            
                        </div>
                    </div>

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#userprofile').addClass("active");
        });
    </script>
   

</html>
