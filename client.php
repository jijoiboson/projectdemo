<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
								<h3 align="center"> CLIENT ADDED </h3>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
								
									<tbody>
										<tr height="80">
                                        	<td>First name: </td>
                                        	<td>XXXXXX</td>
                                            <td>Last name: </td>
                                        	<td>XXXXXX</td>
                                        </tr>
										<tr height="80">
                                        	<td>Reference ID: </td>
                                        	<td>XXXXXX</td>
											<td>Email: </td>
                                        	<td>XXXXXX</td>
                                        </tr>
										<tr height="80">
											<td>Phone: </td>
                                        	<td>XXXXXX</td>
											<td>City: </td>
                                        	<td>XXXXXX</td>
                                        </tr>
										<tr height="80">
											<td>Country: </td>
                                        	<td>XXXXXX</td>
                                        	<td>ZIP CODE: </td>
                                        	<td>XXXXXX</td>
                                        </tr>
										<tr>
											<td align="center" colspan="4">
												<form action="masters.php">
													<input class="btn btn-info btn-fill pull-right" type="submit" value="CLOSE"/>
												</form>
											</td>
										</tr>		
									</tbody>
                                </table>
								

                            </div>
                        </div>
                    </div>
				

    </div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>
   

</html>
