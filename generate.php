<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    $receiptNumber = mt_rand(1111111111,9999999999);
    if((isset($_REQUEST["id"])) && (!empty($_REQUEST["id"]))){
        $currentinvoiceid         = $_REQUEST["id"];
        $get_invoices = "select `invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `narrationText`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate` FROM `invoicemaster` where `invoiceId` = '$currentinvoiceid'";
        $detailstmt       = mysqli_query($connection, $get_invoices); 
        $getcount   = mysqli_num_rows($detailstmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
            $invoiceId          = $row['invoiceId']; 
            $claimId            = (empty($row['claimId']))          ? '' : $row['claimId'];
            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
            $invoiceNumber      = (empty($row['invoiceNumber']))    ? '' : $row['invoiceNumber'];
            $currency           = (empty($row['currency']))         ? '' : $row['currency'];
            $faoName            = (empty($row['faoName']))          ? '' : $row['faoName'];
            $toName             = (empty($row['toName']))           ? '' : $row['toName'];
            $yourReference      = (empty($row['yourReference']))    ? '' : $row['yourReference'];
            $invoiceDate        = (empty($row['invoiceDate']))      ? '' : $row['invoiceDate'];
            $invoiceDate        = date('d/m/Y',strtotime($invoiceDate));
            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
            $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
            $totalAmount        = (empty($row['totalAmount']))      ? '' : $row['totalAmount'];
            $totalInWords       = (empty($row['totalInWords']))     ? '' : $row['totalInWords'];
            $invoiceTerms       = (empty($row['invoiceTerms']))     ? '' : $row['invoiceTerms'];
            $narrationText      = (empty($row['narrationText']))   ? '' : $row['narrationText'];
        
            $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
            $clientstmt       = mysqli_query($connection, $get_client); 
            $getclientcount   = mysqli_num_rows($clientstmt);
            if($getclientcount > 0){
                
              while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
                $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
              }
          }
          $paidAmount = 0;
          $get_receipts = "select `receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom` from `receiptdetails` where invoiceId = '$invoiceId'";
          $receiptstmt       = mysqli_query($connection, $get_receipts); 
          $getreceiptcount   = mysqli_num_rows($receiptstmt);
          if($getreceiptcount > 0){
            while($row = mysqli_fetch_array($receiptstmt, MYSQLI_ASSOC)){
                    $receiptId          = $row['receiptId']; 
                    $receiptAmount      = (empty($row['receiptAmount']))          ? '' : $row['receiptAmount'];
                    $paidAmount  = $paidAmount + $receiptAmount;
                }
            }
            $balanceAmount = $totalAmount - $paidAmount;
            
        }
    }
} else {
    header("Location:receipt.php");
}
}
 if((isset($_POST['receiptdate'])) && (!empty($_POST['receiptdate']))){
     // print_r($_REQUEST);exit;
    $receiptdate       = (empty($_REQUEST['receiptdate']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['receiptdate']));
    $receiptamount     = (empty($_REQUEST['receiptamount']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['receiptamount']));
    $paymentmode        = (empty($_REQUEST['paymentmode']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['paymentmode']));
    $chequenumber       = (empty($_REQUEST['chequenumber']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['chequenumber']));
    $chequedate         = (empty($_REQUEST['chequedate']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['chequedate']));
    $receivedfrom       = (empty($_REQUEST['receivedfrom']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['receivedfrom']));
    //Insert receipt
    $Insert_receipt = "insert into `receiptdetails`(`receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom`, `createdBy`, `createdDate`) values ('$receiptNumber', '$invoiceId', '$receiptdate', '$receiptamount', '$paymentmode', '$chequenumber', '$chequedate', '$receivedfrom', '$loggedin_userid', now())";
    mysqli_query($connection, $Insert_receipt);
    $receiptId    = mysqli_insert_id($connection);
    $_SESSION['currentreceiptid'] = $receiptId;
    $update_status = "update claimmaster set jobStatus = 'R' where claimId = '$claimId'";
    mysqli_query($connection, $update_status);
    header("Location:upreceipt.php");
}   
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">


    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="receipt.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Receipt</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>INSURER</th>
                                    	<th>INVOICE NUMBER</th>
                                    	<th>INVOICE DATE</th>
                                    	<th>NARRATION</th>
                                    	<th>INVOICE AMOUNT</th>
                                    	<th>BALANCE AMOUNT</th>
										<!-- <th></th> -->
                                    </thead>
                                    <tbody>
                                        <tr>
                                        	<td><?php echo $clientName;?></td>
                                            <td><?php echo $invoiceNumber;?></td>
                                            <td><?php echo $invoiceDate;?></td>
                                            <td><?php echo $narrationText;?></td>
                                            <td><?php echo number_format($totalAmount);?></td>
                                            <td><?php echo number_format($balanceAmount)." ".$currency;?></td>
                                        	<!-- <td>
												<a href="receipt.php"><button class="btn btn-info btn-fill pull-right">CANCEL</button></a>
											</td> -->
                                        </tr>
																			
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
		<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">View / Update Receipt</p>
                            </div>
                            <div class="content table-responsive table-full-width">
							<form action="#" method="POST">
								<table class="table table-hover table-striped">
                                    <tbody>
                                        <tr>
                                        	<td>Receipt number</td>
                                        	<td>:</td>
                                        	<td><?php echo $receiptNumber;?></td>
											<td>Receipt date <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text"  name="receiptdate" id="receiptdate" placeholder="Select Date" class="form-control" required/></td>
                                        </tr>
										<tr>
                                        	<td>Invoice number</td>
                                        	<td>:</td>
                                        	<td><?php echo $invoiceNumber;?></td>
											<td>Insurer</td>
                                        	<td>:</td>
                                        	<td><?php echo $clientName;?></td>
                                        </tr>
										<tr>
                                        	<td>Job Number</td>
                                        	<td>:</td>
                                        	<td><?php echo $jobNumber;?></td>
                                        	<td>Balance Amount</td>
                                        	<td>:</td>
                                        	<td><?php echo $balanceAmount;?></td>
                                        </tr>
										<tr>
                                        	<td>Receipt Amount <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input type="number" class="onlynumbers form-control" style="width:200px;" name="receiptamount" id="receiptamount" placeholder="Amount" max="<?php echo $balanceAmount;?>" title="Amount cannot be more than the balance amount." required /></td>
											<td>Mode of Payment <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td>
												<select style="width:200px;" name="paymentmode" id="paymentmode" class="form-control" required>
													<option value="Cheque">Cheque</option>
													<option value="Cash">Cash</option>
												</select>
											</td>
                                        </tr>
										<tr id="chequediv">
                                        	<td>Cheque Number <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" class="form-control" name="chequenumber" id="chequenumber" required></td>
											<td>Cheque date <span class="mandatorystar">*</span></td>
                                        	<td>:</td>
                                        	<td><input style="width:200px;" type="text" class="form-control" name="chequedate" id="chequedate" placeholder="Select Date" required></td>
										</tr>
										<tr>
											<td>Received from <span class="mandatorystar">*</span></td>
											<td>:</td>
											<td><input type="text" style="width:200px;" class="form-control" placeholder="Name" name="receivedfrom" id="receivedfrom" class="nonumbers" required/></td>
											<!-- <td>Narration</td>
											<td>:</td>
											<td><?php echo $narrationText;?></td> -->
										</tr>		
										<tr>
                                        	<td colspan="6"><input class="btn btn-info btn-fill pull-right" type="submit" value="SAVE"/> <a href="receipt.php"><div class="btn btn-info btn-fill pull-right marginrl10">CANCEL</div></a></td>
										</tr>	
                                    </tbody>
									</form>
                                </table>
							</div>
                        </div>
                    </div>
		

    </div>
</div>


</body>
    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jQuery-ui.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#accounts').addClass("active");
            $('#receiptdate').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#chequedate').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#paymentmode').on('change', function() {
              var mode = this.value;
              if(mode == "Cheque"){
                $('#chequediv').show();
                $("#chequenumber").prop('required',true);
                $("#chequedate").prop('required',true);
              } else {
                $('#chequediv').hide();
                $("#chequenumber").prop('required',false);
                $("#chequedate").prop('required',false);
              }
          });
        });
    </script>
   

</html>
