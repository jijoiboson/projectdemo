<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}
  if((isset($_POST['userName'])) && (!empty($_POST['userName']))){
     // print_r($_POST);exit;
    $userName           = (empty($_REQUEST['userName']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['userName']));
    $firstName          = (empty($_REQUEST['firstName']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['firstName']));
    $lastName           = (empty($_REQUEST['lastName']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['lastName']));
    $emailId            = (empty($_REQUEST['emailId']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailId']));
    $address            = (empty($_REQUEST['address']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['address']));
    $city               = (empty($_REQUEST['city']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['city']));
    $country            = (empty($_REQUEST['country']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['country']));
    $postalCode         = (empty($_REQUEST['postalCode'])) ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['postalCode']));
    $isadmin            = (empty($_REQUEST['isadmin']))     ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['isadmin']));
    $useraccess         = $_REQUEST['useraccess'];
    $useraccessstring   = implode(",", $useraccess) ;
    // echo $useraccessstring;exit;
    $password           = md5('password');
    $insert_details = "insert into `usermaster`(`userName`, `firstName`, `lastName`, `emailId`, `password`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `active`, `createdBy`, `createdDate`, `userAccess`) values ('$userName', '$firstName', '$lastName', '$emailId', '$password', '$isadmin', '$address', '$city', '$country', '$postalCode', 'A', '$loggedin_userid', now(), '$useraccessstring')";
    mysqli_query($connection, $insert_details);
    header("Location:userlist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="admin.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add user</h4>
                            </div>
                            <div class="content">
                                <form method="POST" action="">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Company (disabled)</label>
                                                <input type="text" name="company" id="company" class="form-control" disabled placeholder="Company" value="Whitelaw Chartered Loss Adjusters & Surveyors">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Username <span class="mandatorystar">*</span></label>
                                                <input type="text" name="userName" id="userName" class="form-control" placeholder="Username" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" name="emailId" id="emailId" class="form-control" placeholder="Email" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name <span class="mandatorystar">*</span></label>
                                                <input type="text" name="firstName" id="firstName" class="form-control nonumbers" placeholder="First Name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name <span class="mandatorystar">*</span></label>
                                                <input type="text" name="lastName" id="lastName" class="form-control nonumbers" placeholder="Last Name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" name="address" id="address" class="form-control" placeholder="Home Address" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" name="city" id="city" class="form-control nonumbers" placeholder="City" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" name="country" id="country" class="form-control nonumbers" placeholder="Country" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" name="postalCode" id="postalCode" class="form-control" placeholder="Postal Code">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Is Admin? <span class="mandatorystar">*</span></label>
                                                <select name="isadmin" id="isadmin" class="form-control">
                                                    <option value="N">No</option>
                                                    <option value="Y">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Select User Access<span class="mandatorystar">*</span></label>
                                                <div>
                                                   <!--  <input type="checkbox" value="1" class="useraccess[]" name="useraccess[]" id="useraccess1" checked> New Claim -->
                                                    <input type="checkbox" value="2" class="useraccess[]" name="useraccess[]" id="useraccess2" checked> Claim Files
                                                    <input type="checkbox" value="3" class="useraccess[]" name="useraccess[]" id="useraccess3" checked> Accounts
                                                    <input type="checkbox" value="5" class="useraccess[]" name="useraccess[]" id="useraccess5" checked> Time and Expense
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
									<button type="submit" class="btn btn-info btn-fill pull-right">ADD USER</button> <a href="userlist.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#adminpanel').addClass("active");
        });
    </script>
   

</html>
