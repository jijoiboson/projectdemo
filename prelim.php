 <?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if(isset($_SESSION['updatedclaimid'])){
    	$updatedclaimid         = $_SESSION["updatedclaimid"];
    	$updatedjobnumber 		= $_SESSION["updatedjobnumber"];
		$formattedJobnumber = str_replace("/", "", $updatedjobnumber);
		$path       		= "uploads/$formattedJobnumber/prelim/";
		if (!file_exists($path)) {
		    mkdir($path, 0777, true);
		}

   //      $statusReportIssued          = "N";
   //      $prelimReportIssued          = "N";    
   //      //Get claim details
   // $get_details = "select `prelimReportIssued`, `statusReportIssued` from `claimmaster` where claimId = '$updatedclaimid'";
   //  $detailstmt       = mysqli_query($connection, $get_details); 
   //  $getcount   = mysqli_num_rows($detailstmt);
   //  if($getcount > 0){
        
   //    while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
   //      $statusReportIssued          = (empty($row['statusReportIssued']))        ? '' : $row['statusReportIssued'];
   //      $prelimReportIssued          = (empty($row['prelimReportIssued']))        ? '' : $row['prelimReportIssued'];
   //      }
   //  }
    	$prelimreportarray = array();
    	$prelimfeesarray = array();
    	$prelimamountarray = array();
    	// $prelimstatusarray = array();

    	$get_details = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `prelimDate`, `createdDate` FROM `prelimupdates`  where claimId = '$updatedclaimid'";
	    $detailstmt       = mysqli_query($connection, $get_details); 
	    $getcount   = mysqli_num_rows($detailstmt);
	    if($getcount > 0){
	        
	      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
	        $prelimId            = $row['prelimId']; 
	        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
	        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
	        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
	        $prelimDate   		= (empty($row['prelimDate']))  ? '' : $row['prelimDate'];
	        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));	        
	        if($updateType == "R"){
	        	$thisreportarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $prelimDate, "name" => $originalFilename);
	        	array_push($prelimreportarray, $thisreportarray);
	        }
	        if($updateType == "F"){
	        	$thisfeesarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $createdDate);
	        	array_push($prelimfeesarray, $thisfeesarray);
	        }
	        if($updateType == "A"){
	        	$thisamountarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $createdDate);
	        	array_push($prelimamountarray, $thisamountarray);
	        }
	        // if($updateType == "S"){
	        // 	$thisamountarray = array("Id" => $prelimId, "content" => $updateContent, "date" => $createdDate);
	        // 	array_push($prelimstatusarray, $thisamountarray);
	        // }
	    }
	}
    } else {
    	header("Location:open.php");
    }
    
}

if((isset($_FILES['prelimreport']['name']))&&(!empty($_FILES['prelimreport']['name']))){
	$prelimreport    = (empty($_FILES['prelimreport']['name']))  ? '' : $_FILES['prelimreport']['name'];
	$prelimdate  = (empty($_REQUEST['prelimdate']))  ? '' : $_REQUEST['prelimdate'];
	$fullfilename  		= "";
	if($prelimreport){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $prelimreport);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['prelimreport']['tmp_name'], $fullpath);
  	}
  	$insert_photo = "insert into `prelimupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `prelimDate`,  `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'R', '$fullfilename', '$originalname', '$prelimdate', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_photo);
  	$update_status = "update claimmaster set jobStatus = 'P', preliminaryDate = curdate() where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:prelim.php?r=success");
}
if((isset($_REQUEST['projectedfees']))&&(!empty($_REQUEST['projectedfees']))){
	$projectedfees    = (empty($_REQUEST['projectedfees']))  ? '' : $_REQUEST['projectedfees'];
	$reserveamount    = (empty($_REQUEST['reserveamount']))  ? '' : $_REQUEST['reserveamount'];
  	$insert_notes = "insert into `prelimupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'F', '$projectedfees', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_notes);
  	$insert_notes = "insert into `prelimupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'A', '$reserveamount', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_notes);
  	$update_status = "update claimmaster set jobStatus = 'P', preliminaryDate = curdate() where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:prelim.php?f=success");
}
// if((isset($_REQUEST['statusfee']))&&(!empty($_REQUEST['statusfee']))){
// 	$statusfee    = (empty($_REQUEST['statusfee']))  ? '' : $_REQUEST['statusfee'];
//   	$insert_notes = "insert into `prelimupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'S', '$projectedfees', '$loggedin_userid', now())";
//   	mysqli_query($connection, $insert_notes);
//   	$update_status = "update claimmaster set jobStatus = 'S', statusReportIssued = 'Y', statusDate = curdate() where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
//   	header("Location:prelim.php?f=success");
// }
// if((isset($_REQUEST['prelimreportissued']))){
// 	$update_status = "update claimmaster set prelimReportIssued = 'Y', jobStatus = 'P' where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
// 	header("Location:prelim.php?r=success");
// } else {
// 	$update_status = "update claimmaster set prelimReportIssued = 'N' where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
// 	//header("Location:prelim.php?r=success");
// }
// if((isset($_REQUEST['statusreportissued']))){
// 	$update_status = "update claimmaster set statusReportIssued = 'Y', jobStatus = 'S' where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
// 	header("Location:prelim.php?r=success");
// } else {
// $update_status = "update claimmaster set statusReportIssued = 'N' where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
// 	//header("Location:prelim.php?r=success");
// }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>


<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped" border=0>
									<tbody>
                                        <tr height="50">
											<td colspan="3">
												<h5><strong>Preliminary report: </strong></h5>
											</td>
											
										</tr>
										<?php 
										$reportcount = 0;
										// print_r($prelimreportarray);
											foreach ($prelimreportarray as $prelimreports) {

												$reportcount++;
												?>
												<tr>
													<td><?php echo $reportcount; ?> : <a href="<?php echo $path.$prelimreports['content'];?>" target="_blank"><?php echo $prelimreports['name'];?></a></td>
													<td><?php echo $prelimreports['date'];?><!-- <input type="hidden" id="print<?php $reportcount;?>" value="<?php echo $prelimreports['content'];?>"> --></td>
													<td><!-- <button class="btn btn-info btn-fill pull-right printbuttons" id="<?php $reportcount;?>">PRINT</button> --></td>
												</tr>
												<?php
											}
										?>
										<tr height="80">
											<td colspan="3">
												<form method="POST" action="" enctype="multipart/form-data">
												Select a file to attach  <span class="mandatorystar">*</span>:<input type="file" name="prelimreport" id="prelimreport" required  style="display: inline-block;" /> <input style="width:200px;display: inline-block;" type="text" name="prelimdate" class="dateinputs form-control" id="prelimdate" placeholder="Report Date" required/>
												<table width="40%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/>
													</td>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/>
													</td>
												</tr>
												</table>
												</form>
											</td>
											
											
										</tr>
		
										<!-- <tr height="50">
											<td>
												<h5><strong>Print report	: </strong></h5>
											</td>
											
										</tr>
										<tr height="80">
											<td>
												<table width="40%" align="right">
												<tr>
													<td>
														<input align="center" class="btn btn-info btn-fill pull-right" type="submit" value="PRINT"/>
													</td>
													
													<td>
														<input align="center" class="btn btn-info btn-fill pull-right" type="submit" value="SAVE "/>
													</td>
													
												</tr>
												</table>
											</td>
										</tr> -->
										<tr height="50"  style="display: none;">
											<td>
												<h5><strong>Email to insurer: <span class="mandatorystar">*</span> </strong></h5>
											</td>
										</tr>
										<tr height="80"  style="display: none;">
													<td colspan="3">
												<input style="width:250px" type="email" id="insureremail" name="insureremail" placeholder="Enter insurer email" required />
											
												<input class="btn btn-info btn-fill pull-right" type="submit" value="SEND"/>
											</td>
													
										</tr>
										<tr height="50">
											<td colspan="3">
												<h5><strong>Projected fees: </strong></h5>
											</td>
										</tr>
										<?php 
										$feecount = 0;
										// print_r($visitphotoarray);
											foreach ($prelimfeesarray as $prelimfees) {

												$feecount++;
												?>
												<tr>
													<td><?php echo $feecount; ?> : <?php echo $prelimfees['content'];?></td>
													<td>Updated on : <?php echo $prelimfees['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr height="50">
											<td colspan="3">
												<h5><strong>Reserved Amount: </strong></h5>
											</td>
										</tr>
										<?php 
										$amountcount = 0;
										// print_r($visitphotoarray);
											foreach ($prelimamountarray as $prelimamount) {

												$amountcount++;
												?>
												<tr>
													<td><?php echo $amountcount; ?> : <?php echo $prelimamount['content'];?></td>
													<td>Updated on : <?php echo $prelimamount['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr height="80">
											<form method="post" action="" onsubmit="return confirm('Click OK to submit the form.');">
											<td>
												<input style="width:250px" type="text" value="" id="projectedfees" name="projectedfees" placeholder="Enter projected fees" class="onlynumbers form-control" required />
											<!-- 
												<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/> -->
											</td>
											<td>
												<input style="width:250px" type="text" value="" id="reserveamount" name="reserveamount" placeholder="Enter reserve amount" class="onlynumbers form-control" required />
											</td>
											<td>
												<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/>
											</td>
											</form>
										</tr>
										<tr height="80">
											<!-- <form method="post" action="">
											<td>
												<input type="checkbox" value="Y" id="prelimreportissued" name="prelimreportissued" <?php if($prelimReportIssued == "Y") {echo "checked";} ?>/> Preliminary Report Issued
											
											</td>
											<td colspan="2">
												<input type="checkbox" value="Y" id="statusreportissued" name="statusreportissued" <?php if($statusReportIssued == "Y") {echo "checked";} ?>/> Status Report Issued
											</td>
											<td>
												<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/>
											</td>
											</form> -->
										</tr>
	<!-- 									<form method="post" action="">
											<td>
												<input style="width:250px" type="text" value="" id="statusfee" name="statusfee" placeholder="Enter status fees" class="onlynumbers form-control" required />
											
											</td>
											<td>
											</td>
											<td>
												<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/>
											</td>
											</form> -->
                                    </tbody>
                                </table>
                                <table align="center">
									<tr>
										<td>
											<a href="report.php?id=<?php echo $updatedclaimid;?>"><button class="btn btn-info btn-fill">BACK</button></a>
										</td>
									</tr>
								</table>
                            </div>
                        </div>
                    </div>

    </div>
</div>


</body>
	<script>
	function isNumber(evt) {
    			evt = (evt) ? evt : window.event;
    			var charCode = (evt.which) ? evt.which : evt.keyCode;
    			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        		return false;
   			 }
    			return true;
		}
	</script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="assets/js/jQuery-ui.js"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$('.dateinputs').datepicker({ dateFormat: 'dd-mm-yy' });
    		// $('.printbuttons').click(function(){
    		// 	var id = $(this).attr("id");
    		// 	var report = $("#print"+id).val();
    		// 	$.print(report);
    		// });
    	});
    </script>
   

</html>