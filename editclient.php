<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
        if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {
        if((isset($_REQUEST['clientId'])) && (!empty($_REQUEST['clientId']))){
            $edit_clientId= $_REQUEST['clientId'];
        $get_clients = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `poBox` from `clientmaster` where clientId = '$edit_clientId'";
        $stmt       = mysqli_query($connection, $get_clients); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $clientId     = $row['clientId']; 
            $referenceId   = (empty($row['referenceId']))     ? '' : $row['referenceId'];
            $clientName   = (empty($row['clientName']))       ? '' : $row['clientName'];
            $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            $emailId      = (empty($row['emailId']))        ? '' : $row['emailId'];
            $address      = (empty($row['address']))        ? '' : $row['address'];
            $city         = (empty($row['city']))           ? '' : $row['city'];
            $postalCode   = (empty($row['postalCode']))     ? '' : $row['postalCode'];
            $country      = (empty($row['country']))        ? '' : $row['country'];
            $poBox        = (empty($row['poBox']))          ? '' : $row['poBox'];
          }
      }
  }
    }
}
//Add Client
  if((isset($_POST['referenceid'])) && (!empty($_POST['referenceid']))){
     // print_r($_FILES);exit;
    $referenceid         = (empty($_REQUEST['referenceid']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['referenceid']));
    $emailid             = (empty($_REQUEST['emailid']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailid']));
    $phonenum            = (empty($_REQUEST['phonenum']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['phonenum']));
    $companyname         = (empty($_REQUEST['companyname']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['companyname']));
    $homeaddress         = (empty($_REQUEST['homeaddress']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['homeaddress']));
    $clientcity          = (empty($_REQUEST['clientcity']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['clientcity']));
    $clientcountry       = (empty($_REQUEST['clientcountry']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['clientcountry']));
    $poboxnumber       = (empty($_REQUEST['poboxnumber']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['poboxnumber']));
    $zipcode             = (empty($_REQUEST['zipcode'])) ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['zipcode']));
    $update_details = "update `clientmaster` set `referenceId`='$referenceid', `clientName`='$companyname', `phoneNumber`='$phonenum', `emailId`='$emailid', `address`='$homeaddress', `city`='$clientcity', `postalCode`='$zipcode', `country`='$clientcountry', `updatedBy`='$loggedin_userid', `updatedDate`= now(), `poBox` = '$poboxnumber' where clientId = '$edit_clientId'";
    mysqli_query($connection, $update_details);
    header("Location:clientlist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        Whitelaw
    </title>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="clientlist.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Insurer</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Reference ID <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Reference ID" name="referenceid" id="referenceid" value="<?php echo $referenceId;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" class="form-control" placeholder="Email" name="emailid" id="emailid" value="<?php echo $emailId;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" placeholder="Phone" name="phonenum" id="phonenum" value="<?php echo $phoneNumber;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Company" name="companyname" id="companyname" value="<?php echo $clientName;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Address" name="homeaddress" id="homeaddress" value="<?php echo $address;?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="City" name="clientcity" id="clientcity" value="<?php echo $city;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Country"  name="clientcountry" id="clientcountry" value="<?php echo $country;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" class="form-control onlynumbers" placeholder="Postal Code"  name="zipcode" id="zipcode" value="<?php echo $postalCode;?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>PO Box Number <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" placeholder="PO Box Number" name="poboxnumber" id="poboxnumber" value="<?php echo $poBox;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-fill pull-right">EDIT INSURER</button> <a href="clientlist.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>

</html>
