<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    $invoiceNumber          = mt_rand(1111111111,9999999999);
    $liveProjectedFee       = 0;
    if(isset($_SESSION['updatedclaimid'])){
        $updatedclaimid         = $_SESSION["updatedclaimid"];
        $get_details = "select `claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `liveProjectedFee` from `claimmaster` where claimId = '$updatedclaimid'";
        $detailstmt       = mysqli_query($connection, $get_details); 
        $getcount   = mysqli_num_rows($detailstmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
            $claimId            = $row['claimId']; 
            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
            $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
            $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
            $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
            $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
            $claimReference     = (empty($row['claimReference']))   ? '' : $row['claimReference'];
            $brokerReference    = (empty($row['brokerReference']))  ? '' : $row['brokerReference'];
            $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
            $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
            $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
            $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
            $subId              = (empty($row['subId']))            ? '' : $row['subId'];
            $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
            $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
            $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
            $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
            $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
            $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
            $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
            $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
            $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
            $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
            $liveProjectedFee   = (empty($row['liveProjectedFee'])) ? '0' : $row['liveProjectedFee'];
          }
      }
      //Get Client List
      $clientlistoptions = "";
      $get_categories = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster`";
        $clientstmt       = mysqli_query($connection, $get_categories); 
        $getclientcount   = mysqli_num_rows($clientstmt);
        if($getclientcount > 0){
          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                $thisclientId     = $clientrow['clientId']; 
                $thisreferenceId  = (empty($clientrow['referenceId']))   ? '' : $clientrow['referenceId'];
                $thisclientName   = (empty($clientrow['clientName']))    ? '' : $clientrow['clientName'];
                $thisphoneNumber  = (empty($clientrow['phoneNumber']))   ? '' : $clientrow['phoneNumber'];
                $thisemailId      = (empty($clientrow['emailId']))       ? '' : $clientrow['emailId'];
                $thiscity         = (empty($clientrow['city']))          ? '' : $clientrow['city'];
                if($thisclientId == $clientId){
                    $clientlistoptions .= "<option value='$thisclientId' selected>$thisclientName</option>";
                } else {
                    $clientlistoptions .= "<option value='$thisclientId'>$thisclientName</option>";
                }
                
            }
        }
        //Get currency
        $currencylistoptions = "";
        $currencydata = [];
        $get_currency = "select `currencyId`, `currencyCode`, `payableTo`, `ibanNumber`, `swiftCode` from `currencydetails` where active = '1'";
        $currencystmt       = mysqli_query($connection, $get_currency); 
        $getcurrencycount   = mysqli_num_rows($currencystmt);
        if($getcurrencycount > 0){
          while($currencyrow = mysqli_fetch_array($currencystmt, MYSQLI_ASSOC)){
                $currencyId     = (empty($currencyrow['currencyId']))       ? '' : $currencyrow['currencyId'];
                $currencyCode   = (empty($currencyrow['currencyCode']))   ? '' : $currencyrow['currencyCode'];
                $currencydata[$currencyId] = $currencyCode;
                    $currencylistoptions .= "<option value='$currencyId'>$currencyCode</option>";
                
            }
        }
    } else {
        ?>
        <script type="text/javascript">
            // alert("Please select an item from job list first");
            window.location.href = "jobselect.php";
        </script>
        <?php 
    }
}
  if((isset($_POST['invoicenumber'])) && (!empty($_POST['invoicenumber']))){
     // print_r($_REQUEST);exit;
    $invoicenumber   = (empty($_REQUEST['invoicenumber']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['invoicenumber']));
    $currencyid      = (empty($_REQUEST['currency']))       ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['currency']));
    $currency        = $currencydata[$currencyid];
    $invoicetitle    = (empty($_REQUEST['invoicetitle']))       ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['invoicetitle']));
    $faoname         = (empty($_REQUEST['faoname']))        ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['faoname']));
    $insuredName     = (empty($_REQUEST['insuredName']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insuredName']));
    $jobNumber       = (empty($_REQUEST['jobNumber']))      ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['jobNumber']));
    $yourReference   = (empty($_REQUEST['yourReference']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['yourReference']));
    $invoiceDate     = (empty($_REQUEST['invoiceDate']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['invoiceDate']));
    $clientName      = (empty($_REQUEST['clientName']))     ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['clientName']));
    $losslocation    = (empty($_REQUEST['losslocation']))   ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['losslocation']));
    $amountdesc1     = (empty($_REQUEST['amountdesc1']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['amountdesc1']));
    $timetext        = (empty($_REQUEST['timetext']))       ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['timetext']));
    $timelyrate      = (empty($_REQUEST['timelyrate']))     ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['timelyrate']));
    $theamount1      = (empty($_REQUEST['theamount1']))     ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['theamount1']));
    $totalamount     = (empty($_REQUEST['totalamount']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['totalamount']));
    $totalinwords    = (empty($_REQUEST['totalinwords']))   ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['totalinwords']));
    $invoiceterms    = (empty($_REQUEST['invoiceterms']))   ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['invoiceterms']));
    // $narrationText   = (empty($_REQUEST['narrationText']))  ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['narrationText']));
    $amountdesc      = $_REQUEST['amountdesc'];
    $theamount       = $_REQUEST['theamount'];
    // $timelyrate      = $_REQUEST['timelyrate'];
    // $timetext        = $_REQUEST['timetext'];
    if(($invoicetitle == "Interim Invoice 1")||($invoicetitle="Interim Invoice 2")){
        $newliveProjectedFee = $liveProjectedFee + $totalamount;
    }
    $update_fee = "update claimmaster set liveProjectedFee = '$newliveProjectedFee' where claimId = '$updatedclaimid'";
    mysqli_query($connection, $update_fee);
    $narrationText        = $_REQUEST['narrationText'];
    $insert_to_master = "insert into `invoicemaster`(`claimId`, `jobNumber`, `invoiceTitle`, `invoiceNumber`, `currencyId`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `narrationText`, `createdBy`, `createdDate`) values ('$updatedclaimid', '$jobNumber', '$invoicetitle', '$invoicenumber', '$currencyid', '$currency', '$faoname', '$insuredName', '$yourReference', '$invoiceDate', '$clientName', '$losslocation', '$totalamount', '$totalinwords', '$invoiceterms', '$narrationText', '$loggedin_userid', now())";
    mysqli_query($connection, $insert_to_master);
    $invoiceid    = mysqli_insert_id($connection);

    //Insert Details
    // $insert_details = "insert into `invoicedetails`(`invoiceId`, `description`, `timelyRate`, `timelyText`, `amount`) VALUES ('$invoiceid', '$amountdesc1', '$timetext', '$timelyrate', '$theamount1')";
    // mysqli_query($connection, $insert_details);
    for ($i=0; $i < count($amountdesc) ; $i++) { 
        $theamountdesc  = $amountdesc[$i];
        $thistheamount      = $theamount[$i];
        // $thisnarration      = $narrationText[$i];
        if(isset($narrationText[$i])){
            $thisnarration      = $narrationText[$i];
        } else {
            $thisnarration      = "";
        }
        // if(isset($timetext[$i])){
        //     $thistimetext      = $timetext[$i];
        // } else {
        //     $thistimetext      = "";
        // }
        if($theamountdesc != ""){
                $insert_details = "insert into `invoicedetails`(`invoiceId`, `description`, `narrationText`, `amount`) VALUES ('$invoiceid', '$theamountdesc', '$thisnarration', '$thistheamount')";
                mysqli_query($connection, $insert_details);
        }

    }
    $update_status = "update claimmaster set jobStatus = 'I' where claimId = '$updatedclaimid'";
    mysqli_query($connection, $update_status);
    header("Location:viewinvoice.php?id=$invoiceid");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sumoselect.css">

    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="jobselect.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Invoice</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Invoice no:  <span class="mandatorystar">*</span></label><br>
                                                <input type="text" class="form-control" placeholder="Invoice Number" name="invoicenumber" id="invoicenumber" value="<?php echo $invoiceNumber; ?>" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency: <span class="mandatorystar">*</span></label><br>
                                                <select class="col-md-4 form-control" name="currency" id="currency" required>
                                                    <option value="">Select</option>
													<?php echo $currencylistoptions;?>
												</select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Invoice Title: <span class="mandatorystar">*</span></label><br>
                                                <select class="col-md-4 form-control" name="invoicetitle" id="invoicetitle" required>
                                                    <option value="">Select</option>
                                                    <option value="Interim Invoice 1">Interim Invoice 1</option>
                                                    <option value="Interim Invoice 2">Interim Invoice 2</option>
                                                    <option value="Fee Invoice">Fee Invoice</option>
                                                </select>
                                            </div>
                                        </div>
									</div>
									<div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>FAO <span class="mandatorystar">*</span></label>
												<input type="text" class="form-control nonumbers" placeholder="FAO" name="faoname" id="faoname" required>
                                            </div>
                                        </div>
										<div class="col-md-8">
                                            <div class="form-group">
                                                <label>To <span class="mandatorystar">*</span></label><br>
												<input type="text" class="form-control nonumbers" name="insuredName" id="insuredName" value="<?php echo $insuredName;?>" required>
                                            </div>
                                        </div>
									</div>
									<div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Our Reference <span class="mandatorystar">*</span></label>
												<input type="text" class="form-control" placeholder="Our Reference" value="<?php echo $jobNumber;?>" name="jobNumber" id="jobNumber" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Your Reference <span class="mandatorystar">*</span></label>
												<input type="text" class="form-control" placeholder="Your Reference" name="yourReference" id="yourReference" value="<?php echo $claimReference;?>"  required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Invoice date <span class="mandatorystar">*</span></label>
												<input type="type" class="form-control dateinputs" placeholder="Invoice Date" name="invoiceDate" id="invoiceDate" required>
                                            </div>
                                        </div>
										
									</div>
									<div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insurer <span class="mandatorystar">*</span></label>
												<select class="col-md-4 form-control" name="clientName" id="clientName" required>
													<?php echo $clientlistoptions;?>
												</select>
                                            </div>
                                        </div>
										<div class="col-md-8">
                                            <div class="form-group">
                                                <label>Location of loss <span class="mandatorystar">*</span></label>
												<input type="text" class="form-control" placeholder="location" name="losslocation" id="losslocation" value="<?php echo $locationOfLoss;?>" required>
                                            </div>
                                        </div>
										
									</div>
                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Narration <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Narration" name="narrationText" id="narrationText" required>
                                            </div>
                                        </div>
                                        
                                    </div> -->
									<div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Services</label>
											</div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Narration</label>
                                            </div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label>Total payable</label>
											</div>
                                        </div>
										
									</div>
									<div class="row">
										<hr width="100%" size="150">
									</div>
                                    <select class="selectpicker" id="serviceselect" multiple required>
                                    <?php 
                                            $get_services = "select `serviceId`, `service` from `servicemaster` where active = 'A'";
                                                $stmt       = mysqli_query($connection, $get_services); 
                                                $getcount   = mysqli_num_rows($stmt);
                                                if($getcount > 0){
                                                    
                                                  while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                    $serviceId   = (empty($row['serviceId']))   ? '' : $row['serviceId'];
                                                    $service     = (empty($row['service']))     ? '' : $row['service'];
                                                    ?>
                                            <option value="<?php echo $serviceId;?>"><?php echo $service;?></option>

                                        <?php
                                                  }
                                              }
                                        ?>
                                    </select>
                                    <div class="col-xs-12 paddingrl0" id="serviceinputs">
                                        
                                    </div>
									<!-- <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" placeholder="" value="Professional Loss Adjusting Fees" id="amountdesc1" name="amountdesc1" required></label>
											</div>
                                        </div>
										<div class="col-md-5">
                                            <div class="form-group">
                                                <label><input type="text" class="form-control" name="timetext" id="timetext" placeholder="eg: 5 Hours" id="timelyrate" required>@<input type="text" placeholder="eg : 300 AED per Hour" class="form-control" name="timelyrate"  required></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate"  name="theamount1" id="theamount1" onblur="calculateTotal()" required></label>
											</div>
                                        </div>
										
									</div> -->
									<!-- <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" placeholder="" value="Professional Loss Adjusting Fees" required></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="text" class="form-control" placeholder="" value="3250.00" required></label>
											</div>
                                        </div>
										
									</div> -->
<!-- 									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" placeholder="" value="Photographs/Photocopying" id="amountdesc[]" name="amountdesc[]"></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate" placeholder="" name="theamount[]" id="theamount[]"  onblur="calculateTotal()"></label>
											</div>
                                        </div>
										
									</div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" id="amountdesc[]" name="amountdesc[]" placeholder="" value="Telephone/Fax/Post"></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate" placeholder="" name="theamount[]" id="theamount[]" onblur="calculateTotal()"></label>
											</div>
                                        </div>
										
									</div>
									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" id="amountdesc[]" name="amountdesc[]" placeholder="" value="Miscellaneous"></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate" placeholder="" name="theamount[]" id="theamount[]" onblur="calculateTotal()"></label>
											</div>
                                        </div>
										
									</div>
									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" id="amountdesc[]" name="amountdesc[]" placeholder="" ></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate" placeholder="" name="theamount[]" id="theamount[]" onblur="calculateTotal()"></label>
											</div>
                                        </div>
										
									</div>
									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label><input style="width:250px;" type="text" class="form-control" id="amountdesc[]" name="amountdesc[]" placeholder=""></label>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers totalcalculate" placeholder="" name="theamount[]" id="theamount[]" onblur="calculateTotal()"></label>
											</div>
                                        </div>
										
									</div> -->
									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h4><b>TOTAL</b></h4>
											</div>
                                        </div>
										<div class="col-md-2">
                                            <div class="form-group">
                                                <label><input type="number" class="form-control onlynumbers" placeholder="TOTAL" name="totalamount" id="totalamount" required></label>
											</div>
                                        </div>
										
									</div>
									
										<hr width="100%" size="150">
									<div class="row">
                                        <div align="right" class="col-md-12">
                                            <div class="form-group">
                                                <label>TOTAL AMOUNT IN WORDS</label>
                                                <input type="text" class="form-control nonumbers" placeholder="TOTAL in Words" name="totalinwords" id="totalinwords" required>
											</div>
                                        </div>
										
									</div>	
									<div class="row">
                                        <div align="center" class="col-md-12">
                                            <div class="form-group">
												<label><input type="text" style="width:1000px;" class="form-control" name="invoiceterms" id="invoiceterms" placeholder="" value="Terms: Payable within Thirty (30) Days from Date of Invoice" required></label>
											</div>
                                        </div>
									</div>	
									<br><br><br><br><br><br>
									<div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h4><b>Graham Whitelaw (Senior Director)</b></h4>
											</div>
                                        </div>
									</div>
<!-- 									<div class="row">
                                        <div align="center" class="col-md-12">
                                            <div class="form-group">
												<h5>Cheque payable to 'WHITELAW LOSS ADJUSTERS LLC' and ONLY to this name" required</h5>
											</div>
                                        </div>
									</div>
									<div class="row">
                                        <div align="center" class="col-md-12">
                                            <div class="form-group">
                                                <h5>Account Transfers to: IBAN No. <u><b>AE660260001012327362303</b></u> | Swift Code: <u><b>EBILAEAD</b></u></h5>
											</div>
                                        </div>
										
									</div>	 -->		
								</div>
							<button type="submit" class="btn btn-info btn-fill pull-right">CREATE INVOICE</button> <a href="open.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                <div class="clearfix"></div>
                                </form>
								<div class="row">
								</div>
								
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/jQuery-ui.js"></script>
    <script type="text/javascript" src="assets/js/jquery.sumoselect.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#accounts').addClass("active");
            $('#invoiceDate').datepicker({ dateFormat: 'dd-mm-yy' });
            $("#serviceselect").SumoSelect({placeholder: 'Select Service Types'});
            var displayedservices = [];
            $("#serviceselect").on('change', function() {
                var selectedServices = $("#serviceselect").val();
                for (i = 0; i < displayedservices.length; i++) {
                    if($.inArray(displayedservices[i], selectedServices) != -1) {
                        // console.log(displayedservices[i]);console.log("Hello");
                    } else {
                        $('#serviceinputdiv'+displayedservices[i]).remove();
                        displayedservices.splice(i, 1);
                    }
                }
                // console.log(selectedServices);
                // var services = selectedServices.valueOf();
                var services = "";
                for (i = 0; i < selectedServices.length; i++) { 
                    console.log(selectedServices[i]);
                    if($.inArray(selectedServices[i], displayedservices) != -1) {
                        // console.log("123");
                        // $('#serviceinputdiv'+selectedServices[i]).remove();
                        // var position = displayedservices.indexOf(selectedServices[i]);
                        // if ( ~position ) displayedservices.splice(position, 1);
                    } else {
                        displayedservices.push(selectedServices[i]);
                        services = services+","+selectedServices[i];
                    } 
                }
                var dataString = "RequestType=getServices&services="+services;
                // alert(dataString);
                $.ajax({
                    type        : 'POST', 
                    url         : 'include/api.php', 
                    crossDomain : true,
                    data        : dataString,
                    dataType    : 'json', 
                    async       : false,
                    success : function (response)
                        { 
                            $.each(response, function (i, member) {
                                var servicename = member['service'];
                                var serviceid   = member['serviceId'];
                                var serviceinput = "<div class='row' id='serviceinputdiv"+serviceid+"'><div class='col-md-5'><div class='form-group'><input type='text' class='form-control' value='"+servicename+"' id='amountdesc[]' name='amountdesc[]' required></div></div><div class='col-md-5'><input type='text' class='form-control' id='narrationText[]' name='narrationText[]' placeholder='Narration'></div><div class='col-md-2'><div class='form-group'><label><input type='number' class='form-control onlynumbers totalcalculate'  name='theamount[]' id='theamount[]' onblur='calculateTotal()' placeholder='Amount' required></label></div></div></div>";
                                $('#serviceinputs').append(serviceinput);
                            });
                        },
                    error: function(error)
                    {
                        //alert("Something went wrong. Please try again later.");
                        return false;
                    }
                });
             });
        });
        
            function calculateTotal(){
                var totalAmount = 0;
                $(".totalcalculate").each(function() {
                    var value1 = this.value;
                    // console.log(value);
                    if(value1 != ""){
                        totalAmount += parseInt(value1);
                    }
                   $('#totalamount').val(totalAmount);
                   var numberinwords = toWords(totalAmount);
                   numberinwords = ucwords(numberinwords)+"Only";
                   $('#totalinwords').val(numberinwords);
                   // console.log(numberinwords);
                });
                // console.log(totalAmount);

            }
       var th = ['', 'thousand', 'million', 'billion', 'trillion'];

        var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

        var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

        var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        function toWords(s) {
            s = s.toString();
            s = s.replace(/[\, ]/g, '');
            if (s != parseFloat(s)) return 'not a number';
            var x = s.indexOf('.');
            if (x == -1) x = s.length;
            if (x > 15) return 'too big';
            var n = s.split('');
            var str = '';
            var sk = 0;
            for (var i = 0; i < x; i++) {
                if ((x - i) % 3 == 2) {
                    if (n[i] == '1') {
                        str += tn[Number(n[i + 1])] + ' ';
                        i++;
                        sk = 1;
                    } else if (n[i] != 0) {
                        str += tw[n[i] - 2] + ' ';
                        sk = 1;
                    }
                } else if (n[i] != 0) {
                    str += dg[n[i]] + ' ';
                    if ((x - i) % 3 == 0) str += 'hundred ';
                    sk = 1;
                }
                if ((x - i) % 3 == 1) {
                    if (sk) str += th[(x - i - 1) / 3] + ' ';
                    sk = 0;
                }
            }
            if (x != s.length) {
                var y = s.length;
                str += 'point ';
                for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
            }
            return str.replace(/\s+/g, ' ');

        }
        function ucwords (str) {
            return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                return $1.toUpperCase();
            });
        }
    </script>
   

</html>
