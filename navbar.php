<?php
ini_set('display_errors',0);
session_start();  
$userAccess = $_SESSION["loggedin_useraccess"];
$useraccessarray = explode(",", $userAccess);

?>
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="navbar-brand">Whitelaw</a> -->
                </div>
                <div class="collapse navbar-collapse">
<!--                     <ul class="nav navbar-nav navbar-left">
						<li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification">5</span>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul>
                        </li>
                    </ul> -->

                    <ul class="nav navbar-nav navbar-right">
                      <?php //if (in_array("1", $useraccessarray)){ ?>
                       <!--  <li>
                           <a href="newclaim.php">
                               New Claim
                            </a>
                        </li> -->
                        <?php //} ?>
                        <?php if (in_array("2", $useraccessarray)){ ?>
						            <li>
                           <a href="open.php">
                               Claim Files
                            </a>
                        </li>
                        <?php } ?>
						
						<!-- <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Reports
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="open.php">Open reports</a></li>
                                
                                <li><a href="closed.php">Reports</a></li>
                              </ul>
                        </li> -->
                        <li>
                            <a href="logout.php">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>