
<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    }
    if((isset($_POST['location'])) && (!empty($_POST['location']))){
        $location       = (empty($_REQUEST['location']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['location']));
    }
    if((isset($_POST['adjusterid'])) && (!empty($_POST['adjusterid']))){
        $adjusterid       = (empty($_REQUEST['adjusterid']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['adjusterid']));
    }

}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="closed.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Outstanding Prelim Reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped">
                                    <thead>
                                        <!-- <th>File</th> -->
                                    	<th>Number</th>
                                    	<th>Insurer</th>
                                    	<th>Claim no.</th>
                                    	<th>Insured</th>
                                    	<th>Status</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        //Get claim details
                                        $count = 0;
                                        $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster` where claimId IN (select claimId from visitupdates) and claimId NOT IN (select claimId from prelimupdates)";
                                        if(($location != "") && ($adjusterid != "")){
                                            $get_details .= " and officeId = '$location' and adjusterId = '$adjusterid'";
                                        } else {
                                            if($location != ""){
                                                $get_details .= " and officeId = '$location'";
                                            }
                                            if($adjusterid != ""){
                                                $get_details .= " and adjusterId = '$adjusterid'";
                                            }
                                        }
                                        // echo $get_details;exit;
                                        $detailstmt       = mysqli_query($connection, $get_details); 
                                        $getcount   = mysqli_num_rows($detailstmt);
                                        if($getcount > 0){
                                            
                                          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                            $count++;
                                            $claimId            = $row['claimId']; 
                                            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                                            $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
                                            $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
                                            $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
                                            $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
                                            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                                            $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
                                            $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
                                            $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
                                            $subId              = (empty($row['subId']))            ? '' : $row['subId'];
                                            $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
                                            $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
                                            $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
                                            $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
                                            $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
                                            $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
                                            $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
                                            $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
                                            $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
                                            $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
                                            $jobStatusText = "";
                                            if($jobStatus == "O"){
                                                $jobStatusText = "Open";
                                            } elseif ($jobStatus == "V") {
                                                $jobStatusText = "Visit";
                                            } elseif ($jobStatus == "P") {
                                                $jobStatusText = "Preliminiary";
                                            } elseif ($jobStatus == "W") {
                                                $jobStatusText = "Working";
                                            } elseif ($jobStatus == "I") {
                                                $jobStatusText = "Invoiced";
                                            } elseif ($jobStatus == "C") {
                                                $jobStatusText = "Closed";
                                            } elseif ($jobStatus == "R") {
                                                $jobStatusText = "Receipt";
                                            } elseif ($jobStatus == "S") {
                                                $jobStatusText = "Status Report Issued";
                                            } else {
                                                $jobStatusText = "";
                                            }
                                      //Client name
                                      $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
                                            $clientstmt       = mysqli_query($connection, $get_client); 
                                            $getclientcount   = mysqli_num_rows($clientstmt);
                                            if($getclientcount > 0){
                                                
                                              while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
                                                $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                              }
                                          }
                                    
                                    ?>
										<tr>
                                        	<td><?php echo $count;?></td>
                                        	<td><?php echo $clientName;?></td>
                                        	<td><?php echo $jobNumber;?></td>
                                        	<td><?php echo $insuredName;?></td>
                                        	<td><?php echo $jobStatusText;?></td>
                                        </tr>
                                        <?php 
                                                                                  }
                                      }
                                      if($count == 0){
                                        echo "<tr><td colspan='5'>No Outstanding Prelims.</td></tr>";
                                      }
                                        ?>
										
										
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#reports').addClass("active");
        });
    </script>
   

</html>
