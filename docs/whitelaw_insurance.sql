-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2017 at 04:23 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjusters`
--

DROP TABLE IF EXISTS `adjusters`;
CREATE TABLE `adjusters` (
  `adjusterId` int(11) NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adjusters`
--

INSERT INTO `adjusters` (`adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Graham', 'Whitelaw', 'graham', 'graham.whitelaw@wla.com', 'Dubai', 'UAE', '11111', 'hsdkjhskjdh sdf', 'A', '1', '2017-01-19 21:49:10', '1', '2017-01-21 13:58:04'),
(2, 'Jerin', 'Koshy', 'koshy', 'jerin.koshy@wla.com', 'Dubai', 'UAE', '', 'ABC Street', 'A', '1', '2017-01-22 11:59:44', '', '2017-01-26 20:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `brokers`
--

DROP TABLE IF EXISTS `brokers`;
CREATE TABLE `brokers` (
  `brokerId` int(11) NOT NULL,
  `employeeId` varchar(50) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brokers`
--

INSERT INTO `brokers` (`brokerId`, `employeeId`, `firstName`, `lastName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'KWOE109', 'Arya', 'Insurance Brokerage', '97142212838', 'aryainsurance@gmail.com', 'Office Tower Baniyas Rd', 'Dubai', '', 'UAE', 'A', '1', '2017-01-19 21:21:15', '1', '2017-01-27 20:34:33'),
(2, '46KML20', 'Greenshield', 'Insurance Brokers LLC', '97143882200', 'greenshield@gmail.com', 'Boulevard Plaza Tower 1, Burj Khalifa Downtown, Dubai - Sheikh Mohammed bin Rashid Blvd', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:33:36', NULL, '2017-01-27 20:33:36'),
(3, 'WLA1987', 'Omega', 'Insurance Brokers LLC', '97143024555', 'omegainsurance@gmail.com', 'Al Brajem Business Center', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:36:09', NULL, '2017-01-27 20:36:09'),
(4, 'QWI1878', 'Al Manara', 'Insurance Services', '97165444660', 'almanara@gmail.com', 'Corniche St', 'Sharjah', '', 'UAE', 'A', '1', '2017-01-28 07:37:48', NULL, '2017-01-27 20:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `categorymaster`
--

DROP TABLE IF EXISTS `categorymaster`;
CREATE TABLE `categorymaster` (
  `categoryId` int(11) NOT NULL,
  `category` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active,I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorymaster`
--

INSERT INTO `categorymaster` (`categoryId`, `category`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Surveys', 'G', 'A', '1', '2017-01-20 21:40:20', '', '2017-01-26 20:23:21'),
(2, 'Engineering', 'G', 'A', '1', '2017-01-20 21:47:06', '', '2017-01-20 14:29:49'),
(3, 'Domestic', 'G', 'A', '1', '2017-01-20 21:47:32', '', '2017-01-20 14:29:53'),
(4, 'Marine', 'M', 'A', '1', '2017-01-22 10:15:33', '', '2017-01-21 12:15:33'),
(5, 'Machinery', 'G', 'A', '1', '2017-01-22 10:17:09', '', '2017-01-21 12:17:09'),
(6, 'Commercial', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 12:17:30'),
(7, 'Liability', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 12:17:59'),
(8, 'Miscellaneous', 'G', 'A', '1', '2017-01-22 10:18:17', '', '2017-01-21 12:18:17'),
(9, 'Pecuniary', 'G', 'A', '1', '2017-01-22 10:50:58', '', '2017-01-21 12:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `claimmaster`
--

DROP TABLE IF EXISTS `claimmaster`;
CREATE TABLE `claimmaster` (
  `claimId` int(11) NOT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `officeId` int(11) DEFAULT NULL,
  `policyNumber` varchar(50) DEFAULT NULL,
  `insuredName` varchar(50) DEFAULT NULL,
  `insurerName` varchar(200) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `brokerId` int(11) DEFAULT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `instructionTime` varchar(20) DEFAULT NULL,
  `instructionDate` varchar(20) DEFAULT NULL,
  `contactTime` varchar(20) DEFAULT NULL,
  `contactDate` varchar(20) DEFAULT NULL,
  `surveyTime` varchar(20) DEFAULT NULL,
  `surveyDate` varchar(20) DEFAULT NULL,
  `preliminaryDate` varchar(50) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `jobStatus` varchar(2) NOT NULL DEFAULT 'O' COMMENT 'O-Open, V-Visit,P-Preliminary, W-Working, C-Closed',
  `frozen` varchar(2) NOT NULL DEFAULT 'N',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `claimmaster`
--

INSERT INTO `claimmaster` (`claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`) VALUES
(1, 'DXB/G/0001/17', NULL, 1, 'POLICY1001', 'Trade House LTD', 'Robert Phillips', 'Sharjah, UAE', 1, 1, 1, 6, 35, '13:00', '01/18/2017', '14:00', '01/18/2017', '16:00', '01/23/2017', '2017-01-28', NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:47:12', '2017-01-27 21:04:47', NULL),
(2, 'LON/G/0002/17', NULL, 2, 'POLICY1002', 'Omer Hassan Elamin El Sheikh', 'Jacob Anderson', 'Dubai, UAE', 2, 2, 2, 3, 21, '20:00', '01/11/2017', '10:00', '01/12/2017', '11:00', '01/13/2017', '2017-01-28', NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:51:00', '2017-01-27 21:08:16', NULL),
(3, 'DXB/G/0003/17', NULL, 1, 'POLICY1003', 'Mahdy Ramazan Jahan Far', 'Ethan Reyes', 'Dubai, UAE', 3, 3, 1, 6, 35, '20:00', '01/16/2017', '09:00', '01/17/2017', '16:00', '01/17/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 5, '2017-01-28 07:53:15', '2017-01-27 20:53:15', NULL),
(4, 'LON/G/0004/17', NULL, 2, 'POLICY1004', 'Creneau International', 'Omar', 'Dubai, UAE', 5, 4, 2, 2, 5, '09:00', '01/16/2017', '11:00', '01/16/2017', '20:00', '01/16/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 5, '2017-01-28 07:55:20', '2017-01-27 20:55:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clientmaster`
--

DROP TABLE IF EXISTS `clientmaster`;
CREATE TABLE `clientmaster` (
  `clientId` int(11) NOT NULL,
  `referenceId` varchar(100) DEFAULT NULL,
  `clientName` varchar(300) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A - Active, I - Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientmaster`
--

INSERT INTO `clientmaster` (`clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'HAR201600001\n', 'Oman Insurance Company, Dxb\n', '9998887770', 'qwerty@gmail.com', 'Abc address', 'Dubai', '88877', 'UAE', 'A', '1', '2017-01-20 22:08:14', '', '2017-01-21 13:40:12'),
(2, '2013ENG20160100001', 'Ras Al Khaimah Insurance Company', '8887776667', 'abcd@gmail.com', 'RAK', 'RAK', '88888', 'UAE', 'A', '1', '2017-01-22 00:00:00', '', '2017-01-26 20:16:57'),
(3, 'D1C-E19-150003', 'Abu Dhabi National Insurance Company, Dxb', '999999999', 'adninsurance@gmail.com', 'Al Muraikhi Tower, Al Maktoum Street,Opp The Metropolitan PalaceØŒ Deira', 'Diera', '', 'UAE', 'A', '1', '2017-01-28 07:39:31', NULL, '2017-01-27 20:39:31'),
(4, 'F/001/X/16', 'Al Ittihad Al Watani Insurance', '97142823266', 'aiawinsurance@gmail.com', 'M01,M Floor,Al Durrah Building 2,Garhoud', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:41:07', NULL, '2017-01-27 20:41:07'),
(5, '31/PRC/1600003', 'Zurich Insurance Middle East', '971504653102', 'zime@gmail.com', 'Sama Tower - Sheikh Zayed Road', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:42:29', NULL, '2017-01-27 20:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

DROP TABLE IF EXISTS `invoicedetails`;
CREATE TABLE `invoicedetails` (
  `detailsId` int(11) NOT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `timelyRate` varchar(200) DEFAULT NULL,
  `timelyText` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicedetails`
--

INSERT INTO `invoicedetails` (`detailsId`, `invoiceId`, `description`, `timelyRate`, `timelyText`, `amount`) VALUES
(1, 1, 'Professional Loss Adjusting Fees', '4 Hours', '200 AED per hour', '800'),
(2, 1, 'Photographs/Photocopying', '', '', '200'),
(3, 1, 'Telephone/Fax/Post', '', '', '100'),
(4, 1, 'Miscellaneous', '', '', '200'),
(5, 2, 'Professional Loss Adjusting Fees', '3 Hours', '400 AED per Hour', '1200'),
(6, 2, 'Photographs/Photocopying', '', '', '100'),
(7, 2, 'Telephone/Fax/Post', '', '', '100'),
(8, 2, 'Miscellaneous', '', '', '50'),
(9, 3, 'Professional Loss Adjusting Fees', '4 Hours', '200 AED Per Hour', '800'),
(10, 3, 'Photographs/Photocopying', '', '', '200'),
(11, 3, 'Telephone/Fax/Post', '', '', '100'),
(12, 3, 'Miscellaneous', '', '', '100'),
(13, 4, 'Testing', '', '', '1000'),
(14, 4, 'Visit', '', '', '600'),
(15, 5, 'Visit', '400', '100 AED per hour', '400'),
(16, 5, 'Testing', '', '', '600');

-- --------------------------------------------------------

--
-- Table structure for table `invoicemaster`
--

DROP TABLE IF EXISTS `invoicemaster`;
CREATE TABLE `invoicemaster` (
  `invoiceId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `invoiceNumber` varchar(100) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `faoName` varchar(200) DEFAULT NULL,
  `toName` varchar(200) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `invoiceDate` varchar(20) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `narrationText` varchar(500) DEFAULT NULL,
  `totalAmount` varchar(100) DEFAULT NULL,
  `totalInWords` varchar(500) DEFAULT NULL,
  `invoiceTerms` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicemaster`
--

INSERT INTO `invoicemaster` (`invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `narrationText`, `totalAmount`, `totalInWords`, `invoiceTerms`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'DXB/G/0001/17', 'INS98777PM', 'AED', 'Robert Phillips', 'Trade House LTD', 'DXBTD1234', '01/28/2017', 1, 'Sharjah, UAE', 'Damage cover', '1300', 'One Thousand Three Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 5, '2017-01-28 08:04:47', NULL, '2017-01-27 21:04:47'),
(2, 2, 'LON/G/0002/17', 'INS10988', 'AED', 'Roger', 'Omer Hassan Elamin El Sheikh', 'LON1002677', '01/27/2017', 2, 'Dubai, UAE', 'Damage cover', '1450', 'One Thousand Four Hundred Fifty Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 5, '2017-01-28 08:08:16', NULL, '2017-01-27 21:08:16'),
(3, 1, 'DXB/G/0001/17', '1303941142', 'AED', 'Testing', 'Trade House LTD', 'HAR987987', '01/24/2017', 1, 'Sharjah, UAE', 'Testing', '1200', 'One Thousand Two Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-01-31 20:23:21', NULL, '2017-01-31 09:23:21'),
(4, 1, 'DXB/G/0001/17', '1264501671', 'AED', 'Testing', 'Trade House LTD', 'HAR3879879', '02/09/2017', 1, 'Sharjah, UAE', 'Testing', '1600', 'One Thousand Six Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-01 00:04:35', NULL, '2017-01-31 13:04:35'),
(5, 1, 'DXB/G/0001/17', '1207345152', 'AED', 'Testing', 'Trade House LTD', 'HAR987987', '02/08/2017', 1, 'Sharjah, UAE', 'Testing', '1000', 'One Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-01 00:10:55', NULL, '2017-01-31 13:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `officemaster`
--

DROP TABLE IF EXISTS `officemaster`;
CREATE TABLE `officemaster` (
  `officeId` int(11) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officemaster`
--

INSERT INTO `officemaster` (`officeId`, `location`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Dubai', 'Whitelaw Chartered Loss Adjusters & Surveyors', 'DXB', 'A', '', '2017-01-14 00:00:00', '', '2017-01-13 10:05:38'),
(2, 'London', 'Whitelaw Chartered Loss Adjusters London', 'LON', 'A', '', '2017-01-14 00:00:00', '', '2017-01-26 20:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `prelimupdates`
--

DROP TABLE IF EXISTS `prelimupdates`;
CREATE TABLE `prelimupdates` (
  `prelimId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'R-Report, F-Fees',
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prelimupdates`
--

INSERT INTO `prelimupdates` (`prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES
(1, 1, 'F', '9000', NULL, 5, '2017-01-28 08:00:00'),
(2, 2, 'F', '10000', NULL, 5, '2017-01-28 08:07:07');

-- --------------------------------------------------------

--
-- Table structure for table `receiptdetails`
--

DROP TABLE IF EXISTS `receiptdetails`;
CREATE TABLE `receiptdetails` (
  `receiptId` int(11) NOT NULL,
  `receiptNumber` varchar(50) DEFAULT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `receiptDate` varchar(50) DEFAULT NULL,
  `receiptAmount` varchar(50) DEFAULT NULL,
  `paymentMode` varchar(20) DEFAULT NULL,
  `chequeNumber` varchar(50) DEFAULT NULL,
  `chequeDate` varchar(50) DEFAULT NULL,
  `receivedFrom` varchar(200) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receiptdetails`
--

INSERT INTO `receiptdetails` (`receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, '1273298679', 2, '01/27/2017', '500', 'Cheque', 'QWER18768768', '01/30/2017', 'Roger', 5, '2017-01-28 08:11:17', NULL, '2017-01-27 21:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `servicemaster`
--

DROP TABLE IF EXISTS `servicemaster`;
CREATE TABLE `servicemaster` (
  `serviceId` int(11) NOT NULL,
  `service` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A- Active, I- Inactive',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicemaster`
--

INSERT INTO `servicemaster` (`serviceId`, `service`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Visit', 'A', 1, '2017-01-31 07:09:16', 1, '2017-01-31 10:19:33'),
(2, 'Preliminary', 'A', 1, '2017-01-31 23:12:35', NULL, '2017-01-31 20:16:20'),
(3, 'Professional Services', 'A', 1, '2017-01-31 23:12:44', NULL, '2017-01-31 12:12:44');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE `subcategories` (
  `subId` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subId`, `categoryId`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'Jewellers Block', 'G', 'A', '1', '2017-01-21 12:51:02', '', '2017-01-26 20:27:18'),
(2, 2, 'Contractors All Risks (Fire)', 'G', 'A', '1', '2017-01-21 12:51:20', '', '2017-01-20 14:51:20'),
(3, 1, 'Risk Survey', 'G', 'A', '1', '2017-01-22 10:20:09', '', '2017-01-26 20:23:06'),
(4, 1, 'Marine Warranty Survey', 'G', 'A', '1', '2017-01-22 10:22:29', '', '2017-01-26 20:23:10'),
(5, 2, 'Contractors All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:24:52', '', '2017-01-21 12:36:24'),
(6, 2, 'Contractors All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:25:12', '', '2017-01-21 12:36:41'),
(7, 2, 'Contractors All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:28:19', '', '2017-01-21 12:36:46'),
(8, 2, 'Contractors All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:28:54', '', '2017-01-21 12:36:50'),
(9, 2, 'Contractors All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:29:42', '', '2017-01-21 12:36:54'),
(10, 2, 'Erection All Risks (Fire)', 'G', 'A', '1', '2017-01-22 10:29:59', '', '2017-01-21 12:36:58'),
(11, 2, 'Erection All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:32:03', '', '2017-01-21 12:37:04'),
(12, 2, 'Erection All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:34:57', '', '2017-01-21 12:37:08'),
(13, 2, 'Erection All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:39:12', '', '2017-01-21 12:39:12'),
(14, 2, 'Erection All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:40:40', '', '2017-01-21 12:40:40'),
(15, 2, 'Erection All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:40:55', '', '2017-01-21 12:40:55'),
(16, 2, 'Contractors Plant and Machinery (Theft)', 'G', 'A', '1', '2017-01-22 10:41:30', '', '2017-01-21 12:41:30'),
(17, 2, 'Contractors Plant and Machinery (Fire)', 'G', 'A', '1', '2017-01-22 10:42:02', '', '2017-01-21 12:42:02'),
(18, 2, 'Contractors Plant and Machinery (Other)', 'G', 'A', '1', '2017-01-22 10:42:35', '', '2017-01-21 12:42:35'),
(19, 3, 'Domestic / Household (Fire)', 'G', 'A', '1', '2017-01-22 10:43:07', '', '2017-01-21 12:43:07'),
(20, 3, 'Domestic / Household (Theft)', 'G', 'A', '1', '2017-01-22 10:43:52', '', '2017-01-21 12:43:52'),
(21, 3, 'Domestic / Household (Water)', 'G', 'A', '1', '2017-01-22 10:44:23', '', '2017-01-21 12:44:23'),
(22, 3, 'Domestic / Household (Other)', 'G', 'A', '1', '2017-01-22 10:44:44', '', '2017-01-21 12:44:44'),
(23, 4, 'Marine Cargo', 'M', 'A', '1', '2017-01-22 10:45:06', '', '2017-01-21 12:45:06'),
(24, 4, 'Marine Full', 'M', 'A', '1', '2017-01-22 10:45:24', '', '2017-01-21 12:45:24'),
(25, 4, 'Goods in Transit', 'M', 'A', '1', '2017-01-22 10:46:26', '', '2017-01-21 12:46:26'),
(26, 5, 'Machinery Breakdown', 'G', 'A', '1', '2017-01-22 10:47:43', '', '2017-01-21 12:47:43'),
(27, 5, 'Machinery Breakdown + LOP', 'G', 'A', '1', '2017-01-22 10:48:03', '', '2017-01-21 12:48:03'),
(28, 5, 'Stock Deterioration', 'G', 'A', '1', '2017-01-22 10:48:43', '', '2017-01-21 12:48:43'),
(29, 5, 'Stock Deterioration + LOP', 'G', 'A', '1', '2017-01-22 10:50:11', '', '2017-01-21 12:50:11'),
(30, 9, 'Fidelity Guarantee', 'G', 'A', '1', '2017-01-22 10:51:48', '', '2017-01-21 12:51:48'),
(31, 9, 'Loss of Money', 'G', 'A', '1', '2017-01-22 10:52:58', '', '2017-01-21 12:52:58'),
(32, 9, 'Business Interuption', 'G', 'A', '1', '2017-01-22 10:53:14', '', '2017-01-21 12:53:14'),
(33, 9, 'Bankers Blanket Bond', 'G', 'A', '1', '2017-01-22 10:53:31', '', '2017-01-21 12:53:31'),
(34, 9, 'Travel', 'G', 'A', '1', '2017-01-22 10:53:45', '', '2017-01-21 12:53:45'),
(35, 6, 'Commercial property (Fire)', 'G', 'A', '1', '2017-01-22 10:54:04', '', '2017-01-21 12:54:04'),
(36, 6, 'Commercial property (Theft)', 'G', 'A', '1', '2017-01-22 10:54:21', '', '2017-01-21 12:54:21'),
(37, 6, 'Commercial property (Water)', 'G', 'A', '1', '2017-01-22 10:54:51', '', '2017-01-21 12:54:51'),
(38, 6, 'Commercial property (Other)', 'G', 'A', '1', '2017-01-22 10:55:51', '', '2017-01-21 12:55:51'),
(39, 6, 'Commercial property + BI', 'G', 'A', '1', '2017-01-22 10:56:20', '', '2017-01-21 12:56:20'),
(40, 7, 'Employers Liability', 'G', 'A', '1', '2017-01-22 10:58:44', '', '2017-01-21 12:58:44'),
(41, 7, 'Third party / Public Liability (Injury)', 'G', 'A', '1', '2017-01-22 10:59:54', '', '2017-01-21 12:59:54'),
(42, 7, 'Third party / Public Liability (Non-Injury)', 'G', 'A', '1', '2017-01-22 11:00:11', '', '2017-01-21 13:00:11'),
(43, 7, 'Professional Identity', 'G', 'A', '1', '2017-01-22 11:00:53', '', '2017-01-21 13:00:53'),
(44, 7, 'Product Liability', 'G', 'A', '1', '2017-01-22 11:01:12', '', '2017-01-21 13:01:12'),
(45, 7, 'Hauliers Liability', 'G', 'A', '1', '2017-01-22 11:02:40', '', '2017-01-21 13:02:40'),
(46, 7, 'Motor Liability', 'G', 'A', '1', '2017-01-22 11:02:59', '', '2017-01-21 13:02:59'),
(47, 7, 'Workman''s Compensation', 'G', 'A', '1', '2017-01-22 11:03:13', '', '2017-01-21 13:03:13'),
(48, 7, 'Motor', 'G', 'A', '1', '2017-01-22 11:05:15', '', '2017-01-21 13:05:15'),
(49, 8, 'Consultancy / Valuation', 'G', 'A', '1', '2017-01-22 11:05:39', '', '2017-01-21 13:05:39'),
(50, 8, 'Recovery', 'G', 'A', '1', '2017-01-22 11:06:02', '', '2017-01-21 13:06:02'),
(51, 8, 'Experts opinion/ Arbitration', 'G', 'A', '1', '2017-01-22 11:06:33', '', '2017-01-21 13:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `timeandexpense`
--

DROP TABLE IF EXISTS `timeandexpense`;
CREATE TABLE `timeandexpense` (
  `reportId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `reportDate` varchar(50) DEFAULT NULL,
  `reportTime` varchar(50) DEFAULT NULL,
  `serviceId` int(11) DEFAULT NULL,
  `ratePerHour` varchar(100) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeandexpense`
--

INSERT INTO `timeandexpense` (`reportId`, `claimId`, `userId`, `reportDate`, `reportTime`, `serviceId`, `ratePerHour`, `amount`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 2, '2017-01-30', '13:00', 1, '200 AED per hour', '1000', 1, '2017-01-31 21:44:30', NULL, '2017-01-31 10:44:30'),
(2, 1, 2, '2017-01-31', '12:45', 1, '200 AED per hour', '1000', 1, '2017-01-31 21:45:29', NULL, '2017-01-31 10:45:29'),
(3, 1, 7, '2017-01-31', '16:00', 1, '300 AED per Hour', '900', 1, '2017-01-31 22:12:23', NULL, '2017-01-31 11:12:23');

-- --------------------------------------------------------

--
-- Table structure for table `usermaster`
--

DROP TABLE IF EXISTS `usermaster`;
CREATE TABLE `usermaster` (
  `userId` int(11) NOT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `lastLoggedIn` datetime DEFAULT NULL,
  `isAdmin` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'Y-Yes, N-No',
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `aboutMe` varchar(500) DEFAULT NULL,
  `profilePicture` varchar(100) DEFAULT NULL,
  `userAccess` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usermaster`
--

INSERT INTO `usermaster` (`userId`, `userName`, `firstName`, `lastName`, `emailId`, `password`, `lastLoggedIn`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `aboutMe`, `profilePicture`, `userAccess`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'graham', 'Graham', 'Whitelaw', 'graham@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-01-31 19:13:17', 'Y', 'test', 'Dubai', 'UAE', '', 'test', '1484555604678.jpg', '1,2,3,4,5,6', 'A', '', '2017-01-16 00:00:00', '1', '2017-02-01 03:08:58'),
(2, 'mark', 'Mark', 'Smith', 'mark.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:45:23', '', '2017-02-01 03:08:09'),
(3, 'chris', 'Chris', 'Wylie', 'chris.wylie@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:46:51', '', '2017-02-01 03:08:11'),
(4, 'craig', 'Craig', 'Jones', 'craig.jones@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'CDE street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:47:59', '', '2017-02-01 03:08:13'),
(5, 'koshy', 'Jerin', 'Koshy', 'jerin.koshy@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-01-28 07:43:07', 'N', 'QWE Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:49:13', '', '2017-02-01 03:08:15'),
(6, 'sham', 'Sham', 'Jacob', 'sham.jacob@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:50:22', '', '2017-02-01 03:08:17'),
(7, 'steven', 'Steve', 'Smith', 'steve.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ASD Road', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:12', '1', '2017-02-01 03:08:19'),
(8, 'hani', 'Moein', 'Hani', 'moein.hani@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'MNO Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:57', '', '2017-02-01 03:08:21'),
(9, 'shadi', 'Shadi', 'Abu Razouq', 'shadi.razouq@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'QWERTY Street', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:52:39', '', '2017-02-01 03:08:23'),
(10, 'simbulan', 'Maricel', 'Simbulan', 'maricel.simbulan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:02', '', '2017-02-01 03:08:25'),
(11, 'jen', 'Jen', 'Mamalayan', 'jen.mamalayan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:40', '', '2017-02-01 03:08:27'),
(12, 'jackie', 'Jackie', 'Whitelaw', 'jackie.whitelaw@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:55:44', '', '2017-02-01 03:08:29'),
(13, 'flory', 'Flory', 'Dâ€™Mello', 'flory.dmello@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:56:23', '', '2017-02-01 03:08:32'),
(14, 'testing', 'test', 'user', 'testing@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-02-01 08:53:15', 'N', 'test', 'test', 'test', '', NULL, NULL, '2,3,4,5', 'A', '1', '2017-02-01 08:37:47', '1', '2017-02-01 03:23:15');

-- --------------------------------------------------------

--
-- Table structure for table `visitupdates`
--

DROP TABLE IF EXISTS `visitupdates`;
CREATE TABLE `visitupdates` (
  `visitId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'N-Notes, P-Photo, E- Email',
  `updateContent` varchar(500) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitupdates`
--

INSERT INTO `visitupdates` (`visitId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES
(1, 1, 'P', 'fire-smoke-damage-repair-11485570481997.jpg', 'fire-smoke-damage-repair-1.jpg', 5, '2017-01-28 07:58:01'),
(2, 1, 'P', 'images1485570487885.jpg', 'images.jpg', 5, '2017-01-28 07:58:07'),
(3, 1, 'N', 'The building is completely destroyed because of the fire. Estimate big loss.', NULL, 5, '2017-01-28 07:59:01'),
(4, 2, 'P', 'images (1)1485571000726.jpg', 'images (1).jpg', 5, '2017-01-28 08:06:40'),
(5, 2, 'N', 'Heavy damage', NULL, 5, '2017-01-28 08:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `workingupdates`
--

DROP TABLE IF EXISTS `workingupdates`;
CREATE TABLE `workingupdates` (
  `workingId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL,
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjusters`
--
ALTER TABLE `adjusters`
  ADD PRIMARY KEY (`adjusterId`);

--
-- Indexes for table `brokers`
--
ALTER TABLE `brokers`
  ADD PRIMARY KEY (`brokerId`);

--
-- Indexes for table `categorymaster`
--
ALTER TABLE `categorymaster`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `claimmaster`
--
ALTER TABLE `claimmaster`
  ADD PRIMARY KEY (`claimId`);

--
-- Indexes for table `clientmaster`
--
ALTER TABLE `clientmaster`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD PRIMARY KEY (`detailsId`);

--
-- Indexes for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  ADD PRIMARY KEY (`invoiceId`);

--
-- Indexes for table `officemaster`
--
ALTER TABLE `officemaster`
  ADD PRIMARY KEY (`officeId`);

--
-- Indexes for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  ADD PRIMARY KEY (`prelimId`);

--
-- Indexes for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  ADD PRIMARY KEY (`receiptId`);

--
-- Indexes for table `servicemaster`
--
ALTER TABLE `servicemaster`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`subId`);

--
-- Indexes for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  ADD PRIMARY KEY (`reportId`);

--
-- Indexes for table `usermaster`
--
ALTER TABLE `usermaster`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `visitupdates`
--
ALTER TABLE `visitupdates`
  ADD PRIMARY KEY (`visitId`);

--
-- Indexes for table `workingupdates`
--
ALTER TABLE `workingupdates`
  ADD PRIMARY KEY (`workingId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjusters`
--
ALTER TABLE `adjusters`
  MODIFY `adjusterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `brokers`
--
ALTER TABLE `brokers`
  MODIFY `brokerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categorymaster`
--
ALTER TABLE `categorymaster`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `claimmaster`
--
ALTER TABLE `claimmaster`
  MODIFY `claimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clientmaster`
--
ALTER TABLE `clientmaster`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  MODIFY `detailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  MODIFY `invoiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `officemaster`
--
ALTER TABLE `officemaster`
  MODIFY `officeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  MODIFY `prelimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  MODIFY `receiptId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `servicemaster`
--
ALTER TABLE `servicemaster`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  MODIFY `reportId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usermaster`
--
ALTER TABLE `usermaster`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `visitupdates`
--
ALTER TABLE `visitupdates`
  MODIFY `visitId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `workingupdates`
--
ALTER TABLE `workingupdates`
  MODIFY `workingId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
