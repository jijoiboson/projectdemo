-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 25, 2017 at 07:47 PM
-- Server version: 5.5.45
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjusters`
--

CREATE TABLE `adjusters` (
  `adjusterId` int(11) NOT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adjusters`
--

INSERT INTO `adjusters` (`adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Graham', 'Whitelaw', 'graham', 'graham.whitelaw@wla.com', 'Dubai', 'UAE', '11111', 'hsdkjhskjdh sdf', 'A', '1', '2017-01-19 21:49:10', '1', '2017-01-21 09:58:04'),
(2, 'Jerin', 'Koshy', 'koshy', 'jerin.koshy@wla.com', 'Dubai', 'UAE', '', 'ABC Street', 'A', '1', '2017-01-22 11:59:44', '', '2017-01-26 16:14:38');

-- --------------------------------------------------------

--
-- Table structure for table `brokers`
--

CREATE TABLE `brokers` (
  `brokerId` int(11) NOT NULL,
  `employeeId` varchar(50) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brokers`
--

INSERT INTO `brokers` (`brokerId`, `employeeId`, `firstName`, `lastName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'KWOE109', 'Arya', 'Insurance Brokerage', '97142212838', 'aryainsurance@gmail.com', 'Office Tower Baniyas Rd', 'Dubai', '', 'UAE', 'A', '1', '2017-01-19 21:21:15', '1', '2017-01-27 16:34:33'),
(2, '46KML20', 'Greenshield', 'Insurance Brokers LLC', '97143882200', 'greenshield@gmail.com', 'Boulevard Plaza Tower 1, Burj Khalifa Downtown, Dubai - Sheikh Mohammed bin Rashid Blvd', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:33:36', NULL, '2017-01-27 16:33:36'),
(3, 'WLA1987', 'Omega', 'Insurance Brokers LLC', '97143024555', 'omegainsurance@gmail.com', 'Al Brajem Business Center', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:36:09', NULL, '2017-01-27 16:36:09'),
(4, 'QWI1878', 'Al Manara', 'Insurance Services', '97165444660', 'almanara@gmail.com', 'Corniche St', 'Sharjah', '', 'UAE', 'A', '1', '2017-01-28 07:37:48', NULL, '2017-01-27 16:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `categorymaster`
--

CREATE TABLE `categorymaster` (
  `categoryId` int(11) NOT NULL,
  `category` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active,I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorymaster`
--

INSERT INTO `categorymaster` (`categoryId`, `category`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Surveys', 'G', 'A', '1', '2017-01-20 21:40:20', '', '2017-01-26 16:23:21'),
(2, 'Engineering', 'G', 'A', '1', '2017-01-20 21:47:06', '', '2017-01-20 10:29:49'),
(3, 'Domestic', 'G', 'A', '1', '2017-01-20 21:47:32', '', '2017-01-20 10:29:53'),
(4, 'Marine', 'M', 'A', '1', '2017-01-22 10:15:33', '', '2017-01-21 08:15:33'),
(5, 'Machinery', 'G', 'A', '1', '2017-01-22 10:17:09', '', '2017-01-21 08:17:09'),
(6, 'Commercial', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 08:17:30'),
(7, 'Liability', 'G', 'A', '1', '2017-01-22 10:17:30', '', '2017-01-21 08:17:59'),
(8, 'Miscellaneous', 'G', 'A', '1', '2017-01-22 10:18:17', '', '2017-01-21 08:18:17'),
(9, 'Pecuniary', 'G', 'A', '1', '2017-01-22 10:50:58', '', '2017-01-21 08:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `claimmaster`
--

CREATE TABLE `claimmaster` (
  `claimId` int(11) NOT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `officeId` int(11) DEFAULT NULL,
  `policyNumber` varchar(50) DEFAULT NULL,
  `insuredName` varchar(50) DEFAULT NULL,
  `insurerName` varchar(200) DEFAULT NULL,
  `claimReference` varchar(50) DEFAULT NULL,
  `brokerReference` varchar(50) DEFAULT NULL,
  `contactPerson` varchar(50) DEFAULT NULL,
  `contactNumber` varchar(50) DEFAULT NULL,
  `insurerContact` varchar(50) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `brokerId` int(11) DEFAULT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `instructionTime` varchar(20) DEFAULT NULL,
  `instructionDate` varchar(20) DEFAULT NULL,
  `contactTime` varchar(20) DEFAULT NULL,
  `contactDate` varchar(20) DEFAULT NULL,
  `surveyTime` varchar(20) DEFAULT NULL,
  `surveyDate` varchar(20) DEFAULT NULL,
  `preliminaryDate` varchar(50) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `jobStatus` varchar(2) NOT NULL DEFAULT 'O' COMMENT 'O-Open, V-Visit,P-Preliminary, W-Working, C-Closed',
  `frozen` varchar(2) NOT NULL DEFAULT 'N',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `claimmaster`
--

INSERT INTO `claimmaster` (`claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`) VALUES
(1, 'DXB/G/0001/17', NULL, 1, 'POLICY1001', 'Trade House LTD', 'Robert Phillips', NULL, NULL, NULL, NULL, NULL, 'Sharjah, UAE', 1, 1, 1, 6, 35, '13:00', '01/18/2017', '14:00', '01/18/2017', '16:00', '01/23/2017', '2017-03-20', NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:47:12', '2017-03-20 06:33:09', NULL),
(2, 'LON/G/0002/17', NULL, 2, 'POLICY1002', 'Omer Hassan Elamin El Sheikh', 'Jacob Anderson', NULL, NULL, NULL, NULL, NULL, 'Dubai, UAE', 2, 2, 2, 3, 21, '20:00', '01/11/2017', '10:00', '01/12/2017', '11:00', '01/13/2017', '2017-01-28', NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:51:00', '2017-01-27 17:08:16', NULL),
(3, 'DXB/G/0003/17', NULL, 1, 'POLICY1003', 'Mahdy Ramazan Jahan Far', 'Ethan Reyes', NULL, NULL, NULL, NULL, NULL, 'Dubai, UAE', 3, 3, 1, 6, 35, '20:00', '01/16/2017', '09:00', '01/17/2017', '16:00', '01/17/2017', NULL, NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:53:15', '2017-02-05 12:33:40', NULL),
(4, 'LON/G/0004/17', NULL, 2, 'POLICY1004', 'Creneau International', 'Omar', NULL, NULL, NULL, NULL, NULL, 'Dubai, UAE', 5, 4, 2, 2, 5, '09:00', '01/16/2017', '11:00', '01/16/2017', '20:00', '01/16/2017', NULL, NULL, NULL, NULL, NULL, 'I', 'N', 5, '2017-01-28 07:55:20', '2017-03-20 06:26:04', NULL),
(5, 'LON/G/0005/17', NULL, 2, 'asdfw23', 'asdfsdf234', 'asdfasdf234', NULL, NULL, NULL, NULL, NULL, 'ADFVASDG', 2, 1, 1, 3, 19, '01:58', '02/02/2017', '12:00', '02/06/2017', '01:59', '02/07/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-02-04 08:37:13', '2017-02-04 04:37:13', NULL),
(6, 'DXB/G/0006/17', NULL, 1, 'P1234567890', 'Name', 'Insurer', NULL, NULL, NULL, NULL, NULL, 'DUBAI', 5, 3, 2, 6, 36, '10:30', '02/10/2017', '09:57', '02/09/2017', '12:59', '02/10/2017', '2017-02-04', NULL, NULL, NULL, NULL, 'I', 'N', 1, '2017-02-04 18:54:16', '2017-02-04 15:32:24', NULL),
(7, 'LON/G/0007/17', NULL, 2, 'P90909090', 'Name', 'Name', NULL, NULL, NULL, NULL, NULL, 'Sharjah', 4, 3, 2, 8, 50, '22:59', '02/04/2017', '12:59', '01/27/2017', '13:59', '02/15/2017', NULL, NULL, NULL, NULL, NULL, 'V', 'N', 1, '2017-02-04 19:00:42', '2017-02-05 06:09:24', NULL),
(8, 'AUH/G/0008/17', NULL, 3, '12345677', 'XXXXX', 'ALTA', NULL, NULL, NULL, NULL, NULL, 'Abudhabi', 5, 3, 1, 6, 37, '12:59', '02/15/2017', '12:59', '02/13/2017', '12:59', '02/13/2017', '2017-02-05', NULL, NULL, NULL, NULL, 'R', 'N', 1, '2017-02-05 09:24:57', '2017-02-05 05:32:37', NULL),
(9, 'DXB/G/0004/17', NULL, 1, '2/1/3030/2292/12', 'Burt', '', 'F 3890', 'F/osis8/238', 'Jezza', '0505505656', 'Jamalur Rashid', 'Silicon Oasis', 1, 3, 1, 6, 35, '1030', '03/20/2017', '1030', '03/20/2017', '1030', '03/21/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 09:05:05', '2017-03-20 05:05:05', NULL),
(10, 'LON/G/0005/17', NULL, 2, 'dqwwjmpd', 'pefj', '', 'fid', 'pdj', 'pjf', 'pdoj', 'qspojd', 'qef', 2, 3, 1, 3, 20, '1030', '03/20/2017', 'TBC', '03/20/2017', 'TBC', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 09:06:42', '2017-03-20 05:06:42', NULL),
(11, 'DXB/G/0005/17', NULL, 1, '123456789', 'ASDFGH', '', '124578', '123456474', 'KAKKK', '1234567879', '111111', 'TEST', 1, 1, 2, 3, 22, '12345645', '03/20/2017', '11111', '03/20/2017', '11111', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 10:09:53', '2017-03-20 06:09:53', NULL),
(12, 'AUH/G/0002/17', NULL, 3, '44444', 'KKKKK', '', 'KKKK', 'KKKK', 'KKKK', 'KKK', 'KKKK', 'NATURE', 5, 3, 1, 8, 51, '1222', '03/20/2017', '1221', '03/20/2017', '12111', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 10:11:22', '2017-03-20 06:11:22', NULL),
(13, 'LON/G/0006/17', NULL, 2, '1111111', '1111', '', '1111', '1111', '111', '111', '111', 'LOSS', 4, 3, 2, 8, 51, '1111', '03/20/2017', '11111', '03/20/2017', '1111', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 10:12:39', '2017-03-20 06:12:39', NULL),
(14, 'DXB/G/0006/17', NULL, 1, '122222', '11111', '', '11111', '1111', '1', '111', '1111', 'LOSS', 2, 4, 1, 6, 37, '111', '03/20/2017', '444', '03/20/2017', '111', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 10:13:52', '2017-03-20 06:13:52', NULL),
(15, 'DXB/G/0007/17', NULL, 1, '11111', '1111', '', '1111', '111', '111', '111', '111', 'LOSS', 3, 3, 1, 9, 32, '10:30', '03/21/2017', 'TBC', '03/20/2017', 'TBA', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-20 10:21:58', '2017-03-20 06:21:58', NULL),
(16, 'DXB/G/0008/17', NULL, 1, 'POLICY NUMBER', 'INSURED', '', 'CLAINM REFERENCE', 'BROKER', 'CONTACT PERSON', 'CONTACT NUMBER', 'INS CONTACT', 'LOCATION OF LOSS', 4, 4, 1, 5, 28, 'INS RECEIVED', '03/20/2017', 'CONTACT MADE', '03/20/2017', 'SURVEY ARRANGED', '03/20/2017', NULL, NULL, NULL, NULL, NULL, 'I', 'N', 1, '2017-03-20 10:40:35', '2017-03-20 06:47:09', NULL),
(17, 'DXB/G/0009/17', NULL, 1, 'TBE', 'TBA', '', 'TBA', 'TBA', 'TBA', 'TBA', 'TBA', 'DUBAI', 1, 4, 1, 6, 38, '10:30', '03/20/2017', 'TBA', '03/20/2017', 'TBA', '03/20/2017', '2017-03-20', NULL, NULL, NULL, NULL, 'P', 'N', 17, '2017-03-20 11:00:36', '2017-03-20 07:05:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clientmaster`
--

CREATE TABLE `clientmaster` (
  `clientId` int(11) NOT NULL,
  `referenceId` varchar(100) DEFAULT NULL,
  `clientName` varchar(300) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A - Active, I - Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientmaster`
--

INSERT INTO `clientmaster` (`clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'HAR201600001\n', 'Oman Insurance Company, Dxb\n', '9998887770', 'qwerty@gmail.com', 'Abc address', 'Dubai', '88877', 'UAE', 'A', '1', '2017-01-20 22:08:14', '', '2017-01-21 09:40:12'),
(2, '2013ENG20160100001', 'Ras Al Khaimah Insurance Company', '8887776667', 'abcd@gmail.com', 'RAK', 'RAK', '88888', 'UAE', 'A', '1', '2017-01-22 00:00:00', '', '2017-01-26 16:16:57'),
(3, 'D1C-E19-150003', 'Abu Dhabi National Insurance Company, Dxb', '999999999', 'adninsurance@gmail.com', 'Al Muraikhi Tower, Al Maktoum Street,Opp The Metropolitan PalaceØŒ Deira', 'Diera', '', 'UAE', 'A', '1', '2017-01-28 07:39:31', NULL, '2017-01-27 16:39:31'),
(4, 'F/001/X/16', 'Al Ittihad Al Watani Insurance', '97142823266', 'aiawinsurance@gmail.com', 'M01,M Floor,Al Durrah Building 2,Garhoud', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:41:07', NULL, '2017-01-27 16:41:07'),
(5, '31/PRC/1600003', 'Zurich Insurance Middle East', '971504653102', 'zime@gmail.com', 'Sama Tower - Sheikh Zayed Road', 'Dubai', '', 'UAE', 'A', '1', '2017-01-28 07:42:29', NULL, '2017-01-27 16:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetails`
--

CREATE TABLE `invoicedetails` (
  `detailsId` int(11) NOT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `narrationText` varchar(400) DEFAULT NULL,
  `timelyRate` varchar(200) DEFAULT NULL,
  `timelyText` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicedetails`
--

INSERT INTO `invoicedetails` (`detailsId`, `invoiceId`, `description`, `narrationText`, `timelyRate`, `timelyText`, `amount`) VALUES
(1, 1, 'Professional Loss Adjusting Fees', NULL, '4 Hours', '200 AED per hour', '800'),
(2, 1, 'Photographs/Photocopying', NULL, '', '', '200'),
(3, 1, 'Telephone/Fax/Post', NULL, '', '', '100'),
(4, 1, 'Miscellaneous', NULL, '', '', '200'),
(5, 2, 'Professional Loss Adjusting Fees', NULL, '3 Hours', '400 AED per Hour', '1200'),
(6, 2, 'Photographs/Photocopying', NULL, '', '', '100'),
(7, 2, 'Telephone/Fax/Post', NULL, '', '', '100'),
(8, 2, 'Miscellaneous', NULL, '', '', '50'),
(9, 3, 'Professional Loss Adjusting Fees', NULL, '4 Hours', '200 AED Per Hour', '800'),
(10, 3, 'Photographs/Photocopying', NULL, '', '', '200'),
(11, 3, 'Telephone/Fax/Post', NULL, '', '', '100'),
(12, 3, 'Miscellaneous', NULL, '', '', '100'),
(13, 4, 'Testing', NULL, '', '', '1000'),
(14, 4, 'Visit', NULL, '', '', '600'),
(15, 5, 'Visit', NULL, '400', '100 AED per hour', '400'),
(16, 5, 'Testing', NULL, '', '', '600'),
(17, 6, 'Visit', NULL, '111', '', '1000'),
(18, 6, 'Professional Services', NULL, '', '', '1000'),
(19, 6, 'Taxi', NULL, '', '', '100'),
(20, 7, 'Taxi', NULL, '5', '34534', '35345345'),
(21, 8, 'Visit', NULL, '5 hours', '500', '72'),
(22, 10, 'Visit', NULL, '5 hours', '34534', '2344'),
(23, 11, 'Visit', NULL, '', '', '35434'),
(24, 13, 'Visit', NULL, '', '', '5666'),
(25, 14, 'Professional Services', NULL, '', '', '1000'),
(26, 15, 'Visit', 'hhhhhhhh', NULL, NULL, '100000'),
(27, 15, 'Professional Services', 'Professional Fee', NULL, NULL, '20000'),
(28, 16, 'Professional Services', '', NULL, NULL, '1200'),
(29, 16, 'Photographer', '', NULL, NULL, '500'),
(30, 17, 'Professional Services', '', NULL, NULL, '1200'),
(31, 17, 'Photographer', '', NULL, NULL, '498'),
(32, 18, 'Travel Expenses', 'tRAVEL', NULL, NULL, '1000'),
(33, 19, 'Professional Loss Adjusting Fees', '', NULL, NULL, '100'),
(34, 19, 'Photographs / Photocopying', '', NULL, NULL, '200'),
(35, 19, 'Translation', '', NULL, NULL, '300'),
(36, 19, 'Telephone / Post / Fax', '', NULL, NULL, '400'),
(37, 19, 'Travel Expenses', '', NULL, NULL, '5000'),
(38, 19, 'Miscellaneous', '', NULL, NULL, '100000'),
(39, 20, 'Telephone / Post / Fax', '', NULL, NULL, '5001'),
(40, 20, 'Translation', '', NULL, NULL, '2000'),
(41, 20, 'Travel Expenses', '', NULL, NULL, '10000');

-- --------------------------------------------------------

--
-- Table structure for table `invoicemaster`
--

CREATE TABLE `invoicemaster` (
  `invoiceId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `invoiceNumber` varchar(100) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `faoName` varchar(200) DEFAULT NULL,
  `toName` varchar(200) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `invoiceDate` varchar(20) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `narrationText` varchar(500) DEFAULT NULL,
  `totalAmount` varchar(100) DEFAULT NULL,
  `totalInWords` varchar(500) DEFAULT NULL,
  `invoiceTerms` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoicemaster`
--

INSERT INTO `invoicemaster` (`invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `narrationText`, `totalAmount`, `totalInWords`, `invoiceTerms`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'DXB/G/0001/17', 'INS98777PM', 'AED', 'Robert Phillips', 'Trade House LTD', 'DXBTD1234', '01/28/2017', 1, 'Sharjah, UAE', 'Damage cover', '1300', 'One Thousand Three Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 5, '2017-01-28 08:04:47', NULL, '2017-01-27 17:04:47'),
(2, 2, 'LON/G/0002/17', 'INS10988', 'AED', 'Roger', 'Omer Hassan Elamin El Sheikh', 'LON1002677', '01/27/2017', 2, 'Dubai, UAE', 'Damage cover', '1450', 'One Thousand Four Hundred Fifty Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 5, '2017-01-28 08:08:16', NULL, '2017-01-27 17:08:16'),
(3, 1, 'DXB/G/0001/17', '1303941142', 'AED', 'Testing', 'Trade House LTD', 'HAR987987', '01/24/2017', 1, 'Sharjah, UAE', 'Testing', '1200', 'One Thousand Two Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-01-31 20:23:21', NULL, '2017-01-31 05:23:21'),
(4, 1, 'DXB/G/0001/17', '1264501671', 'AED', 'Testing', 'Trade House LTD', 'HAR3879879', '02/09/2017', 1, 'Sharjah, UAE', 'Testing', '1600', 'One Thousand Six Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-01 00:04:35', NULL, '2017-01-31 09:04:35'),
(5, 1, 'DXB/G/0001/17', '1207345152', 'AED', 'Testing', 'Trade House LTD', 'HAR987987', '02/08/2017', 1, 'Sharjah, UAE', 'Testing', '1000', 'One Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-01 00:10:55', NULL, '2017-01-31 09:10:55'),
(6, 2, 'LON/G/0002/17', '1251722644', 'AED', 'FAO', 'Omer Hassan Elamin El Sheikh', 'SSSSSS', '02/02/2017', 2, 'Dubai, UAE', 'Test Narration', '2100', 'Two Thousand One Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-02 19:55:00', NULL, '2017-02-02 15:55:00'),
(7, 1, 'DXB/G/0001/17', '1214416249', 'USD', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/09/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '35345345', 'Thirty Five Million Three Hundred Forty Five Thousand Three Hundred Forty Five Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 08:56:09', NULL, '2017-02-04 04:56:09'),
(8, 1, 'DXB/G/0001/17', '1315874478', 'GBP', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/03/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '72', 'Seventy Two Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:09:51', NULL, '2017-02-04 06:09:51'),
(9, 1, 'DXB/G/0001/17', '1210681925', 'USD', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/03/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '72', 'Seventy Two Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:12:02', NULL, '2017-02-04 06:12:02'),
(10, 1, 'DXB/G/0001/17', '1250660113', 'USD', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/02/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '2344', 'Two Thousand Three Hundred Forty Four Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:16:46', NULL, '2017-02-04 06:16:46'),
(11, 1, 'DXB/G/0001/17', '1153517376', 'USD', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/02/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '35434', 'Thirty Five Thousand Four Hundred Thirty Four Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:17:42', NULL, '2017-02-04 06:17:42'),
(12, 1, 'DXB/G/0001/17', '1405269073', 'USD', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/02/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '35434', 'Thirty Five Thousand Four Hundred Thirty Four Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:20:23', NULL, '2017-02-04 06:20:23'),
(13, 1, 'DXB/G/0001/17', '1142227042', 'AED', 'asdfasdf', 'Trade House LTD', 'aDF F', '02/04/2017', 1, 'Sharjah, UAE', 'ASDFGAG', '5666', 'Five Thousand Six Hundred Sixty Six Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 10:22:18', NULL, '2017-02-04 06:22:18'),
(14, 6, 'DXB/G/0006/17', '1215924430', 'AED', 'FAO', 'Name', 'Reference', '02/15/2017', 5, 'DUBAI', 'Narration', '1000', 'One Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-04 19:32:24', NULL, '2017-02-04 15:32:24'),
(15, 8, 'AUH/G/0008/17', '1116031347', 'AED', 'FAQ', 'XXXXX', '111111', '02/09/2017', 5, 'Abudhabi', 'Array', '120000', 'One Hundred Twenty Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-05 09:31:07', NULL, '2017-02-05 05:31:07'),
(16, 3, 'DXB/G/0003/17', '1383445159', 'AED', 'Chamil Mapalagama', 'Mahdy Ramazan Jahan Far', 'fsfsfsfsf', '02/05/2017', 3, 'Dubai, UAE', 'Array', '1200', 'One Thousand Two Hundred Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-05 16:33:40', NULL, '2017-02-05 12:33:40'),
(17, 3, 'DXB/G/0003/17', '1383445159', 'AED', 'Chamil Mapalagama', 'Mahdy Ramazan Jahan Far', 'fsfsfsfsf', '02/05/2017', 3, 'Dubai, UAE', 'Array', '1698', 'One Thousand Six Hundred Ninety Eight Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-02-05 16:33:43', NULL, '2017-02-05 12:33:43'),
(18, 4, 'LON/G/0004/17', '1339250776', 'USD', 'SSSSS', 'Creneau International', '1111', '03/20/2017', 5, 'Dubai, UAE', 'Array', '1000', 'One Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-03-20 10:26:04', NULL, '2017-03-20 06:26:04'),
(19, 1, 'DXB/G/0001/17', '1224903619', 'AED', 'KKK', 'Trade House LTD', '11111', '03/20/2017', 1, 'Sharjah, UAE', 'Array', '106000', 'One Hundred Six Thousand Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-03-20 10:33:09', NULL, '2017-03-20 06:33:09'),
(20, 16, 'DXB/G/0008/17', '1215607299', 'USD', 'FAO', 'INSURED', 'REFERENCE', '03/20/2017', 4, 'LOCATION OF LOSS', 'Array', '17001', 'Seventeen Thousand One Only', 'Terms: Payable within Thirty (30) Days from Date of Invoice', 1, '2017-03-20 10:47:09', NULL, '2017-03-20 06:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `officemaster`
--

CREATE TABLE `officemaster` (
  `officeId` int(11) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officemaster`
--

INSERT INTO `officemaster` (`officeId`, `location`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Dubai', 'Whitelaw Chartered Loss Adjusters & Surveyors', 'DXB', 'A', '', '2017-01-14 00:00:00', '', '2017-01-13 06:05:38'),
(2, 'London', 'Whitelaw Chartered Loss Adjusters London', 'LON', 'A', '', '2017-01-14 00:00:00', '', '2017-01-26 16:11:42'),
(3, 'Abudhabi', 'Abudhabi office', 'AUH', 'A', '1', '2017-02-05 09:20:27', '1', '2017-03-20 05:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `prelimupdates`
--

CREATE TABLE `prelimupdates` (
  `prelimId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'R-Report, F-Fees',
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prelimupdates`
--

INSERT INTO `prelimupdates` (`prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES
(1, 6, 'R', 'Visa Page1486220292511.pdf', 'Visa Page.pdf', 1, '2017-02-04 18:58:12'),
(2, 6, 'F', '5000', NULL, 1, '2017-02-04 18:58:33'),
(3, 8, 'R', 'Letter for SM1486272445718.docx', 'Letter for SM.docx', 1, '2017-02-05 09:27:25'),
(4, 8, 'R', 'Passport - Visa 2013 expiration1486272465832.PDF', 'Passport - Visa 2013 expiration.PDF', 1, '2017-02-05 09:27:45'),
(5, 8, 'F', '2000', NULL, 1, '2017-02-05 09:28:00'),
(6, 8, 'F', '3000', NULL, 1, '2017-02-05 09:28:15'),
(7, 1, 'F', '4000', NULL, 1, '2017-03-20 08:56:55'),
(8, 17, 'R', 'front page1489993518999.jpg', 'front page.jpg', 17, '2017-03-20 11:05:18'),
(9, 17, 'F', '25000', NULL, 17, '2017-03-20 11:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `receiptdetails`
--

CREATE TABLE `receiptdetails` (
  `receiptId` int(11) NOT NULL,
  `receiptNumber` varchar(50) DEFAULT NULL,
  `invoiceId` int(11) DEFAULT NULL,
  `receiptDate` varchar(50) DEFAULT NULL,
  `receiptAmount` varchar(50) DEFAULT NULL,
  `paymentMode` varchar(20) DEFAULT NULL,
  `chequeNumber` varchar(50) DEFAULT NULL,
  `chequeDate` varchar(50) DEFAULT NULL,
  `receivedFrom` varchar(200) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receiptdetails`
--

INSERT INTO `receiptdetails` (`receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, '1273298679', 2, '01/27/2017', '500', 'Cheque', 'QWER18768768', '01/30/2017', 'Roger', 5, '2017-01-28 08:11:17', NULL, '2017-01-27 17:11:17'),
(2, '1335476702', 1, '02/03/2017', '234234', 'Cheque', '234234234234', '02/06/2017', '23434', 1, '2017-02-04 08:57:57', NULL, '2017-02-04 04:57:57'),
(3, '1300169086', 6, '02/02/2017', '23423', 'Cheque', '23434', '02/17/2017', '234234', 1, '2017-02-04 09:01:02', NULL, '2017-02-04 05:01:02'),
(4, '1407484797', 7, '02/10/2017', '35345345', 'Cheque', '12345', '02/10/2017', 'User1', 1, '2017-02-04 19:20:59', NULL, '2017-02-05 04:09:09'),
(5, '1262560698', 3, '02/11/2017', '1200', 'Cheque', '123456', '02/10/2017', 'Received From', 1, '2017-02-04 19:26:11', NULL, '2017-02-04 15:26:11'),
(6, '1382070301', 14, '02/17/2017', '1000', 'Cheque', 'c1234', '02/23/2017', 'Vinoth', 1, '2017-02-04 19:33:21', NULL, '2017-02-04 15:33:21'),
(7, '1354511382', 15, '02/15/2017', '120000', 'Cheque', '111111', '02/13/2017', 'Tessa', 1, '2017-02-05 09:32:37', NULL, '2017-02-05 05:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `servicemaster`
--

CREATE TABLE `servicemaster` (
  `serviceId` int(11) NOT NULL,
  `service` varchar(200) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A- Active, I- Inactive',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `servicemaster`
--

INSERT INTO `servicemaster` (`serviceId`, `service`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 'Professional Loss Adjusting Fees', 'A', 1, '2017-01-31 07:09:16', 1, '2017-02-25 02:20:25'),
(2, 'Photographs / Photocopying', 'A', 1, '2017-01-31 23:12:35', 1, '2017-02-25 02:19:00'),
(3, 'Telephone / Post / Fax', 'A', 1, '2017-01-31 23:12:44', 1, '2017-02-25 02:20:29'),
(4, 'Translation', 'A', 1, '2017-02-25 11:49:21', NULL, '2017-02-25 02:19:21'),
(5, 'Travel Expenses', 'A', 1, '2017-02-25 11:49:34', NULL, '2017-02-25 02:19:34'),
(7, 'Miscellaneous', 'A', 1, '2017-02-25 11:50:45', NULL, '2017-02-25 02:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `subId` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`subId`, `categoryId`, `name`, `prefix`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'Jewellers Block', 'G', 'A', '1', '2017-01-21 12:51:02', '', '2017-01-26 16:27:18'),
(2, 2, 'Contractors All Risks (Fire)', 'G', 'A', '1', '2017-01-21 12:51:20', '', '2017-01-20 10:51:20'),
(3, 1, 'Risk Survey', 'G', 'A', '1', '2017-01-22 10:20:09', '', '2017-01-26 16:23:06'),
(4, 1, 'Marine Warranty Survey', 'G', 'A', '1', '2017-01-22 10:22:29', '', '2017-01-26 16:23:10'),
(5, 2, 'Contractors All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:24:52', '', '2017-01-21 08:36:24'),
(6, 2, 'Contractors All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:25:12', '', '2017-01-21 08:36:41'),
(7, 2, 'Contractors All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:28:19', '', '2017-01-21 08:36:46'),
(8, 2, 'Contractors All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:28:54', '', '2017-01-21 08:36:50'),
(9, 2, 'Contractors All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:29:42', '', '2017-01-21 08:36:54'),
(10, 2, 'Erection All Risks (Fire)', 'G', 'A', '1', '2017-01-22 10:29:59', '', '2017-01-21 08:36:58'),
(11, 2, 'Erection All Risks (Theft)', 'G', 'A', '1', '2017-01-22 10:32:03', '', '2017-01-21 08:37:04'),
(12, 2, 'Erection All Risks (Water Damage)', 'G', 'A', '1', '2017-01-22 10:34:57', '', '2017-01-21 08:37:08'),
(13, 2, 'Erection All Risks (Other)', 'G', 'A', '1', '2017-01-22 10:39:12', '', '2017-01-21 08:39:12'),
(14, 2, 'Erection All Risks (Plant and Machinery)', 'G', 'A', '1', '2017-01-22 10:40:40', '', '2017-01-21 08:40:40'),
(15, 2, 'Erection All Risks (Underground Services)', 'G', 'A', '1', '2017-01-22 10:40:55', '', '2017-01-21 08:40:55'),
(16, 2, 'Contractors Plant and Machinery (Theft)', 'G', 'A', '1', '2017-01-22 10:41:30', '', '2017-01-21 08:41:30'),
(17, 2, 'Contractors Plant and Machinery (Fire)', 'G', 'A', '1', '2017-01-22 10:42:02', '', '2017-01-21 08:42:02'),
(18, 2, 'Contractors Plant and Machinery (Other)', 'G', 'A', '1', '2017-01-22 10:42:35', '', '2017-01-21 08:42:35'),
(19, 3, 'Domestic / Household (Fire)', 'G', 'A', '1', '2017-01-22 10:43:07', '', '2017-01-21 08:43:07'),
(20, 3, 'Domestic / Household (Theft)', 'G', 'A', '1', '2017-01-22 10:43:52', '', '2017-01-21 08:43:52'),
(21, 3, 'Domestic / Household (Water)', 'G', 'A', '1', '2017-01-22 10:44:23', '', '2017-01-21 08:44:23'),
(22, 3, 'Domestic / Household (Other)', 'G', 'A', '1', '2017-01-22 10:44:44', '', '2017-01-21 08:44:44'),
(23, 4, 'Marine Cargo', 'M', 'A', '1', '2017-01-22 10:45:06', '', '2017-01-21 08:45:06'),
(24, 4, 'Marine Full', 'M', 'A', '1', '2017-01-22 10:45:24', '', '2017-01-21 08:45:24'),
(25, 4, 'Goods in Transit', 'M', 'A', '1', '2017-01-22 10:46:26', '', '2017-01-21 08:46:26'),
(26, 5, 'Machinery Breakdown', 'G', 'A', '1', '2017-01-22 10:47:43', '', '2017-01-21 08:47:43'),
(27, 5, 'Machinery Breakdown + LOP', 'G', 'A', '1', '2017-01-22 10:48:03', '', '2017-01-21 08:48:03'),
(28, 5, 'Stock Deterioration', 'G', 'A', '1', '2017-01-22 10:48:43', '', '2017-01-21 08:48:43'),
(29, 5, 'Stock Deterioration + LOP', 'G', 'A', '1', '2017-01-22 10:50:11', '', '2017-01-21 08:50:11'),
(30, 9, 'Fidelity Guarantee', 'G', 'A', '1', '2017-01-22 10:51:48', '', '2017-01-21 08:51:48'),
(31, 9, 'Loss of Money', 'G', 'A', '1', '2017-01-22 10:52:58', '', '2017-01-21 08:52:58'),
(32, 9, 'Business Interuption', 'G', 'A', '1', '2017-01-22 10:53:14', '', '2017-01-21 08:53:14'),
(33, 9, 'Bankers Blanket Bond', 'G', 'A', '1', '2017-01-22 10:53:31', '', '2017-01-21 08:53:31'),
(34, 9, 'Travel', 'G', 'A', '1', '2017-01-22 10:53:45', '', '2017-01-21 08:53:45'),
(35, 6, 'Commercial property (Fire)', 'G', 'A', '1', '2017-01-22 10:54:04', '', '2017-01-21 08:54:04'),
(36, 6, 'Commercial property (Theft)', 'G', 'A', '1', '2017-01-22 10:54:21', '', '2017-01-21 08:54:21'),
(37, 6, 'Commercial property (Water)', 'G', 'A', '1', '2017-01-22 10:54:51', '', '2017-01-21 08:54:51'),
(38, 6, 'Commercial property (Other)', 'G', 'A', '1', '2017-01-22 10:55:51', '', '2017-01-21 08:55:51'),
(39, 6, 'Commercial property + BI', 'G', 'A', '1', '2017-01-22 10:56:20', '', '2017-01-21 08:56:20'),
(40, 7, 'Employers Liability', 'G', 'A', '1', '2017-01-22 10:58:44', '', '2017-01-21 08:58:44'),
(41, 7, 'Third party / Public Liability (Injury)', 'G', 'A', '1', '2017-01-22 10:59:54', '', '2017-01-21 08:59:54'),
(42, 7, 'Third party / Public Liability (Non-Injury)', 'G', 'A', '1', '2017-01-22 11:00:11', '', '2017-01-21 09:00:11'),
(43, 7, 'Professional Identity', 'G', 'A', '1', '2017-01-22 11:00:53', '', '2017-01-21 09:00:53'),
(44, 7, 'Product Liability', 'G', 'A', '1', '2017-01-22 11:01:12', '', '2017-01-21 09:01:12'),
(45, 7, 'Hauliers Liability', 'G', 'A', '1', '2017-01-22 11:02:40', '', '2017-01-21 09:02:40'),
(46, 7, 'Motor Liability', 'G', 'A', '1', '2017-01-22 11:02:59', '', '2017-01-21 09:02:59'),
(47, 7, 'Workman\'s Compensation', 'G', 'A', '1', '2017-01-22 11:03:13', '', '2017-01-21 09:03:13'),
(48, 7, 'Motor', 'G', 'A', '1', '2017-01-22 11:05:15', '', '2017-01-21 09:05:15'),
(49, 8, 'Consultancy / Valuation', 'G', 'A', '1', '2017-01-22 11:05:39', '', '2017-01-21 09:05:39'),
(50, 8, 'Recovery', 'G', 'A', '1', '2017-01-22 11:06:02', '', '2017-01-21 09:06:02'),
(51, 8, 'Experts opinion/ Arbitration', 'G', 'A', '1', '2017-01-22 11:06:33', '', '2017-01-21 09:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `timeandexpense`
--

CREATE TABLE `timeandexpense` (
  `reportId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `reportDate` varchar(50) DEFAULT NULL,
  `reportTime` varchar(50) DEFAULT NULL,
  `serviceId` int(11) DEFAULT NULL,
  `ratePerHour` varchar(100) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeandexpense`
--

INSERT INTO `timeandexpense` (`reportId`, `claimId`, `userId`, `reportDate`, `reportTime`, `serviceId`, `ratePerHour`, `amount`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 2, '2017-01-30', '13:00', 1, '200 AED per hour', '1000', 1, '2017-01-31 21:44:30', NULL, '2017-01-31 06:44:30'),
(2, 1, 2, '2017-01-31', '12:45', 1, '200 AED per hour', '1000', 1, '2017-01-31 21:45:29', NULL, '2017-01-31 06:45:29'),
(3, 1, 7, '2017-01-31', '16:00', 1, '300 AED per Hour', '900', 1, '2017-01-31 22:12:23', NULL, '2017-01-31 07:12:23'),
(4, 3, 2, '2017-02-15', '12:59', 3, '100', '500', 1, '2017-02-02 19:41:11', NULL, '2017-02-02 15:41:11'),
(5, 3, 1, '2017-02-16', '00:00', 3, '500', '1000', 1, '2017-02-02 19:41:55', NULL, '2017-02-02 15:41:55'),
(6, 7, 4, '2017-02-10', '12:00', 4, '1000', '1000', 1, '2017-02-04 19:10:30', NULL, '2017-02-04 15:10:30'),
(7, 8, 2, '2017-02-15', '12:59', 3, '1200', '1200', 1, '2017-02-05 09:34:00', NULL, '2017-02-05 05:34:00'),
(8, 5, 7, '1111', '12', 3, '1000 / HOUR', '500', 1, '2017-03-20 10:15:46', NULL, '2017-03-20 06:15:46'),
(9, 15, 6, '12/10/2017', '12', 4, '455', '1000', 17, '2017-03-20 11:12:55', NULL, '2017-03-20 07:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `usermaster`
--

CREATE TABLE `usermaster` (
  `userId` int(11) NOT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `lastLoggedIn` datetime DEFAULT NULL,
  `isAdmin` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'Y-Yes, N-No',
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `aboutMe` varchar(500) DEFAULT NULL,
  `profilePicture` varchar(100) DEFAULT NULL,
  `userAccess` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A-Active, I-Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usermaster`
--

INSERT INTO `usermaster` (`userId`, `adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `password`, `lastLoggedIn`, `isAdmin`, `address`, `city`, `country`, `postalCode`, `aboutMe`, `profilePicture`, `userAccess`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, 1, 'graham', 'Graham', 'Whitelaw', 'graham@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-03-22 14:58:42', 'Y', 'test', 'Dubai', 'UAE', '', 'test', '1484555604678.jpg', '1,2,3,4,5,6', 'A', '', '2017-01-16 00:00:00', '1', '2017-03-22 10:58:42'),
(2, NULL, 'mark', 'Mark', 'Smith', 'mark.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:45:23', '', '2017-01-31 23:08:09'),
(3, NULL, 'chris', 'Chris', 'Wylie', 'chris.wylie@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:46:51', '', '2017-01-31 23:08:11'),
(4, NULL, 'craig', 'Craig', 'Jones', 'craig.jones@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'CDE street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:47:59', '', '2017-01-31 23:08:13'),
(5, 2, 'koshy', 'Jerin', 'Koshy', 'jerin.koshy@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-02-05 09:48:19', 'N', 'QWE Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:49:13', '', '2017-02-27 15:15:08'),
(6, NULL, 'sham', 'Sham', 'Jacob', 'sham.jacob@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ABC Road', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:50:22', '', '2017-01-31 23:08:17'),
(7, NULL, 'steven', 'Steve', 'Smith', 'steve.smith@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'ASD Road', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:12', '1', '2017-01-31 23:08:19'),
(8, NULL, 'hani', 'Moein', 'Hani', 'moein.hani@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'MNO Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:51:57', '', '2017-01-31 23:08:21'),
(9, NULL, 'shadi', 'Shadi', 'Abu Razouq', 'shadi.razouq@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'QWERTY Street', 'London', 'England', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:52:39', '', '2017-01-31 23:08:23'),
(10, NULL, 'simbulan', 'Maricel', 'Simbulan', 'maricel.simbulan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:02', '', '2017-01-31 23:08:25'),
(11, NULL, 'jen', 'Jen', 'Mamalayan', 'jen.mamalayan@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-03-22 14:41:31', 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:54:40', '', '2017-03-22 10:41:31'),
(12, NULL, 'jackie', 'Jackie', 'Whitelaw', 'jackie.whitelaw@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:55:44', '', '2017-01-31 23:08:29'),
(13, NULL, 'flory', 'Flory', 'Dâ€™Mello', 'flory.dmello@wla.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'XYZ Street', 'Dubai', 'UAE', '', '', '', '1,2,3,4,5,6', 'A', '1', '2017-01-22 11:56:23', '', '2017-01-31 23:08:32'),
(14, NULL, 'testing', 'test', 'user', 'testing@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-02-01 07:34:53', 'N', 'test', 'test', 'test', '', NULL, NULL, '2,3,4,5', 'A', '1', '2017-02-01 08:37:47', '1', '2017-02-01 03:34:53'),
(15, NULL, 'jeorge', 'jeorge', 'meak', 'jeorge@sdfsd.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'N', 'No-32,23rd street,London', 'Manchester', 'London', '232423423423', NULL, NULL, '1,2,3,4,5,5', 'A', '1', '2017-02-04 08:29:24', '1', '2017-02-04 04:30:39'),
(16, NULL, 'Craig', 'Craig', 'Jones', 'craig@whitelawlossadjusters.ae', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, 'Y', 'Office 104, Apricot Tower, Dubai Silicon Oasis', 'Dubai', 'UAE', '85442', NULL, NULL, '1,2,3,4,5,5', 'I', '1', '2017-02-05 10:05:22', NULL, '2017-02-05 06:05:40'),
(17, NULL, 'MARICEL', 'MARICEL', 'SIMBULAN', 'ADMIN@WHITELAW.COM', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-03-20 11:19:40', 'Y', 'ADDRESS', 'DUBAI', 'UAE', '', NULL, NULL, '2,3,5,5', 'A', '1', '2017-03-20 10:54:26', NULL, '2017-03-20 07:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `visitupdates`
--

CREATE TABLE `visitupdates` (
  `visitId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL COMMENT 'N-Notes, P-Photo, E- Email',
  `updateContent` varchar(500) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitupdates`
--

INSERT INTO `visitupdates` (`visitId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES
(1, 6, 'P', 'Driving License00011486220234689.jpg', 'Driving License0001.jpg', 1, '2017-02-04 18:57:14'),
(2, 6, 'P', 'Health Card1486220246364.jpg', 'Health Card.jpg', 1, '2017-02-04 18:57:26'),
(3, 8, 'P', 'Sethu End of Service Pay00021486272401562.jpg', 'Sethu End of Service Pay0002.jpg', 1, '2017-02-05 09:26:41'),
(4, 7, 'P', 'wltest1486274964890.docx', 'wltest.docx', 1, '2017-02-05 10:09:24'),
(5, 17, 'P', 'Untitled1489993468954.jpg', 'Untitled.jpg', 17, '2017-03-20 11:04:28'),
(6, 17, 'N', 'Notes ', NULL, 17, '2017-03-20 11:04:47');

-- --------------------------------------------------------

--
-- Table structure for table `workingupdates`
--

CREATE TABLE `workingupdates` (
  `workingId` int(11) NOT NULL,
  `claimId` int(11) DEFAULT NULL,
  `updateType` varchar(1) DEFAULT NULL,
  `updateContent` varchar(100) DEFAULT NULL,
  `originalFilename` varchar(500) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjusters`
--
ALTER TABLE `adjusters`
  ADD PRIMARY KEY (`adjusterId`);

--
-- Indexes for table `brokers`
--
ALTER TABLE `brokers`
  ADD PRIMARY KEY (`brokerId`);

--
-- Indexes for table `categorymaster`
--
ALTER TABLE `categorymaster`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `claimmaster`
--
ALTER TABLE `claimmaster`
  ADD PRIMARY KEY (`claimId`);

--
-- Indexes for table `clientmaster`
--
ALTER TABLE `clientmaster`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  ADD PRIMARY KEY (`detailsId`);

--
-- Indexes for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  ADD PRIMARY KEY (`invoiceId`);

--
-- Indexes for table `officemaster`
--
ALTER TABLE `officemaster`
  ADD PRIMARY KEY (`officeId`);

--
-- Indexes for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  ADD PRIMARY KEY (`prelimId`);

--
-- Indexes for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  ADD PRIMARY KEY (`receiptId`);

--
-- Indexes for table `servicemaster`
--
ALTER TABLE `servicemaster`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`subId`);

--
-- Indexes for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  ADD PRIMARY KEY (`reportId`);

--
-- Indexes for table `usermaster`
--
ALTER TABLE `usermaster`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `visitupdates`
--
ALTER TABLE `visitupdates`
  ADD PRIMARY KEY (`visitId`);

--
-- Indexes for table `workingupdates`
--
ALTER TABLE `workingupdates`
  ADD PRIMARY KEY (`workingId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjusters`
--
ALTER TABLE `adjusters`
  MODIFY `adjusterId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `brokers`
--
ALTER TABLE `brokers`
  MODIFY `brokerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categorymaster`
--
ALTER TABLE `categorymaster`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `claimmaster`
--
ALTER TABLE `claimmaster`
  MODIFY `claimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `clientmaster`
--
ALTER TABLE `clientmaster`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `invoicedetails`
--
ALTER TABLE `invoicedetails`
  MODIFY `detailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `invoicemaster`
--
ALTER TABLE `invoicemaster`
  MODIFY `invoiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `officemaster`
--
ALTER TABLE `officemaster`
  MODIFY `officeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prelimupdates`
--
ALTER TABLE `prelimupdates`
  MODIFY `prelimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `receiptdetails`
--
ALTER TABLE `receiptdetails`
  MODIFY `receiptId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `servicemaster`
--
ALTER TABLE `servicemaster`
  MODIFY `serviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `timeandexpense`
--
ALTER TABLE `timeandexpense`
  MODIFY `reportId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `usermaster`
--
ALTER TABLE `usermaster`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `visitupdates`
--
ALTER TABLE `visitupdates`
  MODIFY `visitId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `workingupdates`
--
ALTER TABLE `workingupdates`
  MODIFY `workingId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
