-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 27, 2017 at 08:01 PM
-- Server version: 5.5.45
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientmaster`
--

CREATE TABLE `clientmaster` (
  `clientId` int(11) NOT NULL,
  `referenceId` varchar(100) DEFAULT NULL,
  `clientName` varchar(300) DEFAULT NULL,
  `phoneNumber` varchar(50) DEFAULT NULL,
  `emailId` varchar(200) DEFAULT NULL,
  `poBox` varchar(50) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postalCode` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL COMMENT 'A - Active, I - Inactive',
  `createdBy` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedBy` varchar(10) DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientmaster`
--

INSERT INTO `clientmaster` (`clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `poBox`, `address`, `city`, `postalCode`, `country`, `active`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate`) VALUES
(1, '1', 'Abu Dhabi National Insurance Company', '02 408 0380', 'h.padmanabhan@adnic.ae', '839', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(2, '2', 'Abu Dhabi National Insurance Company', '05 515 4861', 'j.nair@adnic.ae', '11236', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(3, '3', 'Abu Dhabi National Takaful Company', '02 410 7735', 'F.AbuYazbek@takaful.ae', '35335', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(4, '4', 'Adamjee Insurance Company ', '04 360 9762', 'awais.khalid@adamjee.com', '4256', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(5, '5', 'AIG', '04 601 4673', 'Michael.Wijemanne@aig.com', '54400', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(6, '6', 'Al Ain Ahlia Insurance Company', '02 611 9999', 's.gharaibeh@alaininsurance.com', '3077', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(7, '7', 'Al Buhaira National Insurance Company', '06 517 4509', 'maslam@albuhaira.com', '6000', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(8, '8', 'Al Dhafra Insurance Company ', '02 694 9460', 'claims@aldhafrainsurance.ae', '319', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(9, '9', 'Al Fujairah  National Insurance Company', '04 222 3208', 'wilfred.noronha@fujinsco.ae', '277', NULL, 'Fujairah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(10, '10', 'Al Hilal Takaful Company', '02 499 4292', 'svalappil@alhilaltakaful.ae', '111644', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(11, '11', 'Al Ittihad Al Watani Insurance Company', '04 282 3266 x 200', 'a.azzi@unic.ae', '3000', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(12, '12', 'Al Khazna Insurance Company', '02 696 9823', 'tarik@alkhazna.ae', '73343', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(13, '13', 'Al Sagr National Insurance Company', '04 702 8650', 'Ramkumar@alsagrins.ae', '14614', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(14, '14', 'Al Wathba National Insurance Company', '02 418 5430', 'd_sahal@awnic.com', '45154', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(15, '15', 'Alliance Insurance', '4605111', 'nmclaims@alliance-me.com', '5501', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(16, '16', 'Arabia Insurance', '04228 0022', 'mkanchiraju@arabiainsurance.com', '1050', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(17, '17', 'Arabian Scandinavian Insurance Company ', '42824403', 'v.rajendran@ascana.net', '1993', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(18, '18', 'AXA Insurance', '04 440 3726', 'rajeev.nambiar@axa-gulf.com', '5862', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(19, '19', 'Dar Al Takaful', '04 304 1605', 'akumar@dat.ae', '235353', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(20, '20', 'Dubai Insurance', '04 269 3030', 'menezes.l@dubins.ae', '3027', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(21, '21', 'Dubai Islamic Insurance & Reinsurance Company (AMAN)', '04 319 3214', 'akther.hussain@aman.ae', '157', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(22, '22', 'Dubai National Insurance & Reinsurance Company ', '04 295 6700 Ext.430', 'shakir@dnirc.com', '1806', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(23, '23', 'Emirates Insurance Company', '02 698 1569', 'kthomas@eminsco.com', '3856', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(24, '24', 'Insurance House', '02 493 4524', 'imad.shishnieh@insurancehouse.ae', '129921', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(25, '25', 'Iran Insurance Company', '04 221 47 47', 'tqureshi@bimehir.ae', '2004', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(26, '26', 'Iran Insurance Company', '04 221 4747', 'fakher@bimehir.ae', '2004', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(27, '27', 'Islamic Arab Insurance Comapny (SALAMA)', '04 404 0114', 'shahida.khan@salama.ae', '10214', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(28, '28', 'Jordan Insurance', '02 634 4800', 'sunil@jicad.ae', '2197', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(29, '29', 'Jordan Insurance', '02 634 4800', 'elias@jicad.ae', '2197', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(30, '30', 'Methaq Takaful Insurance Company', '02 656 5333', 'dalal.habib@methaq.ae', '32774', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(31, '31', 'Mitsui Sumitomo Insurance Company', '02 627 4834 / 04 336 5335', 'wajih@msi-dubai.ae', '25190', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(32, '32', 'Nasco Middle East ', '04 305 1611', 'Abdul.Khader@nascodubai.com', '62528', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(33, '33', 'National General Insurance Company', '04 211 5912', 'rajan@ngiuae.com', '154', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(34, '34', 'National Takaful Company  (Watania) ', '800928264', 'mohammad.shadab@watania.ae', '6457', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(35, '35', 'New India Assurance Company', '04 352 5563 Ext.4368', 'n.nikhil@nia-dubai.com', '5701', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(36, '36', 'New India Assurance Company', '02 644 0428 Ext.225', 'binu@newindia-uae.com', '46743', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(37, '37', 'Noor Takaful', '04 426 8964', 'andrew.greenwood@noortakaful.com', '49998', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(38, '38', 'Oman Insurance Company', '04 233 7467', 'Balaji.Ganapathy@Tameen.ae', '', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(39, '39', 'Oriental Insurance Company', '04 353 8688 Ext.135', 'yogesh@oicgulf.ae', '478', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:46:32'),
(40, '40', 'Orient Insurance', '04 253 1592', 'Natarajan.Ramesh@alfuttaim.com', '27966', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(41, '41', 'Qatar General Insurance & Reinsurance Company', '04 268 8688', 'vkr@qgirco.com', '8080', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(42, '42', 'Qatar Insurance Company', '04 2224045 Ext.37 ', 'sriraman@qici.com.qa', '4066', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(43, '43', 'RAK Insurance', '07 227 3000', 'balu@rakinsurance.com', '506', NULL, 'Ras Al Khaimah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(44, '44', 'Royal & Sun Alliance Insurance Company', '04 3029901', 'zia-ul.jaweed@ae.rsagroup.com', '28648', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:46:57'),
(45, '45', 'Saudi Arabian Insurance Company', '02 645 8060', 'saicoauh@emirates.net.ae', '585', NULL, 'Abu Dhabi', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(46, '46', 'Sharjah Insurance', '06 519 5666', 'alaa.alqasa@shjins.ae', '792', NULL, 'Sharjah', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(47, '47', 'Tokio Marine & Nichido Fire Insurance Company', '04 361 9777', 'majid@tmnf.ae', '152', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(48, '48', 'Union Insurance Company', '04 378 7678 ', 'narayan.i@unioninsurance.ae', '119227', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:44:34'),
(49, '49', 'United Insurance Company', '04 212 8045', 'joshua.abiero@uic.ae', '110', NULL, 'Dubai', NULL, 'United Arab Emirates', 'A', NULL, NULL, NULL, '2017-03-25 01:47:17'),
(50, 'U1', 'Underwriters per PWS (Dubai)', '9988776655', 'u1@pws.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:02:43', NULL, '2017-03-25 12:32:43'),
(51, 'U2', 'Underwriters per Chedid RE - Dxb', '9988776655', 'u2@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:03:26', NULL, '2017-03-25 12:33:26'),
(52, 'U3', 'Underwriters per RK Harisson', '9988776655', 'u3@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:03:54', NULL, '2017-03-25 12:33:54'),
(53, 'U4', 'Underwriters per Jacky Grunfeld, Belgium', '9988776655', 'u4@chedid.com', '00000', 'Dubai', 'Dubai', '', 'United Arab Emirates', 'A', '1', '2017-03-25 22:04:14', NULL, '2017-03-25 12:34:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientmaster`
--
ALTER TABLE `clientmaster`
  ADD PRIMARY KEY (`clientId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientmaster`
--
ALTER TABLE `clientmaster`
  MODIFY `clientId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
