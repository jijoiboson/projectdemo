<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if(isset($_SESSION['updatedclaimid'])){
    	$updatedclaimid         = $_SESSION["updatedclaimid"];
    	$updatedjobnumber 		= $_SESSION["updatedjobnumber"];
    	$formattedJobnumber = str_replace("/", "", $updatedjobnumber);
		$path       		= "uploads/$formattedJobnumber/working/";
		if (!file_exists($path)) {
			    mkdir($path, 0777, true);
		}
    	//Get visit details
    	$workingphotoarray = array();
    	$workingdocumentarray = array();

    	$get_details = "select `workingId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` from `workingupdates` where claimId = '$updatedclaimid'";
	    $detailstmt       = mysqli_query($connection, $get_details); 
	    $getcount   = mysqli_num_rows($detailstmt);
	    if($getcount > 0){
	        
	      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
	        $workingId          = $row['workingId']; 
	        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
	        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
	        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
	        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));	        
	        if($updateType == "P"){
	        	$thisphotoarray = array("Id" => $workingId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
	        	array_push($workingphotoarray, $thisphotoarray);
	        }
	        if($updateType == "D"){
	        	$thisemailarray = array("Id" => $workingId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
	        	array_push($workingdocumentarray, $thisemailarray);
	        }
	    }
	}

    } else {
    	header("Location:open.php");
    }
    
}

if((isset($_FILES['workingphoto']['name']))&&(!empty($_FILES['workingphoto']['name']))){
	$workingphoto    = (empty($_FILES['workingphoto']['name']))  ? '' : $_FILES['workingphoto']['name'];
	$fullfilename  		= "";
	if($workingphoto){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $workingphoto);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['workingphoto']['tmp_name'], $fullpath);
  	}
  	$insert_photo = "insert into `workingupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'P', '$fullfilename', '$originalname', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_photo);
  	$update_status = "update claimmaster set jobStatus = 'W' where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:working.php?p=success");
}
if((isset($_FILES['workingdoc']['name']))&&(!empty($_FILES['workingdoc']['name']))){
	$workingdoc    = (empty($_FILES['workingdoc']['name']))  ? '' : $_FILES['workingdoc']['name'];
	$fullfilename  		= "";
	if($workingdoc){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $workingdoc);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['workingdoc']['tmp_name'], $fullpath);
  	}
  	$insert_email = "insert into `workingupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'D', '$fullfilename', '$originalname', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_email);
  	$update_status = "update claimmaster set jobStatus = 'W' where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:working.php?d=success");
}
// if((isset($_REQUEST['visitnotes']))&&(!empty($_REQUEST['visitnotes']))){
// 	$visitnotes    = (empty($_REQUEST['visitnotes']))  ? '' : $_REQUEST['visitnotes'];
//   	$insert_notes = "insert into `workingupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'N', '$visitnotes', '$loggedin_userid', now())";
//   	mysqli_query($connection, $insert_notes);
//   	$update_status = "update claimmaster set jobStatus = 'V' where claimId = '$updatedclaimid'";
// 	mysqli_query($connection, $update_status);
//   	header("Location:visit.php?n=success");
// }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
									<tbody>
                                        <tr>
											<td colspan="2">
												<h4><strong>Photo: </strong></h4>
											</td>
										</tr>
										<?php 
										$photocount = 0;
										// print_r($workingphotoarray);
											foreach ($workingphotoarray as $workingphotos) {

												$photocount++;
												?>
												<tr>
													<td><?php echo $photocount; ?> : <a href="<?php echo $path.$workingphotos['content'];?>" target="_blank"><?php echo $workingphotos['name'];?></a></td>
													<td>Uploaded on : <?php echo $workingphotos['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr>
											<td colspan="2">
												<form method="POST" action="#" enctype="multipart/form-data">
												<strong>Select a file to attach (Max 10MB): <span class="mandatorystar">*</span></strong>
													<input type="file" name="workingphoto" id="workingphoto" required /></br>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/>
													</td>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/>
													</td>
												</tr>
												<tr><td colspan="2" style="color:green;" align="right"><?php
												if(isset($_REQUEST['p'])){
													echo "Succesfully uploaded";
												}
												?></td></tr>
												</table>
												</form>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<h4><strong>Documents: </strong></h4>
											</td>
										</tr>
										<?php 
										$doccount = 0;
										// print_r($workingphotoarray);
											foreach ($workingdocumentarray as $workingdocs) {

												$doccount++;
												?>
												<tr>
													<td><?php echo $doccount; ?> : <a href="<?php echo $path.$workingdocs['content'];?>" target="_blank"><?php echo $workingdocs['name'];?></a></td>
													<td>Uploaded on : <?php echo $workingdocs['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr>
											<td colspan="2">
												<form method="POST" action="#" enctype="multipart/form-data">
												<strong>Select a file to attach (Max 10MB): <span class="mandatorystar">*</span></strong><input type="file" name="workingdoc" id="workingdoc" required /></br>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/>
													</td>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/>
													</td>
												</tr>
												<tr><td colspan="2" style="color:green;" align="right"><?php
												if(isset($_REQUEST['d'])){
													echo "Succesfully uploaded";
												}
												?></td></tr>
												</table>
												</form>
											</td>
										</tr>
										
										<!-- <tr>
											<td>
												<h4><strong>Email: </strong></h4>
											</td>
										</tr>
										<tr>
											<td>
												<form method="POST" action="visit.php" onsubmit="show_alert()" enctype="multipart/form-data">
												<strong>Select a file to attach (Max 10MB):</strong> </br>
												<input type="file" name="file" required /></br>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="SEND"/>
													</td>
													
												</tr>
												</table>
												</form>
											</td>
										</tr> -->
										<!-- <tr>
											
											<td>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/>
													</td>
												</tr>
												</table>
											</td>
										</tr> -->
                                    </tbody>
                                </table>
                                <table align="center">
									<tr>
										<td>
											<a href="report.php?id=<?php echo $updatedclaimid;?>"><button class="btn btn-info btn-fill">BACK</button></a>
										</td>
									</tr>
								</table>
                            </div>
                        </div>
                    </div>

    </div>
</div>


</body>

	<script>
		function show_alert(){
			confirm ("Files uploaded");
			
		}	
	</script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

   

</html>
