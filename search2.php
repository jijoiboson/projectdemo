<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

<link rel="stylesheet" type="text/css" href="assets/css/custom.css">

    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>Report Number</th>
                                    	<th>Date of Valuation</th>
                                    	<th>Date of Reporting</th>
                                    	<th>Status</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr>
										<tr>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr>
										<tr>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr>
										<tr>
											<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr><tr>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr>
										</tr><tr>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>XXXXXX</td>
                                        	<td>
											<form action="search2.php">
												<input class="btn btn-info btn-fill pull-right" type="submit" value="VIEW"/>
											</form>
											</td>
                                        </tr>
										
										
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
		 <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
								<h3 align="center"> Report no. #</h3>
								<p class="category">LOC-3/ Category #/ Number ##/ Year ####</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
								
									<tbody>
										<tr height="80">
                                        	<td>Claim number: </td>
                                        	<td>xxxxxx</td>
                                            <td>Insured: </td>
                                        	<td>xxxxxx</td>
                                        </tr>
										<tr height="80">
                                        	<td>Policy number: </td>
                                        	<td>xxxxxx</td>
											<td>Insurer: </td>
                                        	<td>xxxxxx</td>
                                        </tr>
										<tr height="80">
                                        	<td>Broker: </td>
                                        	<td>xxxxxx</td>
											<td>Nature of loss: </td>
                                        	<td>xxxxxx</td>
                                        </tr>
										<tr height="80">
                                        	<td>Broker refernce: </td>
                                        	<td>xxxxxx</td>
											<td>Adjuster: </td>
                                        	<td>xxxxxx</td>
                                        </tr>
										<tr height="80">
                                        	<td>Instruction received: </td>
                                        	<td>date</td>
											<td>Contact made: </td>
                                        	<td>date</td>
                                        </tr>
										<tr height="80">
                                        	<td>Survey arranged for: </td>
                                        	<td>date</td>
											<td>Status</td>
                                        	<td>XXXXXX</td>
										</tr>
										
									</tbody>
                                </table>
								

                            </div>
                        </div>
                    </div>
				<table width="40%" align="right">
				<tr>
					<td align="center">
						<input class="btn btn-info btn-fill pull-right" type="submit" value="PRINT"/>
					</td>
					<!-- <td align="center">
						<input class="btn btn-info btn-fill pull-right" type="submit" value="SAVE AS WORD"/>
					</td>
					<td align="center">
						<input class="btn btn-info btn-fill pull-right" type="submit" value="SAVE AS PDF"/>
					</td> -->
				</tr>
				</table>

		

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

   

</html>
