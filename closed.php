<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">


    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="dashboard.php"><< Back to Dashboard</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Reports</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
								<tbody>
										<tr>
											<td colspan="6">DAILY REPORT</td>
										</tr>
										
                                        <tr height="50px">
											<form action="daily.php" method="POST">
											<td>Location:
												<select style="width:132px;" name="location" id="location">
													<option value="">Select Location</option>
													<?php 
                                                        $get_offices = "select `officeId`, `location`, `name`, `prefix` from `officemaster` where `active` = 'A'";
                                                        $officestmt       = mysqli_query($connection, $get_offices); 
                                                        $getofficecount   = mysqli_num_rows($officestmt);
                                                        if($getofficecount > 0){
                                                            
                                                          while($officerow = mysqli_fetch_array($officestmt, MYSQLI_ASSOC)){
                                                            $officeId   = (empty($officerow['officeId']))           ? '' : $officerow['officeId'];
                                                            $location   = (empty($officerow['location']))         ? '' : $officerow['location'];
                                                            $name       = (empty($officerow['name']))        ? '' : $officerow['name'];
                                                            $prefix     = (empty($officerow['prefix']))         ? '' : $officerow['prefix'];
                                                        ?>
                                                            <option value="<?php echo $officeId;?>"><?php echo $location;?></option>
                                                        <?php
                                                                  }
                                                      }
                                                    ?>
												</select>
											</td>
											<td>Insurer:
												<select style="width:132px;" name="selectclient" id="selectclient">
													<option value="">Select Insurer</option>
													<?php 
                                                    $get_clients = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where `active` = 'A'";
                                                        $clientstmt       = mysqli_query($connection, $get_clients); 
                                                        $getclientcount   = mysqli_num_rows($clientstmt);
                                                        if($getclientcount > 0){
                                                            
                                                          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                            $clientId     = $clientrow['clientId']; 
                                                            $referenceId  = (empty($clientrow['referenceId']))   ? '' : $clientrow['referenceId'];
                                                            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                            $phoneNumber  = (empty($clientrow['phoneNumber']))       ? '' : $clientrow['phoneNumber'];
                                                            $emailId      = (empty($clientrow['emailId']))       ? '' : $clientrow['emailId'];
                                                            $city         = (empty($clientrow['city']))       ? '' : $clientrow['city'];
                                                ?>
                                                    <option value="<?php echo $clientId;?>"><?php echo $clientName;?></option>

                                                <?php
                                                          }
                                                      }
                                                ?>             
												</select>
											</td>
											<td>Adjuster:
												<select style="width:132px;" name="adjusterid" id="adjusterid">
													<option value="">Select Adjuster</option>
													<?php 
		                                            $get_adjusters = "select `adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address` from `adjusters` where `active` = 'A'";
		                                                $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
		                                                $getadjustercount   = mysqli_num_rows($adjusterstmt);
		                                                if($getadjustercount > 0){ 
		                                                  while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
		                                                    $adjusterId     = $adjusterrow['adjusterId']; 
		                                                    $userName   	= (empty($adjusterrow['userName']))   ? '' : $adjusterrow['userName'];
		                                                    $firstName    	= (empty($adjusterrow['firstName']))     ? '' : $adjusterrow['firstName'];
		                                                    $lastName     	= (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
		                                                    $emailId      	= (empty($adjusterrow['emailId']))       ? '' : $adjusterrow['emailId'];
		                                                    $city         	= (empty($adjusterrow['city']))       ? '' : $adjusterrow['city'];
		                                        ?>
		                                            <option value="<?php echo $adjusterId;?>"><?php echo $firstName." ".$lastName;?></option>
		                                        <?php
		                                                  }
		                                              }
		                                        ?>
												</select>
											</td>
											<td>Date: <span class="mandatorystar">*</span>
												<input style="width:132px;" type="text" name="dateselected" id="dateselected" placeholder="Select Date" required>
											</td>
											<td align="center">
												<input class="btn btn-info btn-fill" style="width:180px;" type="submit" value="GENERATE REPORT"/>
											</td>
											</form>
										</tr>
										<tr height="50px"><td colspan="6"></td></tr>
										<tr>
											<td colspan="6"> MONTHLY REPORT</td>
										</tr>
                                        <tr>
										<form action="monthly.php" method="POST">
											<td>Month and Year: <span class="mandatorystar">*</span>
												<input style="width:130px;" type="text" class="hidecalender" name="monthandyear" id="monthandyear" placeholder="Select Month" required>	
											<td>Location:
												<select style="width:132px;" name="location" id="location">
													<option value="">Select Location</option>
													<?php 
                                                        $get_offices = "select `officeId`, `location`, `name`, `prefix` from `officemaster` where `active` = 'A'";
                                                        $officestmt       = mysqli_query($connection, $get_offices); 
                                                        $getofficecount   = mysqli_num_rows($officestmt);
                                                        if($getofficecount > 0){
                                                            
                                                          while($officerow = mysqli_fetch_array($officestmt, MYSQLI_ASSOC)){
                                                            $officeId   = (empty($officerow['officeId']))           ? '' : $officerow['officeId'];
                                                            $location   = (empty($officerow['location']))         ? '' : $officerow['location'];
                                                            $name       = (empty($officerow['name']))        ? '' : $officerow['name'];
                                                            $prefix     = (empty($officerow['prefix']))         ? '' : $officerow['prefix'];
                                                        ?>
                                                            <option value="<?php echo $officeId;?>"><?php echo $location;?></option>
                                                        <?php
                                                                  }
                                                      }
                                                    ?>
												</select>
											</td>
											<td>Insurer:
												<select style="width:132px;" name="selectclient" id="selectclient">
													<option value="">Select Insurer</option>
													<?php 
                                                    $get_clients = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where `active` = 'A'";
                                                        $clientstmt       = mysqli_query($connection, $get_clients); 
                                                        $getclientcount   = mysqli_num_rows($clientstmt);
                                                        if($getclientcount > 0){
                                                            
                                                          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                            $clientId     = $clientrow['clientId']; 
                                                            $referenceId  = (empty($clientrow['referenceId']))   ? '' : $clientrow['referenceId'];
                                                            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                            $phoneNumber  = (empty($clientrow['phoneNumber']))       ? '' : $clientrow['phoneNumber'];
                                                            $emailId      = (empty($clientrow['emailId']))       ? '' : $clientrow['emailId'];
                                                            $city         = (empty($clientrow['city']))       ? '' : $clientrow['city'];
                                                ?>
                                                    <option value="<?php echo $clientId;?>"><?php echo $clientName;?></option>

                                                <?php
                                                          }
                                                      }
                                                ?>             
												</select>
											</td>
											<td>Category:
												<select class="col-md-4" style="width:300px;" name="losscategory" id="losscategory">
                                                    <option value="" style="display:none;">Select Category</option>
                                                    <?php 
                                                        $get_categories = "select `categoryId`, `category`, `prefix` from `categorymaster` where `active` = 'A'";
                                                            $categorystmt       = mysqli_query($connection, $get_categories); 
                                                            $getcategorycount   = mysqli_num_rows($categorystmt);
                                                            if($getcategorycount > 0){                                                   
                                                              while($categoryrow = mysqli_fetch_array($categorystmt, MYSQLI_ASSOC)){
                                                                $categoryId   = (empty($categoryrow['categoryId']))   ? '' : $categoryrow['categoryId'];
                                                                $category     = (empty($categoryrow['category']))     ? '' : $categoryrow['category'];
                                                                $prefix       = (empty($categoryrow['prefix']))       ? '' : $categoryrow['prefix'];
                                                                ?>
                                                                <option value="<?php echo $categoryId;?>"><?php echo $category;?></option>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                </select>
											</td>
											<td>
												<input class="btn btn-info btn-fill pull-right" style="width:180px;" type="submit" value="GENERATE REPORT"/>
											</td>
											</form>
                                        </tr>
										<tr height="50px"><td colspan="6"></td></tr>
                                        <tr>
                                            <td colspan="6"> OUTSTANDING PRELIM REPORT</td>
                                        </tr>
                                        <tr>
                                        <form action="prelimreport.php" method="POST">
                                            <td colspan="2">Location:
                                                <select style="width:132px;" name="location" id="location">
                                                    <option value="">Select Location</option>
                                                    <?php 
                                                        $get_offices = "select `officeId`, `location`, `name`, `prefix` from `officemaster` where `active` = 'A'";
                                                        $officestmt       = mysqli_query($connection, $get_offices); 
                                                        $getofficecount   = mysqli_num_rows($officestmt);
                                                        if($getofficecount > 0){
                                                            
                                                          while($officerow = mysqli_fetch_array($officestmt, MYSQLI_ASSOC)){
                                                            $officeId   = (empty($officerow['officeId']))           ? '' : $officerow['officeId'];
                                                            $location   = (empty($officerow['location']))         ? '' : $officerow['location'];
                                                            $name       = (empty($officerow['name']))        ? '' : $officerow['name'];
                                                            $prefix     = (empty($officerow['prefix']))         ? '' : $officerow['prefix'];
                                                        ?>
                                                            <option value="<?php echo $officeId;?>"><?php echo $location;?></option>
                                                        <?php
                                                                  }
                                                      }
                                                    ?>
                                                </select>
                                            </td>
                                            <td colspan="2">Adjuster:
                                                <select style="width:132px;" name="adjusterid" id="adjusterid">
                                                    <option value="">Select Adjuster</option>
                                                    <?php 
                                                    $get_adjusters = "select `adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address` from `adjusters` where `active` = 'A'";
                                                        $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
                                                        $getadjustercount   = mysqli_num_rows($adjusterstmt);
                                                        if($getadjustercount > 0){ 
                                                          while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
                                                            $adjusterId     = $adjusterrow['adjusterId']; 
                                                            $userName       = (empty($adjusterrow['userName']))   ? '' : $adjusterrow['userName'];
                                                            $firstName      = (empty($adjusterrow['firstName']))     ? '' : $adjusterrow['firstName'];
                                                            $lastName       = (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
                                                            $emailId        = (empty($adjusterrow['emailId']))       ? '' : $adjusterrow['emailId'];
                                                            $city           = (empty($adjusterrow['city']))       ? '' : $adjusterrow['city'];
                                                ?>
                                                    <option value="<?php echo $adjusterId;?>"><?php echo $firstName." ".$lastName;?></option>
                                                <?php
                                                          }
                                                      }
                                                ?>
                                                </select>
                                            </td>
                                            <td align="center">
                                                <input class="btn btn-info btn-fill" style="width:180px;" type="submit" value="GENERATE REPORT"/>
                                            </td>
                                            </form>
                                        </tr>
                                    </tbody>
								</table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jQuery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#dateselected').datepicker({
                maxDate: '0',
                dateFormat: 'dd-mm-yy'
            });
            $('.sidebaritems').removeClass("active");
            $('#reports').addClass("active");
            $("#monthandyear").datepicker({
			    changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        maxDate: '0',
		        dateFormat: 'MM yy',
		        onClose: function(dateText, inst) { 
		            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
				}
		       });
        	$("#monthandyear").focus(function () {
		        $(".ui-datepicker-calendar").hide();
		        $("#ui-datepicker-div").position({
		            my: "center top",
		            at: "center bottom",
		            of: $(this)
		        });
		    });
    });
    </script>
</html>
