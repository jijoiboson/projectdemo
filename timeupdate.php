<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_POST['selectclient']))&&( !empty($_POST['selectclient']))){
    	$selectclient = $_POST['selectclient'];
    	$get_clients = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where clientId = '$selectclient'";
        $stmt       = mysqli_query($connection, $get_clients); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $clientId     = $row['clientId']; 
            $referenceId   = (empty($row['referenceId']))     ? '' : $row['referenceId'];
            $clientName   = (empty($row['clientName']))       ? '' : $row['clientName'];
            $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            $emailId      = (empty($row['emailId']))        ? '' : $row['emailId'];
            $address      = (empty($row['address']))        ? '' : $row['address'];
            $city         = (empty($row['city']))           ? '' : $row['city'];
            $postalCode   = (empty($row['postalCode']))     ? '' : $row['postalCode'];
            $country      = (empty($row['country']))        ? '' : $row['country'];
          }
      }
      //Get claims for this client

    } else {
    	header("Location:time.php");
    }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Time and Expense report</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="printablecontent">
								<col width="17%">
								<col width="17%">
								<col width="17%">
								<col width="17%">
								<col width="17%">
								<col width="17%">
									<thead>
										<th colspan="3"><b>Name of Insured: <?php echo $clientName;?></b></th>
										<th colspan="3"><b> Reference: <?php echo $referenceId; ?></b></th>
									<thead>
									<tbody>
										<tr>
											<td align="center"><b>Date</b></td>
											<td align="center"><b>Adjuster</b></td>
											<td align="center"><b>Job Number</b></td>
											<td align="Center"><b>Activity</b></td>
											<td align="center"><b>Time</b></td>
											<td align="center"><b>Rate (p/hr)</b></td>
											<td align="center"><b>Value</b></td>
										</tr>
										<?php 
										//Get all claims for this user
										//Get claim details
										$completeTotal = 0;
									    $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `createdDate` from `claimmaster` where clientId = '$selectclient'";
									    $detailstmt       = mysqli_query($connection, $get_details); 
									    $getcount   = mysqli_num_rows($detailstmt);
									    if($getcount > 0){
									        
									      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
									        $claimId            = $row['claimId']; 
									        $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
									        $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
									        $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
									        $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
									        $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
									        $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
									        $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
									        $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
									        $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
									        $subId              = (empty($row['subId']))            ? '' : $row['subId'];
									        $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
									        $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
									        $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
									        $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
									        $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
									        $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
									        $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
									        $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
									        $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
									        $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
									        $createdDate        = (empty($row['createdDate']))      ? '' : $row['createdDate'];
									        $createdDate1 		= date('d/m/Y', strtotime($createdDate));
									        $createdTime 		= date('H:i A', strtotime($createdDate));
									        $jobStatusText = "";
                                            if($jobStatus == "O"){
                                                $jobStatusText = "Open";
                                            } elseif ($jobStatus == "V") {
                                                $jobStatusText = "Visit";
                                            } elseif ($jobStatus == "P") {
                                                $jobStatusText = "Preliminiary";
                                            } elseif ($jobStatus == "W") {
                                                $jobStatusText = "Working";
                                            } elseif ($jobStatus == "C") {
                                                $jobStatusText = "Closed";
                                            } elseif ($jobStatus == "I") {
                                                $jobStatusText = "Invoiced";
                                            } elseif ($jobStatus == "R") {
                                                $jobStatusText = "Receipt";
                                            } elseif ($jobStatus == "S") {
                                                $jobStatusText = "Status Report Issued";
                                            } else {
                                                $jobStatusText = "";
                                            }
									        //Adjuster name
										    $get_adjusters = "select `firstName`, `lastName` from `adjusters` where adjusterId = '$adjusterId'";
										    $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
										    $getadjustercount   = mysqli_num_rows($adjusterstmt);
										    if($getadjustercount > 0){
										        
										      while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
										        $adjusterfirstName    = (empty($adjusterrow['firstName']))      ? '' : $adjusterrow['firstName'];
										        $adjusterlastName     = (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
										        $adjustershortname = $adjusterfirstName[0].$adjusterlastName[0];
										      }
										     }
										     //If invoice is created
										     $timelyText = "--";
										     $theamount  = "--";
										     $get_invoice_latest = "select t1.invoiceDate, t1.totalAmount, t1.currency, t2.timelyText, t2.timelyRate,t1.createdDate  from `invoicemaster` as t1, invoicedetails as t2 where t1.invoiceId = t2.invoiceId and t1.claimId = '$claimId' and t2.timelyText != '' order by t1.createdDate desc limit 0,1";
										    $get_invoice_latest_details       = mysqli_query($connection, $get_invoice_latest); 
									    	$getlatestcount   = mysqli_num_rows($get_invoice_latest_details);
									    	if($getlatestcount > 0){
									    		while($lirow = mysqli_fetch_array($get_invoice_latest_details, MYSQLI_ASSOC)){
									    			$invoiceDate     = (empty($lirow['invoiceDate']))       ? '' : $lirow['invoiceDate'];
									    			$totalAmount     = (empty($lirow['totalAmount']))       ? '' : $lirow['totalAmount'];
									    			$currency     = (empty($lirow['currency']))       ? '' : $lirow['currency'];
									    			$timelyText     = (empty($lirow['timelyText']))       ? '' : $lirow['timelyText'];
									    			$completeTotal = $completeTotal + $totalAmount;
									    			$theamount = $totalAmount;
									    		}
									    	}
										     ?>
											<tr>
												<td align="center"><?php echo $createdDate1;?></td>
												<td align="center"><?php echo $adjustershortname; ?></td>
												<td align="center"><?php echo $jobNumber; ?></td>
												<td align="center"><?php echo $jobStatusText;?></td>
												<td align="center"><?php echo $createdTime;?></td>
												<td align="center"><?php echo $timelyText;?></td>
												<td align="center"><?php echo number_format($theamount);?></td>
											</tr>
										     <?php
									      }
									  }
										?>
										
										<tr>
											<td align="center" colspan="6"><b>TOTAL</b></td>
											<td align="center"><?php echo number_format($completeTotal);?></td>
										</tr>
									</tbody>
								</table>
								<table class="table table-hover table-striped">
								<tr>
                                    <td colspan="6">
											<button class="btn btn-info btn-fill pull-right" id="printbutton">PRINT</button>
									</td>
                                </tr>
							</table>
                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
     <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#printbutton').click(function(){
                $('#printablecontent').print();
            });
        });
    </script>
   

</html>
