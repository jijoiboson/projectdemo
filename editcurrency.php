<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
        if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {
        if((isset($_REQUEST['currencyId'])) && (!empty($_REQUEST['currencyId']))){
            $edit_currencyId= $_REQUEST['currencyId'];
        $get_clients = "select `currencyId`, `currencyCode`, `currencyName`, `currencyShort`, `payableTo`, `ibanNumber`, `swiftCode`, `active` from `currencydetails` where currencyId = '$edit_currencyId'";
        $stmt       = mysqli_query($connection, $get_clients); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $currencyId     = $row['currencyId']; 
            $currencyCode   = (empty($row['currencyCode']))     ? '' : $row['currencyCode'];
            $currencyName   = (empty($row['currencyName']))       ? '' : $row['currencyName'];
            $currencyShort  = (empty($row['currencyShort']))    ? '' : $row['currencyShort'];
            $payableTo      = (empty($row['payableTo']))        ? '' : $row['payableTo'];
            $ibanNumber     = (empty($row['ibanNumber']))        ? '' : $row['ibanNumber'];
            $swiftCode      = (empty($row['swiftCode']))           ? '' : $row['swiftCode'];
          }
      }
  }
    }
}

//Add Client
  if((isset($_POST['currencyname'])) && (!empty($_POST['currencyname']))){
     // print_r($_FILES);exit;
    $currencyname           = (empty($_REQUEST['currencyname']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencyname'])); 
    $currencyshortname      = (empty($_REQUEST['currencyshortname']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencyshortname']));
    $currencycode           = (empty($_REQUEST['currencycode']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['currencycode']));
    $payableto              = (empty($_REQUEST['payableto']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['payableto']));
    $ibannumber             = (empty($_REQUEST['ibannumber']))       ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['ibannumber']));
    $swiftcode              = (empty($_REQUEST['swiftcode']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['swiftcode']));
    $update_details = "update `currencydetails` set `currencyCode`='$currencycode', `currencyName`='$currencyname', `currencyShort`='$currencyshortname', `payableTo`='$payableto', `ibanNumber`='$ibannumber', `swiftCode`='$swiftcode', `updatedBy`='$loggedin_userid' where currencyId = '$edit_currencyId'";
    mysqli_query($connection, $update_details);
    header("Location:currencylist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="client_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Currency</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg: US Dollars" name="currencyname" id="currencyname" value="<?php echo $currencyName; ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Short Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg $ or Dhs" name="currencyshortname" value="<?php echo $currencyShort; ?>" id="currencyshortname" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Currency Code <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Eg: AED" name="currencycode" id="currencycode" value="<?php echo $currencyCode; ?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Payable <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Payable To" name="payableto" id="payableto" value="<?php echo $payableTo; ?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>IBAN <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="IBAN Number" name="ibannumber" id="ibannumber" value="<?php echo $ibanNumber; ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Swift Code <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" placeholder="Swift Code"  name="swiftcode" id="swiftcode" value="<?php echo $swiftCode; ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>

									<button type="submit" class="btn btn-info btn-fill pull-right">UPDATE</button>  <a href="currency.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#adminpanel').addClass("active");
        });
    </script>
   

</html>
