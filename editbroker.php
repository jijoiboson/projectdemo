<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {
        if((isset($_REQUEST['brokerId'])) && (!empty($_REQUEST['brokerId']))){
            $edit_brokerId= $_REQUEST['brokerId'];
        $get_brokers = "select `brokerId`, `employeeId`, `firstName`, `lastName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country`, `poBox` from `brokers` where brokerId = '$edit_brokerId'";
        $stmt       = mysqli_query($connection, $get_brokers); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $brokerId     = $row['brokerId']; 
            $employeeId   = (empty($row['employeeId']))     ? '' : $row['employeeId'];
            $firstName    = (empty($row['firstName']))      ? '' : $row['firstName'];
            $lastName     = (empty($row['lastName']))       ? '' : $row['lastName'];
            $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            $emailId      = (empty($row['emailId']))        ? '' : $row['emailId'];
            $address      = (empty($row['address']))        ? '' : $row['address'];
            $city         = (empty($row['city']))           ? '' : $row['city'];
            $postalCode   = (empty($row['postalCode']))     ? '' : $row['postalCode'];
            $country      = (empty($row['country']))        ? '' : $row['country'];
            $poBox        = (empty($row['poBox']))          ? '' : $row['poBox'];
          }
      }
  }
    }
}
if((isset($_POST['employeeid'])) && (!empty($_POST['employeeid']))){
     // print_r($_FILES);exit;
    $employeeid      = (empty($_REQUEST['employeeid']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['employeeid']));
    $emailid         = (empty($_REQUEST['emailid']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['emailid']));
    $phonenum        = (empty($_REQUEST['phonenum']))     ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['phonenum']));
    $firstname       = (empty($_REQUEST['firstname']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['firstname']));
    $lastname        = (empty($_REQUEST['lastname']))     ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['lastname']));
    $homeaddress     = (empty($_REQUEST['homeaddress']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['homeaddress']));
    $city            = (empty($_REQUEST['city']))         ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['city']));
    $country         = (empty($_REQUEST['country']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['country']));
    $zipcode         = (empty($_REQUEST['zipcode']))      ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['zipcode']));
    $poboxnumber       = (empty($_REQUEST['poboxnumber']))    ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['poboxnumber']));
    $update_details  = "update `brokers` set `employeeId`='$employeeid',`firstName`='$firstname',`lastName`='$lastname',`phoneNumber`='$phonenum',`emailId`='$emailid',`address`='$homeaddress', `city`='$city', `postalCode`='$zipcode', `country`='$country', `updatedBy`='$loggedin_userid',`updatedDate`= now(), `poBox` = '$poboxnumber' where brokerId = '$edit_brokerId'";
    mysqli_query($connection, $update_details);
    header("Location:brokerlist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="brokerlist.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Broker</h4>
                            </div>
                            <div class="content">
                                <form action="#" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee ID <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="employeeid" id="employeeid" placeholder="Employee ID"  value="<?php echo $employeeId;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email address <span class="mandatorystar">*</span></label>
                                                <input type="email" class="form-control" name="emailid" id="emailid" placeholder="Email" value="<?php echo $emailId;?>"  required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Phone <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" name="phonenum" id="phonenum" placeholder="Phone" value="<?php echo $phoneNumber;?>"  required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>First Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" name="firstname" id="firstname" placeholder="First Name" value="<?php echo $firstName;?>"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" name="lastname" id="lastname" placeholder="Last Name" value="<?php echo $lastName;?>"  required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Address <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="homeaddress" id="homeaddress" placeholder="Home Address" value="<?php echo $address;?>"  required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>City <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" name="city" id="city" placeholder="City" value="<?php echo $city;?>"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Country <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" name="country" id="country" placeholder="Country" value="<?php echo $country;?>"  required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Postal Code</label>
                                                <input type="number" class="form-control onlynumbers" name="zipcode" id="zipcode" placeholder="ZIP Code"  value="<?php echo $postalCode;?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>PO Box Number <span class="mandatorystar">*</span></label>
                                                <input type="number" class="form-control onlynumbers" placeholder="PO Box Number" name="poboxnumber" id="poboxnumber" value="<?php echo $poBox;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                    </div>
									<button type="submit" class="btn btn-info btn-fill pull-right">EDIT BROKER</button> <a href="brokerlist.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>
   

</html>
