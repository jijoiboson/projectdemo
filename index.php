<?php 
  session_start();  
  include("include/config.php");
  if(isset($_COOKIE["loggedin_username"])) {
    $_SESSION['loggedin_username']  = $_COOKIE["loggedin_username"];
    $_SESSION['loggedin_userid']    = $_COOKIE["loggedin_userid"];
    $_SESSION['loggedin_name']      = $_COOKIE["loggedin_name"];
    $_SESSION['loggedin_emaiid']    = $_COOKIE["loggedin_emaiid"];
    $_SESSION['loggedin_isadmin']   = $_COOKIE["loggedin_isadmin"];
    header("Location:dashboard.php");
  }
  if((isset($_REQUEST['name'])) && (!empty($_REQUEST['name']))){
     // print_r($_REQUEST);exit;
     $name      = $_REQUEST['name'];
     $password  = md5($_REQUEST['password']);
    // $remember  = $_REQUEST['remember'];
     //Check usermaster
     $sql   = "select `userId`, `userName`, `firstName`, `lastName`, `emailId`, `isAdmin`, `userAccess`, `adjusterId` from usermaster where (userName='$name' or emailId='$name') and password='$password' and active='A'";
     // echo $sql;exit;
    $stmt     = mysqli_query($connection, $sql); 
    $getcount   = mysqli_num_rows($stmt);
    if($getcount > 0){
      //Login Succesful
      // echo "1";exit;
      while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
        $userId      = (empty($row['userId']))     ? '' : $row['userId'];
        $userName    = (empty($row['userName']))   ? '' : $row['userName'];
        $firstName   = (empty($row['firstName']))  ? '' : $row['firstName'];
        $lastName    = (empty($row['lastName']))   ? '' : $row['lastName'];
        $emailId     = (empty($row['emailId']))    ? '' : $row['emailId'];
        $isAdmin     = (empty($row['isAdmin']))    ? '' : $row['isAdmin'];
        $adjusterId     = (empty($row['adjusterId']))    ? '' : $row['adjusterId'];
        $userAccess     = (empty($row['userAccess']))       ? '' : $row['userAccess'];
        $useraccessarray = explode(",", $userAccess);
        $_SESSION["loggedin_username"]  = $userName;
        $_SESSION["loggedin_userid"]    = $userId;
        $_SESSION["loggedin_name"]      = $firstName;
        $_SESSION["loggedin_emaiid"]    = $emailId;
        $_SESSION["loggedin_isadmin"]   = $isAdmin;
        $_SESSION["loggedin_useraccess"]  = $userAccess;
        $_SESSION["loggedin_adjusterid"]  = $adjusterId;
     if(!empty($_REQUEST["remember"])) {
        setcookie ("loggedin_username", $userName,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_userid",$userId,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_name",$firstName,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_emaiid",$emailId,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_isadmin",$isAdmin,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_useraccess",$userAccess,time()+ (14 * 24 * 60 * 60));
        setcookie ("loggedin_adjusterid",$adjusterId,time()+ (14 * 24 * 60 * 60));
      } else {
        setcookie ("loggedin_username","");
        setcookie ("loggedin_userid","");
        setcookie ("loggedin_name","");
        setcookie ("loggedin_emaiid","");
        setcookie ("loggedin_isadmin","");
        setcookie ("loggedin_useraccess","");
        setcookie ("loggedin_adjusterid","");
      }
        $update_time  = "update usermaster set lastLoggedIn = now() where userId = '$userId'";   
        mysqli_query($connection, $update_time); 
        header("Location:dashboard.php");
      }
       header("Location:dashboard.php");
    } else {
      header("Location:index.php?error=1");
    }


  }
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="UTF-8" /> 
    <title>
        Whitelaw
    </title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
</head>
<body class="indexbg">

<form action="" method="POST">
  <h1>Log in</h1>
  <div class="inset">
  <p>
    <label for="email">USERNAME: </label>
    <input type="text" name="name" id="name" autofocus required/>
  </p>
  <p>
    <label for="password">PASSWORD: </label>
    <input type="password" name="password" id="password" required/>
  </p>
  <p>
    <input type="checkbox" name="remember" id="remember">
    <label for="remember">Remember me for 14 days</label>
  </p>
  <p align="right" style="font-size: 14px;padding: 5px;">
    <?php 
      if(isset($_REQUEST['error'])){
        echo "Invalid credentials. Please try again";
      }
    ?>
  </p>
  </div>
  <p class="p-container">
    <input type="submit" name="go" id="go" value="Log in">
  </p>
</form>

</body>
</html>
