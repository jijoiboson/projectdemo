-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2017 at 01:44 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitelaw_insurance`
--

-- --------------------------------------------------------

--
-- Table structure for table `claimmaster`
--

CREATE TABLE `claimmaster` (
  `claimId` int(11) NOT NULL,
  `jobNumber` varchar(50) DEFAULT NULL,
  `yourReference` varchar(50) DEFAULT NULL,
  `officeId` int(11) DEFAULT NULL,
  `policyNumber` varchar(50) DEFAULT NULL,
  `insuredName` varchar(50) DEFAULT NULL,
  `insurerName` varchar(200) DEFAULT NULL,
  `claimReference` varchar(50) DEFAULT NULL,
  `brokerReference` varchar(50) DEFAULT NULL,
  `contactPerson` varchar(50) DEFAULT NULL,
  `contactNumber` varchar(50) DEFAULT NULL,
  `insurerContact` varchar(50) DEFAULT NULL,
  `locationOfLoss` varchar(500) DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  `brokerId` int(11) DEFAULT NULL,
  `adjusterId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `instructionTime` varchar(20) DEFAULT NULL,
  `instructionDate` varchar(20) DEFAULT NULL,
  `contactTime` varchar(20) DEFAULT NULL,
  `contactDate` varchar(20) DEFAULT NULL,
  `surveyTime` varchar(20) DEFAULT NULL,
  `surveyDate` varchar(20) DEFAULT NULL,
  `preliminaryDate` varchar(50) DEFAULT NULL,
  `startDate` varchar(50) DEFAULT NULL,
  `startTime` varchar(50) DEFAULT NULL,
  `endDate` varchar(50) DEFAULT NULL,
  `endTime` varchar(50) DEFAULT NULL,
  `jobStatus` varchar(2) NOT NULL DEFAULT 'O' COMMENT 'O-Open, V-Visit,P-Preliminary, W-Working, C-Closed',
  `frozen` varchar(2) NOT NULL DEFAULT 'N',
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `claimmaster`
--

INSERT INTO `claimmaster` (`claimId`, `jobNumber`, `yourReference`, `officeId`, `policyNumber`, `insuredName`, `insurerName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `preliminaryDate`, `startDate`, `startTime`, `endDate`, `endTime`, `jobStatus`, `frozen`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`) VALUES
(1, 'DXB/G/9585/17', NULL, 1, '2/1/020/00010119', 'Philadelphia Private School', '', '9585', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 128, 8, 6, 38, '13:00', '01-03-2017', '14:00', '01-03-2017', '13:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:33:54', '2017-03-26 01:40:59', NULL),
(2, 'DXB/G/9586/17', NULL, 1, 'P/01/1002/2014/503', 'Al Futtaim Auto Centre', '', '9586', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 5, 6, 35, '13:00', '01-03-2017', '14:00', '01-03-2017', '15:00', '01-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:46:08', '2017-03-26 01:41:13', NULL),
(3, 'DXB/G/9587/17', NULL, 1, '4001/2014/01/00049', 'Al Jaber Transport & Gen. Cont.', '', '9587', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 4, 2, 6, '13:00', '01-03-2017', '14:00', '01-03-2017', '13:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:51:39', '2017-03-26 01:41:24', NULL),
(4, 'DXB/G/9588/17', NULL, 1, '2211099', 'Signature Terrace Restaurant', '', '9588', 'CL/DXB/SCL63/2017/000008', 'NA', 'NA', 'NA', 'NA', 5, 80, 9, 6, 38, '13:00', '01-03-2017', '13:00', '01-03-2017', '14:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 12:58:02', '2017-03-26 01:41:35', NULL),
(5, 'DXB/G/9589/17', NULL, 1, 'P/02/ENG/MBB/2016/00001', 'Kimoha Entrepreneurs Limited', '', '9589', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 8, 5, 26, '13:00', '02-03-2017', '13:00', '02-03-2017', '13:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 13:03:08', '2017-03-26 01:41:45', NULL),
(6, 'DXB/G/9590/17', NULL, 1, 'KH/DE/4769/16', 'Al Nasr Contracting & Demolition LLC', '', '9590', 'ARAYA/FGA/CLM/1960/28-02-17', 'NA', 'NA', 'NA', 'NA', 12, 39, 2, 2, 7, '13:00', '02-03-2017', '14:00', '02-03-2017', '13:00', '05-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:06:52', '2017-03-26 01:41:54', NULL),
(7, 'DXB/G/9591/17', NULL, 1, '01-311-133-11-75', 'Caboodle Pampers & Play LLC', '', '9591', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 7, 42, '13:00', '02-03-2017', '14:00', '02-03-2017', '15:00', '03-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:10:27', '2017-03-26 01:42:05', NULL),
(8, 'DXB/G/9592/17', NULL, 1, 'PI/2006/100001', 'Sky Oryx JV', '', '9592', 'NA', 'NA', 'NA', 'NA', 'NA', 8, 174, 2, 7, 43, '13:00', '02-03-2017', '14:00', '02-03-2017', '15:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:13:22', '2017-03-26 01:42:18', NULL),
(9, 'DXB/G/9593/17', NULL, 1, '2/2/020/04002484/3', 'Benjamin David Hughes', '', '9593', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 174, 3, 3, 22, '13:00', '02-03-2017', '14:00', '02-03-2017', '15:00', '02-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:16:11', '2017-03-26 01:42:27', NULL),
(10, 'DXB/G/9594/17', NULL, 1, '2/1/020/00010162', 'Chelsea Plaza Hotel', '', '9594', 'PAR17011', 'NA', 'NA', 'NA', 'NA', 44, 80, 8, 6, 37, '13:00', '02-03-2017', '14:00', '02-03-2017', '13:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:19:13', '2017-03-26 01:42:38', NULL),
(11, 'DXB/G/9595/17', NULL, 1, 'HFAP200500001548/075', 'Giordano Fashion LLC', '', '9595', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 142, 2, 6, 35, '13:00', '04-03-2017', '14:00', '04-03-2017', '13:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:21:55', '2017-03-26 01:42:47', NULL),
(12, 'DXB/G/9596/17', NULL, 1, '2/1/021/0000145', 'Yateem Optician', '', '9596', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 2, 6, 35, '22:00', '04-03-2017', '23:00', '04-03-2017', '22:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:24:29', '2017-03-26 01:43:06', NULL),
(13, 'DXB/G/9597/17', NULL, 1, 'HFAP200500001554/701', 'Al Maya International Ltd FZC', '', '9597', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 2, 6, 35, '10:00', '04-03-2017', '11:00', '04-03-2017', '10:00', '09-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:26:19', '2017-03-26 01:43:18', NULL),
(14, 'DXB/G/9598/17', NULL, 1, 'P/11/1002/2014/71', 'Life Health Care Group', '', '9598', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:28:05', '2017-03-26 01:43:52', NULL),
(15, 'DXB/G/9599/17', NULL, 1, 'P/001/2033/2016/16', 'Allied Transport LLC', '', '9599', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:30:33', '2017-03-26 01:44:03', NULL),
(16, 'DXB/G/9600/17', NULL, 1, 'HFAR201500002271', 'Jereh Energy Corpoporation', '', '9600', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 9, 2, 18, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '05-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:38:51', '2017-03-26 01:44:16', NULL),
(17, 'DXB/G/9601/17', NULL, 1, '2/1/020/22005341', 'Wayne Charles Dooley', '', '9601', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 44, 175, 8, 3, 21, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:41:26', '2017-03-26 01:44:51', NULL),
(18, 'DXB/G/9602/17', NULL, 1, '2003/2017/03/00005', 'Ajmal Group of Companies', '', '9602', 'CL/AZB/SCL15/2017/000006', 'NA', 'NA', 'NA', 'NA', 8, 81, 2, 6, 35, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:43:21', '2017-03-26 01:45:16', NULL),
(19, 'DXB/G/9603/17', NULL, 1, 'D/02/F350/1048/16', 'Matalan Department Stores', '', '9603', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 49, 82, 2, 6, 35, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:46:14', '2017-03-26 01:45:40', NULL),
(20, 'DXB/G/9604/17', NULL, 1, 'P/04/PRO/PRL/2016/10403', 'Union Cement Company', '', '9604', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 43, 175, 2, 5, 27, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:48:09', '2017-03-26 01:45:51', NULL),
(21, 'DXB/G/9605/17', NULL, 1, 'Z1-F13-000228-2', 'Lake Shore Tower', '', '9605', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 2, 53, 3, 6, 37, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '23-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 14:51:01', '2017-03-26 01:46:01', NULL),
(22, 'DXB/G/9606/17', NULL, 1, 'HFHM201700000097', 'Rixos Hotel', '', '9606', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 115, 5, 6, 37, '10:00', '05-03-2017', '11:00', '05-03-2017', '10:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:28:18', '2017-03-26 01:46:26', NULL),
(23, 'DXB/G/9607/17', NULL, 1, '2/1/020/02304266', 'Boston Foods', '', '9607', 'CL/DXB/SCL63/2017/000009', 'NA', 'NA', 'NA', 'NA', 44, 82, 2, 6, 35, '10:00', '06-03-2017', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:29:59', '2017-03-26 01:46:52', NULL),
(24, 'DXB/M/9608/17', NULL, 1, 'To Be Advised', 'Pioneer Gulf Services', '', '9608', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 23, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:32:04', '2017-03-26 01:47:01', NULL),
(25, 'DXB/G/9609/17', NULL, 1, 'HFAP201300004194', 'Sultan Group Investment', '', '9609', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 137, 5, 6, 35, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:34:01', '2017-03-26 01:47:10', NULL),
(26, 'DXB/G/9610/17', NULL, 1, 'P/01/1002/2014/364', 'Al Futtaim Watches & Jewellery', '', '9610', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 2, 6, 35, '10:00', '06-03-2017', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:36:42', '2017-03-26 01:53:48', NULL),
(27, 'DXB/G/9611/17', NULL, 1, '2/2/020/02306066/2', 'The Ice Cream Lab', '', '9611', 'NA', 'NA', 'NA', 'NA', 'NA', 44, 57, 2, 6, 35, '10:00', '06-03-2017', '11:00', '06-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:38:46', '2017-03-26 01:53:36', NULL),
(28, 'DXB/M/9612/17', NULL, 1, 'To Be Advised', 'Al Khayyat Investment (ALPHAMED)', '', '9612', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 27, 175, 9, 4, 23, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:41:08', '2017-03-26 01:53:28', NULL),
(29, 'DXB/G/9613/17', NULL, 1, '03/1101/13/2015/10', 'Mammut Group', '', '9613', 'NA', 'NA', 'NA', 'NA', 'NA', 23, 128, 4, 2, 18, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:45:06', '2017-03-26 01:53:18', NULL),
(30, 'DXB/G/9614/17', NULL, 1, 'PR2/138586/2016/NGI', 'Rivoli Group', '', '9614', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 33, 101, 2, 6, 35, '10:00', '06-03-2017', '11:00', '06-03-2017', '10:00', '14-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 15:53:34', '2017-03-26 01:53:09', NULL),
(31, 'DXB/G/9615/17', NULL, 1, 'EFER20150000021', 'Centaur Electromechanical', '', '9615', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 26, 5, 2, 9, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:08:58', '2017-03-26 01:53:01', NULL),
(32, 'DXB/G/9616/17', NULL, 1, 'DFTP201600001379', 'Frigoglass', '', '9616', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 2, 7, 44, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '06-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:10:55', '2017-03-26 01:52:50', NULL),
(33, 'DXB/G/9617/17', NULL, 1, 'AFCT201000000397', 'Emke Group', '', '9617', 'NA', 'NA', 'NA', 'NA', 'NA', 38, 174, 3, 9, 30, '10:00', '07-03-2017', '11:00', '07-03-2017', '12:00', '07-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:13:08', '2017-03-26 01:52:40', NULL),
(34, 'DXB/G/9618/17', NULL, 1, '01/1101/11/2016/290', 'Patchi LLC', '', '9618', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 87, 5, 6, 35, '10:00', '07-03-2017', '11:00', '07-03-2017', '11:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:15:06', '2017-03-26 01:52:30', NULL),
(35, 'DXB/G/9619/17', NULL, 1, 'To Be Advised', 'National Marine Dredging Co.', '', '9619', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 9, 2, 18, '10:00', '07-03-2017', '11:00', '07-03-2017', '10:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:17:42', '2017-03-26 01:52:22', NULL),
(36, 'DXB/G/9620/17', NULL, 1, 'P/2004/02/20011/2016/00034', 'DKC Veterinary Clinic', '', '9620', 'NA', 'NA', 'NA', 'NA', 'NA', 3, 174, 8, 6, 37, '10:00', '08-03-2017', '11:00', '08-03-2017', '10:00', '10-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:19:39', '2017-03-26 01:52:14', NULL),
(37, 'DXB/G/9621/17', NULL, 1, 'P2/20/16-0807-001404', 'Damas', '', '9621', 'C/00012870', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 1, 1, '10:00', '09-03-2017', '11:00', '09-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 16:23:18', '2017-03-26 01:52:05', NULL),
(38, 'DXB/G/9622/17', NULL, 1, 'UAE/01/220/11952', 'Cocoon Nursery', '', '9622', 'NA', 'NA', 'NA', 'NA', 'NA', 5, 174, 7, 6, 36, '10:00', '09-03-2017', '11:00', '09-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:38:32', '2017-03-26 01:51:52', NULL),
(39, 'DXB/G/9623/17', NULL, 1, 'To Be Advised', 'Green Land Textile', '', '9623', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 5, 175, 8, 6, 37, '10:00', '09-03-2017', '11:00', '09-03-2017', '10:00', '11-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:40:20', '2017-03-26 01:51:43', NULL),
(40, 'DXB/G/9624/17', NULL, 1, 'P2/20/16-0404-002739', 'Damas', '', '9624', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 119, 8, 6, 35, '10:00', '09-03-2017', '11:00', '09-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:42:17', '2017-03-26 01:51:32', NULL),
(41, 'DXB/G/9625/17', NULL, 1, 'P2/20/16-0404-002540', 'Lifestyle LLC', '', '9625', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '12-03-2017', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:43:35', '2017-03-26 01:51:25', NULL),
(42, 'DXB/G/9626/17', NULL, 1, 'To Be Advised', 'Bed, Bags & Beyond LLC', '', '9626', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 26, 175, 2, 6, 35, '10:00', '12-03-2017', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:45:25', '2017-03-26 01:51:14', NULL),
(43, 'DXB/G/9627/17', NULL, 1, 'Z1-F13-000211-2', 'Al Ghurair Exchange Limited', '', '9627', 'C/00012837', 'NA', 'NA', 'NA', 'NA', 2, 118, 2, 6, 35, '10:00', '12-03-2017', '11:00', '12-03-2017', '12:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:47:12', '2017-03-26 01:51:05', NULL),
(44, 'DXB/G/9628/17', NULL, 1, 'P/01/1002/2014/723', 'Cozmo Travel LLC', '', '9628', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 2, 6, 35, '10:00', '13-03-2017', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:49:10', '2017-03-26 01:50:58', NULL),
(45, 'DXB/G/9629/17', NULL, 1, 'HFHM201400000057', 'Seven Tides Limited &/ or Anantara Palm Jumeirah', '', '9629', 'ARYA/FGA/CLM/2378/13-03-2017', 'NA', 'NA', 'NA', 'NA', 38, 39, 8, 6, 37, '10:00', '13-03-2017', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:51:32', '2017-03-26 01:50:48', NULL),
(46, 'DXB/M/9630/17', NULL, 1, '01/3214/36/2016/72', 'Fadel Saif Mubarak Al Mazrouei', '', '9630', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 23, 175, 6, 4, 24, '10:00', '13-03-2017', '11:00', '13-03-2017', '10:00', '14-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:53:48', '2017-03-26 01:50:37', NULL),
(47, 'DXB/G/9631/17', NULL, 1, 'EFER201400001273', 'Palm Jumeirah Co. LLC', '', '9631', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 175, 9, 7, 42, '10:00', '13-03-2017', '11:00', '13-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:55:43', '2017-03-26 01:50:27', NULL),
(48, 'DXB/G/9632/17', NULL, 1, 'P/01/6004/2014/20', 'Al Futtaim Engineering', '', '9632', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 43, '10:00', '14-03-2017', '11:00', '14-03-2017', '10:00', '19-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 19:58:04', '2017-03-26 01:50:20', NULL),
(49, 'DXB/G/9633/17', NULL, 1, 'P/04/2033/2016/36', 'Dashmesh General Transport', '', '9633', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 6, 7, 45, '10:00', '14-03-2017', '11:00', '14-03-2017', '10:00', '15-02-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:08:29', '2017-03-26 01:50:09', NULL),
(50, 'DXB/G/9634/17', NULL, 1, 'P/01/1002/2014/206', 'Al Futtaim Real Estate Private Limited', '', '9634', 'NA', 'NA', 'NA', 'NA', 'NA', 40, 174, 7, 6, 37, '10:00', '15-03-2017', '11:00', '15-03-2017', '10:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:10:29', '2017-03-26 01:49:59', NULL),
(51, 'DXB/G/9635/17', NULL, 1, 'DP/01/3040/17/00004', 'Mazrui Holdings LLC', '', '9635', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 24, 87, 5, 9, 30, '10:00', '15-03-2017', '11:00', '15-03-2017', '10:00', '15-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:12:15', '2017-03-26 01:49:42', NULL),
(52, 'DXB/G/9636/17', NULL, 1, 'P/04/6001/2014/300', 'QGB Group of Companies', '', '9636', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 175, 7, 7, 42, '10:00', '16-03-2017', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:14:13', '2017-03-26 01:49:34', NULL),
(53, 'DXB/G/9637/17', NULL, 1, 'P/01/1002/2015/346', 'Saif Belhasa Holding', '', '9637', '14991 PK', 'NA', 'NA', 'NA', 'NA', 40, 15, 8, 6, 35, '10:00', '16-03-2017', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:16:17', '2017-03-26 01:49:17', NULL),
(54, 'DXB/G/9638/17', NULL, 1, 'P2/20/17-0807-001551', 'Lifestyle LLC', '', '9638', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 42, 128, 8, 6, 35, '10:00', '19-03-2017', '11:00', '19-03-2017', '10:00', '12-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:17:54', '2017-03-26 01:49:06', NULL),
(55, 'LON/G/0358/17', NULL, 2, 'P/40/1002/2014/123', 'Jawhara Jewellery LLC', '', '358', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 40, 17, 8, 6, 35, '10:00', '16-03-2017', '11:00', '16-03-2017', '12:00', '16-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 20:21:36', '2017-03-26 01:48:48', NULL),
(56, 'LON/G/0354/17', NULL, 2, 'To Be Advised', 'One of One Jewellery', '', '354', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 175, 8, 1, 1, '10:00', '01-03-2017', '11:00', '01-03-2017', '12:00', '01-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:06:01', '2017-03-26 01:48:37', NULL),
(57, 'LON/G/0355/17', NULL, 2, 'To Be Advised', 'Al Mumayyaz Jewellers', '', '355', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 51, 175, 8, 1, 1, '10:00', '06-03-2017', '11:00', '06-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:07:27', '2017-03-26 01:47:56', NULL),
(58, 'LON/G/0356/17', NULL, 2, 'To Be Advised', 'Damas Jewellery', '', '356', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 52, 118, 3, 1, 1, '10:00', '08-03-2017', '11:00', '08-03-2017', '12:00', '08-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:11:19', '2017-03-26 01:47:47', NULL),
(59, 'LON/G/0357/17', NULL, 2, 'P/40/5021/2015/4', 'Jawhara Jewellery LLC', '', '357', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 50, 17, 8, 6, 35, '10:00', '13-03-2017', '11:00', '13-03-2017', '12:00', '13-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:13:52', '2017-03-26 01:47:40', NULL),
(60, 'LON/G/0359/17', NULL, 2, 'To Be Advised', 'Whitestar DMCC', '', '359', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 53, 101, 3, 1, 3, '10:00', '19-03-2017', '11:00', '19-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-25 22:15:36', '2017-03-26 01:47:31', NULL),
(61, 'DXB/G/9639/17', NULL, 1, 'DFAR2015781', 'Bilal General Transport', '', '9639', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 38, 88, 9, 2, 18, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:28:29', '2017-03-27 09:58:54', NULL),
(62, 'DXB/G/9640/17', NULL, 1, 'To Be Advised', 'Royal Gardens Agricultural Contracting LLC', '', '9640', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 8, 175, 9, 2, 9, '10:00', '20-03-2017', '11:00', '20-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:37:22', '2017-03-27 10:08:53', NULL),
(63, 'DXB/G/9641/17', NULL, 1, 'P2/20/16/0501/001635', 'Proscape', '', '9641', '15007 PK', 'NA', 'NA', 'NA', 'NA', 42, 15, 5, 2, 6, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:41:56', '2017-03-27 10:11:56', NULL),
(64, 'DXB/G/9642/17', NULL, 1, 'To Be Advised', 'Dubai Investments Real Estate', '', '9462', 'C/00013050', 'NA', 'NA', 'NA', 'NA', 33, 119, 8, 6, 37, '10:00', '20-03-2017', '11:00', '20-03-2017', '12:00', '20-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:44:24', '2017-03-27 10:14:24', NULL),
(65, 'DXB/G/9643/17', NULL, 1, '20/3007/30/2017/2', 'Ejar Cranes & Equipment (Qatar)', '', '9643', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 14, 73, 4, 2, 7, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '21-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:47:36', '2017-03-27 10:17:36', NULL),
(66, 'DXB/G/9644/17', NULL, 1, 'HFCO201500000071/1', 'Menafactors Ltd', '', '9644', '18594', 'NA', 'NA', 'NA', 'NA', 38, 18, 7, 6, 37, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:49:47', '2017-03-27 10:19:47', NULL),
(67, 'DXB/M/9645/17', NULL, 1, 'To Be Advised', 'ADCO', '', '9645', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 2, 175, 9, 4, 23, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:53:30', '2017-03-27 10:23:30', NULL),
(68, 'DXB/M/9646/17', NULL, 1, 'To Be Advised', 'ADCO', '', '9646', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 1, 175, 9, 4, 23, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '23-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:56:59', '2017-03-27 10:26:59', NULL),
(69, 'DXB/G/9647/17', NULL, 1, 'CBPLI2017000106', 'Sama Events Management', '', '9647', 'To Be Advised', 'NA', 'NA', 'NA', 'NA', 48, 16, 3, 7, 42, '10:00', '20-03-2017', '11:00', '20-03-2017', '10:00', '22-03-2017', NULL, NULL, NULL, NULL, NULL, 'O', 'N', 1, '2017-03-27 15:59:31', '2017-03-27 10:29:31', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `claimmaster`
--
ALTER TABLE `claimmaster`
  ADD PRIMARY KEY (`claimId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `claimmaster`
--
ALTER TABLE `claimmaster`
  MODIFY `claimId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
