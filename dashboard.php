<?php 
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_adjusterid    = $_SESSION["loggedin_adjusterid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
}
//Get Bashboard numbers

$openclaimcount = 0;
$claimsforthemonth = 0;
$feeforthemonth = 0;
$feefortheyear = 0;
$outstandingprelim = 0;

//Open Claims

$opencountqry = "select count(*) as opencount from `claimmaster` where jobStatus = 'O'";
if($loggedin_isadmin != "Y"){
    $opencountqry .= " and adjusterId = '$loggedin_adjusterid'";
}
$stmt       = mysqli_query($connection, $opencountqry); 
$getcount   = mysqli_num_rows($stmt);
if($getcount > 0){
 while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
    $openclaimcount     = $row['opencount']; 
    }
}

//Claims For Month
$claimsmonthqry = "select count(*) as claimcount from `claimmaster` where date_format(instructionDate, '%Y-%m')=date_format(now(), '%Y-%m')";
if($loggedin_isadmin != "Y"){
    $claimsmonthqry .= " and adjusterId = '$loggedin_adjusterid'";
}
$stmt2       = mysqli_query($connection, $claimsmonthqry); 
$getcount2   = mysqli_num_rows($stmt2);
if($getcount2 > 0){
 while($row2 = mysqli_fetch_array($stmt2, MYSQLI_ASSOC)){
    $claimsforthemonth     = $row2['claimcount']; 
    }
}
$get_received = "select sum(t1.receiptAmount) as feesformonth from receiptdetails as t1, invoicemaster as t2 where t1.invoiceId = t2.invoiceId and date_format(t1.createdDate, '%Y-%m')=date_format(now(), '%Y-%m')";
if($loggedin_isadmin != "Y"){
    $get_received .= " and t1.createdBy = '$loggedin_userid'";
}
$recievedstmt       = mysqli_query($connection, $get_received); 
$getrcount   = mysqli_num_rows($recievedstmt);
    if($getrcount > 0){
        
      while($row4 = mysqli_fetch_array($recievedstmt, MYSQLI_ASSOC)){
        $feesformonth         = (empty($row4['feesformonth']))        ? 0 : $row4['feesformonth'];
        $feeforthemonth       += $feesformonth;
        }
    }
$get_receivedyear = "select sum(t1.receiptAmount) as feesforyear from receiptdetails as t1, invoicemaster as t2 where t1.invoiceId = t2.invoiceId and date_format(t1.createdDate, '%Y')=date_format(now(), '%Y')";
if($loggedin_isadmin != "Y"){
    $get_receivedyear .= " and t2.createdBy = '$loggedin_userid'";
}
$recievedstmt4       = mysqli_query($connection, $get_receivedyear); 
$getrcount4   = mysqli_num_rows($recievedstmt4);
    if($getrcount4 > 0){
        
      while($row4 = mysqli_fetch_array($recievedstmt4, MYSQLI_ASSOC)){
        $feesforyear         = (empty($row4['feesforyear']))        ? 0 : $row4['feesforyear'];
        $feefortheyear       += $feesforyear;
        }
    }
//Prelim Claims

$prelimcountqry = "select count(*) as prelimcount from `claimmaster` where jobStatus = 'P'";
if($loggedin_isadmin != "Y"){
    $prelimcountqry .= " and adjusterId = '$loggedin_adjusterid'";
}
$stmt5       = mysqli_query($connection, $prelimcountqry); 
$getcount5   = mysqli_num_rows($stmt5);
if($getcount5 > 0){
 while($row5 = mysqli_fetch_array($stmt5, MYSQLI_ASSOC)){
    $outstandingprelim     = $row5['prelimcount']; 
    }
}

$taskitems = array();
$gettasks = "select `claimId`, `jobNumber`, `jobStatus`, `createdDate`, `updatedDate` from `claimmaster` where adjusterId = '$loggedin_adjusterid' and jobStatus != 'C'";
$taskstmt       = mysqli_query($connection, $gettasks); 
$gettaskcount   = mysqli_num_rows($taskstmt);
if($gettaskcount > 0){
 while($taskrow = mysqli_fetch_array($taskstmt, MYSQLI_ASSOC)){
        $row['claimId']            = (empty($taskrow['claimId']))       ? '' : $taskrow['claimId'];
        $row['jobNumber']          = (empty($taskrow['jobNumber']))       ? '' : $taskrow['jobNumber'];
        $row['jobStatus']          = (empty($taskrow['jobStatus']))       ? '' : $taskrow['jobStatus'];
        $jobStatus                 = (empty($taskrow['jobStatus']))       ? '' : $taskrow['jobStatus'];
        $row['createdDate']        = (empty($taskrow['createdDate']))       ? '' : date("d M, Y", strtotime($taskrow['createdDate']));
        $row['updatedDate']        = (empty($taskrow['updatedDate']))       ? '' : date("d M, Y", strtotime($taskrow['updatedDate']));
        $actiontext = "";
        if($jobStatus == "O"){
            $jobStatusText = "Open";
        } elseif ($jobStatus == "V") {
            $jobStatusText = "Visit";
        } elseif ($jobStatus == "P") {
            $jobStatusText = "Preliminiary";
        } elseif ($jobStatus == "W") {
            $jobStatusText = "Working";
        } elseif ($jobStatus == "C") {
            $jobStatusText = "Closed";
        } elseif ($jobStatus == "I") {
            $jobStatusText = "Invoiced";
        } elseif ($jobStatus == "R") {
            $jobStatusText = "Receipt";
        } elseif ($jobStatus == "S") {
            $jobStatusText = "Status Report Issued";
        } else {
            $jobStatusText = "";
        }
        $row['jobStatusText'] = $jobStatusText;
        array_push($taskitems, $row);
    }
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
        <link href="assets/css/custom.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card" style="min-height: 120px;padding-top: 10px;">
                            <div  class="col-xs-0 col-sm-0 col-md-1">

                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-2 bgcolor1 infoboards">
                                <div class="topinfo"><?php echo $openclaimcount;?></div><div class="bottominfo">Open Claims</div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-2 bgcolor2 infoboards">
                                <div class="topinfo"><?php echo $claimsforthemonth;?></div><div class="bottominfo">Claims For The Month</div>
                            </div>
                            <!-- <div class="col-xs-6 col-sm-3 col-md-2 bgcolor3 infoboards">
                                <div class="topinfo">25</div><div class="bottominfo">Claims For The Year</div>
                            </div> -->
                            <div class="col-xs-6 col-sm-3 col-md-2 bgcolor1 infoboards">
                                <div class="topinfo"><?php echo number_format($feeforthemonth);?></div><div class="bottominfo">Fees For The Month</div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-2 bgcolor2 infoboards">
                                <div class="topinfo"><?php echo number_format($feefortheyear);?></div><div class="bottominfo">Fees For The Year</div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-2 bgcolor3 infoboards">
                                <div class="topinfo"><?php echo $outstandingprelim;?></div><div class="bottominfo">Outstanding Preliminary Reports</div>
                            </div>                              
                        </div>
                    
                    </div>
                </div>
                <?php if($loggedin_isadmin == "Y"){ ?>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Stages of claim reports</h4>
                            </div>
                            <div class="content">
                                <div id="claimstagechart" style=" height: 400px;"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Graphical representation of Adjusters</h4>
                                <p class="category">Yearly performance</p>
                            </div>
                            <div class="content">
                                <div id="adjusterchart" style=" height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Tasks</h4>
                            </div>
                            <div class="content">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>#</th>
                                            <th>Job Number</th>
                                            <th>Created Date</th>
                                            <th>Last Updated</th>
                                            <th>Status</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                        <?php 
                                            $count = 0;
                                            foreach ($taskitems as $items) {
                                                $count++;
                                                $itemjobnumber = $items['jobNumber'];
                                                $itemjobstatus = $items['jobStatusText'];
                                                $itemcreated   = $items['createdDate'];
                                                $itemupdated   = $items['updatedDate'];
                                                $itemjobstatus = $items['jobStatusText'];
                                                echo "<tr><td>$count</td><td>$itemjobnumber</td><td>$itemcreated</td><td>$itemupdated</td><td>$itemjobstatus</td></tr>";
                                            }

                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    
	<!-- Table DEMO methods, -->
	<script src="assets/js/demo.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('.sidebaritems').removeClass("active");
      $('#dashboard').addClass("active");

      //Get count
      var opencount = 0;
      var visitcount = 0;
      var prelimcount = 0;
      var workingcount = 0;
      var invoicecount = 0;
      var closedcount = 0;
      var statuscount = 0;

      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {


        var dataString = "RequestType=getClaimStatusCounts";
        $.ajax({
            type        : 'POST', 
            url         : 'include/api.php', 
            crossDomain : true,
            data        : dataString,
            dataType    : 'json', 
            async       : false,
            success : function (response)
                { 
                    $.each(response, function (i, member) {
                        // 
                        if(i == "O"){
                            opencount = parseInt(member);
                        }
                        if(i == "V"){
                            visitcount = parseInt(member);
                        }
                        if(i == "P"){
                            prelimcount = parseInt(member);
                        }
                        if(i == "W"){
                            workingcount = parseInt(member);
                        }
                        if(i == "I"){
                            invoicecount = parseInt(member);
                        }
                        if(i == "R"){
                            receiptcount = parseInt(member);
                        }
                        if(i == "C"){
                            closedcount = parseInt(member);
                        }
                        if(i == "S"){
                            statuscount = parseInt(member);
                        }
                    });
                    // console.log(opencount);
                    var data = google.visualization.arrayToDataTable([
                      ['Claim Stage', 'Count'],
                      ['Open',  opencount],
                      ['Visit',  visitcount],
                      ['Prelim', prelimcount],
                      ['Working', workingcount],
                      ['Invoice', invoicecount],
                      ['Receipt', receiptcount],
                      ['Closed', closedcount],
                      ['Status Report Issued', statuscount]
                    ]);
                    // console.log(data);
                          var options = {
                            legend: 'none',
                            pieSliceText: 'label',
                            title: '',
                            pieStartAngle: 100,
                          };

                    var chart = new google.visualization.PieChart(document.getElementById('claimstagechart'));
                    chart.draw(data, options);
                            },
                        error: function(error)
                        {
                            // alert("Something went wrong. Please try again later.");
                            return false;
                        }
                    });
                var data1 = new google.visualization.DataTable();
                
                var dataString = "RequestType=getAdjusterCount";
                // alert(dataString);
                $.ajax({
                    type        : 'POST', 
                    url         : 'include/api.php', 
                    crossDomain : true,
                    data        : dataString,
                    dataType    : 'json', 
                    async       : false,
                    success : function (response)
                        { 
                            // alert(1);
                            data1.addColumn('string', 'Adjuster');
                            data1.addColumn('number', 'Open Claims');
                            data1.addColumn('number', 'Visited');
                            data1.addColumn('number', 'Preliminary');
                            data1.addColumn('number', 'Invoiced');
                            data1.addColumn('number', 'Receipt');
                            data1.addColumn('number', 'Working');
                            data1.addColumn('number', 'Closed');
                            data1.addColumn('number', 'Status Report Issued');

                            $.each(response, function (i, member) {
                                if(member['O'] === "undefined"){
                                    member['O'] = 0;
                                }
                                if(member['I'] === "undefined"){
                                    member['I'] = 0;
                                }
                                if(member['R'] === "undefined"){
                                    member['R'] = 0;
                                }
                                if(member['V'] === "undefined"){
                                    member['V'] = 0;
                                }
                                if(member['P'] === "undefined"){
                                    member['P'] = 0;
                                }
                                if(member['W'] === "undefined"){
                                    member['W'] = 0;
                                }
                                if(member['C'] === "undefined"){
                                    member['C'] = 0;
                                }
                                if(member['S'] === "undefined"){
                                    member['S'] = 0;
                                }
                                data1.addRow([i, parseInt(member['O']), parseInt(member['V']), parseInt(member['P']), parseInt(member['I']), parseInt(member['R']), parseInt(member['W']), parseInt(member['C']), , parseInt(member['S'])]);
                                // console.log(member['I']);
                                // console.log(member);
                            });

                        },
                    error: function(error)
                    {
                        // alert("Something went wrong. Please try again later.");
                        return false;
                    }
                });
        

        
        
        
        // data1.addColumn('number', 'Expense');
        // data1.addRow(['Sunday',  10, 10]);
         // ['Day', 'Sales', 'Expenses'],
          // ['Sunday',  1000,      400],
          // ['Monday',  1170,      460],
          // ['Tuesday',  660,       1120],
          // ['Wednesday',  1030,      540],
          // ['Thursday',  1030,      540],
          // ['Friday',  1030,      540],
          // ['Saturday',  1030,      540]

        var options1 = {
          // title: 'Company Performance',
          isStacked: true,
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart1 = new google.visualization.ColumnChart(document.getElementById('adjusterchart'));

        chart1.draw(data1, options1);
    }

               
            });  
    </script>
</html>
