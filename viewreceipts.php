<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_REQUEST['id'])) && (!empty($_REQUEST['id']))){
        $selectedinvoiceId  = $_REQUEST['id'];
    } else {
        header("Location:receipt.php");
    }

}
    
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="receipt.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Receipt </p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>RECIEPT</th>
                                    	<th>INVOICE NUMBER</th>
                                    	<th>RECEIPT DATE</th>
                                    	<!-- <th>NARRATION</th> -->
                                    	<th>INVOICE AMOUNT</th>
                                    	<th>RECEIPT AMOUNT</th>
                                        <th>PAYMENT MODE</th>
										<th>CHEQUE NUMBER</th>
                                        <th>CHEQUE DATE</th>
                                        <th>RECIEVED FROM</th>
                                    </thead>
                                    <tbody>
                                        <?php 

                                            //Get all invoices
                                            $get_invoices = "select `invoiceId`, `claimId`, `jobNumber`, `invoiceNumber`, `currency`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `narrationText`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate` from `invoicemaster` where invoiceId = '$selectedinvoiceId'";
                                            $detailstmt       = mysqli_query($connection, $get_invoices); 
                                            $getcount   = mysqli_num_rows($detailstmt);
                                            if($getcount > 0){
                                                
                                              while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                                $invoiceId          = $row['invoiceId']; 
                                                $claimId            = (empty($row['claimId']))          ? '' : $row['claimId'];
                                                $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                                                $invoiceNumber      = (empty($row['invoiceNumber']))    ? '' : $row['invoiceNumber'];
                                                $currency           = (empty($row['currency']))         ? '' : $row['currency'];
                                                $faoName            = (empty($row['faoName']))          ? '' : $row['faoName'];
                                                $toName             = (empty($row['toName']))           ? '' : $row['toName'];
                                                $yourReference      = (empty($row['yourReference']))    ? '' : $row['yourReference'];
                                                $invoiceDate        = (empty($row['invoiceDate']))      ? '' : $row['invoiceDate'];
                                                $invoiceDate        = date('d/m/Y',strtotime($invoiceDate));
                                                $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                                                $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
                                                $totalAmount        = (empty($row['totalAmount']))      ? '' : $row['totalAmount'];
                                                $totalInWords       = (empty($row['totalInWords']))     ? '' : $row['totalInWords'];
                                                $invoiceTerms       = (empty($row['invoiceTerms']))     ? '' : $row['invoiceTerms'];
                                                $narrationText      = (empty($row['narrationText']))   ? '' : $row['narrationText'];
                                            
                                                $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
                                                $clientstmt       = mysqli_query($connection, $get_client); 
                                                $getclientcount   = mysqli_num_rows($clientstmt);
                                                if($getclientcount > 0){
                                                    
                                                  while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                    $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
                                                    $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                  }
                                              }
                                              $paidAmount = 0;
                                              $get_receipts = "select `receiptId`, `receiptNumber`, `invoiceId`, `receiptDate`, `receiptAmount`, `paymentMode`, `chequeNumber`, `chequeDate`, `receivedFrom` from `receiptdetails` where invoiceId = '$invoiceId'";
                                              $receiptstmt       = mysqli_query($connection, $get_receipts); 
                                              $getreceiptcount   = mysqli_num_rows($receiptstmt);
                                              $receiptcount      = 0;
                                              if($getreceiptcount > 0){
                                                $receiptcount++;
                                                while($row = mysqli_fetch_array($receiptstmt, MYSQLI_ASSOC)){
                                                        $receiptId          = $row['receiptId']; 
                                                        $receiptNumber      = (empty($row['receiptNumber']))          ? '' : $row['receiptNumber'];
                                                        $invoiceId          = (empty($row['invoiceId']))          ? '' : $row['invoiceId'];
                                                        $receiptDate        = (empty($row['receiptDate']))          ? '' : $row['receiptDate'];
                                                        $receiptAmount      = (empty($row['receiptAmount']))          ? '' : $row['receiptAmount'];
                                                        $paymentMode        = (empty($row['paymentMode']))          ? '' : $row['paymentMode'];
                                                        $chequeNumber       = (empty($row['chequeNumber']))          ? '--' : $row['chequeNumber'];
                                                        $chequeDate         = (empty($row['chequeDate']))          ? '--' : $row['chequeDate'];
                                                        $receivedFrom       = (empty($row['receivedFrom']))          ? '' : $row['receivedFrom'];
                                                        $paidAmount         = $paidAmount + $receiptAmount;
                                                    }
                                                }
                                                $balanceAmount = $totalAmount - $paidAmount;
                                              ?>
                                              <tr>
                                                <td><?php echo $receiptNumber;?></td>
                                                <td><?php echo $invoiceNumber;?></td>
                                                <td><?php echo $receiptDate;?></td>
                                                <td><?php echo number_format($totalAmount)." ".$currency;?></td>
                                                <td><?php echo number_format($receiptAmount)." ".$currency;?></td>
                                                <td><?php echo $paymentMode;?></td>
                                                <td><?php echo $chequeNumber;?></td>
                                                <td><?php echo $chequeDate;?></td>
                                                <td><?php echo $receivedFrom;?></td>
<!--                                                 <td>
                                                    <?php 
                                                    if($receiptcount > 0){
                                                        ?>
                                                    <a href="viewreceipts.php?id=<?php echo $invoiceId;?>"><button class="btn btn-info btn-fill pull-right">VIEW RECEIPTS</button></a>                                                        
                                                        <?php
                                                    }
                                                ?>
                                                </td>
                                                <td>
                                                <?php 
                                                    if($balanceAmount > 0){
                                                        ?>
                                                    <a href="generate.php?id=<?php echo $invoiceId;?>"><button class="btn btn-info btn-fill pull-right">MAKE RECEIPT</button></a>                                                        
                                                        <?php
                                                    }
                                                ?>
 -->
                                                </td>
                                            </tr>
                                              <?php
                                              }
                                          } else {
                                            echo "<tr><td colspan='7'>No invoice generated yet. Please create an invoice first.</td></tr>";
                                          }

                                        ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
  
    <script type="text/javascript">
        $('.sidebaritems').removeClass("active");
            $('#accounts').addClass("active");
    </script>
</html>
