<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
}
  if((isset($_POST['clientname'])) && (!empty($_POST['clientname']))){
     // print_r($_REQUEST);exit;
    $officelocation		= (empty($_REQUEST['officelocation']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['officelocation']));
	$insurername		= (empty($_REQUEST['insurername']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insurername']));
	$insuredname		= (empty($_REQUEST['insuredname']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insuredname']));
	$policynumber		= (empty($_REQUEST['policynumber']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['policynumber']));
	$clientname			= (empty($_REQUEST['clientname']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['clientname']));
	$brokername			= (empty($_REQUEST['brokername']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['brokername']));
	$adjustername		= (empty($_REQUEST['adjustername']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['adjustername']));
    $losslocation       = (empty($_REQUEST['losslocation']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['losslocation']));
    $losscategory       = (empty($_REQUEST['losscategory']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['losscategory']));
    $losssubcategory    = (empty($_REQUEST['losssubcategory'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['losssubcategory']));
    $claimreference    = (empty($_REQUEST['claimreference'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['claimreference']));
    $brokerreference    = (empty($_REQUEST['brokerreference'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['brokerreference']));
    $contactperson    = (empty($_REQUEST['contactperson'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['contactperson']));
    $contactnumber    = (empty($_REQUEST['contactnumber'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['contactnumber']));
    $insurercontact    = (empty($_REQUEST['insurercontact'])) ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['insurercontact']));
	// $categories			= (empty($_REQUEST['categories']))		? '' : $_REQUEST['categories'];
	// // print_r($categories);exit;
	// $selectedcategory = $categories[0];
	// foreach ($categories as $category) {
	// 	if($category != ""){
	// 		$selectedcategory = $category;
	// 	}
	// }
	// $categories 		= explode("~~", $selectedcategory);
	//$categoryid 		= $categories[0];
	//$subid 				= $categories[1];
    $categoryid         = $losscategory;
    $subid              = $losssubcategory;
	$instructiontime	= (empty($_REQUEST['instructiontime']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['instructiontime']));
	$instructiondate	= (empty($_REQUEST['instructiondate']))	? '' : mysqli_real_escape_string($connection, trim($_REQUEST['instructiondate']));
    $instructiondate    = date("Y-m-d", strtotime($instructiondate));
	$contacttime		= (empty($_REQUEST['contacttime']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['contacttime']));
	$contactdate		= (empty($_REQUEST['contactdate']))		? 'TBA' : mysqli_real_escape_string($connection, trim($_REQUEST['contactdate']));
	$surveytime			= (empty($_REQUEST['surveytime']))		? '' : mysqli_real_escape_string($connection, trim($_REQUEST['surveytime']));
	$surveydate			= (empty($_REQUEST['surveydate']))		? 'TBA' : mysqli_real_escape_string($connection, trim($_REQUEST['surveydate']));
    $dateofloss         = (empty($_REQUEST['dateofloss']))      ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['dateofloss']));
    $insert_details = "insert into `claimmaster`(`jobNumber`, `officeId`, `insurerName`, `insuredName`, `claimReference`, `brokerReference`, `contactPerson`, `contactNumber`, `insurerContact`, `locationOfLoss`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `createdBy`, `createdDate`, `dateOfLoss`) values ('', '$officelocation', '$insurername', '$insuredname', '$claimreference', '$brokerreference', '$contactperson', '$contactnumber', '$insurercontact', '$losslocation', '$policynumber', '$clientname', '$brokername', '$adjustername', '$categoryid', '$subid', '$instructiontime', '$instructiondate', '$contacttime', '$contactdate', '$surveytime', '$surveydate', '$loggedin_userid', now(), '$dateofloss')";
    // echo $insert_details;exit;
    mysqli_query($connection, $insert_details);
    $claimid 		= mysqli_insert_id($connection);
    $jobclaim  		= sprintf("%04d", $claimid);
    $get_officeprefix = "select `prefix` from `officemaster` where `officeId` = '$officelocation'";
    $prefixstmt       = mysqli_query($connection, $get_officeprefix);
    $getprefixcount   = mysqli_num_rows($prefixstmt);
    if($getprefixcount > 0){
        while($prefixrow = mysqli_fetch_array($prefixstmt, MYSQLI_ASSOC)){
            $selectedprefix   = (empty($prefixrow['prefix']))           ? '' : $prefixrow['prefix'];
	    }
	}
    //Get last claim id of this location
    $lastlocationcount = 0;
    //$get_last_location = "select count(*) as lastlocationcount, SUBSTRING_INDEX(`jobNumber`, '/', 2) as lastreferencecount from claimmaster where SUBSTRING_INDEX(`jobNumber`, '/', 1) = '$selectedprefix'";
    // echo $get_last_location; S
    $get_last_location = "select count(*) as lastlocationcount, max(SUBSTRING_INDEX(SUBSTRING_INDEX(`jobNumber`, '/', -2), '/', 1)) as lastreferencecount from claimmaster where SUBSTRING_INDEX(`jobNumber`, '/', 1) = '$selectedprefix'";
    $loccountstmt       = mysqli_query($connection, $get_last_location);
    $locationount   = mysqli_num_rows($loccountstmt);
    if($locationount > 0){
        while($locrow = mysqli_fetch_array($loccountstmt, MYSQLI_ASSOC)){
            $lastlocationcount   = (empty($locrow['lastreferencecount']))  ? 0 : $locrow['lastreferencecount'];
             $lastlocationcount =  $lastlocationcount+1;
        }
    }
    if ($lastlocationcount == 0){
        $jobclaim       = sprintf("%04d", 1);
    } else {
        $jobclaim       = sprintf("%04d", $lastlocationcount);
    }
	$get_catprefix = "select `prefix` from `categorymaster` where `categoryId` = '$categoryid'";
    $catprefixstmt       = mysqli_query($connection, $get_catprefix);
    $getcatprefixcount   = mysqli_num_rows($catprefixstmt);
    if($getcatprefixcount > 0){
        while($catprefixrow = mysqli_fetch_array($catprefixstmt, MYSQLI_ASSOC)){
            $selectedcatprefix   = (empty($catprefixrow['prefix']))  ? '' : $catprefixrow['prefix'];
	    }
	}
	$currentyear = date("y");
	$jobnumber = $selectedprefix."/".$selectedcatprefix."/".$jobclaim."/".$currentyear;
	// echo $jobnumber;exit;
	$update_jobnum = "update claimmaster set jobNumber = '$jobnumber' where claimId = '$claimid'";
	mysqli_query($connection, $update_jobnum);
	$_SESSION['currentclaimid'] = $claimid;
    if($claimid != ""){
        ?>
        <script type="text/javascript">
           // alert("New claim registered.");
            window.location.href = "claimupdate.php";
        </script>
        <?php 
    } else {
            ?>
        <script type="text/javascript">
            //alert("Something went wrong. Please try again.");
            return false;
        </script>
        <?php 
    }

  }
  ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">


    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="dashboard.php"><< Back to Dashboard</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Claim registration</h4>
                            </div>
                            <div class="content">
                                <form action="" method="POST">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Office Location <span class="mandatorystar">*</span></label><br>
                                                <select class="col-md-4 form-control" name="officelocation" id="officelocation" required>
                                                    <option value="">Select</option>
                                                    <?php 
                                                        $get_offices = "select `officeId`, `location`, `name`, `prefix` from `officemaster` where `active` = 'A'";
                                                        $officestmt       = mysqli_query($connection, $get_offices); 
                                                        $getofficecount   = mysqli_num_rows($officestmt);
                                                        if($getofficecount > 0){
                                                            
                                                          while($officerow = mysqli_fetch_array($officestmt, MYSQLI_ASSOC)){
                                                            $officeId   = (empty($officerow['officeId']))           ? '' : $officerow['officeId'];
                                                            $location   = (empty($officerow['location']))         ? '' : $officerow['location'];
                                                            $name       = (empty($officerow['name']))        ? '' : $officerow['name'];
                                                            $prefix     = (empty($officerow['prefix']))         ? '' : $officerow['prefix'];
                                                        ?>
                                                            <option value="<?php echo $officeId;?>"><?php echo $location;?></option>
                                                        <?php
                                                                  }
                                                      }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insurer <span class="mandatorystar">*</span></label>
                                                <select class="col-md-4 form-control" name="clientname" id="clientname" required>
                                                    <option value="">Select</option>
                                                    <?php 
                                                    $get_categories = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where `active` = 'A'";
                                                        $clientstmt       = mysqli_query($connection, $get_categories); 
                                                        $getclientcount   = mysqli_num_rows($clientstmt);
                                                        if($getclientcount > 0){
                                                            
                                                          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
                                                            $clientId     = $clientrow['clientId']; 
                                                            $referenceId  = (empty($clientrow['referenceId']))   ? '' : $clientrow['referenceId'];
                                                            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
                                                            $phoneNumber  = (empty($clientrow['phoneNumber']))       ? '' : $clientrow['phoneNumber'];
                                                            $emailId      = (empty($clientrow['emailId']))       ? '' : $clientrow['emailId'];
                                                            $city         = (empty($clientrow['city']))       ? '' : $clientrow['city'];
                                                ?>
                                                    <option value="<?php echo $clientId;?>"><?php echo $clientName;?></option>

                                                <?php
                                                          }
                                                      }
                                                ?>                      
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Broker <span class="mandatorystar">*</span></label>
                                                <select class="col-xs-4 form-control" name="brokername" id="brokername" required>
                                                    <option value="">Select</option>
                                                    <?php 
                                                    $get_brokers = "select `brokerId`, `employeeId`, `firstName`, `lastName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `brokers` where `active` = 'A'";
                                                    $brokerstmt       = mysqli_query($connection, $get_brokers); 
                                                    $getbrokercount   = mysqli_num_rows($brokerstmt);
                                                    if($getbrokercount > 0){
                                                      while($brokerrow = mysqli_fetch_array($brokerstmt, MYSQLI_ASSOC)){
                                                        $count = $count+1;
                                                        $brokerId     = $brokerrow['brokerId']; 
                                                        $employeeId   = (empty($brokerrow['employeeId']))   ? '' : $brokerrow['employeeId'];
                                                        $firstName    = (empty($brokerrow['firstName']))     ? '' : $brokerrow['firstName'];
                                                        $lastName     = (empty($brokerrow['lastName']))       ? '' : $brokerrow['lastName'];
                                                        $phoneNumber  = (empty($brokerrow['phoneNumber']))       ? '' : $brokerrow['phoneNumber'];
                                                        $emailId      = (empty($brokerrow['emailId']))       ? '' : $brokerrow['emailId'];
                                                        $city         = (empty($brokerrow['city']))       ? '' : $brokerrow['city'];
                                                        ?>
                                                    <option value="<?php echo $brokerId;?>"><?php echo $firstName." ".$lastName; ?></option>

                                                    <?php
                                                              }
                                                          }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Location of Loss <span class="mandatorystar">*</span></label>
                                                    <input type="text" class="form-control" name="losslocation" id="losslocation" placeholder="Location of Loss" required>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Policy Number <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="policynumber" id="policynumber" placeholder="Policy Number" required>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insurer <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="insurername" id="insurername" placeholder="Name of Insurer" required>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insured <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="insuredname" id="insuredname" placeholder="Name of Insured" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Claim Reference<span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="claimreference" id="claimreference" placeholder="Claim Reference" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Broker Reference <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="brokerreference" id="brokerreference" placeholder="Broker Reference" required>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insurer <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="insurername" id="insurername" placeholder="Name of Insurer" required>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Contact Person <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="contactperson" id="contactperson" placeholder="Contact Person" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Contact Number<span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="contactnumber" id="contactnumber" placeholder="Contact Number" required>
                                            </div>
                                        </div>
                                    </div>
									<div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Insurer Contact<span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control" name="insurercontact" id="insurercontact" placeholder="Insurer Contact" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Adjuster <span class="mandatorystar">*</span></label>
                                                <select class="col-md-4 form-control" name="adjustername" id="adjustername" required>
													<option value="">Select</option>
													<?php 
		                                            $get_adjusters = "select `adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address` from `adjusters` where `active` = 'A'";
		                                                $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
		                                                $getadjustercount   = mysqli_num_rows($adjusterstmt);
		                                                if($getadjustercount > 0){ 
		                                                  while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
		                                                    $adjusterId     = $adjusterrow['adjusterId']; 
		                                                    $userName   	= (empty($adjusterrow['userName']))   ? '' : $adjusterrow['userName'];
		                                                    $firstName    	= (empty($adjusterrow['firstName']))     ? '' : $adjusterrow['firstName'];
		                                                    $lastName     	= (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
		                                                    $emailId      	= (empty($adjusterrow['emailId']))       ? '' : $adjusterrow['emailId'];
		                                                    $city         	= (empty($adjusterrow['city']))       ? '' : $adjusterrow['city'];
		                                        ?>
		                                            <option value="<?php echo $adjusterId;?>"><?php echo $firstName." ".$lastName;?></option>
		                                        <?php
		                                                  }
		                                              }
		                                        ?>
												</select>
                                            </div>
                                        </div>
									</div>
									<div class="row">
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nature of Incident</label>
                                            </div>
                                        </div>
									</div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Category <span class="mandatorystar">*</span></label>
                                                <select class="col-md-4" style="width:300px;" name="losscategory" id="losscategory" required>
                                                    <option value="" style="display:none;">Select Category</option>
                                                    <?php 
                                                        $get_categories = "select `categoryId`, `category`, `prefix` from `categorymaster` where `active` = 'A'";
                                                            $categorystmt       = mysqli_query($connection, $get_categories); 
                                                            $getcategorycount   = mysqli_num_rows($categorystmt);
                                                            if($getcategorycount > 0){                                                   
                                                              while($categoryrow = mysqli_fetch_array($categorystmt, MYSQLI_ASSOC)){
                                                                $categoryId   = (empty($categoryrow['categoryId']))   ? '' : $categoryrow['categoryId'];
                                                                $category     = (empty($categoryrow['category']))     ? '' : $categoryrow['category'];
                                                                $prefix       = (empty($categoryrow['prefix']))       ? '' : $categoryrow['prefix'];
                                                                ?>
                                                                <option value="<?php echo $categoryId;?>"><?php echo $category;?></option>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sub-Category <span class="mandatorystar">*</span></label>
                                                <select class="col-md-4" style="width:300px;" name="losssubcategory" id="losssubcategory" required>
                                                    <option value="">Select Sub-Category</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                        </div>
                                    </div>
									<div class="row" style="display:none;">
										<?php 
                                            $get_categories = "select `categoryId`, `category`, `prefix` from `categorymaster` where `active` = 'A'";
                                                $categorystmt       = mysqli_query($connection, $get_categories); 
                                                $getcategorycount   = mysqli_num_rows($categorystmt);
                                                if($getcategorycount > 0){                                                   
                                                  while($categoryrow = mysqli_fetch_array($categorystmt, MYSQLI_ASSOC)){
                                                    $categoryId   = (empty($categoryrow['categoryId']))   ? '' : $categoryrow['categoryId'];
                                                    $category     = (empty($categoryrow['category']))     ? '' : $categoryrow['category'];
                                                    $prefix       = (empty($categoryrow['prefix']))       ? '' : $categoryrow['prefix'];
                                        			?>
                                        			<div class="col-md-4">
			                                            <div class="form-group">
			                                                <label><?php echo $category;?></label>
															<select class="col-md-4 lossnature"  style="width:300px;" name="categories[]" id="categories">
																<option value="">None</option>
																<!-- <option value="<?php echo $categoryId."~~0";?>">None</option> -->
																<?php 
																$get_subcategories = "select `subId`, `categoryId`, `name`, `prefix` from `subcategories` where categoryId = '$categoryId'";
				                                                $substmt       = mysqli_query($connection, $get_subcategories); 
				                                                $getsubcount   = mysqli_num_rows($substmt);
				                                                if($getsubcount > 0){
				                                                    
				                                                  while($subrow = mysqli_fetch_array($substmt, MYSQLI_ASSOC)){
				                                                    $subId1      = (empty($subrow['subId']))        ? '' : $subrow['subId'];
				                                                    $categoryId1 = (empty($subrow['categoryId']))   ? '' : $subrow['categoryId'];
				                                                    $name1       = (empty($subrow['name']))         ? '' : $subrow['name'];
				                                                    $prefix1     = (empty($subrow['prefix']))       ? '' : $subrow['prefix'];

				                                        ?>
				                                            <option value="<?php echo $categoryId1."~~".$subId1;?>"><?php echo $name1;?></option>

				                                        <?php
				                                                  }
				                                              }
																?>
															</select>
			                                            </div>
			                                        </div>
                                        			<?php
                                                  }
                                              }
                                        ?>
									</div>
									<br>
									<div class="row">
										<div class="col-md-10">
                                            <div class="form-group">
                                                <label>Instruction Received <span class="mandatorystar">*</span></label><br>
													<input type="time" style="width:220px;" name="instructiontime" id="instructiontime" required/>
													<input type="text" style="width:220px;" name="instructiondate" class="dateinputs" id="instructiondate" placeholder="Select Date" required/>
                                            </div>
                                        </div>
									</div>
									<div class="row">
										<div class="col-md-10">
                                            <div class="form-group">
                                                <label>Contact Made</label><br>
													<input type="time" name="contacttime" id="contacttime" style="width:220px;"/>
													<input type="text" name="contactdate" id="contactdate" class="dateinputs" style="width:220px;" placeholder="Select Date"/>
                                            </div>
                                        </div>
									</div>
									<div class="row">
										<div class="col-md-10">
                                            <div class="form-group">
                                                <label>Survey Arranged for </label><br>
													<input type="time" name="surveytime" id="surveytime" style="width:220px;"/>
													<input type="text" name="surveydate" id="surveydate" class="dateinputs" style="width:220px;" placeholder="Select Date"/>
                                            </div>
                                        </div>
									</div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label>Date Of Loss</label><br>
                                                    <input type="text" name="dateofloss" id="dateofloss" class="dateinputs" style="width:220px;" placeholder="Select Date"/>
                                            </div>
                                        </div>
                                    </div>
									
									</div>
									<table align="right" width="30%">
									<tr>
										<td>
											<button type="submit" class="btn btn-info btn-fill pull-right">ADD NEW CLAIM</button>
										</td>
										<td></td>
										<td>
											<button type="reset" class="btn btn-info btn-fill pull-right">RESET</button>
										</td>
									</tr>
									</table>

                                    <div class="clearfix"></div>
                                </form>
								<div class="row">
								</div>
								
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
  //   	jQuery(function($){
		//     $("form").find(".lossnature").change(function(){
		//     	var selected = $(this).val();
		//     	// alert(selected);
		//         $(".lossnature").val("");
		//         $(this).val(selected);
		//     });   
		// });
    </script>
   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#newclaim').addClass("active");
            $('.dateinputs').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#losscategory').on('change', function() {
              // alert( this.value );
              var selectedCategory = this.value;
              var dataString = "RequestType=getSubCategories&catId="+selectedCategory;
                // alert(dataString);
                $('#losssubcategory').empty();
                $.ajax({
                    type        : 'POST', 
                    url         : 'include/api.php', 
                    crossDomain : true,
                    data        : dataString,
                    dataType    : 'json', 
                    async       : false,
                    success : function (response)
                        { 
                            $.each(response, function (i, member) {
                                var option = "<option value='"+member.subId+"'>"+member.name+"</option>";
                                $('#losssubcategory').append(option);
                            });
                        },
                    error: function(error)
                    {
                        alert("Something went wrong. Please try again later.");
                        return false;
                    }
                });
            })

        });
    </script>

</html>
