
<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_adjusterid    = $_SESSION["loggedin_adjusterid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_POST['location'])) && (!empty($_POST['location']))){
        $location       = (empty($_REQUEST['location']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['location']));
    }
    if((isset($_POST['adjusterid'])) && (!empty($_POST['adjusterid']))){
        $adjusterid       = (empty($_REQUEST['adjusterid']))    ? '' : mysqli_real_escape_string($connection, trim($_REQUEST['adjusterid']));
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        Whitelaw
    </title>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="accounts.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Received Payments</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                            <table class="table">
                                <tr>
                                    <td style="padding-left: 5px;padding-right: 0px;">
                                        <label>Status</label>
                                        <select id="selectfilestatus" name="selectfilestatus" class="form-control">
                                            <option value="A" selected>View All</option>
                                            <option value="O">Open</option>
                                            <option value="C">Closed</option>
                                        </select>
                                    </td>
                                    <?php if($loggedin_isadmin == "Y"){
                                            ?>
                                    <td style="padding-left: 20px;" >
                                        <label>Adjuster</label>
                                        <select id="selectadjuster" name="selectadjuster" class="form-control">
                                            <option value="A" selected>View All</option>
                                            <?php 
                                                $get_adjusters = "select `adjusterId`, `firstName`, `lastName`, `userName`, `emailId`, `city`, `country`, `postalCode`, `address` from `adjusters` where active = 'A'";
                                                $stmt       = mysqli_query($connection, $get_adjusters); 
                                                $getcount   = mysqli_num_rows($stmt);
                                                $count = 0;
                                                if($getcount > 0){
                                                    
                                                  while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                    $count = $count+1;
                                                    $adjusterId     = $row['adjusterId']; 
                                                    $userName   = (empty($row['userName']))   ? '' : $row['userName'];
                                                    $firstName    = (empty($row['firstName']))     ? '' : $row['firstName'];
                                                    $lastName     = (empty($row['lastName']))       ? '' : $row['lastName'];
                                                    $emailId      = (empty($row['emailId']))       ? '' : $row['emailId'];
                                                    $city         = (empty($row['city']))       ? '' : $row['city'];
                                                    ?>
                                                    <option value="<?php  echo $adjusterId; ?>"><?php echo $userName; ?></option>
                                                    <?php
                                                  }
                                              }
                                            ?>
                                        </select>
                                    </td>
                                            <?php
                                        }
                                        ?>

                                    <td style="padding-left: 20px;vertical-align: middle;padding-top: 37px;" rowspan="2">
                                            <button class="btn filterbutton" id="filterbutton">FILTER</button>
                                        </td>
                            </table>
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>JOB NUMBER</th>
                                        <th>PROJECTED FEES</th>
                                        <th>RESERVED AMOUNT</th>
                                        <th>STATUS FEE</th>
                                        <th>DATE</th>
                                    </thead>
                                    <tbody id="filteredcontent">
                                        <?php 

                                            //Get all invoices
                                        $count = 0;
                                        $totalprojectedfee = 0;
                                        $totalreservedamount = 0;   
                                        $totalstatusfee = 0; 
                                        $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster`";
                                        if($loggedin_isadmin != "Y"){
                                            $get_details .= " where adjusterId = '$loggedin_adjusterid'";
                                        }
                                        // echo $get_details;exit;
                                        $detailstmt1       = mysqli_query($connection, $get_details); 
                                        $getcount1   = mysqli_num_rows($detailstmt1);
                                        if($getcount1 > 0){

                                          while($row1 = mysqli_fetch_array($detailstmt1, MYSQLI_ASSOC)){
                                            $claimId            = $row1['claimId']; 
                                            $jobNumber          = $row1['jobNumber'];
                                           $count++;  
                                           $projectedfee = 0;
                                           $reservedamount = 0;   
                                           $statusfee = 0;                                        
                                                    $get_details = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `prelimupdates`  where claimId = '$claimId'";
                                                    $detailstmt       = mysqli_query($connection, $get_details); 
                                                    $getcount   = mysqli_num_rows($detailstmt);
                                                    if($getcount > 0){
                                                        
                                                      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                                        $prelimId            = $row['prelimId']; 
                                                        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
                                                        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
                                                        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
                                                        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));         
                                                        if($updateType == "F"){
                                                            $projectedfee += $updateContent;
                                                            $totalprojectedfee += $updateContent;
                                                        }
                                                        if($updateType == "A"){
                                                            $reservedamount += $updateContent;
                                                            $totalreservedamount += $updateContent;
                                                        }
                                                    }
                                                } 
                                                // $get_status_details = "select `statusId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `statusupdates`  where claimId = '$claimId'";
                                                //     $sdetailstmt       = mysqli_query($connection, $get_status_details); 
                                                //     $getscount   = mysqli_num_rows($sdetailstmt);
                                                //     if($getscount > 0){
                                                        
                                                //       while($srow = mysqli_fetch_array($sdetailstmt, MYSQLI_ASSOC)){
                                                //         $statusId            = $row['statusId']; 
                                                //         $updateType         = (empty($srow['updateType']))        ? '' : $srow['updateType'];
                                                //         $updateContent      = (empty($srow['updateContent']))     ? '' : $srow['updateContent'];
                                                //         $originalFilename   = (empty($srow['originalFilename']))  ? '' : $srow['originalFilename'];
                                                //         $createdDate        = (empty($srow['createdDate']))       ? '' : date('d M, Y', strtotime($srow['createdDate']));         
                                                //         if($updateType == "F"){
                                                //             $statusfee += $updateContent;
                                                //             $totalstatusfee += $updateContent;
                                                //         }                                                       
                                                //     }
                                                // } 
                                                $get_status_details = "select `statusId`, `claimId`, `report`, `reserveAmount`, `statusDate`, `originalFilename`, `createdBy`, `createdDate` FROM `statusreports`  where claimId = '$claimId'";
                                                // echo $get_status_details;exit;
                                                $sdetailstmt       = mysqli_query($connection, $get_status_details); 
                                                $getscount   = mysqli_num_rows($sdetailstmt);
                                                if($getscount > 0){
                                                    
                                                  while($statusrow = mysqli_fetch_array($sdetailstmt, MYSQLI_ASSOC)){
                                                    $statusId            = $statusrow['statusId']; 
                                                    $report         = (empty($statusrow['report']))        ? '' : $statusrow['report'];
                                                    $statusAmount      = (empty($statusrow['reserveAmount']))     ? '' : $statusrow['reserveAmount'];
                                                    $originalFilename      = (empty($statusrow['originalFilename']))     ? '' : $statusrow['originalFilename'];
                                                    $statusDate   = (empty($statusrow['statusDate']))  ? '' : date('d M, Y', strtotime($statusrow['statusDate']));        
                                                    // if($updateType == "F"){
                                                        $statusfee += $statusAmount;
                                                        $totalstatusfee += $statusAmount;
                                                    // }                                                       
                                                }
                                            }     
                                                if(($projectedfee + $reservedamount + $statusfee) > 0){


                                                ?>
                                                                <tr>
                                                                    <td><?php echo $jobNumber; ?></td>
                                                                    <td><?php echo $projectedfee; ?></td>
                                                                    <td><?php echo $reservedamount; ?></td>
                                                                    <td><?php echo $statusfee; ?></td>
                                                                    <td><?php echo $createdDate; ?></td>
                                                                </tr>
                                                            <?php     
                                            }} 
                                                  
                                        }
                                        ?>
                                                                <tr>
                                                                    <td><strong>TOTAL : </strong></td>
                                                                    <td><strong><?php echo $totalprojectedfee; ?></strong></td>
                                                                    <td><strong><?php echo $totalreservedamount; ?></strong></td>
                                                                    <td><strong><?php echo $totalstatusfee; ?></strong></td>
                                                                    <td></td>
                                                                </tr>
                                                            <?php 
                                        if($count <= 0){
                                                echo "<tr><td colspan='7'>No Projected Fees </td></tr>";
                                            }
                                            
                            

                                        ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#accounts').addClass("active");
                        $('#filterbutton').click(function(){
                //Get filter values
                var statusid = $('#selectfilestatus').val();
                var adjusterid = $('#selectadjuster').val();
                if(adjusterid == undefined){
                    adjusterid = '<?php echo $loggedin_adjusterid;?>';
                }
                var dataString = "RequestType=getFilteredFees&statusid="+statusid+"&adjusterid="+adjusterid;
                console.log(dataString);
                $('#filteredcontent').empty();
                $.ajax({
                    type        : 'POST', 
                    url         : 'include/api.php', 
                    crossDomain : true,
                    data        : dataString,
                    dataType    : 'json', 
                    async       : false,
                    success : function (response)
                        { 
                            $.each(response, function (i, member) {
                                // console.log(member);
                               $('#filteredcontent').append(member);
                            });
                        },
                    error: function(error)
                    {
                        alert("Something went wrong. Please try again later.");
                        return false;
                    }
                });
            });
        });
    </script>
   

</html>
