<?php
ini_set('display_errors',0);
 // ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    $currencytext = "";
    $currencyshort = "";
    if((isset($_REQUEST["id"])) && (!empty($_REQUEST["id"]))){
        $selectedinvoiceid = $_REQUEST["id"];
        $get_invoice = "select `invoiceId`, `claimId`, `jobNumber`, `invoiceTitle`, `invoiceNumber`, `currency`, `currencyId`, `faoName`, `toName`, `yourReference`, `invoiceDate`, `clientId`, `locationOfLoss`, `totalAmount`, `totalInWords`, `invoiceTerms`, `createdBy`, `createdDate`, `updatedBy`, `updatedDate` FROM `invoicemaster` where invoiceId = '$selectedinvoiceid'";
        $detailstmt       = mysqli_query($connection, $get_invoice); 
        $getcount   = mysqli_num_rows($detailstmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
            $invoiceId          = $row['invoiceId']; 
            $claimId            = (empty($row['claimId']))          ? '' : $row['claimId'];
            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
            $invoiceNumber      = (empty($row['invoiceNumber']))    ? '' : $row['invoiceNumber'];
            $invoiceTitle       = (empty($row['invoiceTitle']))     ? '' : $row['invoiceTitle'];
            $currency           = (empty($row['currency']))         ? '' : $row['currency'];
            $thiscurrencyId     = (empty($row['currencyId']))         ? '' : $row['currencyId'];

            $get_currency = "select `currencyId`, `currencyCode`, `payableTo`, `ibanNumber`, `swiftCode`, `currencyName`, `currencyShort` from `currencydetails` where currencyId = '$thiscurrencyId'";
            $currencystmt       = mysqli_query($connection, $get_currency); 
            $getcurrencycount   = mysqli_num_rows($currencystmt);
            if($getcurrencycount > 0){
              while($currencyrow = mysqli_fetch_array($currencystmt, MYSQLI_ASSOC)){
                    $currencytext = (empty($currencyrow['currencyName']))         ? '' : $currencyrow['currencyName'];
                    $currencyshort = (empty($currencyrow['currencyShort']))         ? '' : $currencyrow['currencyShort'];
                    $payableTo = (empty($currencyrow['payableTo']))         ? '' : $currencyrow['payableTo'];
                    $ibanNumber = (empty($currencyrow['ibanNumber']))         ? '' : $currencyrow['ibanNumber'];
                    $swiftCode = (empty($currencyrow['swiftCode']))         ? '' : $currencyrow['swiftCode'];
                    
                }
            }

            // if($currency == "AED"){
            //     $currencytext = "UAE Dirhams";
            //     $currencyshort = "Dhs";
            // } elseif ($currency == "USD") {
            //     $currencytext = "US Dollars";
            //     $currencyshort = "$";
            // } elseif ($currency == "GBP") {
            //     $currencytext = "GB Pounds";
            //     $currencyshort = "&pound;";
            // } else {
            //     $currencytext = "";
            //     $currencyshort = "";
            // }

            $faoName            = (empty($row['faoName']))          ? '' : $row['faoName'];
            $toName             = (empty($row['toName']))           ? '' : $row['toName'];
            $yourReference      = (empty($row['yourReference']))    ? '' : $row['yourReference'];
            $invoiceDate        = (empty($row['invoiceDate']))      ? '' : $row['invoiceDate'];
            $invoiceDate        = date('d/m/Y',strtotime($invoiceDate));
            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
            $locationOfLoss     = (empty($row['locationOfLoss']))   ? '' : $row['locationOfLoss'];
            $totalAmount        = (empty($row['totalAmount']))      ? '' : $row['totalAmount'];
            $totalInWords       = (empty($row['totalInWords']))     ? '' : $row['totalInWords'];
            $invoiceTerms       = (empty($row['invoiceTerms']))     ? '' : $row['invoiceTerms'];
          }
      }
      //Get claim details
        $get_claimdetails = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `locationOfLoss`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster` where claimId = '$claimId'";
        $claimdetailstmt       = mysqli_query($connection, $get_claimdetails); 
        $getclaimcount   = mysqli_num_rows($claimdetailstmt);
        if($getclaimcount > 0){
            while($claimrow = mysqli_fetch_array($claimdetailstmt, MYSQLI_ASSOC)){
                $claimofficeId           = (empty($claimrow['officeId']))         ? '' : $claimrow['officeId'];
                $claiminsurerName        = (empty($claimrow['insurerName']))      ? '' : $claimrow['insurerName'];
                $claimpolicyNumber       = (empty($claimrow['policyNumber']))     ? '' : $claimrow['policyNumber'];
                $claiminsuredName        = (empty($claimrow['insuredName']))      ? '' : $claimrow['insuredName'];
                $claimclientId           = (empty($claimrow['clientId']))         ? '' : $claimrow['clientId'];
                $claimbrokerId           = (empty($claimrow['brokerId']))         ? '' : $claimrow['brokerId'];
                $claimadjusterId         = (empty($claimrow['adjusterId']))       ? '' : $claimrow['adjusterId'];
                $claimcategoryId         = (empty($claimrow['categoryId']))       ? '' : $claimrow['categoryId'];
                $claimsubId              = (empty($claimrow['subId']))            ? '' : $claimrow['subId'];
              }
          }
      //Get subcategory
      $get_subdetails   = "select `name`, `prefix` from `subcategories` where subId = '$claimsubId'";
      $subdetailstmt    = mysqli_query($connection, $get_subdetails); 
      $getsubcount      = mysqli_num_rows($subdetailstmt);
      if($getsubcount > 0){
        while($subrow = mysqli_fetch_array($subdetailstmt, MYSQLI_ASSOC)){
            $causeofloss           = (empty($subrow['name']))         ? '' : $subrow['name'];
        }
      }
      //Get client details
       $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
        $clientstmt       = mysqli_query($connection, $get_client); 
        $getclientcount   = mysqli_num_rows($clientstmt);
        if($getclientcount > 0){
            
          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
            $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
          }
      }

    }
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">


    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<div class="wrapper">
    <?php include("sidebar.php");?>
    <div class="main-panel">
        <?php include("navbar.php");?>
        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="jobselect.php"><< Back to Invoices</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Generated Invoice</h4>
                            </div>
                            <div class="content" id="printablecontent">
                                <div class="col-xs-12 printheader paddingrl0" align="center">
                                    <!-- <img src="assets/img/header.png"> -->
                                    <div style="height:60px;"></div>
                                </div>
                                <table width="700px;">
                                    <tr>
                                        <th colspan="2" style="font-size:20px;padding-top:20px;padding-bottom:20px;text-transform:uppercase;"><strong><?php echo $invoiceTitle;?></strong></th>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-top:20px;padding-bottom:20px;">Invoice No</td>
                                        <td style="font-size:15px;padding-top:20px;padding-bottom:20px;"><?php echo $invoiceNumber;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-top:10px;">FAO</td>
                                        <td style="font-size:15px;padding-top:10px;"><?php echo $faoName;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-bottom:20px;">To</td>
                                        <td style="font-size:15px;padding-bottom:20px;"><?php echo $toName;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-top:10px;">Our Ref.</td>
                                        <td style="font-size:15px;padding-top:10px;"><?php echo $jobNumber;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;">Your Ref.</td>
                                        <td style="font-size:15px;"><?php echo $yourReference;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-bottom:10px;">Invoice Date.</td>
                                        <td style="font-size:15px;padding-bottom:10px;"><?php echo $invoiceDate;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-top:10px;">Insurer.</td>
                                        <td style="font-size:15px;padding-top:10px;"><?php echo $clientName;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;">Location of Loss.</td>
                                        <td style="font-size:15px;"><?php echo $locationOfLoss;?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-size:15px;padding-bottom:30px;">Cause Of Loss.</td>
                                        <td style="font-size:15px;padding-bottom:30px;"><?php echo $causeofloss;?></td>
                                    </tr>
                                </table>   
                                <table width="700px;">
                                    <tr style="border-bottom:2px solid #000;margin-bottom:10px;">
                                        <th style="width:350px;font-size:17px;hpadding-bottom:10px;">Description</th>
                                        <th colspan="2" style="width:350px;font-size:17px;text-align:right;padding-bottom:10px;">Total Payable</th>
                                    </tr>
                                    <?php 
                                        //Get invoice details
                                        $getinvoicedetails = "select `detailsId`, `invoiceId`, `description`, `narrationText`, `timelyRate`, `timelyText`, `amount` from `invoicedetails` where invoiceId = '$selectedinvoiceid'";
                                        // echo $getinvoicedetails;
                                        $invoicedetailsstmt       = mysqli_query($connection, $getinvoicedetails); 
                                        $getdetailscount   = mysqli_num_rows($invoicedetailsstmt);
                                        if($getdetailscount > 0){
                                            
                                          while($detailsrow = mysqli_fetch_array($invoicedetailsstmt, MYSQLI_ASSOC)){
                                            $description   = (empty($detailsrow['description']))     ? '' : $detailsrow['description'];
                                            $amount   = (empty($detailsrow['amount']))       ? '' : $detailsrow['amount'];
                                            $timelyText   = (empty($detailsrow['timelyText']))       ? '' : $detailsrow['timelyText'];
                                            $timelyRate   = (empty($detailsrow['timelyRate']))       ? '' : $detailsrow['timelyRate'];
                                            $narrationText  = (empty($detailsrow['narrationText']))  ? '' : $detailsrow['narrationText'];
                                            if((!empty($timelyRate))&&(!empty($timelyText))){
                                                $timelyWords = $timelyRate." @ ".$timelyText;
                                            } else {
                                                $timelyWords = "";
                                            }
                                            ?>
                                            <tr>
                                                <td style="font-size:15px;padding-bottom:10px;"><?php echo $description;?></td>
                                                <td style="font-size:15px;padding-bottom:10px;"><?php echo $narrationText;?></td>
                                                <td style="font-size:15px;padding-bottom:10px;text-align:right;"><?php echo number_format($amount);?></td>
                                            </tr>
                                            <?php
                                          }
                                      }
                                    ?>
                                </table>    
                                <table width="700px;">
                                    <tr style="border-bottom:2px solid #000;margin-bottom:10px;">
                                        <th style="width:350px;font-size:17px;padding-bottom:10px;">TOTAL (<?php echo $currencytext;?>)</th>
                                        <th style="width:225px;font-size:17px;text-align:right;padding-bottom:10px;"><?php echo $currencyshort;?></th>
                                        <th style="width:125px;font-size:17px;text-align:right;padding-bottom:10px;"><?php echo number_format($totalAmount);?></th>
                                    </tr>
                                </table>   
                                <table width="700px;">
                                    <tr>
                                        <td style="text-align:right;font-size:17px;padding-top:20px;"><strong><i>( <span style="color:red;"><?php echo $totalInWords;?></span> )</i></strong></td>
                                    </tr>
                                </table>    
                                <table style="width:700px;margin-top:10px;margin-bottom:10px;">
                                    <tr>
                                        <td style="border:2px solid #000;text-align:center;font-size:17px;color:red;background-color:#C5D9F1;"><strong><?php echo $invoiceTerms;?></strong></td>
                                    </tr>
                                </table>  
                                <table style="width:700px;margin-top:60px;">
                                    <tr>
                                        <td style="text-align:left;font-size:16px"><strong>Graham Whitelaw (Senior Director)</strong></td>
                                    </tr>
                                </table>
                                <table style="width:700px;margin-top:10px;margin-bottom:10px;">
                                    <tr>
                                        <td style="border:2px solid #000;text-align:center;font-size:17px;color:red;background-color:#C5D9F1;"><strong>Cheque Payable to '<?php echo $payableTo;?>' and ONLY to this name.</strong></td>
                                    </tr>
                                </table> 
                                <table style="width:700px;">
                                    <tr>
                                        <td style="text-align:center;font-size:16px">Account Transfers to: IBAN No: <strong><u><?php echo $ibanNumber;?></u></strong> | Swift Code : <strong><u><?php echo $swiftCode;?></u></strong></td>
                                    </tr>
                                </table>                     
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                         <a href="receipt.php"><button class="btn btn-info btn-fill pull-right">DONE</button></a><button class="btn btn-info btn-fill pull-right" id="printbutton" style="margin-left:10px;margin-right:10px;">PRINT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#printbutton').click(function(){
                $('#printablecontent').print();
            });
            $('.sidebaritems').removeClass("active");
            $('#accounts').addClass("active");
        });
    </script>
   

</html>
