<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>
        
        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="dashboard.php"><< Back to Dashboard</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"></h4>
                                <p class="category">Admin Page</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table align="center" width="80%" border=0>
									<tbody>
										<tr height="100px"><td></td></tr>
									    <tr>
											<td align="center">
												<a href="masters.php"><button align="center" class="btn btn-info btn-fill pull-right" style="width:160px;">MASTERS</button></a>
											</td>
										    <td></td>
											<td align="center">
												<a href="userlist.php"><button align="center" class="btn btn-info btn-fill pull-right" style="width:160px;">EDIT USER</button></a>
											</td>
										    <td></td>
											<td>
												<a href="add.php"><button class="btn btn-info btn-fill pull-right" style="width:160px;">ADD USER</button></a>
											</td>
                                            <td>
                                                <a href="newclaim.php"><button class="btn btn-info btn-fill pull-right" style="width:160px;">NEW CLAIM</button></a>
                                            </td>
                                        </tr>
										<tr height="100px"><td></td></tr>
										<tr>
                                            <td align="center">
                                                <a href="currency.php"><button align="center" class="btn btn-info btn-fill pull-right" style="width:160px;">CURRENCY</button></a>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
								</table>

                            </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#adminpanel').addClass("active");
        });
    </script>

</html>
