<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}
  if((isset($_POST['category'])) && (!empty($_POST['category']))){
     // print_r($_FILES);exit;
    $category      = (empty($_REQUEST['category']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['category']));
    $prefix        = (empty($_REQUEST['prefix']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['prefix']));
    $insert_details = "insert into `categorymaster`(`category`, `prefix`, `active`, `createdBy`, `createdDate`) VALUES ('$category', '$prefix', 'A', '$loggedin_userid', now())";
    mysqli_query($connection, $insert_details);
    header("Location:categorylist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="category_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Category master</h4>
                            </div>
                            <div class="content">
                                <form action="#" method="POST">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Category <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Category" name="category" id="category" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Prefix for category <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Prefix for category" name="prefix" id="prefix" required>
                                            </div>
                                        </div>
										
                                    </div>
									<button type="submit" class="btn btn-info btn-fill pull-right">ADD CATEGORY</button> <a href="category_login.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
								<div class="row">
								</div>
								
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>
   

</html>
