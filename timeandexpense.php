<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_adjusterid    = $_SESSION["loggedin_adjusterid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if((isset($_SESSION['updatedclaimid']))&&(!empty($_SESSION['updatedclaimid']))){
    $selectedclaimid = $_SESSION['updatedclaimid'];
    $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `createdDate` from `claimmaster` where claimId = '$selectedclaimid'";
      // echo $get_details;exit;
            $detailstmt       = mysqli_query($connection, $get_details); 
            $getclaimcount   = mysqli_num_rows($detailstmt);
            if($getclaimcount > 0){
                
              while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                $claimId            = $row['claimId']; 
                $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
                // $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
                // $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
                // $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
                $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                // $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
                // $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
                // $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
                // $subId              = (empty($row['subId']))            ? '' : $row['subId'];
                // $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
                // $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
                // $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
                // $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
                // $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
                // $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
                // $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
                // $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
                // $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
                // $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
                // $createdDate        = (empty($row['createdDate']))      ? '' : $row['createdDate'];
            }
        }
        //Client Details
        $get_clients = "select `clientId`, `referenceId`, `clientName`, `phoneNumber`, `emailId`, `address`, `city`, `postalCode`, `country` from `clientmaster` where clientId = '$clientId'";
        $stmt       = mysqli_query($connection, $get_clients); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $clientId     = $row['clientId']; 
            $referenceId   = (empty($row['referenceId']))     ? '' : $row['referenceId'];
            // $clientName   = (empty($row['clientName']))       ? '' : $row['clientName'];
            // $phoneNumber  = (empty($row['phoneNumber']))    ? '' : $row['phoneNumber'];
            // $emailId      = (empty($row['emailId']))        ? '' : $row['emailId'];
            // $address      = (empty($row['address']))        ? '' : $row['address'];
            // $city         = (empty($row['city']))           ? '' : $row['city'];
            // $postalCode   = (empty($row['postalCode']))     ? '' : $row['postalCode'];
            // $country      = (empty($row['country']))        ? '' : $row['country'];
          }
      }
    } else {
        header("Location:selectjob.php");
    }
}
if ((isset($_POST['selectuserid'])) && (!empty($_POST['selectuserid']))) {
    $selectuserid    = (empty($_REQUEST['selectuserid']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['selectuserid']));
    $selectdate      = (empty($_REQUEST['selectdate']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['selectdate']));
    $selectservice   = (empty($_REQUEST['selectservice']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['selectservice']));
    $selecttime      = (empty($_REQUEST['selecttime']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['selecttime']));
    $selectrate      = (empty($_REQUEST['selectrate']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['selectrate']));
    $enteredamount      = (empty($_REQUEST['enteredamount']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['enteredamount']));

    $insert_report = "insert into `timeandexpense`(`claimId`, `userId`, `reportDate`, `reportTime`, `serviceId`, `ratePerHour`, `amount`, `createdBy`, `createdDate`) values ('$selectedclaimid', '$selectuserid', '$selectdate', '$selecttime', '$selectservice', '$selectrate', '$enteredamount', '$loggedin_userid', now())";
    // echo $insert_report;exit;
    mysqli_query($connection, $insert_report);
    header("Location:timeandexpense.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
        Whitelaw
    </title>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/jQuery-ui.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
        <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="selectjob.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" id="printablecontent">
                                <div class="col-xs-12 printheader paddingrl0" align="center">
                                            <!-- <img src="assets/img/header.png"> -->
                                    <div style="height:60px;"></div>
                                            <h3>TIME AND EXPENSE REPORT</h3>
                                </div>
                            <div class="header hideprint">
                                <h4 class="title"></h4>
                                <p class="category">Time and Expense report</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="exportcontents">
                                <col width="17%">
                                <col width="17%">
                                <col width="17%">
                                <col width="17%">
                                <col width="17%">
                                <col width="17%">
                                    <thead>
                                        <tr>
                                            <th colspan="3"><b>Job Number: <?php echo $jobNumber;?></b></th>
                                            <th colspan="3"><b> Reference: <?php echo $referenceId;?></b></th>                                        
                                        </tr>
                                    <thead>
                                    <tbody>
                                        <tr>
                                            <td align="center"><b>Adjuster</b></td>
                                            <td align="center"><b>Date</b></td>
                                            <td align="Center"><b>Activity</b></td>
                                            <td align="center"><b>Time</b></td>
                                            <td align="center"><b>Rate (p/hr)</b></td>
                                            <td align="center"><b>Value (AED)</b></td>
                                            <!-- <td></td> -->
                                            <td></td>
                                        </tr>
                                        <?php 
                                        //Get all claims for this user
                                        //Get claim details
                                        $completeTotal = 0;
                                        $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen`, `createdDate`, `createdBy` from `claimmaster` where claimId = '$selectedclaimid'";
                                        if($loggedin_isadmin != "Y"){
                                            $get_details .= " and adjusterId = '$loggedin_adjusterid'";
                                        }
                                        $detailstmt       = mysqli_query($connection, $get_details); 
                                        $getcount   = mysqli_num_rows($detailstmt);
                                        if($getcount > 0){
                                            
                                          while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
                                            $claimId            = $row['claimId']; 
                                            $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
                                            $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
                                            $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
                                            $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
                                            $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
                                            $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
                                            $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
                                            $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
                                            $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
                                            $subId              = (empty($row['subId']))            ? '' : $row['subId'];
                                            $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
                                            $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
                                            $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
                                            $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
                                            $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
                                            $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
                                            $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
                                            $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
                                            $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
                                            $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
                                            $createdDate        = (empty($row['createdDate']))      ? '' : $row['createdDate'];
                                            $createdBy          = (empty($row['createdBy']))      ? '' : $row['createdBy'];
                                            $createdDate1       = date('d/m/Y', strtotime($createdDate));
                                            $createdTime        = date('H:i A', strtotime($createdDate));
                                            $jobStatusText = "";
                                            if($jobStatus == "O"){
                                                $jobStatusText = "Open";
                                            } elseif ($jobStatus == "V") {
                                                $jobStatusText = "Visit";
                                            } elseif ($jobStatus == "P") {
                                                $jobStatusText = "Preliminiary";
                                            } elseif ($jobStatus == "W") {
                                                $jobStatusText = "Working";
                                            } elseif ($jobStatus == "C") {
                                                $jobStatusText = "Closed";
                                            } elseif ($jobStatus == "I") {
                                                $jobStatusText = "Invoiced";
                                            } elseif ($jobStatus == "R") {
                                                $jobStatusText = "Receipt";
                                            } elseif ($jobStatus == "S") {
                                                $jobStatusText = "Status Report Issued";
                                            } else {
                                                $jobStatusText = "";
                                            }
                                            $get_vdetails = "select t1.visitId, t1.createdBy, t1.createdDate, t2.firstName FROM `visitupdates` as t1, usermaster as t2  where claimId = '$selectedclaimid' and t1.createdBy = t2.userId";
                                                $detailvstmt       = mysqli_query($connection, $get_vdetails); 
                                                $getvcount   = mysqli_num_rows($detailvstmt);
                                                if($getvcount > 0){
                                                    
                                                  while($vrow = mysqli_fetch_array($detailvstmt, MYSQLI_ASSOC)){
                                                    $timelyText = "--";
                                                    $theamount  = "--";
                                                    $visitId            = $vrow['visitId']; 
                                                    $updateType         = (empty($vrow['updateType']))        ? '' : $vrow['updateType'];
                                                    $updateContent      = (empty($vrow['updateContent']))     ? '' : $vrow['updateContent'];
                                                    $originalFilename   = (empty($vrow['originalFilename']))  ? '' : $vrow['originalFilename'];
                                                    $createdDate        = (empty($vrow['createdDate']))       ? '' : date('d/m/Y', strtotime($vrow['createdDate']));
                                                    $createdTime        = (empty($vrow['createdDate']))       ? '' : date('h:i A', strtotime($vrow['createdDate']));
                                                    $firstName        = (empty($vrow['firstName']))       ? '' : $vrow['firstName']; 
                                                    ?>
                                                     <tr>
                                                        <td align="center"><?php echo $firstName; ?></td>
                                                        <td align="center"><?php echo $createdDate;?></td>
                                                        <!-- <td align="center"><?php echo $jobNumber; ?></td> -->
                                                        <td align="center">Visit</td>
                                                        <td align="center"><?php echo $createdTime;?></td>
                                                        <td align="center">--</td>
                                                        <td align="center">--</td>
                                                        <!-- <td>--</td> -->
                                                        <td>--</td>
                                                    </tr>
                                                    <?php
                                                }
                                            }  
                                            $get_pdetails = "select t1.prelimId, t1.createdBy, t1.createdDate, t2.firstName FROM `prelimupdates` as t1, usermaster as t2  where claimId = '$selectedclaimid' and t1.createdBy = t2.userId";
                                                $detailpstmt       = mysqli_query($connection, $get_pdetails); 
                                                $getpcount   = mysqli_num_rows($detailpstmt);
                                                if($getpcount > 0){
                                                    
                                                  while($prow = mysqli_fetch_array($detailpstmt, MYSQLI_ASSOC)){
                                                    $timelyText = "--";
                                                    $theamount  = "--";
                                                    $prelimId            = $prow['prelimId']; 
                                                    $updateType         = (empty($prow['updateType']))        ? '' : $prow['updateType'];
                                                    $updateContent      = (empty($prow['updateContent']))     ? '' : $prow['updateContent'];
                                                    $originalFilename   = (empty($prow['originalFilename']))  ? '' : $prow['originalFilename'];
                                                    $createdDate        = (empty($prow['createdDate']))       ? '' : date('d/m/Y', strtotime($prow['createdDate']));
                                                    $createdTime        = (empty($prow['createdDate']))       ? '' : date('h:i A', strtotime($prow['createdDate']));
                                                    $firstName        = (empty($prow['firstName']))       ? '' : $prow['firstName']; 
                                                    ?>
                                                     <tr>
                                                        <td align="center"><?php echo $firstName; ?></td>
                                                        <td align="center"><?php echo $createdDate;?></td>
                                                        <!-- <td align="center"><?php echo $jobNumber; ?></td> -->
                                                        <td align="center">Preliminary</td>
                                                        <td align="center"><?php echo $createdTime;?></td>
                                                        <td align="center">--</td>
                                                        <td align="center">--</td>
                                                        <!-- <td>--</td> -->
                                                        <td>--</td>
                                                    </tr>
                                                    <?php
                                                }
                                            }          
                                            $get_wdetails = "select t1.visitId, t1.createdBy, t1.createdDate, t2.firstName FROM `visitupdates` as t1, usermaster as t2  where claimId = '$selectedclaimid' and t1.createdBy = t2.userId";
                                                $detailwstmt       = mysqli_query($connection, $get_wdetails); 
                                                $getwcount   = mysqli_num_rows($detailwstmt);
                                                if($getwcount > 0){
                                                    
                                                  while($wrow = mysqli_fetch_array($detailwstmt, MYSQLI_ASSOC)){
                                                    $timelyText = "--";
                                                    $theamount  = "--";
                                                    $visitId            = $wrow['visitId']; 
                                                    $updateType         = (empty($wrow['updateType']))        ? '' : $wrow['updateType'];
                                                    $updateContent      = (empty($wrow['updateContent']))     ? '' : $wrow['updateContent'];
                                                    $originalFilename   = (empty($wrow['originalFilename']))  ? '' : $wrow['originalFilename'];
                                                    $createdDate        = (empty($wrow['createdDate']))       ? '' : date('d/m/Y', strtotime($wrow['createdDate']));
                                                    $createdTime        = (empty($wrow['createdDate']))       ? '' : date('h:i A', strtotime($wrow['createdDate']));
                                                    $firstName        = (empty($wrow['firstName']))       ? '' : $wrow['firstName']; 
                                                    ?>
                                                     <tr>
                                                        <td align="center"><?php echo $firstName; ?></td>
                                                        <td align="center"><?php echo $createdDate;?></td>
                                                        <!-- <td align="center"><?php echo $jobNumber; ?></td> -->
                                                        <td align="center">Working</td>
                                                        <td align="center"><?php echo $createdTime;?></td>
                                                        <td align="center">--</td>
                                                        <td align="center">--</td>
                                                        <!-- <td>--</td> -->
                                                        <td>--</td>
                                                    </tr>
                                                    <?php
                                                }
                                            }                                  
                                            //Adjuster name
                                            $get_adjusters = "select `firstName`, `lastName` from `usermaster` where userId = '$createdBy'";
                                            $adjusterstmt       = mysqli_query($connection, $get_adjusters); 
                                            $getadjustercount   = mysqli_num_rows($adjusterstmt);
                                            if($getadjustercount > 0){
                                                
                                              while($adjusterrow = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
                                                $adjusterfirstName    = (empty($adjusterrow['firstName']))      ? '' : $adjusterrow['firstName'];
                                                $adjusterlastName     = (empty($adjusterrow['lastName']))       ? '' : $adjusterrow['lastName'];
                                                // $adjustershortname = $adjusterfirstName[0].$adjusterlastName[0];
                                                // $adjustershortname = 
                                              }
                                             }

                                             //If invoice is created
                                             $timelyText = "--";
                                             $theamount  = "--";
                                             $get_invoice_latest = "select distinct t1.invoiceDate, t1.totalAmount, t1.currency, t2.timelyText, t2.timelyRate,t1.createdDate,t1.createdBy, t3.firstName  from `invoicemaster` as t1, invoicedetails as t2, usermaster as t3 where t1.invoiceId = t2.invoiceId and t1.claimId = '$selectedclaimid' and t1.createdBy = t3.userId order by t1.createdDate desc";
                                             // echo $get_invoice_latest;exit;
                                            $get_invoice_latest_details       = mysqli_query($connection, $get_invoice_latest); 
                                            $getlatestcount   = mysqli_num_rows($get_invoice_latest_details);
                                            if($getlatestcount > 0){
                                                while($lirow = mysqli_fetch_array($get_invoice_latest_details, MYSQLI_ASSOC)){
                                                    $invoiceDate     = (empty($lirow['invoiceDate']))       ? '' : $lirow['invoiceDate'];
                                                    $totalAmount     = (empty($lirow['totalAmount']))       ? '' : $lirow['totalAmount'];
                                                    $currency     = (empty($lirow['currency']))       ? '' : $lirow['currency'];
                                                    $timelyText     = (empty($lirow['timelyText']))       ? '' : $lirow['timelyText'];
                                                    $completeTotal = $completeTotal + $totalAmount;
                                                    $theamount = $totalAmount;
                                                    $firstName        = (empty($lirow['firstName']))       ? '' : $lirow['firstName'];
                                             ?>
                                            <tr>
                                                <td align="center"><?php echo $firstName; ?></td>
                                                <td align="center"><?php echo $createdDate1;?></td>
                                                <td align="center"><?php echo $jobStatusText;?></td>
                                                <td align="center"><?php echo $createdTime;?></td>
                                                <td align="center"><?php echo $timelyText;?></td>
                                                <td align="center"><?php echo number_format($theamount);?></td>
                                                <!-- <td>--</td> -->
                                                <td>--</td>
                                            </tr>
                                             <?php
                                            }
                                            }
                                            $timeandexpense = "select t1.reportId, t1.claimId, t1.userId, t1.reportDate, t1.reportTime, t1.serviceId, t1.ratePerHour, t1.amount, t2.firstName from `timeandexpense` as t1, usermaster as t2 where t1.claimId = '$selectedclaimid' and t1.userId = t2.userId";
                                             // echo $timeandexpense;exit;
                                            $timeandexpense_details       = mysqli_query($connection, $timeandexpense); 
                                            $gettaecount   = mysqli_num_rows($timeandexpense_details);
                                            if($gettaecount > 0){
                                                while($taerow = mysqli_fetch_array($timeandexpense_details, MYSQLI_ASSOC)){
                                                    $reportDate     = (empty($taerow['reportDate']))       ? '' : date('d/m/Y', strtotime($taerow['reportDate']));
                                                    $reportTime     = (empty($taerow['reportTime']))       ? '' : date('h:i A', strtotime($taerow['reportTime']));
                                                    $amount     = (empty($taerow['amount']))       ? '' : $taerow['amount'];
                                                    $ratePerHour     = (empty($taerow['ratePerHour']))       ? '' : $taerow['ratePerHour'];
                                                    $completeTotal = $completeTotal + $amount;
                                                    $theamount = $amount;
                                                    $firstName        = (empty($taerow['firstName']))       ? '' : $taerow['firstName'];
                                                    $serviceId        = (empty($taerow['serviceId']))       ? '' : $taerow['serviceId'];
                                                     $reportId        = (empty($taerow['reportId']))       ? '' : $taerow['reportId'];

                                                    //Get Service
                                                            $get_service = "select `serviceId`, `service` from `servicemaster` where serviceId = '$serviceId'";
                                                            $stmt       = mysqli_query($connection, $get_service); 
                                                            $getcount   = mysqli_num_rows($stmt);
                                                            if($getcount > 0){
                                                                
                                                              while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                                $serviceText       = (empty($row['service']))    ? '' : $row['service'];

                                                              }
                                                          }
                                             ?>
                                            <tr>
                                                <td align="center" ><?php echo $firstName; ?></td>
                                                <td align="center"><?php echo $reportDate;?></td>
                                                <td align="center"><?php echo $serviceText;?></td>
                                                <td align="center"><?php echo $reportTime;?></td>
                                                <td align="center"><?php echo $ratePerHour;?></td>
                                                <td align="center"><?php echo number_format($theamount);?></td>
                                                <!-- <td><button class="btn btn-info btn-fill">EDIT</button></td> -->
                                                <td><button class="btn btn-info btn-fill deletebuttons" id="<?php echo $reportId;?>">DELETE</button></td>
                                            </tr>
                                             <?php
                                            }
                                            }
                                          }
                                      }
                                        ?>
                                        <tr>
                                            <td align="center" colspan="5"><b>TOTAL</b></td>
                                            <td align="center"><?php echo number_format($completeTotal);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php if($loggedin_isadmin == "Y"){ ?>
                                <table class="table table-hover table-responsive">
                                    <form method="POST" action="">
                                        <tr class="hideprint">
                                            <td align="center">
                                                <select class="col-md-4" name="selectuserid" id="selectuserid" style="width:150px;" required>
                                                    <option value="">Select</option>
                                            <?php 
                                            $get_users = "select `userId`, `userName`, `firstName`, `lastName`, `emailId`, `address`, `city`, `country` from `usermaster` where active='A'";
                                                $stmt       = mysqli_query($connection, $get_users); 
                                                $getcount   = mysqli_num_rows($stmt);
                                                $count = 0;
                                                if($getcount > 0){
                                                    
                                                  while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                    $count = $count+1;
                                                    $userId         = (empty($row['userId']))           ? '' : $row['userId'];
                                                    $userName       = (empty($row['userName']))         ? '' : $row['userName'];
                                                    $firstName      = (empty($row['firstName']))        ? '' : $row['firstName'];
                                                    $lastName       = (empty($row['lastName']))         ? '' : $row['lastName'];
                                                    ?>
                                                    <option value="<?php echo $userId;?>"><?php echo $firstName." ".$lastName; ?></option>
                                                    <?php
                                                }
                                                }?>
                                                </select>
                                            </td>
                                            <td align="center">
                                                <input type="text" name="selectdate" id="selectdate" placeholder="Date" required/>
                                            </td>
                                            <td align="center">
                                                <select class="col-md-4" name="selectservice" id="selectservice" style="width:150px;" required>
                                                    <option value="">Select</option>
                                                    <?php 
                                                    $get_services = "select `serviceId`, `service` from `servicemaster` where active = 'A'";
                                                        $stmt       = mysqli_query($connection, $get_services); 
                                                        $getcount   = mysqli_num_rows($stmt);
                                                        if($getcount > 0){
                                                            
                                                          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                            $serviceId   = (empty($row['serviceId']))   ? '' : $row['serviceId'];
                                                            $service     = (empty($row['service']))     ? '' : $row['service'];
                                                            ?>
                                                    <option value="<?php echo $serviceId;?>"><?php echo $service;?></option>
                                                    <?php
                                                          }
                                                      }
                                                ?>
                                                </select>
                                            </td>
                                            <td align="center">
                                                <input type="time" placeholder="Time in hrs." name="selecttime" id="selecttime" required/>
                                            </td>
                                            <td align="center">
                                                <input type="text" placeholder="Rate (p/hr)" name="selectrate" id="selectrate" required/>
                                            </td>
                                            <td align="center">
                                                <input type="number" placeholder="Amount" name="enteredamount" id="enteredamount" required/>
                                            </td>
                                        </tr>
                                        <tr class="hideprint">
                                            <td colspan="6"><input type="submit" class="btn btn-info btn-fill pull-right" name="submitbtn" value="SAVE"></td>
                                        </tr>
                                        </form>
                                </table>
                                <?php } ?>
                              <table style="margin-left:49%;width: 50%;" class="hideprint">
                              <tr>
                                      
                                      <td>
                                          <button class="btn btn-info btn-fill pull-right" id="printbutton">PRINT</button>
                                      </td>
                                      <td>
                                          <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'csv', fileName:'timeExpenseReportCsv'});">Export as CSV</button>
                                      </td>
                                      <td>
                                          <button class="btn btn-info btn-fill pull-right" onClick ="$('#exportcontents').tableExport({type:'excel', fileName:'timeExpenseReportExcel'});"> Export as Excel</button>
                                      </td>
                                  </tr>
                          </table>
                            </div>
                        </div>
                        </div>
                    </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/jQuery-ui.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>
    <script type="text/javascript" src="assets/js/jQuery.print.js"></script>
    <script type="text/javascript" src="assets/js/FileSaver.min.js"></script>
 <script type="text/javascript" src="assets/js/xlsx.core.min.js"></script>
 <script type="text/javascript" src="assets/js/tableExport.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#timeandexpense').addClass("active");
            $('#selectdate').datepicker({ dateFormat: 'dd-mm-yy' });
            $('#printbutton').click(function(){
                $('#printablecontent').print();
            });

            //Delete timesheet entries
            $('.deletebuttons').click(function(){
                var reportid = $(this).attr('id');
                // alert(reportid);
                var check = confirm("This entry will be deleted. Click OK to continue");
                if(check){
                    window.location.href = "deactivate.php?reportId="+reportid;
                } else {
                    return false;
                }
            });
        });
    </script>
   

</html>
