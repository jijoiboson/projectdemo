<?php
header("Access-Control-Allow-Origin:*");
header("Access-Control-Allow-Method:GET, POST, OPTIONS, REQUEST");
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
include("config.php");

$type 		= $_REQUEST['RequestType'];
$currentdatetime    = date("Y-m-d G:i:s ");
$current_date       = date('Y-m-d');
$host_server    = $_SERVER['SERVER_NAME'].dirname($_SERVER['PHP_SELF']);

if($type == "getSubCategories"){
	$catId    = $_REQUEST['catId'];
	$subcategories 	  = array();
	$get_subcategories = "select `subId`, `categoryId`, `name`, `prefix` from `subcategories` where categoryId = '$catId'";
    $substmt       = mysqli_query($connection, $get_subcategories); 
    $getsubcount   = mysqli_num_rows($substmt);
    if($getsubcount > 0){
        
      while($subrow = mysqli_fetch_array($substmt, MYSQLI_ASSOC)){
        $res['subId']      = (empty($subrow['subId']))        ? '' : $subrow['subId'];
        $res['name']       = (empty($subrow['name']))         ? '' : $subrow['name'];
		array_push($subcategories,$res);
		}
	}
	echo json_encode($subcategories);
} elseif ($type == "getFilteredJobList") {
	$statusid = $_REQUEST['statusid'];
	$clientid = $_REQUEST['clientid'];
    $adjusterid = $_REQUEST['adjusterid'];
    $isadmin = $_REQUEST['ia'];
    $userid  = $_REQUEST['ui'];
    $jobNumber = mysqli_real_escape_string($connection, $_REQUEST['jobnumber']);
    $claimdate =  $_REQUEST['date'];
    $claimtodate = $_REQUEST['todate'];
    $filestatus = $_REQUEST['filestatus'];
    if($claimdate != ""){
        $claimdate        = date('Y/m/d',strtotime($claimdate));
    } else {
        $claimdate = "";
    }
    if($claimtodate != ""){
        $claimtodate        = date('Y/m/d',strtotime($claimtodate));
    } else {
        $claimtodate = "";
    }
    
	$filtered = array();
	if(($statusid == 'A')&&($clientid == 'A')&&($adjusterid == 'A')){
		$wherecondition = " where (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
	} else {
		if(($statusid != 'A') &&($clientid != 'A')){
            if($adjusterid == 'A'){
                $wherecondition = " where jobStatus = '$statusid' and clientId = '$clientid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            } else {
                $wherecondition = " where jobStatus = '$statusid' and clientId = '$clientid' and adjusterId = '$adjusterid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            }
		}
        if(($statusid != 'A') &&($clientid == 'A')){
            if($adjusterid == 'A'){
                $wherecondition = " where jobStatus = '$statusid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            } else{
                $wherecondition = " where jobStatus = '$statusid' and adjusterId = '$adjusterid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            }
        }
		if(($clientid != 'A') && ($statusid == 'A')){
            if($adjusterid == 'A'){
    			$wherecondition = " where clientId = '$clientid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            } else {
                $wherecondition = " where clientId = '$clientid' and adjusterId = '$adjusterid' and (jobNumber like '%$jobNumber%' OR policyNumber like '%$jobNumber%' OR claimReference like '%$jobNumber%' OR insuredName like '%$jobNumber%')";
            }
		}
	}
	$count = 0;
    $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster` $wherecondition";

    if($isadmin != 'Y'){
        $get_details .= " and adjusterId = '$userid'";
    }
    if($claimdate != ""){
         $get_details .= " and DATE_FORMAT(instructionDate, '%Y/%m/%d') >= '$claimdate'";
        // $get_details .= " and instructionDate = '$claimdate'";
    }
    if($claimtodate != ""){
         $get_details .= " and DATE_FORMAT(instructionDate, '%Y/%m/%d') <= '$claimtodate'";
        // $get_details .= " and instructionDate = '$claimdate'";
    }
    if($filestatus != "A"){
        $get_details .= " and fileStatus = '$filestatus'";
    }
    // echo $get_details;exit;
    $detailstmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($detailstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
        $count++;
        $claimId            = $row['claimId']; 
        $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
        $officeId           = (empty($row['officeId']))         ? '' : $row['officeId'];
        $insurerName        = (empty($row['insurerName']))      ? '' : $row['insurerName'];
        $policyNumber       = (empty($row['policyNumber']))     ? '' : $row['policyNumber'];
        $insuredName        = (empty($row['insuredName']))      ? '' : $row['insuredName'];
        $clientId           = (empty($row['clientId']))         ? '' : $row['clientId'];
        $brokerId           = (empty($row['brokerId']))         ? '' : $row['brokerId'];
        $adjusterId         = (empty($row['adjusterId']))       ? '' : $row['adjusterId'];
        $categoryId         = (empty($row['categoryId']))       ? '' : $row['categoryId'];
        $subId              = (empty($row['subId']))            ? '' : $row['subId'];
        $instructionTime    = (empty($row['instructionTime']))  ? '' : $row['instructionTime'];
        $instructionDate    = (empty($row['instructionDate']))  ? '' : $row['instructionDate'];
        $instruction        = date('d M, Y',strtotime($instructionDate)). ", ".date('h:i A',strtotime($instructionTime));
        $contactTime        = (empty($row['contactTime']))      ? '' : $row['contactTime'];
        $contactDate        = (empty($row['contactDate']))      ? '' : $row['contactDate'];
        $contactmade        = date('d M, Y',strtotime($contactDate)). ", ".date('h:i A',strtotime($contactTime));
        $surveyTime         = (empty($row['surveyTime']))       ? '' : $row['surveyTime'];
        $surveyDate         = (empty($row['surveyDate']))       ? '' : $row['surveyDate'];
        $surveyset          = date('d M, Y',strtotime($surveyDate)). ", ".date('h:i A',strtotime($surveyTime));
        $jobStatus          = (empty($row['jobStatus']))        ? '' : $row['jobStatus'];
        $jobStatusText = "";
        if($jobStatus == "O"){
            $jobStatusText = "Open";
        } elseif ($jobStatus == "V") {
            $jobStatusText = "Visit";
        } elseif ($jobStatus == "P") {
            $jobStatusText = "Preliminiary";
        } elseif ($jobStatus == "W") {
            $jobStatusText = "Working";
        } elseif ($jobStatus == "C") {
            $jobStatusText = "Closed";
        } elseif ($jobStatus == "I") {
            $jobStatusText = "Invoiced";
        } elseif ($jobStatus == "R") {
            $jobStatusText = "Receipt";
        } elseif ($jobStatus == "S") {
            $jobStatusText = "Status Report Issued";
        } else {
            $jobStatusText = "";
        }
  //Client name
        $clientName  = "";
  $get_client = "select `referenceId`, `clientName` from `clientmaster` where clientId = '$clientId'";
        $clientstmt       = mysqli_query($connection, $get_client); 
        $getclientcount   = mysqli_num_rows($clientstmt);
        if($getclientcount > 0){
            
          while($clientrow = mysqli_fetch_array($clientstmt, MYSQLI_ASSOC)){
            $referenceId   = (empty($clientrow['referenceId']))     ? '' : $clientrow['referenceId'];
            $clientName   = (empty($clientrow['clientName']))       ? '' : $clientrow['clientName'];
          }
      }
      $eachrow = "<tr><td>$count</td><td>$clientName</td><td>$jobNumber</td><td>$insuredName</td><td>$jobStatusText</td><td><a href='report.php?id=$claimId'><button class='btn btn-info btn-fill pull-right'>VIEW</button></a></td></tr>";
      array_push($filtered,$eachrow);
      }
      
      }
echo json_encode($filtered);
} elseif ($type == "getClaimStatusCounts") {
    $countarray = array();
    $countarray['O'] = 0;
    $countarray['I'] = 0;
    $countarray['V'] = 0;
    $countarray['P'] = 0;
    $countarray['W'] = 0;
    $countarray['C'] = 0;
    $countarray['R'] = 0;
    $getstatuscount = "select `jobStatus`, count(*) as count FROM `claimmaster` group by jobStatus";
    $statusstmt     = mysqli_query($connection, $getstatuscount); 
    $getcount       = mysqli_num_rows($statusstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($statusstmt, MYSQLI_ASSOC)){
        $jobStatus      = $row['jobStatus']; 
        $count          = (empty($row['count']))        ? '' : $row['count'];
        // $res[$jobStatus] = $count;
        // array_push($countarray, $res);
        $countarray[$jobStatus] = $count;
    }
}
echo json_encode($countarray);
} elseif ($type == "getAdjusterCount") {
    $adjustercountarray = array();
    // $getadjustercount = "select `adjusterId`, count(*) as count from `claimmaster` where YEARWEEK(createdDate) = YEARWEEK(NOW()) group by adjusterId";
    // order_date >= DATE_SUB(NOW(),INTERVAL 1 YEAR);
    $getadjustercount = "select `adjusterId`, count(*) as count , jobStatus from `claimmaster` where createdDate >= DATE_SUB(NOW(),INTERVAL 1 YEAR) group by adjusterId , jobStatus";
    // echo $getadjustercount;
    // select `adjusterId`, count(*) as count, jobStatus, EXTRACT( YEAR_MONTH FROM `createdDate` ) from `claimmaster` where createdDate >= DATE_SUB(NOW(),INTERVAL 1 YEAR) group by adjusterId , jobStatus, EXTRACT( YEAR_MONTH FROM `createdDate` )
    $adjusterstmt     = mysqli_query($connection, $getadjustercount); 
    $getadjcount       = mysqli_num_rows($adjusterstmt);
    if($getadjcount > 0){
      while($row = mysqli_fetch_array($adjusterstmt, MYSQLI_ASSOC)){
        $adjusterId      = $row['adjusterId']; 
        $jobStatus      = $row['jobStatus']; 
        $count          = (empty($row['count']))        ? 0 : $row['count'];
//Get adjuster name
        $get_adjusters = "select `adjusterId`, `userName`, `firstName`, `lastName`, `emailId`, `address`, `city`, `postalCode`, `country` from `adjusters` where adjusterId = '$adjusterId'";
        $stmt       = mysqli_query($connection, $get_adjusters); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $firstName    = (empty($row['firstName']))      ? '' : $row['firstName'];
            $lastName     = (empty($row['lastName']))       ? '' : $row['lastName'];
            $adjustername = $firstName." ".$lastName;
          }
      }
        $adjustercountarray[$adjustername][$jobStatus] = $count;
    }
}
// print_r($adjustercountarray);
echo json_encode($adjustercountarray);
} elseif ($type == "getServices") {
    $services = $_REQUEST['services'];
    $servicearray = array();
    $idarray = explode(",", $services);
    foreach ($idarray as $ids) {
        //Get selected Service
        $get_service = "select `serviceId`, `service` from `servicemaster` where serviceId = '$ids'";
        $stmt       = mysqli_query($connection, $get_service); 
        $getcount   = mysqli_num_rows($stmt);
        if($getcount > 0){
            
          while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
            $res['serviceId']     = (empty($row['serviceId']))    ? '' : $row['serviceId'];
            $res['service']       = (empty($row['service']))    ? '' : $row['service'];
            array_push($servicearray, $res);
          }
      }
    }
    echo json_encode($servicearray);
}
 elseif ($type == "getFilteredFees") {
    $statusid = $_REQUEST['statusid'];
    $adjusterid = $_REQUEST['adjusterid'];
    
    $filtered = array();
    if(($statusid == 'A')&&($adjusterid == 'A')){
        $wherecondition = "";
    } else {
        if($statusid != 'A'){
            if($adjusterid == 'A'){
                $wherecondition = " where fileStatus = '$statusid'";
            } else {
                $wherecondition = " where fileStatus = '$statusid' and adjusterId = '$adjusterid'";
            }
        } else {
            if($adjusterid == 'A'){
                $wherecondition = "";
            } else {
                $wherecondition = " where adjusterId = '$adjusterid'";
            }
        }
    }
    $count = 0;
    $totalprojectedfee = 0;
    $totalreservedamount = 0;   
    $totalstatusfee = 0; 
    $get_details = "select `claimId`, `jobNumber`, `officeId`, `insurerName`, `insuredName`, `policyNumber`, `clientId`, `brokerId`, `adjusterId`, `categoryId`, `subId`, `instructionTime`, `instructionDate`, `contactTime`, `contactDate`, `surveyTime`, `surveyDate`, `jobStatus`, `frozen` from `claimmaster` $wherecondition";
    // echo $get_details;exit;
    $detailstmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($detailstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
        $count++;
        $claimId            = $row['claimId']; 
        $jobNumber          = (empty($row['jobNumber']))        ? '' : $row['jobNumber'];
$projectedfee = 0;
$reservedamount = 0;   
$statusfee = 0;                                        
    $get_details = "select `prelimId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `prelimupdates`  where claimId = '$claimId'";
    $detailstmt       = mysqli_query($connection, $get_details); 
    $getcount   = mysqli_num_rows($detailstmt);
    if($getcount > 0){
        
      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
        $prelimId            = $row['prelimId']; 
        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));         
        if($updateType == "F"){
            $projectedfee += $updateContent;
            $totalprojectedfee += $updateContent;
        }
        if($updateType == "A"){
            $reservedamount += $updateContent;
            $totalreservedamount += $updateContent;
        }
    }
} 
// $get_status_details = "select `statusId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `statusupdates`  where claimId = '$claimId'";
$get_status_details = "select `statusId`, `claimId`, `report`, `reserveAmount`, `statusDate`, `originalFilename`, `createdBy`, `createdDate` FROM `statusreports`  where claimId = '$claimId'";
    $sdetailstmt       = mysqli_query($connection, $get_status_details); 
    $getscount   = mysqli_num_rows($sdetailstmt);
    if($getscount > 0){
        
      while($statusrow = mysqli_fetch_array($sdetailstmt, MYSQLI_ASSOC)){
        $statusId            = $statusrow['statusId']; 
        $report         = (empty($statusrow['report']))        ? '' : $statusrow['report'];
        $reserveAmount      = (empty($statusrow['reserveAmount']))     ? '' : $statusrow['reserveAmount'];
        $originalFilename      = (empty($statusrow['originalFilename']))     ? '' : $statusrow['originalFilename'];
        $statusDate   = (empty($statusrow['statusDate']))  ? '' : date('d M, Y', strtotime($statusrow['statusDate']));        
        // if($updateType == "F"){
            $statusfee += $reserveAmount;
            $totalstatusfee += $reserveAmount;
        // }                                                       
    }
}   
if(($projectedfee + $reservedamount + $statusfee) > 0){

      $eachrow = "<tr><td>$jobNumber</td><td>$projectedfee</td><td>$reservedamount</td><td>$statusfee</td><td>$createdDate</td></tr>";

      array_push($filtered,$eachrow);
      }} 
                                                  
    }
    $totalrow = "<tr><td><strong>TOTAL : </strong></td><td><strong>$totalprojectedfee</strong></td><td><strong>$totalreservedamount</strong></td><td><strong>$totalstatusfee</strong></td><td></td></tr>";
    array_push($filtered,$totalrow);
echo json_encode($filtered);
} 