<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if($loggedin_isadmin != "Y"){
        header("Location:dashboard.php");
    } else {

    }
}
  if((isset($_POST['subcategory'])) && (!empty($_POST['subcategory']))){
     // print_r($_REQUEST);exit;
    $categoryid      = (empty($_REQUEST['categoryid']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['categoryid']));
    $subcategory      = (empty($_REQUEST['subcategory']))   ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['subcategory']));
    $prefix        = (empty($_REQUEST['prefix']))  ? '' : mysqli_real_escape_string($connection,trim($_REQUEST['prefix']));
    $insert_details = "insert into `subcategories`(`categoryId`, `name`, `prefix`, `active`, `createdBy`, `createdDate`) values ('$categoryid', '$subcategory', '$prefix', 'A', '$loggedin_userid', now())";
    mysqli_query($connection, $insert_details);
    header("Location:subcategorylist.php");
  }
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
                <div class="col-xs-12" style="margin:10px;font-size: 1.2em;color:#000;">
            <a href="sub_login.php"><< Back</a>
        </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Sub-category master</h4>
                            </div>
                            <div class="content">
                                <form action="#" method="post">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Category <span class="mandatorystar">*</span></label><br>
                                                <select class="col-md-4"  style="width:300px;" id="categoryid" name="categoryid" required>
                                                    <option value="" style="display:none">Select Category</option>
                                                    <?php 
                                                        $get_categories = "select `categoryId`, `category`, `prefix` from `categorymaster`";
                                                            $stmt       = mysqli_query($connection, $get_categories); 
                                                            $getcount   = mysqli_num_rows($stmt);
                                                            $count = 0;
                                                            if($getcount > 0){
                                                                
                                                              while($row = mysqli_fetch_array($stmt, MYSQLI_ASSOC)){
                                                                $count = $count+1;
                                                                $categoryId   = (empty($row['categoryId']))   ? '' : $row['categoryId'];
                                                                $category     = (empty($row['category']))     ? '' : $row['category'];
                                                                $prefix       = (empty($row['prefix']))       ? '' : $row['prefix'];
                                                    ?>
                                                        <option value="<?php echo $categoryId;?>"><?php echo $category; ?></option>

                                                    <?php
                                                              }
                                                          }
                                                    ?>
													<!-- <option>Surveys</option>
													<option>Engineering</option>
													<option>Domestic</option>
													<option>Marine</option>
													<option>Machinery</option>
													<option>Commercial</option>
													<option>Liability</option>
													<option>Pecuniary</option>
													<option>Miscellaneous</option> -->
												</select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Sub-category <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Sub-category" id="subcategory" name="subcategory" required>
                                            </div>
                                        </div>
										<div class="col-md-4">
                                            <div class="form-group">
                                                <label>Prefix for sub-category <span class="mandatorystar">*</span></label>
                                                <input type="text" class="form-control nonumbers" placeholder="Prefix for sub-category" id="prefix" name="prefix" required>
                                            </div>
                                        </div>
										
                                    </div>
									</div>
									<button type="submit" class="btn btn-info btn-fill pull-right">ADD SUB-CATEGORY</button> <a href="sub_login.php"><div class="btn pull-right marginrl10">CANCEL</div></a>
                                    <div class="clearfix"></div>
                                </form>
								<div class="row">
								</div>
								
                            </div>
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>


        

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="assets/js/common.js"></script>

   <script type="text/javascript">
        $(document).ready(function(){
            $('.sidebaritems').removeClass("active");
            $('#masters').addClass("active");
        });
    </script>

</html>
