Please see my comments
1.)    Dashboard does not calculate the new claim during the month. This should be updated as soon as a new claim is registered.  same goes for open claims, etc.  Do they need one more square at the top to show New Claims this month count? I'm showing a count of all open claims now, regardless of the month. Should I show only the ones that was created this month?
Claims for the Month Not Based on created date, to be based on Instruction Received Date
2.)    Please include the date of loss upon claim registration  (Time is not required) One extra datepicker for date of loss? I should show it in all subsequent page right? I'll need a day to do this.
               Correct
3.)    Please make an option on the Contact Made, Survey Arrange to put only To Be Advised.  At this time, when the date field is left blank, the date becomes a strange date... something in year 2070. Can I keep these two not mandatory and if no value comes in I take the value as To Be Advised? Or should it be a checkbox? Let me know.
Yes you can make it not mandatory
4.)    We should be able to filter claims by location. They can now filter by location if they type the short name for location in the Job number. I could add a filter. 
No Need to add, I have explained them how to filter location.
5.)    Please put an option to delete a registered claim.  we have registered LON/G/362/17 TWO TIMES..PLEASE DELETE, LON/G/364/17. Will delete this one. Should I put a delete option? 
No need to delete, please provide an option to Void let the data be there. Void only if status is open.
6.)    Some of the wordings which are uploaded in the system is still not appropriate. Wrong spellings on the fields are still there, capitalization are still not fixed. Can you ask them to be specific and tell where these issues are?
Will Check and update.