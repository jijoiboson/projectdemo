<?php
	session_start();
	session_destroy();
	setcookie("loggedin_username", "", time() - 3600);
	header("location:index.php");
?>