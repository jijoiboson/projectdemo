<?php
ini_set('display_errors',0);
session_start();  
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $isAdmin    = $_SESSION["loggedin_isadmin"];
    $userAccess = $_SESSION["loggedin_useraccess"];
    $useraccessarray = explode(",", $userAccess);
  }
?>
    <div class="sidebar" data-color="match" data-image="assets/img/sidebar-5.jpg">
       	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="dashboard.php">
                    <img src="assets/img/new_logo.svg"/> 
                </a>
            </div>

            <ul class="nav">
                <li class="sidebaritems" id="dashboard">
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="sidebaritems" id="userprofile">
                    <a href="user.php">
                        <i class="pe-7s-id"></i>
                        <p>User profile</p>
                    </a>
                </li>
				<?php if($isAdmin == "Y"){ ?>
                <li class="sidebaritems" id="adminpanel">
                    <a href="admin.php">
                        <i class="pe-7s-door-lock"></i>
                        <p>Admin</p>
                    </a>
                </li>
                <?php } ?>
                <?php //if (in_array("1", $useraccessarray)){ ?>
                <!-- <li class="sidebaritems" id="newclaim">
                    <a href="newclaim.php">
                        <i class="pe-7s-plus"></i>
                        <p>New Claim</p>
                    </a>
                </li> -->
                <?php //} ?>
                <?php if (in_array("2", $useraccessarray)){ ?>
                <li class="sidebaritems" id="joblist">
                    <a href="open.php">
                        <i class="pe-7s-photo-gallery"></i>
                        <p>Claim Files</p>
                    </a>
                </li>
                <?php } ?>
                <?php if($isAdmin == "Y"){ ?>
                <li class="sidebaritems" id="masters">
                    <a href="masters.php">
                        <i class="pe-7s-user"></i>
                        <p>Masters</p>
                    </a>
                </li>
                <?php } ?>
                <?php if (in_array("3", $useraccessarray)){ ?>
                <li class="sidebaritems" id="accounts">
                    <a href="accounts.php">
                        <i class="pe-7s-note"></i>
                        <p>Accounts</p>
                    </a>
                </li>
                <?php } ?>
                <?php if (in_array("5", $useraccessarray)){ ?>
                <li class="sidebaritems" id="timeandexpense">
                    <a href="selectjob.php">
                        <i class="pe-7s-close-circle"></i>
                        <p>Time & Expense</p>
                    </a>
                </li>
                <?php } ?>
                <?php if($isAdmin == "Y"){ ?>
				<li class="sidebaritems" id="reports">
                    <a href="closed.php">
                        <i class="pe-7s-close-circle"></i>
                        <p>Management</p>
                    </a>
                </li>
                <?php } ?>
				
            </ul>
    	</div>
    </div>