<?php
//ini_set('display_errors',0);
 ini_set('display_errors',1);
// error_reporting(E_ALL);
session_start();  
include("include/config.php");
if(!isset($_SESSION["loggedin_username"])) {
    header("Location:index.php");
  } else {
    $loggedin_userid        = $_SESSION["loggedin_userid"];
    $loggedin_isadmin       = $_SESSION["loggedin_isadmin"];
    if(isset($_SESSION['updatedclaimid'])){
    	$updatedclaimid         = $_SESSION["updatedclaimid"];
    	$updatedjobnumber = $_SESSION["updatedjobnumber"];
    	$formattedJobnumber = str_replace("/", "", $updatedjobnumber);
    	$path       		= "uploads/$formattedJobnumber/visit/";
    	if (!file_exists($path)) {
			    mkdir($path, 0777, true);
		}
    	//Get visit details
    	$visitphotoarray = array();
    	$visitemailarray = array();
    	$visitnotesarray = array();

    	$get_details = "select `visitId`, `claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate` FROM `visitupdates`  where claimId = '$updatedclaimid'";
	    $detailstmt       = mysqli_query($connection, $get_details); 
	    $getcount   = mysqli_num_rows($detailstmt);
	    if($getcount > 0){
	        
	      while($row = mysqli_fetch_array($detailstmt, MYSQLI_ASSOC)){
	        $visitId            = $row['visitId']; 
	        $updateType         = (empty($row['updateType']))        ? '' : $row['updateType'];
	        $updateContent      = (empty($row['updateContent']))     ? '' : $row['updateContent'];
	        $originalFilename   = (empty($row['originalFilename']))  ? '' : $row['originalFilename'];
	        $createdDate        = (empty($row['createdDate']))       ? '' : date('d M, Y', strtotime($row['createdDate']));	        
	        if($updateType == "P"){
	        	$thisphotoarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
	        	array_push($visitphotoarray, $thisphotoarray);
	        }
	        if($updateType == "E"){
	        	$thisemailarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate, "name" => $originalFilename);
	        	array_push($visitemailarray, $thisemailarray);
	        }
	        if($updateType == "N"){
	        	$thisnotesarray = array("Id" => $visitId, "content" => $updateContent, "date" => $createdDate);
	        	array_push($visitnotesarray, $thisnotesarray);
	        }
	    }
	}

    } else {
    	header("Location:open.php");
    }
    
}

if((isset($_FILES['visitphoto']['name']))&&(!empty($_FILES['visitphoto']['name']))){
	$visitphoto    = (empty($_FILES['visitphoto']['name']))  ? '' : $_FILES['visitphoto']['name'];
	$fullfilename  		= "";
	if($visitphoto){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $visitphoto);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['visitphoto']['tmp_name'], $fullpath);
  	}
  	$insert_photo = "insert into `visitupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'P', '$fullfilename', '$originalname', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_photo);
  	$update_status = "update claimmaster set jobStatus = 'V' where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:visit.php?p=success");
}
if((isset($_FILES['visitemail']['name']))&&(!empty($_FILES['visitemail']['name']))){
	$visitemail    = (empty($_FILES['visitemail']['name']))  ? '' : $_FILES['visitemail']['name'];
	$fullfilename  		= "";
	if($visitemail){
      	$date          = round(microtime(true)*1000);
      	$imgtype       = explode('.', $visitemail);
      	$ext           = end($imgtype);
      	$filename      = $imgtype[0];
      	$fullfilename  = $filename.$date.".".$ext;
      	$originalname  = $filename.".".$ext;
      	$fullpath      = $path . $fullfilename;
      	move_uploaded_file($_FILES['visitemail']['tmp_name'], $fullpath);
  	}
  	$insert_email = "insert into `visitupdates`(`claimId`, `updateType`, `updateContent`, `originalFilename`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'E', '$fullfilename', '$originalname', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_email);
  	$update_status = "update claimmaster set jobStatus = 'V' where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:visit.php?e=success");
}
if((isset($_REQUEST['visitnotes']))&&(!empty($_REQUEST['visitnotes']))){
	$visitnotes    = (empty($_REQUEST['visitnotes']))  ? '' : $_REQUEST['visitnotes'];
  	$insert_notes = "insert into `visitupdates`(`claimId`, `updateType`, `updateContent`, `createdBy`, `createdDate`) VALUES ('$updatedclaimid', 'N', '$visitnotes', '$loggedin_userid', now())";
  	mysqli_query($connection, $insert_notes);
  	$update_status = "update claimmaster set jobStatus = 'V' where claimId = '$updatedclaimid'";
	mysqli_query($connection, $update_status);
  	header("Location:visit.php?n=success");
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>
        Whitelaw
    </title>
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">



    <!--  icons     -->
		<link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <?php include("sidebar.php");?>

    <div class="main-panel">
        <?php include("navbar.php");?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" border=0>
									<tbody>
                                        <tr>
											<td colspan="2">
												<h4><strong>Photographs: </strong></h4>
											</td>
										</tr>
										<?php 
										$photocount = 0;
										// print_r($visitphotoarray);
											foreach ($visitphotoarray as $visitphotos) {

												$photocount++;
												?>
												<tr>
													<td><?php echo $photocount; ?> : <a href="<?php echo $path.$visitphotos['content'];?>" target="_blank"><?php echo $visitphotos['name'];?></a></td>
													<td>Uploaded on : <?php echo $visitphotos['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr>
											<td colspan="2">
												<form method="POST" action="#" enctype="multipart/form-data">
												<strong>Select a file to attach (Max 10MB): <span class="mandatorystar">*</span></strong>
												<input type="file" name="visitphoto" id="visitphoto" accept="image/*" required /></br>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="UPLOAD"/>
													</td>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="reset" value="RESET"/>
													</td>
												</tr>
												<tr><td colspan="2" style="color:green;" align="right"><?php
												if(isset($_REQUEST['p'])){
													echo "Succesfully uploaded";
												}
												?></td></tr>
												</table>
												</form>
											</td>
										</tr>
										<!-- <tr>
											<td colspan="2">
												<h4><strong>Email: </strong></h4>
											</td>
										</tr> -->
										<?php 
										// $emailcount = 0;
										// // print_r($visitphotoarray);
										// 	foreach ($visitemailarray as $visitemails) {

										// 		$emailcount++;
												?>
												<!-- <tr>
													<td><?php echo $emailcount; ?> : <a href="<?php echo $path.$visitemails['content'];?>" target="_blank"><?php echo $visitemails['name'];?></a></td>
													<td>Uploaded on : <?php echo $visitemails['date'];?></td>
												</tr> -->
												<?php
											// }
										?>
										<!-- <tr>
											<td colspan="2">
												<form method="POST" action="#" enctype="multipart/form-data">
												<strong>Select a file to attach (Max 10MB):</strong> </br>
												<input type="file" name="visitemail" id="visitemail" required /></br>
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="SEND"/>
													</td>
													
												</tr>
												<tr><td colspan="2" style="color:green;" align="right"><?php
												// if(isset($_REQUEST['e'])){
												// 	echo "Succesfully uploaded";
												// }
												?></td></tr>
												</table>
												</form>
											</td>
										</tr> -->
										</tbody>
										</table>
										<form name="notesform" id="notesform" method="POST" action="#">
										<table class="table table-hover table-striped" border=0>
										<tr>
											<td colspan="2">
												<h4><strong>Notes: </strong></h4>
											</td>
										</tr>
										<?php 
										$notecount = 0;
										// print_r($visitphotoarray);
											foreach ($visitnotesarray as $visitnote) {

												$notecount++;
												?>
												<tr>
													<td><?php echo $notecount; ?> : <?php echo $visitnote['content'];?></td>
													<td>Written on : <?php echo $visitnote['date'];?></td>
												</tr>
												<?php
											}
										?>
										<tr>
											<td colspan="2">
												<textarea rows="5" cols="140" name="visitnotes" id="visitnotes"></textarea>
												
											</td>
										</tr>
										<tr>
											
											<td colspan="2">
												<table width="30%" align="right">
												<tr>
													<td>
														<input class="btn btn-info btn-fill pull-right" type="submit" value="SUBMIT"/>
													</td>
												</tr>
												<tr><td style="color:green;" align="right"><?php
												if(isset($_REQUEST['n'])){
													echo "Succesfully updated";
												}
												?></td></tr>
												</table>
											</td>
										</tr>		
                                </table>
                                </form>
                                <table align="center">
									<tr>
										<td>
											<a href="report.php?id=<?php echo $updatedclaimid;?>"><button class="btn btn-info btn-fill">BACK</button></a>
										</td>
									</tr>
								</table>
                            </div>
                        </div>
                    </div>

    </div>
</div>


</body>

	<script>
		function show_alert(){
			confirm ("Files uploaded");
			
		}	
	</script>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

   

</html>
